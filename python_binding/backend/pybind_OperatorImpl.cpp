/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <string>

#include "aidge/graph/Node.hpp"
#include "aidge/operator/Operator.hpp"
#include "aidge/backend/OperatorImpl.hpp"

namespace py = pybind11;
namespace Aidge {

/**
 * @brief Trampoline class for binding
 *
 */
class pyOperatorImpl: public OperatorImpl {
public:
    using OperatorImpl::OperatorImpl; // Inherit constructors

    void forward() override {
        PYBIND11_OVERRIDE(
            void,
            OperatorImpl,
            forward

        );
    }

    void backward() override {
        PYBIND11_OVERRIDE(
            void,
            OperatorImpl,
            backward

        );
    }

    std::shared_ptr<ProdConso> getProdConso() const override {
        PYBIND11_OVERRIDE_NAME(
            std::shared_ptr<ProdConso>,
            OperatorImpl,
            "get_prod_conso",
            getProdConso
        );
    }

    std::vector<ImplSpec> getAvailableImplSpecs() const noexcept override {
        PYBIND11_OVERRIDE_NAME(
            std::vector<ImplSpec>,
            OperatorImpl,
            "get_available_impl_specs",
            getAvailableImplSpecs
        );
    }
};

// See https://pybind11.readthedocs.io/en/stable/advanced/classes.html#binding-protected-member-functions
class OperatorImpl_Publicist : public OperatorImpl {
public:
    using OperatorImpl::getProdConso;
    using OperatorImpl::getAvailableImplSpecs;
};

void init_OperatorImpl(py::module& m){
    py::class_<ImplSpec::IOSpec>(m, "IOSpec")
    .def(py::init<DataType, DataFormat, const std::vector<std::pair<int, int>>&>(), py::arg("type"), py::arg("format") = DataFormat::Any, py::arg("dims") = std::vector<std::pair<int, int>>{})
    ;

    py::class_<ImplSpec>(m, "ImplSpec")
    .def(py::init<const DynamicAttributes&>(), py::arg("attr") = DynamicAttributes())
    .def(py::init<const ImplSpec::IOSpec&, const DynamicAttributes&>(), py::arg("io"), py::arg("attr") = DynamicAttributes())
    .def(py::init<const ImplSpec::IOSpec&, const ImplSpec::IOSpec&, const DynamicAttributes&>(), py::arg("i"), py::arg("o"), py::arg("attr") = DynamicAttributes())
    .def(py::init<const std::vector<ImplSpec::IOSpec>&, const std::vector<ImplSpec::IOSpec>&, const DynamicAttributes&>(), py::arg("i"), py::arg("o"), py::arg("attr") = DynamicAttributes())
    .def("__eq__", static_cast<bool(*)(const ImplSpec&, const ImplSpec&)>(&operator==))
    .def("__repr__", [](ImplSpec self){
        return fmt::format("{}\n", self);
    })
    .def_readwrite("inputs", &ImplSpec::inputs)
    .def_readwrite("outputs", &ImplSpec::outputs)
    .def_readwrite("attrs", &ImplSpec::attrs)
    ;

    py::class_<OperatorImpl, std::shared_ptr<OperatorImpl>, pyOperatorImpl>(m, "OperatorImpl", py::dynamic_attr())
    .def(py::init<const Operator&, const std::string&>(), py::keep_alive<1, 1>(), py::keep_alive<1, 2>(), py::keep_alive<1,3>())
    .def("forward", &OperatorImpl::forward)
    .def("backward", &OperatorImpl::backward)
    .def("prod_conso", &OperatorImpl::prodConso)
    .def("backend", &OperatorImpl::backend)
    .def("get_operator", &OperatorImpl::getOperator)
    .def("get_required_spec", &OperatorImpl::getRequiredSpec)
    .def("get_best_match", &OperatorImpl::getBestMatch)
    .def("get_adaptation", &OperatorImpl::getAdaptation)
    .def("get_best_adaptation", &OperatorImpl::getBestAdaptation)
    .def("get_prod_conso", &OperatorImpl_Publicist::getProdConso)
    .def("get_available_impl_specs", &OperatorImpl_Publicist::getAvailableImplSpecs)
    ;
}
} // namespace Aidge
