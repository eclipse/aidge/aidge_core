/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <algorithm>  // std::transform
#include <cctype>     // std::tolower
#include <string>     // std::string
#include <vector>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/operators.h>

#include "aidge/data/Elts.hpp"

namespace py = pybind11;
namespace Aidge {

template <class T>
void bindEnum(py::module& m, const std::string& name) {
    // Define enumeration names for python as lowercase type name
    // This defined enum names compatible with basic numpy type
    // name such as: float32, flot64, [u]int32, [u]int64, ...
    auto python_enum_name = [](const T& type) {
        auto str_lower = [](std::string& str) {
            std::transform(str.begin(), str.end(), str.begin(),
                           [](unsigned char c){
                               return std::tolower(c);
                           });
        };
        auto type_name = std::string(Aidge::format_as(type));
        str_lower(type_name);
        return type_name;
    };

    // Auto generate enumeration names from lowercase type strings
    std::vector<std::string> enum_names;
    for (auto type_str : EnumStrings<T>::data) {
        auto type = static_cast<T>(enum_names.size());
        auto enum_name = python_enum_name(type);
        enum_names.push_back(enum_name);
    }

    // Define python side enumeration aidge_core.type
    auto e_type = py::enum_<T>(m, name.c_str());

    // Add enum value for each enum name
    for (std::size_t idx = 0; idx < enum_names.size(); idx++) {
        e_type.value(enum_names[idx].c_str(), static_cast<T>(idx));
    }

    // Define str() to return the bare enum name value, it allows
    // to compare directly for instance str(tensor.type())
    // with str(nparray.type)
    e_type.def("__str__", [enum_names](const T& type) {
        return enum_names[static_cast<int>(type)];
    }, py::prepend());
}

void init_Elts(py::module& m) {
    bindEnum<Elts_t::EltType>(m, "EltType");
    m.def("format_as", (const char* (*)(Elts_t::EltType)) &format_as, py::arg("elt"));
    
    py::class_<Elts_t, std::shared_ptr<Elts_t>>(
        m, "Elts_t", py::dynamic_attr())
        .def_static("none_elts", &Elts_t::NoneElts)
        .def_static("data_elts", &Elts_t::DataElts, py::arg("data"), py::arg("token") = 1)
        .def_static("token_elts", &Elts_t::TokenElts, py::arg("token"))
        .def_readwrite("data", &Elts_t::data)
        .def_readwrite("token", &Elts_t::token)
        .def_readwrite("type", &Elts_t::type)
        .def(py::self + py::self)
        .def(py::self += py::self)
        .def(py::self < py::self)
        .def(py::self > py::self);
}

} // namespace Aidge
