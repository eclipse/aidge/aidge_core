/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/data/Interpolation.hpp"
#include "aidge/utils/Registrar.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Interpolation(py::module &m) {
    auto pyInterpolation = py::class_<Aidge::Interpolation>(m, "Interpolation");

    py::enum_<Interpolation::Mode>(pyInterpolation, "Mode")
    .value("CUBIC", Interpolation::Mode::Cubic)
    .value("LINEAR", Interpolation::Mode::Linear)
    .value("ROUND_PREFER_FLOOR", Interpolation::Mode::RoundPreferFloor)
    .value("ROUND_PREFER_CEIL", Interpolation::Mode::RoundPreferCeil)
    .value("FLOOR", Interpolation::Mode::Floor)
    .value("CEIL", Interpolation::Mode::Ceil)
    .export_values();

    py::enum_<Interpolation::CoordinateTransformation>(pyInterpolation, "CoordinateTransformation")
    .value("HALF_PIXEL", Interpolation::CoordinateTransformation::HalfPixel)
    .value("HALF_PIXEL_SYMETRIC", Interpolation::CoordinateTransformation::HalfPixelSymmetric)
    .value("PYTORCH_HALF_PIXEL", Interpolation::CoordinateTransformation::PytorchHalfPixel)
    .value("ALIGN_CORNERS", Interpolation::CoordinateTransformation::AlignCorners)
    .value("ASYMMETRIC", Interpolation::CoordinateTransformation::Asymmetric)
    .export_values();
}

} // namespace Aidge
