/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Data.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Data(py::module& m){
    py::class_<Data, std::shared_ptr<Data>>(m,"Data");
}

} // namespace Aidge
