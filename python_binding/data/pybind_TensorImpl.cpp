/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/operators.h>
#include <pybind11/numpy.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/TensorImpl.hpp"
#include "aidge/backend/cpu/data/TensorImpl.hpp"

namespace py = pybind11;
namespace Aidge {

void init_TensorImpl(py::module& m){
  py::class_<TensorImpl, std::shared_ptr<TensorImpl>>(m, "TensorImpl");

  py::class_<TensorImpl_cpu<double>, std::shared_ptr<TensorImpl_cpu<double>>, TensorImpl>(m, "TensorImpl_cpu_float64")
    .def(py::init<DeviceIdx_t, std::vector<DimSize_t>>());
    
  py::class_<TensorImpl_cpu<float>, std::shared_ptr<TensorImpl_cpu<float>>, TensorImpl>(m, "TensorImpl_cpu_float32")
    .def(py::init<DeviceIdx_t, std::vector<DimSize_t>>());

  py::class_<TensorImpl_cpu<half_float::half>, std::shared_ptr<TensorImpl_cpu<half_float::half>>, TensorImpl>(m, "TensorImpl_cpu_float16")
    .def(py::init<DeviceIdx_t, std::vector<DimSize_t>>());

  py::class_<TensorImpl_cpu<int64_t>, std::shared_ptr<TensorImpl_cpu<int64_t>>, TensorImpl>(m, "TensorImpl_cpu_int64")
    .def(py::init<DeviceIdx_t, std::vector<DimSize_t>>());

  py::class_<TensorImpl_cpu<int32_t>, std::shared_ptr<TensorImpl_cpu<int32_t>>, TensorImpl>(m, "TensorImpl_cpu_int32")
    .def(py::init<DeviceIdx_t, std::vector<DimSize_t>>());

  py::class_<TensorImpl_cpu<int16_t>, std::shared_ptr<TensorImpl_cpu<int16_t>>, TensorImpl>(m, "TensorImpl_cpu_int16")
    .def(py::init<DeviceIdx_t, std::vector<DimSize_t>>());

  py::class_<TensorImpl_cpu<int8_t>, std::shared_ptr<TensorImpl_cpu<int8_t>>, TensorImpl>(m, "TensorImpl_cpu_int8")
    .def(py::init<DeviceIdx_t, std::vector<DimSize_t>>());

  py::class_<TensorImpl_cpu<uint64_t>, std::shared_ptr<TensorImpl_cpu<uint64_t>>, TensorImpl>(m, "TensorImpl_cpu_uint64")
    .def(py::init<DeviceIdx_t, std::vector<DimSize_t>>());

  py::class_<TensorImpl_cpu<uint32_t>, std::shared_ptr<TensorImpl_cpu<uint32_t>>, TensorImpl>(m, "TensorImpl_cpu_uint32")
    .def(py::init<DeviceIdx_t, std::vector<DimSize_t>>());

  py::class_<TensorImpl_cpu<uint16_t>, std::shared_ptr<TensorImpl_cpu<uint16_t>>, TensorImpl>(m, "TensorImpl_cpu_uint16")
    .def(py::init<DeviceIdx_t, std::vector<DimSize_t>>());

  py::class_<TensorImpl_cpu<uint8_t>, std::shared_ptr<TensorImpl_cpu<uint8_t>>, TensorImpl>(m, "TensorImpl_cpu_uint8")
    .def(py::init<DeviceIdx_t, std::vector<DimSize_t>>());

}
}
