/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/stl.h>
#include <memory>
#include <string>
#include "aidge/graph/Matching.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/utils/Types.h"
//#include "aidge/data/Data.hpp"

namespace py = pybind11;
namespace Aidge {
void init_SinglePassGraphMatching(py::module& m) {
    py::class_<Aidge::SinglePassGraphMatching::MatchingResult>(m,"MatchingResult")
        .def(py::init<>())
        .def_readwrite("graph", &Aidge::SinglePassGraphMatching::MatchingResult::graph)
        .def_readwrite("anchors", &Aidge::SinglePassGraphMatching::MatchingResult::anchors)
        .def_readwrite("startNode", &Aidge::SinglePassGraphMatching::MatchingResult::startNode);
 
    py::class_<Aidge::SinglePassGraphMatching>(m, "SinglePassGraphMatching") 
        .def(py::init<std::shared_ptr<GraphView>>(), py::arg("graph"))
        .def("match", 
            [](Aidge::SinglePassGraphMatching& self, const std::string& query, bool disjoint){
                // Note: Need to convert set to vector has MatchingResult is not hashable and 
                // set<MatchingResult> cannot be binded
                std::set<Aidge::SinglePassGraphMatching::MatchingResult> set_res = self.match(query, disjoint);
                std::vector<Aidge::SinglePassGraphMatching::MatchingResult> vec_res(set_res.begin(), set_res.end());
                return vec_res;
            },
            py::arg("query"), py::arg("disjoint") = false, 
            R"mydelimiter( Matches a query by direct, single-pass parse and match.
            :param query: The query string to search.
            :param disjoint: If true, only keep the longest disjoint matches.
            :return: A set of MatchingResult instances.
            )mydelimiter")
        .def("add_node_lambda", &SinglePassGraphMatching::addNodeLambda, py::arg("name"), py::arg("func"));

}
}  // namespace Aidge
