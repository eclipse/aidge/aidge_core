/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "aidge/graph/StaticAnalysis.hpp"

namespace py = pybind11;
namespace Aidge {

/**
 * @brief Trampoline class for binding
 *
 */
class pyOperatorStats: public OperatorStats {
public:
    using OperatorStats::OperatorStats; // Inherit constructors

    size_t getNbArithmOps() const override {
        PYBIND11_OVERRIDE(
            size_t,
            OperatorStats,
            getNbArithmOps
        );
    }

    size_t getNbLogicOps() const override {
        PYBIND11_OVERRIDE(
            size_t,
            OperatorStats,
            getNbLogicOps
        );
    }

    size_t getNbCompOps() const override {
        PYBIND11_OVERRIDE(
            size_t,
            OperatorStats,
            getNbCompOps
        );
    }

    size_t getNbNLOps() const override {
        PYBIND11_OVERRIDE(
            size_t,
            OperatorStats,
            getNbNLOps
        );
    }

    size_t getNbArithmIntOps() const override {
        PYBIND11_OVERRIDE(
            size_t,
            OperatorStats,
            getNbArithmIntOps
        );
    }

    size_t getNbMACOps() const override {
        PYBIND11_OVERRIDE(
            size_t,
            OperatorStats,
            getNbMACOps
        );
    }
};

class pyStaticAnalysis: public StaticAnalysis {
public:
    using StaticAnalysis::StaticAnalysis; // Inherit constructors

    size_t getNbParams(std::shared_ptr<Node> node) const override {
        PYBIND11_OVERRIDE(
            size_t,
            StaticAnalysis,
            getNbParams,
            node
        );
    }

    size_t getParamsSize(std::shared_ptr<Node> node) const override {
        PYBIND11_OVERRIDE(
            size_t,
            StaticAnalysis,
            getParamsSize,
            node
        );
    }

    void summary(bool incProducers) const override {
        PYBIND11_OVERRIDE(
            void,
            StaticAnalysis,
            summary,
            incProducers
        );
    }
};

void init_StaticAnalysis(py::module& m){
    py::class_<OperatorStats, std::shared_ptr<OperatorStats>, pyOperatorStats>(m, "OperatorStats", py::multiple_inheritance(), py::dynamic_attr())
    .def(py::init<const Operator&>(), py::arg("op"))
    .def("get_operator", &OperatorStats::getOperator)
    .def("get_nb_arithm_ops", &OperatorStats::getNbArithmOps)
    .def("get_nb_logic_ops", &OperatorStats::getNbLogicOps)
    .def("get_nb_comp_ops", &OperatorStats::getNbCompOps)
    .def("get_nb_nl_ops", &OperatorStats::getNbNLOps)
    .def("get_nb_ops", &OperatorStats::getNbOps)
    .def("get_nb_arithm_int_ops", &OperatorStats::getNbArithmIntOps)
    .def("get_nb_arithm_fp_ops", &OperatorStats::getNbArithmFpOps)
    .def("get_nb_mac_ops", &OperatorStats::getNbMACOps)
    ;
    declare_registrable<OperatorStats>(m, "OperatorStats");

    py::class_<StaticAnalysis, std::shared_ptr<StaticAnalysis>, pyStaticAnalysis>(m, "StaticAnalysis", py::multiple_inheritance(), py::dynamic_attr())
    .def(py::init<std::shared_ptr<GraphView>>(), py::arg("graph"))
    .def("get_graph", &StaticAnalysis::getGraph)
    .def("get_nb_params", &StaticAnalysis::getNbParams, py::arg("node"))
    .def("get_params_size", &StaticAnalysis::getParamsSize, py::arg("node"))
    .def("get_nb_arithm_ops", &StaticAnalysis::getNbArithmOps)
    .def("get_nb_logic_ops", &StaticAnalysis::getNbLogicOps)
    .def("get_nb_comp_ops", &StaticAnalysis::getNbCompOps)
    .def("get_nb_nl_ops", &StaticAnalysis::getNbNLOps)
    .def("get_nb_ops", &StaticAnalysis::getNbOps)
    .def("get_nb_arithm_int_ops", &StaticAnalysis::getNbArithmIntOps)
    .def("get_nb_arithm_fp_ops", &StaticAnalysis::getNbArithmFpOps)
    .def("get_nb_mac_ops", &StaticAnalysis::getNbMACOps)
    .def("summary", &StaticAnalysis::summary, py::arg("inc_producers") = false)
    .def("get_op_stats", &StaticAnalysis::getOpStats, py::arg("node"))
    ;
}
}
