#include <pybind11/pybind11.h>
#include "aidge/utils/sys_info/CoreVersionInfo.hpp"

namespace py = pybind11;
namespace Aidge {
void init_CoreSysInfo(py::module& m){
    m.def("show_version", &showCoreVersion);
    m.def("get_project_version", &getCoreProjectVersion);
    m.def("get_git_hash", &getCoreGitHash);
}
}
