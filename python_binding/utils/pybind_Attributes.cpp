/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "aidge/utils/Attributes.hpp"
#include "aidge/utils/DynamicAttributes.hpp"
#include "aidge/utils/StaticAttributes.hpp"

namespace py = pybind11;
namespace Aidge {

DynamicAttributes test_DynamicAttributes_binding() {
    DynamicAttributes attrs;
    attrs.addAttr<int>("a", 42);
    attrs.addAttr<std::string>("b", "test");
    attrs.addAttr<std::vector<bool>>("c", {true, false, true});
    attrs.addAttr("mem.a", 1);
    attrs.addAttr("mem.data.b", 1.0f);
    attrs.addAttr("impl.c", std::string("test"));
    return attrs;
}

double test_DynamicAttributes_binding_check(const DynamicAttributes& attrs) {
    return attrs.getAttr<double>("d");
}

void init_Attributes(py::module& m){
    py::class_<Attributes, std::shared_ptr<Attributes>>(m, "Attributes")
    .def("has_attr", &Attributes::hasAttr, py::arg("name"))
    .def("get_attr", &Attributes::getAttrPy, py::arg("name"))
    .def("__getattr__", &Attributes::getAttrPy, py::arg("name"))
    .def("set_attr", &Attributes::setAttrPy, py::arg("name"), py::arg("value"))
    .def("__setattr__", &Attributes::setAttrPy, py::arg("name"), py::arg("value"))
    .def("dict", &Attributes::dict)
    .def("__str__", &Attributes::str)
    .def("__repr__", &Attributes::repr);


    py::class_<DynamicAttributes, std::shared_ptr<DynamicAttributes>, Attributes>(m, "DynamicAttributes")
    .def(py::init<>())
    .def("add_attr", &DynamicAttributes::addAttrPy, py::arg("name"), py::arg("value"))
    .def("del_attr", &DynamicAttributes::delAttr, py::arg("name"));

    m.def("test_DynamicAttributes_binding", &test_DynamicAttributes_binding);
    m.def("test_DynamicAttributes_binding_check", &test_DynamicAttributes_binding_check, py::arg("attrs"));
}

} // namespace Aidge
