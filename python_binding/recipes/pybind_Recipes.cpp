/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <cstddef>
#include <string>

#include "aidge/graph/GraphView.hpp"
#include "aidge/recipes/Recipes.hpp"
#include "aidge/utils/Types.h"

namespace py = pybind11;

namespace Aidge {
void init_Recipes(py::module &m)
{


  m.def("matmul_to_fc", static_cast<void(*)(std::shared_ptr<GraphView>)>(matMulToFC), py::arg("graph_view"), R"mydelimiter(
    Recipe to Fuse MatMul and Add operators into an :py:class:`aidge_core.FC` operator.

    :param graph_view: Graph view on which we want to apply the recipe
    :type graph_view: :py:class:`aidge_core.GraphView`
    )mydelimiter");

  // m.def("matmul_to_fc", static_cast<void(*)(std::set<std::shared_ptr<Node>>)>(matMulToFC), py::arg("nodes"), R"mydelimiter(
  //   recipe to Fuse MatMul and Add operators into an :py:class:`aidge_core.FC` operator.

  //   :param nodes: The MatMul and Add nodes to fuse.
  //   :type nodes: list of :py:class:`aidge_core.Node`
  //   )mydelimiter");

  m.def("remove_node", removeNode, py::arg("graph_view"), py::arg("type"), py::arg("incProducers") = false, R"mydelimiter(
    Recipe to remove operators of a given type.

    :param graph_view: Graph view on which we want to apply the recipe
    :type graph_view: :py:class:`aidge_core.GraphView`
    :param type: Type of the operators to remove
    :type type: str
    :param incProducers: If true, also removed attached Producers
    :type incProducers: bool
    :return: Number of removed operators.
    :rtype: int
    )mydelimiter");

  m.def("remove_dropout", removeDropout, py::arg("graph_view"), R"mydelimiter(
    Recipe to remove dropout operators.

    :param graph_view: Graph view on which we want to apply the recipe
    :type graph_view: :py:class:`aidge_core.GraphView`
    :return: Number of removed operators.
    :rtype: int
    )mydelimiter");

  m.def("remove_identity", removeIdentity, py::arg("graph_view"), R"mydelimiter(
    Recipe to remove identity operators.

    :param graph_view: Graph view on which we want to apply the recipe
    :type graph_view: :py:class:`aidge_core.GraphView`
    :return: Number of removed operators.
    :rtype: int
    )mydelimiter");

  m.def("remove_flatten", static_cast<void(*)(std::shared_ptr<GraphView>)>(removeFlatten), py::arg("graph_view"), R"mydelimiter(
    Recipe to remove a Flatten operator if it is followed by a FC or a MatMul.
    The recipe can remove multiple Flatten operator if they are one after the other.

    :param graph_view: Graph view on which we want to apply the recipe.
    :type graph_view: :py:class:`aidge_core.GraphView`
    )mydelimiter");

  m.def("remove_constantOfShape", static_cast<size_t(*)(std::shared_ptr<GraphView>)>(removeConstantOfShape), py::arg("graph_view"), R"mydelimiter(
    Fuses constant => Generic | constantOfShape and transforms it into a Producer

    :param graph_view: Graph view on which we want to apply the recipe.
    :type graph_view: :py:class:`aidge_core.GraphView`
    )mydelimiter");

  m.def("fuse_batchnorm", static_cast<void(*)(std::shared_ptr<GraphView>)>(fuseBatchNorm), py::arg("graph_view"), R"mydelimiter(
    Recipe to remove a flatten operator.

    :param graph_view: Graph view on which we want to apply the recipe
    :type graph_view: :py:class:`aidge_core.GraphView`
    )mydelimiter");

  m.def("get_conv_horizontal_tiling", static_cast<std::set<std::shared_ptr<Node>>(*)(const std::shared_ptr<Node>&, const DimIdx_t, const std::size_t)>(getConvHorizontalTiling),
        py::arg("node"), py::arg("axis"), py::arg("nb_slices"));

  // m.def("fuse_batchnorm", static_cast<void(*)(std::set<std::shared_ptr<Node>>)>(fuseBatchNorm), py::arg("nodes"), R"mydelimiter(
  //   recipe to remove a flatten operator.

  //   :param nodes: The flatten operator to remove.
  //   :type nodes: list of :py:class:`aidge_core.Node`
  //   )mydelimiter");

  m.def("expand_metaops", static_cast<void(*)(std::shared_ptr<GraphView>, bool)>(expandMetaOps), py::arg("graph_view"), py::arg("recursive") = false, R"mydelimiter(
    Flatten the graph by replacing the meta operators by their micro graph.

    :param graph_view: Graph view on which we want to apply the recipe
    :type graph_view: :py:class:`aidge_core.GraphView`
    :param recursive: If true, recursively replace meta operators until there is no more meta operator in the graph.
    :type recursive: bool
    )mydelimiter");

  m.def("fuse_to_metaops", py::overload_cast<SinglePassGraphMatching&, const std::string&, const std::string&>(fuseToMetaOps), py::arg("gm"), py::arg("query"), py::arg("type") = "", R"mydelimiter(
    Fuse each sub-graph matching a query in a Meta Operator.

    :param gm: SinglePassGraphMatching containing the graph to manipulate
    :type gm: :py:class:`aidge_core.SinglePassGraphMatching`
    :param query: Sub-graph matching query
    :type query: str
    :param type: Type name of the resulting meta operators
    :type type: str, optional
    :return: Number of sub-graph actually fused in a Meta Operator.
    :rtype: int
    )mydelimiter");

  m.def("fuse_to_metaops", py::overload_cast<std::shared_ptr<GraphView>, const std::string&, const std::string&>(fuseToMetaOps), py::arg("graph_view"), py::arg("query"), py::arg("type") = "", R"mydelimiter(
    Fuse each sub-graph matching a query in a Meta Operator.

    :param graph_view: Graph view on which we want to apply the recipe
    :type graph_view: :py:class:`aidge_core.GraphView`
    :param query: Sub-graph matching query
    :type query: str
    :param type: Type name of the resulting meta operators
    :type type: str, optional
    :return: Number of sub-graph actually fused in a Meta Operator.
    :rtype: int
    )mydelimiter");

  m.def("adapt_to_backend", adaptToBackend, py::arg("graph_view"), R"mydelimiter(
    Adapt the graph to a specific backend.

    :param graph_view: Graph view on which we want to apply the recipe
    :type graph_view: :py:class:`aidge_core.GraphView`
    )mydelimiter");

  m.def("to_generic_op", toGenericOp, py::arg("node"), R"mydelimiter(
    Transform to a Generic Operator.

    :param node: Node which Operator will turn into a Generic Operator
    :type graph_view: :py:class:`aidge_core.Node`
    )mydelimiter");

  m.def("apply_weightinterleaving", applyWeightInterleaving, py::arg("node"), R"mydelimiter(
    Replace weight Producer linked to the given node with a weight producer with interleaving and format NHWC.
    This recipe is specific to the ARM cortex-m export for low bit integer support.

    :param node: Node which linked weights will recieve interleaving
    :type graph_view: :py:class:`aidge_core.Node`
    )mydelimiter");
}

} // namespace Aidge
