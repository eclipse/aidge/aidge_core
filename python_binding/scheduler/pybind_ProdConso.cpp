/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <string>

#include "aidge/operator/Operator.hpp"
#include "aidge/scheduler/ProdConso.hpp"

namespace py = pybind11;
namespace Aidge {

/**
 * @brief Trampoline class for binding
 *
 */
class pyProdConso: public ProdConso {
public:
    using ProdConso::ProdConso; // Inherit constructors

    Elts_t getNbRequiredData(const IOIndex_t inputIdx) const override {
        PYBIND11_OVERRIDE_NAME(
            Elts_t,
            ProdConso,
            "get_nb_required_data",
            getNbRequiredData,
            inputIdx
        );
    }
    Elts_t getNbRequiredProtected(const IOIndex_t inputIdx) const override {
        PYBIND11_OVERRIDE_NAME(
            Elts_t,
            ProdConso,
            "get_nb_required_protected",
            getNbRequiredProtected,
            inputIdx

        );
    }
    Elts_t getRequiredMemory(const IOIndex_t outputIdx,
    const std::vector<DimSize_t> &inputsSize) const override {
        PYBIND11_OVERRIDE_NAME(
            Elts_t,
            ProdConso,
            "get_required_memory",
            getRequiredMemory,
            outputIdx,
            inputsSize

        );
    }
    Elts_t getNbConsumedData(const IOIndex_t inputIdx) const override {
        PYBIND11_OVERRIDE_NAME(
            Elts_t,
            ProdConso,
            "get_nb_consumed_data",
            getNbConsumedData,
            inputIdx

        );
    }
    Elts_t getNbProducedData(const IOIndex_t outputIdx) const override {
        PYBIND11_OVERRIDE_NAME(
            Elts_t,
            ProdConso,
            "get_nb_produced_data",
            getNbProducedData,
            outputIdx

        );
    }
    void updateConsummerProducer() override {
        PYBIND11_OVERRIDE_NAME(
            void,
            ProdConso,
            "update_consummer_producer",
            updateConsummerProducer,

        );
    }
    void resetConsummerProducer() override {
        PYBIND11_OVERRIDE_NAME(
            void,
            ProdConso,
            "reset_consummer_producer",
            resetConsummerProducer,

        );
    }
};

void init_ProdConso(py::module& m){

    py::class_<ProdConso, std::shared_ptr<ProdConso>, pyProdConso>(m, "ProdConso", py::dynamic_attr())
    .def(py::init<const Operator&, bool>(), py::keep_alive<1, 1>(), py::keep_alive<1, 2>(), py::keep_alive<1,3>())
    .def_static("default_model", &ProdConso::defaultModel)
    .def_static("in_place_model", &ProdConso::inPlaceModel)
    .def("get_operator", &ProdConso::getOperator)
    .def("get_nb_required_data", &ProdConso::getNbRequiredData)
    .def("get_nb_required_protected", &ProdConso::getNbRequiredProtected)
    .def("get_required_memory", &ProdConso::getRequiredMemory)
    .def("get_nb_consumed_data", &ProdConso::getNbConsumedData)
    .def("get_nb_produced_data", &ProdConso::getNbProducedData)
    .def("update_consummer_producer", &ProdConso::updateConsummerProducer)
    .def("reset_consummer_producer", &ProdConso::resetConsummerProducer)
    ;
}
}
