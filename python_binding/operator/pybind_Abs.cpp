/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Abs.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Abs(py::module& m) {
    py::class_<Abs_Op, std::shared_ptr<Abs_Op>, OperatorTensor>(m, "AbsOp", py::multiple_inheritance())
    .def(py::init<>())
    .def_static("get_inputs_name", &Abs_Op::getInputsName)
    .def_static("get_outputs_name", &Abs_Op::getOutputsName)
    .def_readonly_static("Type", &Abs_Op::Type);
    declare_registrable<Abs_Op>(m, "AbsOp");

    m.def("Abs", &Abs, py::arg("name") = "");
}
}  // namespace Aidge