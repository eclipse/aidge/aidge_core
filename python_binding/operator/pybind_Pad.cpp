/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
#include <array>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Pad.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Types.h"

namespace py = pybind11;
namespace Aidge {

// Template function for declaring Pad operator for different dimensions
template <DimIdx_t DIM> void declare_PadOp(py::module &m) {
  const std::string pyClassName("Pad" + std::to_string(DIM) + "DOp");

  py::class_<Pad_Op<DIM>, std::shared_ptr<Pad_Op<DIM>>, OperatorTensor>(
    m, pyClassName.c_str(),
    py::multiple_inheritance(),
    R"mydelimiter(
        Initialize a Pad operator for a tensor.

        This operator applies padding to the input tensor along specified dimensions.

        :param beginEndTuples: Padding configurations for each dimension in the form [begin, end].
        :type beginEndTuples: List[int]
        :param borderType: Type of padding to be applied (default is Constant).
        :type borderType: PadBorderType
        :param borderValue: Value used for padding if borderType is Constant (default is 0.0).
        :type borderValue: float
    )mydelimiter")
  .def(py::init<const std::array<DimSize_t, 2*DIM> &,
                PadBorderType,
                double>(),
        py::arg("beginEndTuples"),
        py::arg("borderType") = PadBorderType::Constant,
        py::arg("borderValue") = 0.0)
    .def_static("get_inputs_name", &Pad_Op<DIM>::getInputsName)
    .def_static("get_outputs_name", &Pad_Op<DIM>::getOutputsName)
    .def_readonly_static("Type", &Pad_Op<DIM>::Type);

  declare_registrable<Pad_Op<DIM>>(m, pyClassName);

  m.def(("Pad" + std::to_string(DIM) + "D").c_str(), [](const std::vector<DimSize_t>& beginEndTuples,
                                                        const std::string& name,
                                                        PadBorderType borderType = PadBorderType::Constant,
                                                        double borderValue = 0.0) {
        AIDGE_ASSERT(beginEndTuples.size() == 2*DIM, "begin_end_tuples size [{}] does not match DIM [{}]", beginEndTuples.size(), 2*DIM);
        return Pad<DIM>(to_array<2*DIM>(beginEndTuples.begin()), name, borderType, borderValue);
    },
       py::arg("begin_end_tuples"),
       py::arg("name") = "",
       py::arg("border_type") = PadBorderType::Constant,
       py::arg("border_value") = 0.0,
       R"mydelimiter(
        Initialize a node containing a Pad operator.

        This function applies padding to the tensor along the specified dimensions 
        using the given padding type and value.

        :param begin_end_tuples: Padding configuration for each dimension in the format [begin, end] for each dimension.
        :type begin_end_tuples: List[int]
        :param name: Name of the operator node (optional).
        :type name: str
        :param border_type: Type of padding (Constant, Edge, Reflect, Wrap) (default is Constant).
        :type border_type: PadBorderType
        :param border_value: The value used for padding if border_type is Constant (default is 0.0).
        :type border_value: float
    )mydelimiter");
}

// Initialize the Pad operator for different dimensions
void init_Pad(py::module &m) {
  py::enum_<PadBorderType>(m, "pad_border_type")
    .value("Constant", PadBorderType::Constant)
    .value("Edge",     PadBorderType::Edge)
    .value("Reflect",  PadBorderType::Reflect)
    .value("Wrap",     PadBorderType::Wrap)
    .export_values();

  declare_PadOp<1>(m);
  declare_PadOp<2>(m);
  //declare_PadOp<3>(m); // Uncomment if needed for 3D pad
}

} // namespace Aidge
