/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <array>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/ReduceMean.hpp"
#include "aidge/utils/Types.h"

namespace py = pybind11;
namespace Aidge {

void declare_ReduceMeanOp(py::module &m) {
  const std::string pyClassName("ReduceMeanOp");
  py::class_<ReduceMean_Op, std::shared_ptr<ReduceMean_Op>, OperatorTensor>(
    m, pyClassName.c_str(), py::multiple_inheritance(),
      R"mydelimiter(
		Initialize a ReduceMean operator.
			:param axes: Axes along which to do the reduction. The accepted range is [-r, r-1], 
						where r is the rank of the input tensor.
			:type axes: List[int]
			:param keepdims: If True (default), retains the reduced dimensions with size 1. If False, 
							the reduced dimensions are removed.
			:type keepdims: bool
			:param noop_with_empty_axes: If True, the operator just copies the input, 
      if False, the operatpr reduces all the dimensions.
			:type noop_with_empty_axes: bool
		)mydelimiter")
    .def(py::init<std::vector<std::int32_t>, bool, bool>(), py::arg("axes") = std::vector<std::int32_t>(), py::arg("keep_dims") = true, py::arg("noop_with_empty_axes") = false)
    .def_static("get_inputs_name", &ReduceMean_Op::getInputsName)
    .def_static("get_outputs_name", &ReduceMean_Op::getOutputsName)
    .def_readonly_static("Type", &ReduceMean_Op::Type)
    ;
  declare_registrable<ReduceMean_Op>(m, pyClassName);

  m.def("ReduceMean", [](const std::vector<int>& axes,
                          bool keepDims,
                          bool noopWithEmptyAxes,
                          const std::string& name) {
        // AIDGE_ASSERT(axes.size() == DIM, "axes size [{}] does not match DIM [{}]", axes.size(), DIM);

        return ReduceMean(axes, keepDims, noopWithEmptyAxes, name);
    }, py::arg("axes") = std::vector<std::int32_t>(),
       py::arg("keep_dims") = true,
       py::arg("noop_with_empty_axes") = false,
       py::arg("name") = "",
	   R"mydelimiter(
        Initialize a node containing a ReduceMean operator.
			:param axes: Axes along which to do the reduction. The accepted range is [-r, r-1], 
						where r is the rank of the input tensor.
			:type axes: List[int]
			:param keepdims: If True (default), retains the reduced dimensions with size 1. If False, 
							the reduced dimensions are removed.
			:type keepdims: bool
			:param noop_with_empty_axes: If True, the operator just copies the input, 
      if False, the operatpr reduces all the dimensions.
			:type noop_with_empty_axes: bool
			:param name : name of the node.
		)mydelimiter");
}


void init_ReduceMean(py::module &m) {
  declare_ReduceMeanOp(m);
//   declare_ReduceMeanOp<2>(m);
//   declare_ReduceMeanOp<3>(m);

  // FIXME:
  // m.def("ReduceMean1D", static_cast<NodeAPI(*)(const char*, int, int, int const
  // (&)[1])>(&ReduceMean));
}
} // namespace Aidge
