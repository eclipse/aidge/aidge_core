/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Mul.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Mul(py::module& m) {
    py::class_<Mul_Op, std::shared_ptr<Mul_Op>, OperatorTensor>(m, "MulOp", py::multiple_inheritance(),
    R"mydelimiter(
        Initialize a Mul operator, which performs element-wise multiplication between two tensors.

        :param name: Name of the node (optional).
        :type name: str
    )mydelimiter")
    .def(py::init<>())
    .def_static("get_inputs_name", &Mul_Op::getInputsName)
    .def_static("get_outputs_name", &Mul_Op::getOutputsName)
    .def_readonly_static("Type", &Mul_Op::Type);
    declare_registrable<Mul_Op>(m, "MulOp");

    m.def("Mul", &Mul, py::arg("name") = "",
    R"mydelimiter(
        Initialize a node containing a Mul operator that performs element-wise multiplication.

        This operator performs element-wise multiplication between two tensors.

        :param name: Name of the node (optional).
        :type name: str
        :return: A node containing the Mul operator.
        :rtype: :py:class:`MulOp`
    )mydelimiter");
}

}  // namespace Aidge
