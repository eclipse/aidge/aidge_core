/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Identity.hpp"
#include "aidge/operator/Operator.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Identity(py::module& m) {
    py::class_<Identity_Op, std::shared_ptr<Identity_Op>, OperatorTensor>(m, "IdentityOp", py::multiple_inheritance(),
    R"mydelimiter(
    A class representing the Identity operator, which returns the input as-is.
    )mydelimiter")
        .def(py::init<>())
        .def_static("get_inputs_name", &Identity_Op::getInputsName)
        .def_static("get_outputs_name", &Identity_Op::getOutputsName)
        .def_readonly_static("Type", &Identity_Op::Type);

    m.def("Identity", &Identity, py::arg("name") = "",
    R"mydelimiter(
    Creates an Identity operation, which returns the input as-is.

    :param name: Name of the node.
    )mydelimiter");
}

}  // namespace Aidge
