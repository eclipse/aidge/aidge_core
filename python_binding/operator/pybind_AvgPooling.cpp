/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <array>
#include <string>
#include <vector>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/AvgPooling.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Types.h"
#include "aidge/utils/StaticAttributes.hpp"

namespace py = pybind11;
namespace Aidge {

// Template function for declaring AvgPooling operator for different dimensions
template <DimIdx_t DIM> void declare_AvgPoolingOp(py::module &m) {

  const std::string pyClassName("AvgPooling" + std::to_string(DIM) + "DOp");
  const std::string pyStaticAttrClassName("StaticAttributes" + pyClassName);
  
  py::class_<AvgPooling_Op<DIM>, std::shared_ptr<AvgPooling_Op<DIM>>, OperatorTensor>(
        m, pyClassName.c_str(),
        py::multiple_inheritance(),
        R"mydelimiter(
        Initialize an AvgPooling operator for a tensor.

        This operator performs average pooling on the input tensor using the specified kernel dimensions 
        and stride dimensions.

        :param kernel_dims: The size of the kernel (filter) applied during pooling. 
                             Specifies the dimensions of the kernel (e.g., [3, 3] for 2D pooling).
        :type kernel_dims: List[int]
        :param stride_dims: The stride of the pooling operation. Specifies how much the kernel moves in each step.
                             By default, the stride is set to 1 for all dimensions.
        :type stride_dims: List[int], optional
        )mydelimiter")
    .def(py::init<const std::array<DimSize_t, DIM> &,
                  const std::array<DimSize_t, DIM> &>(),
            py::arg("kernel_dims"),
            py::arg("stride_dims") = create_array<DimSize_t, DIM>(1))
    .def("get_inputs_name", &AvgPooling_Op<DIM>::getInputsName)
    .def("get_outputs_name", &AvgPooling_Op<DIM>::getOutputsName)
    .def_readonly_static("Type", &AvgPooling_Op<DIM>::Type);

  declare_registrable<AvgPooling_Op<DIM>>(m, pyClassName);

  m.def(("AvgPooling" + std::to_string(DIM) + "D").c_str(), [](const std::vector<DimSize_t>& kernel_dims,
                                                                  const std::string& name,
                                                                  const std::vector<DimSize_t>& stride_dims) {
        AIDGE_ASSERT(kernel_dims.size() == DIM, "kernel_dims size [{}] does not match DIM [{}]", kernel_dims.size(), DIM);
        AIDGE_ASSERT(stride_dims.size() == DIM, "stride_dims size [{}] does not match DIM [{}]", stride_dims.size(), DIM);

        return AvgPooling<DIM>(to_array<DIM>(kernel_dims.begin()), name, to_array<DIM>(stride_dims.begin()));
    }, py::arg("kernel_dims"),
       py::arg("name") = "",
       py::arg("stride_dims") = std::vector<DimSize_t>(DIM, 1),
       R"mydelimiter(
        Initialize a node containing an AvgPooling operator.

        This function performs average pooling on the tensor with the given kernel and stride dimensions.

        :param kernel_dims: Size of the kernel applied during pooling.
        :type kernel_dims: List[int]
        :param name: Name of the operator node (optional).
        :type name: str
        :param stride_dims: Stride dimensions for the pooling operation.
        :type stride_dims: List[int], optional
        )mydelimiter");
}

// Initialize the AvgPooling operator for different dimensions
void init_AvgPooling(py::module &m) {
  declare_AvgPoolingOp<1>(m);
  declare_AvgPoolingOp<2>(m);
  declare_AvgPoolingOp<3>(m);

  // FIXME:
  // m.def("AvgPooling1D", static_cast<NodeAPI(*)(const char*, int, int, int const
  // (&)[1])>(&AvgPooling));
}

} // namespace Aidge
