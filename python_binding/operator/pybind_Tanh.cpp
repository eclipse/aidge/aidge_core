/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Tanh.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Tanh(py::module& m) {
    py::class_<Tanh_Op, std::shared_ptr<Tanh_Op>, OperatorTensor>(m, "TanhOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize a Tanh operator.
        The Tanh (hyperbolic tangent) function is applied element-wise to the input tensor.
        It is defined as:
            tanh(x) = (exp(x) - exp(-x)) / (exp(x) + exp(-x))
        :param name : name of the node (optional).
        :type name : str
        )mydelimiter")
    .def(py::init<>())
    .def_static("get_inputs_name", &Tanh_Op::getInputsName)
    .def_static("get_outputs_name", &Tanh_Op::getOutputsName)
    .def_readonly_static("Type", &Tanh_Op::Type);

    m.def("Tanh", &Tanh, py::arg("name") = "",
        R"mydelimiter(
        Initialize a node containing a Tanh operator that applies the tanh function element-wise.

        The tanh function is applied element-wise, and the operation is defined as:
            tanh(x) = (exp(x) - exp(-x)) / (exp(x) + exp(-x))

        :param name: Name of the node (optional).
        :type name: str
        :return: A node containing the Tanh operator.
        :rtype: :py:class:`TanhOp`
        )mydelimiter");
}

}  // namespace Aidge
