/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <string>
#include <vector>
#include <array>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/ConvDepthWise.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Types.h"
#include "aidge/data/Tensor.hpp"

namespace py = pybind11;
namespace Aidge {

// Template function to declare the ConvDepthWise operator for a specific dimensionality.
template <DimIdx_t DIM>
void declare_ConvDepthWiseOp(py::module &m) {
  const std::string pyClassName("ConvDepthWise" + std::to_string(DIM) + "DOp");
  py::class_<ConvDepthWise_Op<DIM>, std::shared_ptr<ConvDepthWise_Op<DIM>>, OperatorTensor>(
    m, pyClassName.c_str(),
    py::multiple_inheritance(), 
    R"mydelimiter(
    Initialize a Depthwise Convolution operator.

    :param kernel_dims : The dimensions of the convolution kernel (filter size).
    :type kernel_dims : List[int]
    :param stride_dims : The stride size for the convolution.
    :type stride_dims : List[int]
    :param dilation_dims : The dilation size for the convolution.
    :type dilation_dims : List[int]
    )mydelimiter")
  .def(py::init([](const std::array<DimSize_t, DIM> &kernel_dims,
                   const std::array<DimSize_t, DIM> &stride_dims,
                   const std::array<DimSize_t, DIM> &dilation_dims) {
        AIDGE_ASSERT(kernel_dims.size() == DIM, "kernel_dims size [{}] does not match DIM [{}]", kernel_dims.size(), DIM);
        AIDGE_ASSERT(stride_dims.size() == DIM, "stride_dims size [{}] does not match DIM [{}]", stride_dims.size(), DIM);
        AIDGE_ASSERT(dilation_dims.size() == DIM, "dilation_dims size [{}] does not match DIM [{}]", dilation_dims.size(), DIM);

        return new ConvDepthWise_Op<DIM>(kernel_dims, stride_dims, dilation_dims);
    }), py::arg("kernel_dims"),
        py::arg("stride_dims"),
        py::arg("dilation_dims"))
  .def_static("get_inputs_name", &ConvDepthWise_Op<DIM>::getInputsName)
  .def_static("get_outputs_name", &ConvDepthWise_Op<DIM>::getOutputsName)
  .def("nb_channels", &ConvDepthWise_Op<DIM>::nbChannels)
  .def_readonly_static("Type", &ConvDepthWise_Op<DIM>::Type);

  declare_registrable<ConvDepthWise_Op<DIM>>(m, pyClassName);
  m.def(("ConvDepthWise" + std::to_string(DIM) + "D").c_str(), [](const DimSize_t nb_channels,
                                                                  const std::vector<DimSize_t>& kernel_dims,
                                                                  const std::string& name,
                                                                  const std::vector<DimSize_t> &stride_dims,
                                                                  const std::vector<DimSize_t> &dilation_dims,
                                                                  bool no_bias) {
        AIDGE_ASSERT(kernel_dims.size() == DIM, "kernel_dims size [{}] does not match DIM [{}]", kernel_dims.size(), DIM);
        AIDGE_ASSERT(stride_dims.size() == DIM, "stride_dims size [{}] does not match DIM [{}]", stride_dims.size(), DIM);
        AIDGE_ASSERT(dilation_dims.size() == DIM, "dilation_dims size [{}] does not match DIM [{}]", dilation_dims.size(), DIM);

        return ConvDepthWise<DIM>(nb_channels, to_array<DIM>(kernel_dims.begin()), name, to_array<DIM>(stride_dims.begin()), to_array<DIM>(dilation_dims.begin()), no_bias);
    }, 
    py::arg("nb_channels"),
    py::arg("kernel_dims"),
    py::arg("name") = "",
    py::arg("stride_dims") = std::vector<DimSize_t>(DIM,1),
    py::arg("dilation_dims") = std::vector<DimSize_t>(DIM,1),
    py::arg("no_bias")= false,
    R"mydelimiter(
    Initialize a node containing a depthwise convolution operator.

    :param nb_channels : The number of channels in the input tensor (i.e., depth of the tensor).
    :type nb_channels : int
    :param kernel_dims : The dimensions of the convolution kernel (filter size).
    :type kernel_dims : List[int]
    :param name : The name of the operator node (optional).
    :type name : str
    :param stride_dims : The stride size for the convolution (default is [1]).
    :type stride_dims : List[int]
    :param dilation_dims : The dilation size for the convolution (default is [1]).
    :type dilation_dims : List[int]
    :param no_bias : Whether to disable bias in the operation (default is False).
    :type no_bias : bool
    :return : A new Depthwise Convolution operator node.
    :rtype : :py:class:`ConvDepthWiseOp`
    )mydelimiter");
}

// Function to initialize the Depthwise Convolution operators for different dimensionalities.
void init_ConvDepthWise(py::module &m) {
  declare_ConvDepthWiseOp<1>(m);
  declare_ConvDepthWiseOp<2>(m);
  // Uncomment the following line to add support for 3D convolution
  // declare_ConvDepthWiseOp<3>(m);
}

} // namespace Aidge
