/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Sigmoid.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Sigmoid(py::module& m) {
    py::class_<Sigmoid_Op, std::shared_ptr<Sigmoid_Op>, OperatorTensor>(m, "SigmoidOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize a Sigmoid operator.
        The Sigmoid function is applied element-wise to the input tensor.
        It is defined as:
            Sigmoid(x) = 1 / (1 + exp(-x))
        This operator applies the sigmoid activation function to each element of the tensor, squashing each value into the range (0, 1).
        :param name : name of the node (optional).
        :type name : str
        )mydelimiter")
        .def(py::init<>())
        .def_static("get_inputs_name", &Sigmoid_Op::getInputsName)
        .def_static("get_outputs_name", &Sigmoid_Op::getOutputsName)
        .def_readonly_static("Type", &Sigmoid_Op::Type);


    declare_registrable<Sigmoid_Op>(m, "SigmoidOp");

    m.def("Sigmoid", &Sigmoid, py::arg("name") = "",
        R"mydelimiter(
        Initialize a node containing a Sigmoid operator that applies the Sigmoid function element-wise.

        The Sigmoid function is applied element-wise and is defined as:
            Sigmoid(x) = 1 / (1 + exp(-x))

        This operation squashes each value of the tensor into the range (0, 1), making it commonly used for activation functions in neural networks.

        :param name: Name of the node (optional).
        :type name: str
        :return: A node containing the Sigmoid operator.
        :rtype: :py:class:`SigmoidOp`
        )mydelimiter");
}

}  // namespace Aidge
