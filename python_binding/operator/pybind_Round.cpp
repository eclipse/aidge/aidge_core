/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/operator/Round.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Round(py::module& m) {
    py::class_<Round_Op, std::shared_ptr<Round_Op>, OperatorTensor>(m, "RoundOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize a Round operator, which rounds the values of a tensor element-wise.

        This operator rounds each value in the input tensor to the nearest integer. 
        For values exactly halfway between two integers, it rounds to the nearest even integer.

        :param name: The name of the node (optional).
        :type name: str
        )mydelimiter")
    .def(py::init<>())
    .def_static("get_inputs_name", &Round_Op::getInputsName)
    .def_static("get_outputs_name", &Round_Op::getOutputsName)
    .def_readonly_static("Type", &Round_Op::Type);

    declare_registrable<Round_Op>(m, "RoundOp");

    m.def("Round", &Round, py::arg("name") = "",
        R"mydelimiter(
        Initialize a node containing a Round operator that rounds tensor values element-wise.

        This operator processes the input tensor and rounds each value to the nearest integer. 
        If a value is exactly halfway between two integers, it rounds to the nearest even integer.

        :param name: The name of the node (optional).
        :type name: str
        :return: A node containing the Round operator.
        :rtype: :py:class:`RoundOp`
        )mydelimiter");
}
}  // namespace Aidge
