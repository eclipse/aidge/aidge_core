/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <string>

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/BatchNorm.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Types.h"

namespace py = pybind11;
namespace Aidge {

template <DimSize_t DIM>
void declare_BatchNormOp(py::module& m) {
    const std::string pyClassName("BatchNorm" + std::to_string(DIM) + "DOp");
    py::class_<BatchNorm_Op<DIM>, std::shared_ptr<BatchNorm_Op<DIM>>, OperatorTensor>(
    m, pyClassName.c_str(), py::multiple_inheritance(),
    R"mydelimiter(
    Initialize a BatchNorm operator.

    :param epsilon : A small value added to the denominator for numerical stability.
    :type epsilon : :py:class:`float`
    :param momentum : The momentum factor for the moving averages.
    :type momentum : :py:class:`float`
    :param training_mode : Whether the operator is in training mode (for batch statistics) or inference mode (for using the moving averages).
    :type training_mode : :py:class:`bool`
    )mydelimiter")
        .def(py::init<float, float, bool>(),
            py::arg("epsilon"),
            py::arg("momentum"),
            py::arg("training_mode"))
        .def_static("get_inputs_name", &BatchNorm_Op<DIM>::getInputsName)
        .def_static("get_outputs_name", &BatchNorm_Op<DIM>::getOutputsName)
        .def_readonly_static("Type", &BatchNorm_Op<DIM>::Type);

    declare_registrable<BatchNorm_Op<DIM>>(m, pyClassName);

    m.def(("BatchNorm" + std::to_string(DIM) + "D").c_str(), &BatchNorm<DIM>, py::arg("nb_features"), py::arg("epsilon") = 1.0e-5F, py::arg("momentum") = 0.1F, py::arg("training_mode") = false, py::arg("name") = "",
        R"mydelimiter(
        Initialize a node containing a BatchNorm operator.

        :param nb_features : The number of features in the input tensor.
        :type nb_features : :py:class:`int`
        :param epsilon : A small value added to the denominator for numerical stability.
        :type epsilon : :py:class:`float`
        :param momentum : The momentum factor for the moving averages.
        :type momentum : :py:class:`float`
        :param training_mode : Whether the operator is in training mode or inference mode.
        :type training_mode : :py:class:`bool`
        :param name : Name of the node.
        :type name : :py:class:`str`
        )mydelimiter");
}

void init_BatchNorm(py::module &m) {
    declare_BatchNormOp<2>(m);
}

}  // namespace Aidge

