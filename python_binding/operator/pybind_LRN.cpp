/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/LRN.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_LRN(py::module& m) {
    py::class_<LRN_Op, std::shared_ptr<LRN_Op>, OperatorTensor>(m, "LRNOp", py::multiple_inheritance(),
    R"mydelimiter(
    A class representing the Local Response Normalization (LRN) operator.

    This operator performs Local Response Normalization, which normalizes each element in the input tensor
    based on its neighbors within a local region defined by the given size parameter.
    )mydelimiter")
        .def(py::init<std::int32_t>(), py::arg("size"))
        .def_static("get_inputs_name", &LRN_Op::getInputsName)
        .def_static("get_outputs_name", &LRN_Op::getOutputsName)
        .def_readonly_static("Type", &LRN_Op::Type);

    m.def("LRN", &LRN, py::arg("size"), py::arg("name") = "",
    R"mydelimiter(
    Create a node containing the Local Response Normalization operator.

    :param size: The size of the local region for normalization.
    :param name: The name of the node (optional).
    )mydelimiter");
}

}  // namespace Aidge
