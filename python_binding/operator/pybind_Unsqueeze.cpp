/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <string>
#include <vector>

#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Unsqueeze.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Unsqueeze(py::module &m) {
  py::class_<Unsqueeze_Op, std::shared_ptr<Unsqueeze_Op>, OperatorTensor>(
      m, "UnsqueezeOp", py::multiple_inheritance(),
      R"mydelimiter(
		Initialize an unsqueeze operator.
		:param axes :   axes to unsqueeze between [-r;r-1] 
						with r = input_tensor.nbDims() + len(axes)
		:type axes : :py:class: List[Int]
		)mydelimiter")
      // Here we bind the methods of the Unsqueeze_Op that will want to access
      .def("get_inputs_name", &Unsqueeze_Op::getInputsName)
      .def("get_outputs_name", &Unsqueeze_Op::getOutputsName)
      .def("axes", &Unsqueeze_Op::axes);
  // Here we bind the constructor of the Unsqueeze Node. We add an argument for
  // each attribute of the operator (in here we only have 'axes') and the last
  // argument is the node's name.
  m.def("Unsqueeze", &Unsqueeze, py::arg("axes") = std::vector<int8_t>({}),
        py::arg("name") = "",
        R"mydelimiter(
    Initialize a node containing an unsqueeze operator.
	:param axes :   axes to unsqueeze between [-r;r-1] 
					with r = input_tensor.nbDims() + len(axes)
	:type axes : :py:class: List[Int]
    :param name : name of the node.
)mydelimiter");
}
} // namespace Aidge
