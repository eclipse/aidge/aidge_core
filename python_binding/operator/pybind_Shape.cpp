/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cstdint>  // std::int64_t

#include <pybind11/pybind11.h>

#include "aidge/operator/Shape.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Shape(py::module& m) {
    py::class_<Shape_Op, std::shared_ptr<Shape_Op>, OperatorTensor>(m, "ShapeOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize a Shape operator, which extracts a slice of the tensor's shape.

        :param start: The starting index (inclusive) for the shape slice. The accepted range is [-r; r-1], 
                      where r is the rank of the input tensor.
        :type start: int
        :param end: The ending index (exclusive) for the shape slice. The accepted range is [-r; r-1], 
                    where r is the rank of the input tensor. If not specified, the slice will go until the end of the shape.
        :type end: int
        )mydelimiter")
        .def(py::init<const std::int64_t, const std::int64_t>(), py::arg("start"), py::arg("end"))
        .def_static("get_inputs_name", &Shape_Op::getInputsName)
        .def_static("get_outputs_name", &Shape_Op::getOutputsName)
        .def_readonly_static("Type", &Shape_Op::Type);

    declare_registrable<Shape_Op>(m, "ShapeOp");

    m.def("Shape", &Shape, py::arg("start") = 0, py::arg("end") = -1, py::arg("name") = "",
        R"mydelimiter(
        Initialize a node containing a Shape operator that extracts a slice of the tensor's shape.

        This operator extracts a slice of the tensor's shape from the specified start to end indices. 
        The start and end indices are inclusive and exclusive respectively, and can be positive or negative. 
        The accepted range for both is [-r, r-1], where r is the rank of the input tensor.

        :param start: The starting index (inclusive) for the shape slice. The accepted range is [-r; r-1].
        :type start: int
        :param end: The ending index (exclusive) for the shape slice. The accepted range is [-r; r-1].
        :type end: int
        :param name: Name of the node (optional).
        :type name: str
        :return: A node containing the Shape operator.
        :rtype: :py:class:`ShapeOp`
        )mydelimiter");
}

}  // namespace Aidge
