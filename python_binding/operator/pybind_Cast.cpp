/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include <string>
#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Cast.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Types.h"

namespace py = pybind11;
namespace Aidge {

void init_Cast(py::module &m) {
    // Binding for CastOp class
    auto pyCastOp = py::class_<Cast_Op, std::shared_ptr<Cast_Op>, OperatorTensor>(m, "CastOp", py::multiple_inheritance(),R"mydelimiter(
        CastOp is a tensor operator that casts the input tensor to a data type specified by the target_type argument.
        :param target_type: data type of the output tensor 
        :type target_type: Datatype
        :param name: name of the node.
    )mydelimiter")
        .def(py::init<DataType>(), py::arg("target_type"))
        .def("target_type", &Cast_Op::targetType, "Get the targeted type, output tensor data type")
        .def_static("get_inputs_name", &Cast_Op::getInputsName, "Get the names of the input tensors.")
        .def_static("get_outputs_name", &Cast_Op::getOutputsName, "Get the names of the output tensors.");

    // Binding for the Cast function
    m.def("Cast", &Cast, py::arg("target_type"), py::arg("name") = "",
        R"mydelimiter(
        CastOp is a tensor operator that casts the input tensor to a data type specified by the target_type argument.
        :param target_type: data type of the output tensor 
        :type target_type: Datatype
        :param name: name of the node.
    )mydelimiter");
}
} // namespace Aidge