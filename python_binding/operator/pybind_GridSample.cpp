/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <string>
#include <vector>
#include <array>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/GridSample.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Types.h"
#include "aidge/utils/Registrar.hpp" // declare_registrable


static typename Aidge::GridSample_Op::Mode stringToInterpolationMode(const std::string& mode) {
    static std::unordered_map<std::string, typename Aidge::GridSample_Op::Mode> map = {
        {"linear", Aidge::GridSample_Op::Mode::Linear},
        {"nearest", Aidge::GridSample_Op::Mode::Nearest},
        {"cubic", Aidge::GridSample_Op::Mode::Cubic}
    };
    return map[mode];
}

static typename Aidge::GridSample_Op::PaddingMode stringToPaddingMode(const std::string& mode) {
    static std::unordered_map<std::string, typename Aidge::GridSample_Op::PaddingMode> map = {
        {"zeros", Aidge::GridSample_Op::PaddingMode::Zeros},
        {"border", Aidge::GridSample_Op::PaddingMode::Border},
        {"reflection", Aidge::GridSample_Op::PaddingMode::Reflection}
    };
    return map[mode];
}

namespace py = pybind11;
namespace Aidge {

void declare_GridSampleOp(py::module &m) {
  const std::string pyClassName("GridSampleOp");
  py::class_<GridSample_Op, std::shared_ptr<GridSample_Op>, OperatorTensor>(
    m, pyClassName.c_str(),
    py::multiple_inheritance(),
    R"mydelimiter(
    A class representing the GridSample operator, which performs grid sampling.

    :param mode: Interpolation mode to use for sampling.
    :param padding_mode: Padding mode for out-of-bound coordinates.
    :param align_corners: Whether to align the corners of the grid.
    )mydelimiter")
        .def(py::init([](const std::string& mode,
                         const std::string& padding_mode,
                         bool align_corners) {
            return new GridSample_Op(stringToInterpolationMode(mode), stringToPaddingMode(padding_mode), align_corners);
        }), py::arg("mode") = "linear",
            py::arg("padding_mode") = "zeros",
            py::arg("align_corners") = false)
        .def_static("get_inputs_name", &GridSample_Op::getInputsName)
        .def_static("get_outputs_name", &GridSample_Op::getOutputsName)
        .def_readonly_static("Type", &GridSample_Op::Type)
        ;

  declare_registrable<GridSample_Op>(m, pyClassName);

  m.def("GridSample", [](const std::string& mode,
                        const std::string& padding_mode,
                        bool align_corners,
                        const std::string& name) {
        return GridSample(stringToInterpolationMode(mode), stringToPaddingMode(padding_mode), align_corners, name);
    }, py::arg("mode"),
       py::arg("padding_mode"),
       py::arg("align_corners"),
       py::arg("name") = "",
       R"mydelimiter(
    Creates a GridSample operation.

    :param mode: Interpolation mode to use for sampling.
    :param padding_mode: Padding mode for out-of-bound coordinates.
    :param align_corners: Whether to align the corners of the grid.
    :param name: Name of the node.
    )mydelimiter");
}


void init_GridSample(py::module &m) {
  declare_GridSampleOp(m);
}

} // namespace Aidge
