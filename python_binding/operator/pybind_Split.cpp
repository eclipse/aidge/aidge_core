/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <vector>

#include "aidge/operator/Split.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Split(py::module& m) {
    py::class_<Split_Op, std::shared_ptr<Split_Op>, OperatorTensor>(m, "SplitOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize a Split operator, which splits a tensor along a specified axis.

        :param nb_outputs: The number of outputs to split the input tensor into. Should be a positive integer.
        :type nb_outputs: int
        :param axis: The axis along which to split the input tensor. Must be in the range [-r, r-1] where r is the number of dimensions in the input tensor.
        :type axis: int
        :param split: A list of integers specifying how to split the input tensor along the specified axis.
                      The sum of the values in the list should be equal to the size of the input tensor along the split axis.
        :type split: List[int]
        )mydelimiter")
    .def(py::init<DimSize_t, std::int8_t, std::vector<DimSize_t>&>(),
            py::arg("nb_outputs"),
            py::arg("axis"),
            py::arg("split"))
    .def_static("get_inputs_name", &Split_Op::getInputsName)
    .def_static("get_outputs_name", &Split_Op::getOutputsName)
    .def_readonly_static("Type", &Split_Op::Type);

    declare_registrable<Split_Op>(m, "SplitOp");

    m.def("Split", 
          &Split, 
          py::arg("nb_outputs"), 
          py::arg("axis") = 0, 
          py::arg("split") = std::vector<DimSize_t>(), 
          py::arg("name") = "",
          R"mydelimiter(
          Initialize a node containing a Split operator that splits a tensor along a specified axis.

          The operator splits the input tensor along the specified axis. The number of splits is defined by `nb_outputs`, and the `split` argument specifies how to divide the input tensor.
          The `axis` argument defines the axis along which the split occurs.

          :param nb_outputs: The number of splits (outputs) from the input tensor. Must be a positive integer.
          :type nb_outputs: int
          :param axis: The axis along which to perform the split. Must be in the range [-r, r-1], where r is the number of dimensions in the input tensor.
          :type axis: int
          :param split: A list of integers indicating the size of each split along the specified axis. The sum of all values in the list must be equal to the size of the input tensor along the split axis.
          :type split: List[int]
          :param name: The name of the node (optional).
          :type name: str
          :return: A node containing the Split operator.
          :rtype: :py:class:`SplitOp`
          )mydelimiter");
}

}  // namespace Aidge
