#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <string>
#include <vector>
#include <array>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Types.h"
#include "aidge/utils/Registrar.hpp" // declare_registrable

namespace py = pybind11;
namespace Aidge {

template <DimIdx_t DIM> 
void declare_ConvOp(py::module &m) {
  const std::string pyClassName("Conv" + std::to_string(DIM) + "DOp");
  py::class_<Conv_Op<DIM>, std::shared_ptr<Conv_Op<DIM>>, OperatorTensor>(
    m, pyClassName.c_str(),
    py::multiple_inheritance(),
    R"mydelimiter(
    Initialize a Convolution operator.

    :param kernel_dims : The dimensions of the convolution kernel (filter size).
    :type kernel_dims : List[int]
    :param stride_dims : The stride size for the convolution.
    :type stride_dims : List[int]
    :param dilation_dims : The dilation size for the convolution.
    :type dilation_dims : List[int]
    )mydelimiter")
        .def(py::init([](const std::vector<DimSize_t>& kernel_dims,
                         const std::vector<DimSize_t> &stride_dims,
                         const std::vector<DimSize_t> &dilation_dims) {
            AIDGE_ASSERT(kernel_dims.size() == DIM, "kernel_dims size [{}] does not match DIM [{}]", kernel_dims.size(), DIM);
            AIDGE_ASSERT(stride_dims.size() == DIM, "stride_dims size [{}] does not match DIM [{}]", stride_dims.size(), DIM);
            AIDGE_ASSERT(dilation_dims.size() == DIM, "dilation_dims size [{}] does not match DIM [{}]", dilation_dims.size(), DIM);

            return new Conv_Op<DIM>(to_array<DIM>(kernel_dims.begin()), to_array<DIM>(stride_dims.begin()), to_array<DIM>(dilation_dims.begin()));
        }), py::arg("kernel_dims"),
            py::arg("stride_dims") = std::vector<DimSize_t>(DIM,1),
            py::arg("dilation_dims") = std::vector<DimSize_t>(DIM,1))
        .def_static("get_inputs_name", &Conv_Op<DIM>::getInputsName)
        .def_static("get_outputs_name", &Conv_Op<DIM>::getOutputsName)
        .def("in_channels", &Conv_Op<DIM>::inChannels)
        .def("out_channels", &Conv_Op<DIM>::outChannels)
        .def_readonly_static("Type", &Conv_Op<DIM>::Type)
        ;

  declare_registrable<Conv_Op<DIM>>(m, pyClassName);

  m.def(("Conv" + std::to_string(DIM) + "D").c_str(), [](DimSize_t in_channels,
                                                         DimSize_t out_channels,
                                                         const std::vector<DimSize_t>& kernel_dims,
                                                         const std::string& name,
                                                         const std::vector<DimSize_t> &stride_dims,
                                                         const std::vector<DimSize_t> &dilation_dims,
                                                         bool noBias) {
        AIDGE_ASSERT(kernel_dims.size() == DIM, "kernel_dims size [{}] does not match DIM [{}]", kernel_dims.size(), DIM);
        AIDGE_ASSERT(stride_dims.size() == DIM, "stride_dims size [{}] does not match DIM [{}]", stride_dims.size(), DIM);
        AIDGE_ASSERT(dilation_dims.size() == DIM, "dilation_dims size [{}] does not match DIM [{}]", dilation_dims.size(), DIM);

        return Conv<DIM>(in_channels, out_channels, to_array<DIM>(kernel_dims.begin()), name, to_array<DIM>(stride_dims.begin()), to_array<DIM>(dilation_dims.begin()), noBias);
    }, 
    py::arg("in_channels"),
    py::arg("out_channels"),
    py::arg("kernel_dims"),
    py::arg("name") = "",
    py::arg("stride_dims") = std::vector<DimSize_t>(DIM,1),
    py::arg("dilation_dims") = std::vector<DimSize_t>(DIM,1),
    py::arg("no_bias") = false,
    R"mydelimiter(
    Initialize a node containing a convolution operator.

    :param in_channels : The number of input channels (depth of the input tensor).
    :type in_channels : int
    :param out_channels : The number of output channels (depth of the output tensor).
    :type out_channels : int
    :param kernel_dims : The dimensions of the convolution kernel (filter size).
    :type kernel_dims : List[int]
    :param name : The name of the operator (optional).
    :type name : str
    :param stride_dims : The stride size for the convolution (default is [1]).
    :type stride_dims : List[int]
    :param dilation_dims : The dilation size for the convolution (default is [1]).
    :type dilation_dims : List[int]
    :param no_bias : Whether to disable bias (default is False).
    :type no_bias : bool
    :return : A new Convolution operator node.
    :rtype : :py:class:`ConvOp`
    )mydelimiter");
}


void init_Conv(py::module &m) {
  declare_ConvOp<1>(m);
  declare_ConvOp<2>(m);
//   declare_ConvOp<3>(m);
}

} // namespace Aidge
