/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <array>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/ArgMax.hpp"
#include "aidge/utils/Types.h"

namespace py = pybind11;
namespace Aidge {

void init_ArgMax(py::module &m) {
  const std::string pyClassName("ArgMaxOp");
  py::class_<ArgMax_Op, std::shared_ptr<ArgMax_Op>, OperatorTensor>(
    m, pyClassName.c_str(), py::multiple_inheritance(),
      R"mydelimiter(
		Initialize an ArgMax operator.
			:param axis: The axis along which to compute the max element. The accepted range is [-r, r-1], 
						where r is the rank of the input tensor.
			:type axis: int
			:param keepdims: If True (default), retains the reduced dimensions with size 1. If False, 
							the reduced dimensions are removed.
			:type keepdims: bool
			:param select_last_index: If True, selects the last index if there are multiple occurrences 
									of the max value. If False (default), selects the first occurrence.
			:type select_last_index: bool
		)mydelimiter")
    .def(py::init<std::int32_t, bool, bool>(), py::arg("axis"), py::arg("keep_dims"), py::arg("select_last_index"))
    .def_static("get_inputs_name", &ArgMax_Op::getInputsName)
    .def_static("get_outputs_name", &ArgMax_Op::getOutputsName)
    ;
  declare_registrable<ArgMax_Op>(m, pyClassName);

  m.def("ArgMax", [](std::int32_t axis,
                    bool keepDims,
                    bool selectLastIndex,
                    const std::string& name) {
        return ArgMax(axis, keepDims, selectLastIndex, name);
    }, py::arg("axis") = 0,
       py::arg("keep_dims") = true,
       py::arg("select_last_index") = false,
       py::arg("name") = "",
	   R"mydelimiter(
        Initialize a node containing an ArgMax operator.
			:param axis: The axis along which to compute the max element. The accepted range is [-r, r-1], 
						where r is the rank of the input tensor.
			:type axis: int
			:param keepdims: If True (default), retains the reduced dimensions with size 1. If False, 
							the reduced dimensions are removed.
			:type keepdims: bool
			:param select_last_index: If True, selects the last index if there are multiple occurrences 
									of the max value. If False (default), selects the first occurrence.
			:type select_last_index: bool
			:param name : name of the node.
		)mydelimiter");
}
} // namespace Aidge
