/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <string>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Erf.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Erf(py::module& m) {
    py::class_<Erf_Op, std::shared_ptr<Erf_Op>, OperatorTensor>(m, "ErfOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize an Erf operator, which computes the error function (erf) element-wise.
        The error function (erf) is defined as:
            erf(x) = (2 / sqrt(pi)) * integral from 0 to x of exp(-t^2) dt
        )mydelimiter")
        .def(py::init<>())
        .def_static("get_inputs_name", &Erf_Op::getInputsName)
        .def_static("get_outputs_name", &Erf_Op::getOutputsName)
        .def_readonly_static("Type", &Erf_Op::Type);

    declare_registrable<Erf_Op>(m, "ErfOp");

    m.def("Erf", &Erf, py::arg("name") = "",
        R"mydelimiter(
        Initialize a node containing an Erf operator that computes the error function (erf) element-wise.
        The error function (erf) is computed element-wise as follows:
            erf(x) = (2 / sqrt(pi)) * integral from 0 to x of exp(-t^2) dt
        :param name : name of the node (optional).
        :type name : str
        :return : A node containing the Erf operator.
        :rtype : :py:class:`ErfOp`
        )mydelimiter");
}

}  // namespace Aidge
