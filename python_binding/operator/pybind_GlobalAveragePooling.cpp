/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/operator/GlobalAveragePooling.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Attributes.hpp"

namespace py = pybind11;
namespace Aidge {

const std::string pyClassName("GlobalAveragePoolingOp");

void init_GlobalAveragePooling(py::module &m) {
  py::class_<GlobalAveragePooling_Op, std::shared_ptr<GlobalAveragePooling_Op>,
             OperatorTensor>(m, pyClassName.c_str(),
                             py::multiple_inheritance(),
                             R"mydelimiter(
                             Initialize a Global Average Pooling operator.
                             
                             This operation performs global average pooling on an input tensor, where the input
                             tensor is reduced across its spatial dimensions (height and width) to a single value
                             per channel.
                             
                             :param name : Name of the node (optional).
                             :type name : str
                             )mydelimiter")
      .def(py::init<>())
      .def_static("get_inputs_name", &GlobalAveragePooling_Op::getInputsName)
      .def_static("get_outputs_name", &GlobalAveragePooling_Op::getOutputsName)
      .def_readonly_static("Type", &GlobalAveragePooling_Op::Type);

  declare_registrable<GlobalAveragePooling_Op>(m, pyClassName);

  m.def("globalaveragepooling", &GlobalAveragePooling, py::arg("name") = "",
        R"mydelimiter(
        Initialize a node containing a Global Average Pooling operator.
        
        This operation performs global average pooling on the input tensor. The result is a tensor where
        each channel of the input tensor is reduced to a single value by averaging all the elements in 
        that channel.

        :param name : Name of the node (optional).
        :type name : str
        :return : A node containing the Global Average Pooling operator.
        :rtype : :py:class:`GlobalAveragePoolingOp`
        )mydelimiter");
}

} // namespace Aidge
