/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <string>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Gather.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Gather(py::module& m) {
    py::class_<Gather_Op, std::shared_ptr<Gather_Op>, OperatorTensor>(m, "GatherOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize a Gather operator, which extracts elements from a tensor at specified indices along a given axis.
        
        This operation selects values along the specified axis based on the provided indices, which can be 
        a 1D or multidimensional tensor. The resulting tensor will have the same shape as the input tensor 
        except along the given axis, where the size will be determined by the indices.
        
        :param axis : Axis along which to gather the elements.
        :type axis : int
        :param indices : Indices to gather along the axis.
        :type indices : :py:class:`List[int]`
        :param gathered_shape : Shape of the gathered result.
        :type gathered_shape : :py:class:`List[int]`
        )mydelimiter")
        .def(py::init<std::int8_t,
                      const std::vector<int64_t>,
                      const std::vector<DimSize_t>>(),
                py::arg("axis"),
                py::arg("indices"),
                py::arg("gathered_shape"))
        .def_static("get_inputs_name", &Gather_Op::getInputsName)
        .def_static("get_outputs_name", &Gather_Op::getOutputsName)
        .def_readonly_static("Type", &Gather_Op::Type);

    declare_registrable<Gather_Op>(m, "GatherOp");

    m.def("Gather", &Gather, 
          py::arg("axis") = 0,
          py::arg("indices") = std::vector<std::int64_t>(),
          py::arg("gathered_shape") = std::vector<std::size_t>(),
          py::arg("name") = "",
          R"mydelimiter(
          Initialize a node containing a Gather operator that extracts elements from a tensor along a specified axis.
          
          This operation selects values along the specified axis using the provided indices. The resulting tensor
          will have the same shape as the input tensor except along the given axis, where the size will be determined
          by the indices.

          :param axis : Axis along which to gather the elements (default is 0).
          :type axis : int
          :param indices : Indices to gather along the axis.
          :type indices : :py:class:`List[int]`
          :param gathered_shape : Shape of the gathered result.
          :type gathered_shape : :py:class:`List[int]`
          :param name : Name of the node (optional).
          :type name : str
          :return : A node containing the Gather operator.
          :rtype : :py:class:`GatherOp`
          )mydelimiter");
}

}  // namespace Aidge
