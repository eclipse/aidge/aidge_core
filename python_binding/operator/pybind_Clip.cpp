/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Clip.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/utils/Types.h"

namespace py = pybind11;
namespace Aidge {

void init_Clip(py::module& m) {
    py::class_<Clip_Op, std::shared_ptr<Clip_Op>, OperatorTensor>(m, "ClipOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize a Clip operator.

        :param min : Minimum clipping value. Default is the lowest possible float value.
        :type min : :py:class:`float`
        :param max : Maximum clipping value. Default is the highest possible float value.
        :type max : :py:class:`float`
        )mydelimiter")
    .def(py::init<float, float>(), py::arg("min") = std::numeric_limits<float>::lowest(), py::arg("max") = std::numeric_limits<float>::max())
    .def_static("get_inputs_name", &Clip_Op::getInputsName)
    .def_static("get_outputs_name", &Clip_Op::getOutputsName)
    .def("min", &Clip_Op::min, py::return_value_policy::reference_internal)
    .def("max", &Clip_Op::max, py::return_value_policy::reference_internal);

    declare_registrable<Clip_Op>(m, "ClipOp");

    m.def("Clip", &Clip, py::arg("name") = "",
        py::arg("min") = std::numeric_limits<float>::lowest(),
        py::arg("max") = std::numeric_limits<float>::max(),
        R"mydelimiter(
        ClipOp is a tensor operator that performs a clipping operation on tensor elements.
        This class allows limiting tensor values to a specified range, defined by the `min` 
        and `max` parameters. Values outside this range are replaced by the corresponding 
        limit values. When `min` is greater than `max`, the clip operator sets all the 'input' values to the value of `max`.

        :param min: Minimum clipping value.
        :type min: :py:class:`float`
        :param max: Maximum clipping value.
        :type max: :py:class:`float`
        :param name: Name of the node.
        :type name: :py:class:`str`
        )mydelimiter");
}

}  // namespace Aidge
