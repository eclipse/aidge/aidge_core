/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Scaling.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;

namespace Aidge {

void init_Scaling(py::module& m) {
    py::class_<Scaling_Op, std::shared_ptr<Scaling_Op>, OperatorTensor>(
        m, "ScalingOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize a Scaling operator for element-wise tensor scaling.

        This operator scales tensor elements by a specified scaling factor, 
        optionally constraining the output to a specified bit-width and signedness.

        :param scaling_factor: The scaling factor to apply to tensor elements.
        :type scaling_factor: float
        :param nb_bits: The number of bits for quantization of the output. Must be a positive integer.
        :type nb_bits: int
        :param is_output_unsigned: Specifies whether the output should be unsigned (True) or signed (False).
        :type is_output_unsigned: bool
        )mydelimiter")
        .def(py::init<float, size_t, bool>(),
             py::arg("scaling_factor"),
             py::arg("nb_bits"),
             py::arg("is_output_unsigned"))
        .def_static("get_inputs_name", &Scaling_Op::getInputsName)
        .def_static("get_outputs_name", &Scaling_Op::getOutputsName)
        .def_readonly_static("Type", &Scaling_Op::Type);

    declare_registrable<Scaling_Op>(m, "ScalingOp");

    m.def("Scaling", &Scaling,
          py::arg("scaling_factor") = 1.0f,
          py::arg("nb_bits") = 8,
          py::arg("is_output_unsigned") = true,
          py::arg("name") = "",
          R"mydelimiter(
          Initialize a node containing a Scaling operator to scale tensor elements.

          This operator applies a scaling factor to each element of the input tensor. The result 
          can optionally be quantized to a specific bit-width and constrained to unsigned or signed output.

          :param scaling_factor: The factor by which to scale the tensor elements. Default is 1.0.
          :type scaling_factor: float
          :param nb_bits: The number of bits for quantized output. Default is 8.
          :type nb_bits: int
          :param is_output_unsigned: Indicates whether the output tensor values should be unsigned. Default is True.
          :type is_output_unsigned: bool
          :param name: The name of the node (optional).
          :type name: str
          :return: A node containing the Scaling operator.
          :rtype: :py:class:`ScalingOp`
          )mydelimiter");
}

}  // namespace Aidge
