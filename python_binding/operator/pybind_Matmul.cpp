/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/MatMul.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Types.h"

namespace py = pybind11;
namespace Aidge {

void init_MatMul(py::module &m) {
  py::class_<MatMul_Op, std::shared_ptr<MatMul_Op>, OperatorTensor>(
    m, "MatMulOp", py::multiple_inheritance(),
    R"mydelimiter(
    Initialize an MatMul operator.
    This operator performs Matrix Multiplication between two input tensors.
    The operation is defined as:
        Output = Input1 @ Input2
    This operator implements generalized matrix multiplication, supporting batched 
    matrix multiplication and broadcasting rules consistent with Numpy.

    :param name: Optional name of the operator.
    :type name: str
    )mydelimiter")
    .def(py::init<>())
    .def_static("get_inputs_name", &MatMul_Op::getInputsName)
    .def_static("get_outputs_name", &MatMul_Op::getOutputsName)
    .def_readonly_static("Type", &MatMul_Op::Type);

  declare_registrable<MatMul_Op>(m, "MatMulOp");

  m.def("MatMul", &MatMul, py::arg("name") = "",
        R"mydelimiter(
        Initialize a node containing an MatMul operator that performs Matrix Multiplication between two tensors.
        The operation is defined as:
          Output = Input1 @ Input2

        This operator implements generalized matrix multiplication, supporting batched 
        matrix multiplication and broadcasting rules consistent with Numpy.
        Example:
          Input A: (M, K), Input B: (K, N) -> Output: (M, N)
          Input A: (batch_size, M, K), Input B: (K, N) -> Output: (batch_size, M, N)
        :param name: Optional name of the node.
        :type name: str
        :return: A node containing the MatMul operator.
        :rtype: :py:class:`MatMulOp`

        )mydelimiter");
}

} // namespace Aidge
