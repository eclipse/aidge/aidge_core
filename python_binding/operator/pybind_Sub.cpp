/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Sub.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Sub(py::module& m) {
    py::class_<Sub_Op, std::shared_ptr<Sub_Op>, OperatorTensor>(m, "SubOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize a Subtraction operator.
        This operator performs element-wise subtraction between two input tensors. The operation is defined as:
            Output = Input1 - Input2
        The output tensor shape is determined by taking the maximum size along each dimension of the input tensors after broadcasting.
        Examples:
            Input A: (3, 4, 2), Input B: (2), Output: (3, 4, 2)
            Input A: (1, 5, 3), Input B: (2, 1, 3), Output: (2, 5, 3)

        :param name : Name of the node (optional).
        :type name : str
        )mydelimiter")
    .def(py::init<>())
    .def_static("get_inputs_name", &Sub_Op::getInputsName)
    .def_static("get_outputs_name", &Sub_Op::getOutputsName)
    .def_readonly_static("Type", &Sub_Op::Type);
    declare_registrable<Sub_Op>(m, "SubOp");
    m.def("Sub", &Sub, py::arg("name") = "",
        R"mydelimiter(
        Initialize a node containing a Subtraction operator that performs element-wise subtraction between two tensors.
        The operation is defined as:
            Output = Input1 - Input2
        The output tensor shape is determined by taking the maximum size along each dimension of the input tensors after broadcasting.
        Examples:
            Input A: (3, 4, 2), Input B: (2), Output: (3, 4, 2)
            Input A: (1, 5, 3), Input B: (2, 1, 3), Output: (2, 5, 3)

        :param name : Name of the node (optional).
        :type name : str
        :return: A node containing the Sub operator.
        :rtype: :py:class:`SubOp`
        )mydelimiter");
}

}  // namespace Aidge

