/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Stack.hpp"

namespace py = pybind11;
namespace Aidge {
void init_Stack(py::module &m) {
    py::class_<StackOp, std::shared_ptr<StackOp>, OperatorTensor>(
        m,
        "StackOp",
        py::multiple_inheritance(),
        R"mydelimiter(Initialize a Stack operator.)mydelimiter")
        .def(py::init<const std::uint32_t>(), py::arg("max_elements"))
        .def_static("get_inputs_name", &StackOp::getInputsName)
        .def_static("get_outputs_name", &StackOp::getOutputsName)
        .def_readonly_static("Type", &StackOp::s_type);

    m.def("Stack",
          &Stack,
          py::arg("max_elements") = 0,
          py::arg("name") = "",
          R"mydelimiter(
        Initialize a node containing a Stack operator.
            :param max_elements : the maximum number of tensors to be stacked.
			:param name: name of the node.
		)mydelimiter");
}
} // namespace Aidge
