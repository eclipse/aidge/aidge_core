/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>

#include <pybind11/pybind11.h>

#include "aidge/operator/Add.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Types.h"

namespace py = pybind11;
namespace Aidge {

void declare_Add(py::module &m) {
  py::class_<Add_Op, std::shared_ptr<Add_Op>, OperatorTensor>(m, "AddOp", py::multiple_inheritance(),
    R"mydelimiter(
    Initialize an Add operator.
    This operator performs element-wise addition between two input tensors.
    The operation is defined as:
        Output = Input1 + Input2
    The output tensor shape is determined by taking the maximum size along each dimension of the input tensors after broadcasting.
    Examples:
        Input A: (3, 4, 2), Input B: (2), Output: (3, 4, 2)
        Input A: (1, 5, 3), Input B: (2, 1, 3), Output: (2, 5, 3)
    :param name : Name of the node (optional).
    :type name : str
    )mydelimiter")
    .def(py::init<>())
    .def_static("get_inputs_name", &Add_Op::getInputsName)
    .def_static("get_outputs_name", &Add_Op::getOutputsName)
    .def_readonly_static("Type", &Add_Op::Type);

  declare_registrable<Add_Op>(m, "AddOp");

  m.def("Add", &Add, py::arg("name") = "",
    R"mydelimiter(
    Initialize a node containing an Add operator that performs element-wise addition between two tensors.
    The operation is defined as:
        Output = Input1 + Input2
    The output tensor shape is determined by taking the maximum size along each dimension of the input tensors after broadcasting.
    Examples:
        Input A: (3, 4, 2), Input B: (2), Output: (3, 4, 2)
        Input A: (1, 5, 3), Input B: (2, 1, 3), Output: (2, 5, 3)

    :param name : Name of the node (optional).
    :type name : str
    :return: A node containing the Add operator.
    :rtype: :py:class:`AddOp`
    )mydelimiter");
}

void init_Add(py::module &m) {
  declare_Add(m);
}

} // namespace Aidge
