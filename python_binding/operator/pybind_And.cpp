/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/And.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_And(py::module& m) {
    py::class_<And_Op, std::shared_ptr<And_Op>, OperatorTensor>(m, "AndOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize an And operator.
        This operator performs element-wise logical AND between two input tensors. The operation is defined as:
            Output = Input1 AND Input2
        The inputs must be boolean tensors (with values 0 or 1).

        :param name : Name of the node (optional).
        :type name : str
        )mydelimiter")
    .def(py::init<>())
    .def_static("get_inputs_name", &And_Op::getInputsName)
    .def_static("get_outputs_name", &And_Op::getOutputsName);

    declare_registrable<And_Op>(m, "AndOp");

    m.def("And", &And, py::arg("name") = "",
        R"mydelimiter(
        Initialize a node containing an And operator that performs element-wise logical AND between two tensors.
        The operation is defined as:
            Output = Input1 AND Input2

        The inputs must be boolean tensors (with values 0 or 1).

        :param name : Name of the node (optional).
        :type name : str
        :return: A node containing the And operator.
        :rtype: :py:class:`AndOp`
        )mydelimiter");
}

}  // namespace Aidge

