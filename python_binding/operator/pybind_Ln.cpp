/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Ln.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Ln(py::module& m) {
    py::class_<Ln_Op, std::shared_ptr<Ln_Op>, OperatorTensor>(m, "LnOp", py::multiple_inheritance(),
    R"mydelimiter(
    A class representing the natural logarithm operator (Ln).

    The operator computes the element-wise natural logarithm of the input tensor.
    )mydelimiter")
    .def(py::init<>())
    .def_static("get_inputs_name", &Ln_Op::getInputsName)
    .def_static("get_outputs_name", &Ln_Op::getOutputsName)
    .def_readonly_static("Type", &Ln_Op::Type);

    m.def("Ln", &Ln, py::arg("name") = "",
    R"mydelimiter(
    Create a node with the natural logarithm operator.

    :param name: The name of the node.
    )mydelimiter");
}

}  // namespace Aidge
