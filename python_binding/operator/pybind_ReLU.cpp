/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/ReLU.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_ReLU(py::module& m) {
    py::class_<ReLU_Op, std::shared_ptr<ReLU_Op>, OperatorTensor>(m, "ReLUOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize a ReLU (Rectified Linear Unit) operator.
        The ReLU function is applied element-wise to the input tensor.
        It is defined as:
            ReLU(x) = max(0, x)
        This operator sets all negative values in the tensor to zero, while leaving positive values unchanged.
        :param name : name of the node (optional).
        :type name : str
        )mydelimiter")
        .def(py::init<>())
        .def_static("get_inputs_name", &ReLU_Op::getInputsName)
        .def_static("get_outputs_name", &ReLU_Op::getOutputsName)
        .def_readonly_static("Type", &ReLU_Op::Type);

    declare_registrable<ReLU_Op>(m, "ReLUOp");

    m.def("ReLU", &ReLU, py::arg("name") = "",
        R"mydelimiter(
        Initialize a node containing a ReLU operator that applies the ReLU function element-wise.

        The ReLU function is applied element-wise and is defined as:
            ReLU(x) = max(0, x)

        The operation sets all negative values to zero and leaves positive values unchanged.

        :param name: Name of the node (optional).
        :type name: str
        :return: A node containing the ReLU operator.
        :rtype: :py:class:`ReLUOp`
        )mydelimiter");
}

}  // namespace Aidge
