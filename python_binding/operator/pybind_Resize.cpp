/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cstddef>  // std::size_t

#include <pybind11/pybind11.h>

#include "aidge/data/Interpolation.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Pad.hpp"
#include "aidge/operator/Resize.hpp"
#include "aidge/utils/Registrar.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Resize(py::module &m) {
  py::class_<Resize_Op, std::shared_ptr<Resize_Op>, OperatorTensor>(
          m, "ResizeOp", py::multiple_inheritance())
          .def(py::init<Interpolation::CoordinateTransformation, Interpolation::Mode, float, PadBorderType>(), py::arg("coordinate_transformation_mode"), py::arg("interpolation_mode"), py::arg("cubic_coeff_a") = -0.75f, py::arg("padding_mode") = PadBorderType::Edge)
          .def_static("get_inputs_name", &Resize_Op::getInputsName)
          .def_static("get_outputs_name", &Resize_Op::getOutputsName)
          .def_readonly_static("Type", &Resize_Op::Type);

  declare_registrable<Resize_Op>(m, "ResizeOp");

  m.def("Resize", &Resize,
        py::arg("scale") = std::vector<float>({}),
        py::arg("size") = std::vector<std::size_t>({}),
        py::arg("coord_transfo_mode") =
            Interpolation::CoordinateTransformation::HalfPixel,
        py::arg("interpolation_mode") =
            Interpolation::Mode::RoundPreferFloor,
        py::arg("cubic_interpolation_coefficient_a") = -.75f,
        py::arg("name") = "", R"mydelimiter(
    Initialize a node containing a Resize operator.
    This node can take 4 different inputs.
    #0 Input to resize
    #1 ROI NOT SUPPORTED (optional) - Tensor(double|float|float16)
    #2 scales (optional) - tensor(float): #3 sizes - tensor(int64)
    #3 sizes - tensor(int64)
	:type coordinate_transformation_mode : :py:class: List[Int]
	:param interpolationMode : Type of interpolation used in case of upsampling
	:type interpolationMode : Interpolation::Mode
	:param cubic_coeff_a : "A" coefficient of cubic interpolation. Only used if interpolation_mode = Interpolation::Mode::Cubic
	:type cubic_coeff_a  : float
    :param name : name of the node.
    :type name : str
    )mydelimiter");
}
} // namespace Aidge
