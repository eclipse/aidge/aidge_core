/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Div.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Div(py::module& m) {
    py::class_<Div_Op, std::shared_ptr<Div_Op>, OperatorTensor>(m, "DivOp", py::multiple_inheritance(),
    R"mydelimiter(
    Initialize a Div operator.
    This operator performs element-wise division between two input tensors.
    The operation is defined as:
        Output = Input1 / Input2
    The output tensor shape is determined by taking the maximum size along each dimension of the input tensors after broadcasting.
    Examples:
        Input A: (3, 4, 2), Input B: (2), Output: (3, 4, 2)
        Input A: (1, 5, 3), Input B: (2, 1, 3), Output: (2, 5, 3)
    :param name : Name of the node (optional).
    :type name : str
    )mydelimiter")
        .def(py::init<>())
        .def_static("get_inputs_name", &Div_Op::getInputsName)
        .def_static("get_outputs_name", &Div_Op::getOutputsName)
        .def_readonly_static("Type", &Div_Op::Type);

    declare_registrable<Div_Op>(m, "DivOp");

    m.def("Div", &Div, py::arg("name") = "",
    R"mydelimiter(
    Initialize a node containing a Div operator that performs element-wise division between two tensors.
    The operation is defined as:
        Output = Input1 / Input2
    The output tensor shape is determined by taking the maximum size along each dimension of the input tensors after broadcasting.
    Examples:
        Input A: (3, 4, 2), Input B: (2), Output: (3, 4, 2)
        Input A: (1, 5, 3), Input B: (2, 1, 3), Output: (2, 5, 3)

    :param name : Name of the node (optional).
    :type name : str
    :return: A node containing the Div operator.
    :rtype: :py:class:`DivOp`
    )mydelimiter");
}

}  // namespace Aidge
