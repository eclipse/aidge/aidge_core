/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/operator/Heaviside.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;

namespace Aidge {

void init_Heaviside(py::module &m) {
    py::class_<Heaviside_Op, std::shared_ptr<Heaviside_Op>, OperatorTensor>(
        m,
        "HeavisideOp",
        py::multiple_inheritance(),
         R"mydelimiter(
          Initialize an Heaviside node. This node will compute a heaviside step function 
          on each element of the input tensor.
          heaviside(input, values) = { 0  if input < 0 
                                     { values if input == 0
                                     { 1 if input > 0

          :param value : The value use for the output tensor when input is 0.
          :type value : float
          :param name : Name of the node.
          )mydelimiter")
        .def(py::init<float>(), py::arg("value"))
        .def_static("get_inputs_name", &Heaviside_Op::getInputsName)
        .def_static("get_outputs_name", &Heaviside_Op::getOutputsName)
        .def_readonly_static("Type", &Heaviside_Op::Type);

    declare_registrable<Heaviside_Op>(m, "HeavisideOp");
    m.def("Heaviside", &Heaviside, py::arg("value"), py::arg("name") = "",
            R"mydelimiter(
          Initialize an Heaviside node. This node will compute a heaviside step function 
          on each element of the input tensor.
          heaviside(input, values) = { 0  if input < 0 
                                     { values if input == 0
                                     { 1 if input > 0

          :param value : The value use for the output tensor when input is 0.
          :type value : float
          :param name : Name of the node.
          )mydelimiter");
}
} // namespace Aidge
