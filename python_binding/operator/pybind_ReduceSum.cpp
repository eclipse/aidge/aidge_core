/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <array>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/ReduceSum.hpp"
#include "aidge/utils/Types.h"

namespace py = pybind11;
namespace Aidge {

void init_ReduceSum(py::module &m) {
  const std::string pyClassName("ReduceSumOp");
  py::class_<ReduceSum_Op, std::shared_ptr<ReduceSum_Op>, OperatorTensor>(
    m, pyClassName.c_str(), py::multiple_inheritance(),
      R"mydelimiter(
		Initialize a ReduceMean operator.
			:param axes: Axes along which to do the reduction. The accepted range is [-r, r-1], 
						where r is the rank of the input tensor.
			:type axes: List[int]
			:param keepdims: If True (default), retains the reduced dimensions with size 1. If False, 
							the reduced dimensions are removed.
			:type keepdims: bool
			:param noop_with_empty_axes: If True, the operator just copies the input, 
      if False, the operatpr reduces all the dimensions.
			:type noop_with_empty_axes: bool
		)mydelimiter")
    .def(py::init<std::vector<std::int32_t>, bool, bool>(), py::arg("axes"), py::arg("keep_dims"), py::arg("noop_with_empty_axes"))
    .def_static("get_inputs_name", &ReduceSum_Op::getInputsName)
    .def_static("get_outputs_name", &ReduceSum_Op::getOutputsName)
    ;
  declare_registrable<ReduceSum_Op>(m, pyClassName);

  m.def("ReduceSum", [](const std::vector<int>& axes,
                        bool keepDims,
                        bool noopWithEmptyAxes,
                        const std::string& name) {
        return ReduceSum(axes, keepDims, noopWithEmptyAxes, name);
    }, py::arg("axes") = std::vector<std::int32_t>(),
       py::arg("keep_dims") = true,
       py::arg("noop_with_empty_axes") = false,
       py::arg("name") = "",
	   R"mydelimiter(
        Initialize a node containing a ReduceMean operator.
			:param axes: Axes along which to do the reduction. The accepted range is [-r, r-1], 
						where r is the rank of the input tensor.
			:type axes: List[int]
			:param keepdims: If True (default), retains the reduced dimensions with size 1. If False, 
							the reduced dimensions are removed.
			:type keepdims: bool
			:param noop_with_empty_axes: If True, the operator just copies the input, 
      if False, the operatpr reduces all the dimensions.
			:type noop_with_empty_axes: bool
			:param name : name of the node.
		)mydelimiter");
}
} // namespace Aidge
