/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <string>
#include <vector>

#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Reshape.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Reshape(py::module& m) {
    py::class_<Reshape_Op, std::shared_ptr<Reshape_Op>, OperatorTensor>(m, "ReshapeOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize a Reshape operator that reshapes the input tensor to a specified shape.

        :param shape: The target shape to reshape the tensor to. It should be a list of integers, 
                      where values are between [-r; r-1], with r = input_tensor.nbDims(), 
                      representing the dimensions of the input tensor. 
        :type shape: List[int]
        :param allowzero: If True, zero-size dimensions are allowed. If False, an error is raised 
                           if the reshaped tensor has a zero-size dimension.
        :type allowzero: bool
        )mydelimiter")
    .def(py::init<const std::vector<std::int64_t>&, bool>(), py::arg("shape"), py::arg("allowzero"))
    .def_static("get_inputs_name", &Reshape_Op::getInputsName)
    .def_static("get_outputs_name", &Reshape_Op::getOutputsName)
    .def_readonly_static("Type", &Reshape_Op::Type);

    declare_registrable<Reshape_Op>(m, "ReshapeOp");

    m.def("Reshape", &Reshape, py::arg("shape") = std::vector<std::int64_t>(), py::arg("allowzero") = false, py::arg("name") = "",
        R"mydelimiter(
        Initialize a node containing a Reshape operator.

        This operator reshapes the input tensor to the specified shape. The shape should be provided as a list of 
        integers, where values are between [-r; r-1], with r = input_tensor.nbDims(), 
        representing the dimensions of the input tensor. The operator also has a flag for allowing zero-size dimensions.

        :param shape: The target shape to reshape the tensor to.
        :type shape: List[int]
        :param allowzero: Whether to allow zero-size dimensions.
        :type allowzero: bool
        :param name: Name of the node (optional).
        :type name: str
        :return: A node containing the Reshape operator.
        :rtype: :py:class:`ReshapeOp`
        )mydelimiter");
}

}  // namespace Aidge
