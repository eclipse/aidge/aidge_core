
/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>
#include <string>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/operator/Operator.hpp"
#include "aidge/utils/Types.h"

namespace py = pybind11;
namespace Aidge {
void init_Operator(py::module& m){
    py::enum_<OperatorType>(m, "OperatorType")
        .value("Data", OperatorType::Data)
        .value("Tensor", OperatorType::Tensor);

    py::enum_<InputCategory>(m, "InputCategory")
        .value("Data", InputCategory::Data)
        .value("Param", InputCategory::Param)
        .value("OptionalData", InputCategory::OptionalData)
        .value("OptionalParam", InputCategory::OptionalParam);

    py::class_<Operator, std::shared_ptr<Operator>>(m, "Operator")
    .def("__repr__", &Operator::repr)
    .def("backend", &Operator::backend)
    .def("clone", &Operator::clone)
    .def("set_output", py::overload_cast<const IOIndex_t, const std::shared_ptr<Data>&>(&Operator::setOutput, py::const_), py::arg("outputIdx"), py::arg("data"))
    .def("set_input", py::overload_cast<const IOIndex_t, const std::shared_ptr<Data>&>(&Operator::setInput), py::arg("inputIdx"), py::arg("data"))
    .def("get_raw_output", &Operator::getRawOutput, py::arg("outputIdx"))
    .def("set_input", py::overload_cast<const IOIndex_t, const std::shared_ptr<Data>&>(&Operator::setInput), py::arg("inputIdx"), py::arg("data"))
    .def("get_raw_input", &Operator::getRawInput, py::arg("inputIdx"))
    .def("nb_inputs", &Operator::nbInputs)
    .def("nb_outputs", &Operator::nbOutputs)
    .def("input_category", static_cast<std::vector<InputCategory>(Operator::*)() const>(&Operator::inputCategory),
    R"mydelimiter(
    Category of the inputs (Data or Param, optional or not).
    Data inputs exclude inputs expecting parameters (weights or bias).

    :rtype: list(InputCategory)
    )mydelimiter")
    .def("input_category", static_cast<InputCategory(Operator::*)(IOIndex_t) const>(&Operator::inputCategory), py::arg("idx"),
    R"mydelimiter(
    Category of a specific input (Data or Param, optional or not).
    Data inputs exclude inputs expecting parameters (weights or bias).

    :rtype: InputCategory
    )mydelimiter")
    .def("is_optional_input", &Operator::isOptionalInput, py::arg("inputIdx"))
    .def("associate_input", &Operator::associateInput, py::arg("inputIdx"), py::arg("data"))
    .def("set_datatype", &Operator::setDataType, py::arg("dataType"))
    .def("set_dataformat", &Operator::setDataFormat, py::arg("dataFormat"))
    .def("set_backend", py::overload_cast<const std::string&, DeviceIdx_t>(&Operator::setBackend), py::arg("name"), py::arg("device") = 0)
    .def("set_backend", py::overload_cast<const std::vector<std::pair<std::string, DeviceIdx_t>>&>(&Operator::setBackend), py::arg("backends"))
    .def("forward", &Operator::forward)
    // py::keep_alive forbide Python to garbage collect the implementation lambda as long as the Operator is not deleted !
    .def("set_impl", &Operator::setImpl, py::arg("implementation"), py::keep_alive<1, 2>())
    .def("type", &Operator::type)
    .def("get_impl", &Operator::getImpl)
    .def_property_readonly("attr", &Operator::attributes)
    .def("set_back_edges", &Operator::setBackEdges, py::arg("input_indexes"))
    .def("is_back_edge", &Operator::isBackEdge, py::arg("input_index"))
    .def("is_atomic", &Operator::isAtomic)
    ;
}
}
