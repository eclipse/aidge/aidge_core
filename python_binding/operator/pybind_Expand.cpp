/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/operator/Expand.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

const std::string pyClassName("ExpandOp");
void init_Expand(py::module &m) {
    py::class_<Expand_Op, std::shared_ptr<Expand_Op>, OperatorTensor>(
        m,
        pyClassName.c_str(),
        py::multiple_inheritance(),
        R"mydelimiter(
      Operator that broadcasts an input tensor to a larger provided
      shape.

      This operator takes an input tensor and expands (broadcasts) it
      to a target shape while following ONNX broadcasting semantics.
      Broadcasting allows a tensor to be repeated along dimensions to match
      the desired output dimensions.
     
      input#0 : Tensor to be broadcasted
      input#1 : Expand shape
     
       EXAMPLE:
         input#0 = [[[[2, 1, 3]]]] => dims = [1, 1, 3, 1]
         input#1 = [2, 1, 1]       => converted to [1,2,1,1]
         output = [[[[2, 1, 3], [2, 1, 3]]]] => dims = [1, 2, 3, 1]
     
      SEE: https://onnx.ai/onnx/repo-docs/Broadcasting.html for detailed ONNX
      broadcasting rules
)mydelimiter")
        .def(py::init<>())
        .def_static("get_inputs_name", &Expand_Op::getInputsName)
        .def_static("get_outputs_name", &Expand_Op::getOutputsName)
        .def_readonly_static("Type", &Expand_Op::Type);

    declare_registrable<Expand_Op>(m, pyClassName);

    m.def("expand",
          &Expand,
          py::arg("name") = "",
          R"mydelimiter(
    Initialize a node containing an expand operator.
    This operator will broadcast values given via input#0 to a shape given via input#1's values
    If one of the inputs has less dimensions than the other, dimension will be appended 1's to the left.

    Example : 
            input#0 = [[[[2, 1, 3]]]] => dims = [1, 1, 3, 1]
            input#1 = [2, 1, 1]       => converted to [1,2,1,1]
            output = [[[[2, 1, 3], [2, 1, 3]]]] => dims = [1, 2, 3, 1]

    See https://onnx.ai/onnx/repo-docs/Broadcasting.html for detailed ONNX broadcasting rules

    :param name : name of the node.
	:type name : str 
)mydelimiter");
}

} // namespace Aidge
