/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/operator/Sqrt.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Sqrt(py::module& m) {
    py::class_<Sqrt_Op, std::shared_ptr<Sqrt_Op>, OperatorTensor>(m, "SqrtOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize a Square Root operator.
        This operator computes the square root of each element in the input tensor. The input values must be non-negative.
        )mydelimiter")
    .def(py::init<>())
    .def_static("get_inputs_name", &Sqrt_Op::getInputsName)
    .def_static("get_outputs_name", &Sqrt_Op::getOutputsName)
    .def_readonly_static("Type", &Sqrt_Op::Type);

    declare_registrable<Sqrt_Op>(m, "SqrtOp");

    m.def("Sqrt", 
          &Sqrt, 
          py::arg("name") = "",
          R"mydelimiter(
          Initialize a node containing a Square Root operator that computes the element-wise square root of the input tensor.
          The input tensor values must be non-negative for the square root to be computed.

          :param name : The name of the node (optional).
          :type name : str
          :return : A node containing the Sqrt operator.
          :rtype : :py:class:`SqrtOp`
          )mydelimiter");
}

}  // namespace Aidge
