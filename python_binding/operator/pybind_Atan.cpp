/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Atan.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Atan(py::module& m) {
    py::class_<Atan_Op, std::shared_ptr<Atan_Op>, OperatorTensor>(m, "AtanOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize an Atan operator.

        :param type : The type of the Atan operation.
        :type type : :py:class:`str`
        )mydelimiter")
    .def(py::init<>())
    .def_static("get_inputs_name", &Atan_Op::getInputsName)
    .def_static("get_outputs_name", &Atan_Op::getOutputsName);

    declare_registrable<Atan_Op>(m, "AtanOp");

    m.def("Atan", &Atan, py::arg("name") = "",
        R"mydelimiter(
        Initialize a node containing an Atan operator.

        :param name : Name of the node.
        :type name : :py:class:`str`
        )mydelimiter");
}

}  // namespace Aidge

