/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <string>
#include <vector>

#include "aidge/operator/Memorize.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Memorize(py::module& m) {
    py::class_<Memorize_Op, std::shared_ptr<Memorize_Op>, OperatorTensor>(m, "MemorizeOp", py::multiple_inheritance())
        .def(py::init<const std::uint32_t>(), py::arg("end_step"))
        .def_static("get_inputs_name", &Memorize_Op::getInputsName)
        .def_static("get_outputs_name", &Memorize_Op::getOutputsName);

    declare_registrable<Memorize_Op>(m, "MemorizeOp");

    m.def("Memorize", &Memorize, py::arg("end_step"), py::arg("name") = "");
}

}  // namespace Aidge
