/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include "aidge/operator/WeightInterleaving.hpp"

namespace py = pybind11;

namespace Aidge {

void declare_WeightInterleaving(py::module &m) {
  py::class_<WeightInterleaving_Op, std::shared_ptr<WeightInterleaving_Op>, OperatorTensor>(m, "WeightInterleavingOp", py::multiple_inheritance())
    .def(py::init<>())
    .def_static("get_inputs_name", &WeightInterleaving_Op::getInputsName)
    .def_static("get_outputs_name", &WeightInterleaving_Op::getOutputsName)
    .def_readonly_static("Type", &WeightInterleaving_Op::Type)

    .def("__repr__", [](WeightInterleaving_Op& b) {
        return fmt::format("Operator(type='{}')", b.Type);
    });

  declare_registrable<WeightInterleaving_Op>(m, "WeightInterleavingOp");

  m.def("WeightInterleaving", &WeightInterleaving, py::arg("name") = "");
}

void init_WeightInterleaving(py::module &m) {
  declare_WeightInterleaving(m);
}

}  // namespace Aidge
