/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <string>
#include <vector>
#include <array>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/MaxPooling.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Types.h"

namespace py = pybind11;
namespace Aidge {

template <DimIdx_t DIM> void declare_MaxPoolingOp(py::module &m) {
  const std::string pyClassName("MaxPooling" + std::to_string(DIM) + "DOp");
  py::class_<MaxPooling_Op<DIM>, std::shared_ptr<MaxPooling_Op<DIM>>, OperatorTensor>(
    m, pyClassName.c_str(),
    py::multiple_inheritance(),
    R"mydelimiter(
        Initialize a MaxPooling operator for a specified dimension.

        :param kernel_dims: The size of the kernel to apply to each dimension.
        :type kernel_dims: List[int]
        :param stride_dims: The stride (step size) to move the kernel over the input.
        :type stride_dims: List[int]
        :param ceil_mode: Whether to use ceil or floor when calculating the output dimensions.
        :type ceil_mode: bool
    )mydelimiter")
  .def(py::init<const std::array<DimSize_t, DIM> &,
                const std::array<DimSize_t, DIM> &,
                bool>(),
        py::arg("kernel_dims"),
        py::arg("stride_dims"),
        py::arg("ceil_mode"))
  .def_static("get_inputs_name", &MaxPooling_Op<DIM>::getInputsName)
  .def_static("get_outputs_name", &MaxPooling_Op<DIM>::getOutputsName)
  .def_readonly_static("Type", &MaxPooling_Op<DIM>::Type);
  
  declare_registrable<MaxPooling_Op<DIM>>(m, pyClassName);

  m.def(("MaxPooling" + std::to_string(DIM) + "D").c_str(), [](const std::vector<DimSize_t>& kernel_dims,
                                                                  const std::string& name,
                                                                  const std::vector<DimSize_t> &stride_dims,
                                                                  bool ceil_mode) {
        AIDGE_ASSERT(kernel_dims.size() == DIM, "kernel_dims size [{}] does not match DIM [{}]", kernel_dims.size(), DIM);
        AIDGE_ASSERT(stride_dims.size() == DIM, "stride_dims size [{}] does not match DIM [{}]", stride_dims.size(), DIM);

        return MaxPooling<DIM>(to_array<DIM>(kernel_dims.begin()), name, to_array<DIM>(stride_dims.begin()), ceil_mode);
    }, py::arg("kernel_dims"),
       py::arg("name") = "",
       py::arg("stride_dims") = std::vector<DimSize_t>(DIM, 1),
       py::arg("ceil_mode") = false,
    R"mydelimiter(
        Initialize a node containing a MaxPooling operator.

        This operator performs max pooling, which reduces the input tensor size by selecting 
        the maximum value in each kernel-sized window, with optional strides and ceiling for dimension 
        calculation.

        :param kernel_dims: The size of the kernel to apply to each dimension.
        :type kernel_dims: List[int]
        :param stride_dims: The stride (step size) to move the kernel over the input.
        :type stride_dims: List[int]
        :param ceil_mode: Whether to use ceil or floor when calculating the output dimensions.
        :type ceil_mode: bool
        :param name: Name of the node (optional).
        :type name: str
        :return: A node containing the MaxPooling operator.
        :rtype: :py:class:`MaxPoolingOp`
    )mydelimiter");
}

void init_MaxPooling(py::module &m) {
  declare_MaxPoolingOp<1>(m);
  declare_MaxPoolingOp<2>(m);
  declare_MaxPoolingOp<3>(m);
}

} // namespace Aidge

