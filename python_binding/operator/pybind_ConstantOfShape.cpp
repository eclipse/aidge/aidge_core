
/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <string>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/ConstantOfShape.hpp"

namespace py = pybind11;
namespace Aidge {

void init_ConstantOfShape(py::module &m) {
  py::class_<ConstantOfShape_Op, std::shared_ptr<ConstantOfShape_Op>, OperatorTensor>(
      m, "ConstantOfShapeOp", py::multiple_inheritance(),
      R"mydelimiter(
      Initialize a ConstantOfShape operator.

      :param value : Tensor with a given datatype that contains the value 
                     that will fill the output tensor.
      :type value : :py:class:`Tensor`
      )mydelimiter")
      .def("get_inputs_name", &ConstantOfShape_Op::getInputsName)
      .def("get_outputs_name", &ConstantOfShape_Op::getOutputsName)
      .def("value", &ConstantOfShape_Op::value);

  m.def("ConstantOfShape", &ConstantOfShape, py::arg("value") = Tensor(0.f),
        py::arg("name") = "",
        R"mydelimiter(
        Initialize a node containing a ConstantOfShape operator.

        :param value : Tensor with a given datatype that contains the value 
                       that will fill the output tensor.
        :type value : :py:class:`Tensor`
        :param name  : Name of the node.
        :type name : :py:class:`str`
        )mydelimiter");
}

} // namespace Aidge


