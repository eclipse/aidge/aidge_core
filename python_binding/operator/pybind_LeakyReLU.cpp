/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/LeakyReLU.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_LeakyReLU(py::module& m) {
    py::class_<LeakyReLU_Op, std::shared_ptr<LeakyReLU_Op>, OperatorTensor>(m, "LeakyReLUOp", py::multiple_inheritance(),
    R"mydelimiter(
    A class representing the LeakyReLU operator.

    The LeakyReLU activation function performs element-wise leaky ReLU:
    f(x) = x if x > 0, else negative_slope * x.
    The negative_slope parameter controls the angle of the negative part of the function.
    )mydelimiter")
        .def(py::init<float>(), py::arg("negative_slope"))
        .def_static("get_inputs_name", &LeakyReLU_Op::getInputsName)
        .def_static("get_outputs_name", &LeakyReLU_Op::getOutputsName)
        .def_readonly_static("Type", &LeakyReLU_Op::Type);

    declare_registrable<LeakyReLU_Op>(m, "LeakyReLUOp");

    m.def("LeakyReLU", &LeakyReLU, py::arg("negative_slope") = 0.0f, py::arg("name") = "",
    R"mydelimiter(
    Create a LeakyReLU node with a specified negative slope.

    :param negative_slope: The slope for the negative part of the function. Defaults to 0.0.
    :param name: The name of the node.
    )mydelimiter");
}

}  // namespace Aidge

