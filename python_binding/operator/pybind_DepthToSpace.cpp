/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/DepthToSpace.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Attributes.hpp"
#include "aidge/utils/Types.h"

static typename Aidge::DepthToSpace_Op::Mode stringToMode(const std::string& mode) {
static std::unordered_map<std::string, typename Aidge::DepthToSpace_Op::Mode> map = {
    {"DCR", Aidge::DepthToSpace_Op::Mode::DCR},
    {"CRD", Aidge::DepthToSpace_Op::Mode::CRD}
};
return map[mode];
}

namespace py = pybind11;
namespace Aidge {

void declare_DepthToSpace(py::module &m) {

    py::class_<DepthToSpace_Op, std::shared_ptr<DepthToSpace_Op>, OperatorTensor> (m, "DepthToSpaceOp", py::multiple_inheritance())
    .def(py::init([](const std::uint32_t blockSize, const std::string& mode) {
            return new DepthToSpace_Op(blockSize, stringToMode(mode));
        }), py::arg("block_size"), py::arg("mode") = "CRD")
    .def_static("get_inputs_name", &DepthToSpace_Op::getInputsName)
    .def_static("get_outputs_name", &DepthToSpace_Op::getOutputsName)
    .def_readonly_static("Type", &DepthToSpace_Op::Type)
    .def("__repr__", [](DepthToSpace_Op& b) {
        return fmt::format("Operator(type='{}')", b.Type);
    });

  declare_registrable<DepthToSpace_Op>(m, "DepthToSpaceOp");

  m.def("DepthToSpace", [](
            const std::uint32_t blockSize,
            const std::string& mode,
            const std::string& name) {
        return DepthToSpace(blockSize, stringToMode(mode), name);
    }, py::arg("block_size"), py::arg("mode") = "CRD", py::arg("name") = "");
}

void init_DepthToSpace(py::module &m) {
  declare_DepthToSpace(m);
}

} // namespace Aidge
