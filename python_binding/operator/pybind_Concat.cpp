/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Concat.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Concat(py::module& m) {
    py::class_<Concat_Op, std::shared_ptr<Concat_Op>, OperatorTensor>(m, "ConcatOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize a Concat operator.

        :param nb_inputs : The number of input tensors to concatenate.
        :type nb_inputs : :py:class:`int`
        :param axis : The axis along which to concatenate the tensors.
        :type axis : :py:class:`int`
        )mydelimiter")
        .def(py::init<const IOIndex_t, const int>(),
             py::arg("nb_inputs"),
             py::arg("axis"))
        .def_static("get_inputs_name", &Concat_Op::getInputsName)
        .def_static("get_outputs_name", &Concat_Op::getOutputsName)
        .def_readonly_static("Type", &Concat_Op::Type);

    declare_registrable<Concat_Op>(m, "ConcatOp");

    m.def("Concat", &Concat, py::arg("nb_inputs"), py::arg("axis"), py::arg("name") = "",
        R"mydelimiter(
        Initialize a node containing a Concat operator.

        :param nb_inputs : The number of input tensors to concatenate.
        :type nb_inputs : :py:class:`int`
        :param axis : The axis along which to concatenate the tensors.
        :type axis : :py:class:`int`
        :param name : Name of the node.
        :type name : :py:class:`str`
        )mydelimiter");
}

}  // namespace Aidge
