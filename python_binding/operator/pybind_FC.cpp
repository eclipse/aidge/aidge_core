/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/FC.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Attributes.hpp"
#include "aidge/utils/Types.h"

namespace py = pybind11;
namespace Aidge {

void declare_FC(py::module &m) {
  py::class_<FC_Op, std::shared_ptr<FC_Op>, OperatorTensor>(m, "FCOp", py::multiple_inheritance(),
    R"mydelimiter(
    Initialize a Fully Connected (FC) operator.

    :param type : The type of the Fully Connected operation.
    :type type : :py:class:`str`
    )mydelimiter")
    .def(py::init<>())
    .def_static("get_inputs_name", &FC_Op::getInputsName)
    .def_static("get_outputs_name", &FC_Op::getOutputsName)
    .def_readonly_static("Type", &FC_Op::Type)
    .def("out_channels", &FC_Op::outChannels)
    .def("__repr__", [](FC_Op& b) {
        return fmt::format("Operator(type='{}')", b.Type);
    });

  declare_registrable<FC_Op>(m, "FCOp");

  m.def("FC", &FC, py::arg("in_channels"), py::arg("out_channels"), py::arg("no_bias") = false, py::arg("name") = "",
    R"mydelimiter(
    Initialize a node containing a Fully Connected (FC) operator.

    :param in_channels : The number of input channels (features).
    :type in_channels : :py:class:`int`
    :param out_channels : The number of output channels (features).
    :type out_channels : :py:class:`int`
    :param no_bias : Whether to include bias in the operation. Defaults to `False`.
    :type no_bias : :py:class:`bool`
    :param name : Name of the node.
    :type name : :py:class:`str`
    )mydelimiter");
}

void init_FC(py::module &m) {
  declare_FC(m);
}

} // namespace Aidge
