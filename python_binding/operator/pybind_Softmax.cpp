/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Softmax.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Softmax(py::module& m) {
    py::class_<Softmax_Op, std::shared_ptr<Softmax_Op>, OperatorTensor>(m, "SoftmaxOp", py::multiple_inheritance(),
        R"mydelimiter(
            Initialize a Softmax operator.
            :param axis: Axis along which to compute the softmax. The accepted range is [-r, r-1], 
                        where r is the rank (number of dimensions) of the input tensor.
            :type axis: int
        )mydelimiter")
        .def(py::init<std::int32_t>(), py::arg("axis"))
        .def_static("get_inputs_name", &Softmax_Op::getInputsName)
        .def_static("get_outputs_name", &Softmax_Op::getOutputsName)
        .def_readonly_static("Type", &Softmax_Op::Type);
    declare_registrable<Softmax_Op>(m, "SoftmaxOp");
    m.def("Softmax", &Softmax, py::arg("axis"), py::arg("name") = "",
        R"mydelimiter(
            Initialize a node containing a Softmax operator that computes the softmax along the specified axis.

            :param axis: Axis along which to compute the softmax. The accepted range is [-r, r-1], 
                        where r is the rank (number of dimensions) of the input tensor.
            :type axis: int
            :param name: Name of the node (optional).
            :type name: str
            :return: A node containing the Softmax operator.
            :rtype: :py:class:`SoftmaxOp`
        )mydelimiter");
}
}  // namespace Aidge
