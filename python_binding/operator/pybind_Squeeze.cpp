/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>
#include <pybind11/pybind11.h>
#include <string>
#include <vector>

#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Squeeze.hpp"
#include "aidge/utils/Attributes.hpp"
#include "aidge/utils/Types.h"

namespace py = pybind11;
namespace Aidge {

void init_Squeeze(py::module &m) {
  py::class_<Squeeze_Op, std::shared_ptr<Squeeze_Op>, OperatorTensor>(
      m, "SqueezeOp", py::multiple_inheritance(),
		R"mydelimiter(
		Initialize squeeze operator
		:param axes :   axes to squeeze between [-r;r-1] 
						with r = input_tensor.nbDims()
						& r in [-128 , 127]
		:type axes : :py:class: List[Int]
		)mydelimiter")
      .def("get_inputs_name", &Squeeze_Op::getInputsName)
      .def("get_outputs_name", &Squeeze_Op::getOutputsName)
      .def("axes", &Squeeze_Op::axes);
  // Here we bind the constructor of the Squeeze Node. We add an argument
  // for each attribute of the operator (in here we only have 'axes') and
  // the last argument is the node's name.
  m.def("Squeeze", &Squeeze, py::arg("axes") = std::vector<int8_t>({}),
        py::arg("name") = "",
        R"mydelimiter(
    Initialize a node containing a squeeze operator.
	:param axes :   axes to squeeze between [-r;r-1] 
					with r = input_tensor.nbDims()
					& r in [-128 , 127]
	:type axes : :py:class: List[Int]
    :param name : name of the node.
)mydelimiter");
}
} // namespace Aidge
