/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Slice.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Slice(py::module& m) {
    py::class_<Slice_Op, std::shared_ptr<Slice_Op>, OperatorTensor>(m, "SliceOp", py::multiple_inheritance(),
        R"mydelimiter(
        Initialize a Slice operator, which slices a tensor along specified axes.

        :param starts: The start indices for the slice along each axis. The accepted range for each value is [-r, r-1],
                       where r is the rank of the input tensor.
        :type starts: List[int]
        :param ends: The end indices for the slice along each axis. The accepted range for each value is [-r, r-1], 
                     where r is the rank of the input tensor. The slicing is exclusive at the end index.
        :type ends: List[int]
        :param axes: The axes along which to slice the tensor. If not specified, slices all axes.
        :type axes: List[int]
        :param steps: The step size for each axis in the slice. Defaults to 1.
        :type steps: List[int]
        )mydelimiter")
    .def(py::init<const std::vector<std::int64_t>&,
                  const std::vector<std::int64_t>&,
                  const std::vector<std::int8_t>&,
                  const std::vector<std::int64_t>&>(),
                  py::arg("starts"),
                  py::arg("ends"),
                  py::arg("axes") = std::vector<std::int8_t>(),
                  py::arg("steps") = std::vector<std::int64_t>())
    .def_static("get_inputs_name", &Slice_Op::getInputsName)
    .def_static("get_outputs_name", &Slice_Op::getOutputsName)
    .def_readonly_static("Type", &Slice_Op::Type);

    declare_registrable<Slice_Op>(m, "SliceOp");

    m.def("Slice", 
          &Slice,
          py::arg("starts") = std::vector<std::int64_t>(),
          py::arg("ends") = std::vector<std::int64_t>(),
          py::arg("axes") = std::vector<std::int8_t>(),
          py::arg("steps") = std::vector<std::int64_t>(),
          py::arg("name") = "",
          R"mydelimiter(
          Initialize a node containing a Slice operator that slices a tensor along specified axes.

          The slicing is done by specifying the `starts`, `ends`, `axes`, and `steps` for each axis. The accepted range for each of the `starts` and `ends` is [-r, r-1], where r is the rank of the input tensor. The `axes` specify which axes to apply the slice on. The `steps` specify the step size along each axis. If `steps` is not provided, it defaults to 1.

          :param starts: The start indices for the slice along each axis. The accepted range is [-r, r-1].
          :type starts: List[int]
          :param ends: The end indices for the slice along each axis. The accepted range is [-r, r-1], exclusive at the end index.
          :type ends: List[int]
          :param axes: The axes along which to slice the tensor. If not specified, slices all axes.
          :type axes: List[int]
          :param steps: The step size for each axis in the slice. Defaults to 1.
          :type steps: List[int]
          :param name: Name of the node (optional).
          :type name: str
          :return: A node containing the Slice operator.
          :rtype: :py:class:`SliceOp`
          )mydelimiter");
}

}  // namespace Aidge
