/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>

#include "aidge/graph/Node.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Matching.hpp"
#include "aidge/operator/MetaOperator.hpp"
#include "aidge/recipes/Recipes.hpp"

void Aidge::adaptToBackend(std::shared_ptr<GraphView> graphView) {
    const auto nodes = graphView->getNodes();
    for (auto node : nodes) {
        auto impl = node->getOperator()->getImpl();
        AIDGE_ASSERT(impl, "Missing implementation for node {} (of type {})",
            node->name(), node->type());
        auto adaptedNode = impl->getBestAdaptation(impl->getRequiredSpec());

        if (adaptedNode == nullptr) {
            Log::notice("Unable to adapt node {} (of type {}) to backend {}",
                node->name(), node->type(), impl->backend());
        }
        else if (!adaptedNode->getOperator()->isAtomic()) {
            Log::info("Adapted node {} (of type {}) to backend {}",
                node->name(), node->type(), impl->backend());
            AIDGE_ASSERT(GraphView::replace({node}, {adaptedNode}), "Unable to replace adapted node!");
            expandMetaOp(adaptedNode);
        }
    }
}
