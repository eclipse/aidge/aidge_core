/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <set>
#include <cassert>
#include <memory>
#include <string>

#include "aidge/operator/FC.hpp"
#include "aidge/recipes/Recipes.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/operator/GenericOperator.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/operator/MatMul.hpp"
#include "aidge/graph/Matching.hpp"


void Aidge::matMulToFC(std::shared_ptr<Aidge::Node> matmulNode, std::shared_ptr<Aidge::Node> addNode) {
    // Fuse Mulmat & Add into FC
    // Inputs : old nodes (pointers on mul & add)
    AIDGE_ASSERT((matmulNode->type() == "MatMul" && (addNode == nullptr || addNode->type() == "Add")),
        "Wrong type for the nodes to replace: {} and {}",
        matmulNode->type(), (addNode) ? addNode->type() : "nullptr");


    // Step 1 : Create FC
    // Fetch the output dimension through the bias size
    std::shared_ptr<Node> bias = nullptr;
    if (addNode) {
        if (addNode->getParent(0) == matmulNode) {
            AIDGE_ASSERT(addNode->getParent(1), "No bias detected to produce the matMulToFC recipe.");
            bias = addNode->getParent(1);
        }
        else if (addNode->getParent(1) == matmulNode) {
            AIDGE_ASSERT(addNode->getParent(0), "No bias detected to produce the matMulToFC recipe.");
            bias = addNode->getParent(0);
        }
    }

    std::shared_ptr<Node> weight = nullptr;
    if ((matmulNode->getParent(1) && !matmulNode->getParent(0))
        || (matmulNode->getParent(1) && matmulNode->getParent(1)->getOperator()->type() == Producer_Op::Type
            && matmulNode->getParent(0) && matmulNode->getParent(0)->getOperator()->type() != Producer_Op::Type))
    {
        weight = matmulNode->getParent(1);
        // Transpose weights because weight Tensor is in first input
        auto weightOpTensor = std::static_pointer_cast<OperatorTensor>(weight->getOperator());
        const std::shared_ptr<Aidge::Tensor>& weightTensor = weightOpTensor->getOutput(0);
        std::vector<DimSize_t> shape =  weightTensor->dims();
        std::reverse(shape.begin(), shape.end());
        weightTensor->copyTranspose(*weightTensor, std::vector<Aidge::DimSize_t>({1ul, 0ul}));
    }
    else if ((matmulNode->getParent(0) && !matmulNode->getParent(1))
        || (matmulNode->getParent(0) && matmulNode->getParent(0)->getOperator()->type() == Producer_Op::Type
            && matmulNode->getParent(1) && matmulNode->getParent(1)->getOperator()->type() != Producer_Op::Type))
    {
        weight = matmulNode->getParent(0);
    }
    else if (matmulNode->getParent(0) && matmulNode->getParent(0)->getOperator()->type() == Producer_Op::Type
        && matmulNode->getParent(1) && matmulNode->getParent(1)->getOperator()->type() == Producer_Op::Type)
    {
        // If both inputs are producers, there is an ambiguity, but both options
        // result in a correct solution.
        Log::notice("both MatMul inputs are Producers, assume data at input#0 and weights at input#1.");
        weight = matmulNode->getParent(1);
    }
    AIDGE_ASSERT(weight != nullptr, "Could not deduce weight input for MatMul operator.");

    // Instantiate FC
    std::string fcName = matmulNode->name();
    if (addNode && !addNode->name().empty()) {
        fcName += "_" + addNode->name();
    }

    std::shared_ptr<Node> fc = std::make_shared<Node>(std::make_shared<FC_Op>(), fcName);

    // Step 2 : Branch existing producers & create the others
    // link weights & bias
    weight->cloneSharedOperators()->addChild(fc, 0, 1);
    if (bias) {
        bias->cloneSharedOperators()->addChild(fc, 0, 2);
    }

    // Step 3 : Update all graphviews that contains at least one node to replace
        // Case 1 : If all nodes are in a graph view : delete old nodes & branch input & output
        // Case 2 : If not all nodes are in a graph view : only delete the nodes from the graphview
        // Maybe create a central mechanism to update automatically all graph views rather than each node have graphview presence memory?
    if (addNode) {
        auto newNodes = std::set<std::shared_ptr<Node>>({fc, fc->getParent(1), fc->getParent(2)});
        GraphView::replace({matmulNode, addNode, bias, weight}, newNodes);
    }
    else {
        auto newNodes = std::set<std::shared_ptr<Node>>({fc, fc->getParent(1)});
        GraphView::replace({matmulNode, weight}, newNodes);
    }

}

void Aidge::matMulToFC(std::shared_ptr<Aidge::GraphView> graphView){
    const auto matches = SinglePassGraphMatching(graphView).match("MatMul->Add#?");

    for (const auto& match : matches) {
        const auto it = match.anchors.find("Add");
        matMulToFC(match.graph->rootNode(), (it != match.anchors.end()) ? it->second.at("#") : nullptr);
    }
}
