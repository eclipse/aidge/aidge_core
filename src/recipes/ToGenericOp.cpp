/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>

#include "aidge/graph/Node.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/operator/GenericOperator.hpp"
#include "aidge/recipes/Recipes.hpp"

void Aidge::toGenericOp(std::shared_ptr<Node> node) {
    auto newGenOp = {GenericOperator(node->type(),  std::dynamic_pointer_cast<Aidge::OperatorTensor>(node->getOperator()), node->name())};
    auto OldOp = {node};
    GraphView::replace(OldOp, newGenOp);
}
