/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
#include "aidge/recipes/Recipes.hpp"

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <functional>
#include <memory>
#include <numeric>
#include <set>
#include <stdexcept>
#include <string>

#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/filler/Filler.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Matching.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/ConstantOfShape.hpp"
#include "aidge/operator/GenericOperator.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

size_t removeConstantOfShape(std::shared_ptr<GraphView> graph_view) {
  const auto matches =
      SinglePassGraphMatching(graph_view).match("Producer->ConstantOfShape");

  size_t nbReplaced = 0;
  for (const auto &match : matches) {
    const auto prod_node = match.graph->rootNode();
    const auto prod_op =
        std::static_pointer_cast<Producer_Op>(prod_node->getOperator());

    const NodePtr constantofshape_node =
        prod_node->getOrderedChildren().at(0).at(0);

    const auto constantofshape_op =
        std::static_pointer_cast<ConstantOfShape_Op>(
            constantofshape_node->getOperator());

    if (prod_op->getOutput(0)->nbDims() != 1) {
      Log::debug("{} : Producer output dimension number is {} != 1 and {} "
                 "input has to have 1 dim, skipping match.",
                 __func__, prod_op->getOutput(0)->nbDims(),
                 ConstantOfShape_Op::Type);
      continue;
    }
    if (!prod_op->constant()) {
      Log::debug("{} : Producer is not constant, skipping match.", __func__);
      continue;
    }
    if (prod_op->getOutput(0)->dataType() != DataType::Int64) {
      AIDGE_THROW_OR_ABORT(
          std::runtime_error,
          "{} : Producer output dtype is {} != int64 and {} "
          "input type is restricted to int64_t, this is an error."
          "Fix your network. skipping match.",
          __func__, prod_op->getOutput(0)->dataType(),
          ConstantOfShape_Op::Type);
      continue;
    }

    auto graph_to_replace = std::make_shared<GraphView>();
    auto new_graph = std::make_shared<GraphView>();
    graph_to_replace->add(constantofshape_node);
    if (prod_node->getChildren().size() == 1) {
      graph_to_replace->add(prod_node);
    } else {
      Log::debug("{} : Producer node has multiple children, only"
                 "replacing the {} node.",
                 __func__, ConstantOfShape_Op::Type);
    }

    prod_node->forward();
    std::shared_ptr<Tensor> prod_output = prod_op->getOutput(0);
    std::vector<DimSize_t> new_input_dims;
    new_input_dims.reserve(prod_output->dims()[0]);
    for (DimSize_t i = 0; i < prod_output->size(); ++i) {
      new_input_dims.push_back(prod_output->get<int64_t>(i));
    }

    auto new_input = std::make_shared<Tensor>(new_input_dims);
    new_input->setBackend(prod_op->backend() == "" ? "cpu"
                                                   : prod_op->backend());
    new_input->setDataType(constantofshape_op->value().dataType());
    for (std::size_t i = 0; i < new_input->size(); ++i) {
      new_input->getImpl()->copy(
          constantofshape_op->value().getImpl()->rawPtr(), 1, i);
    }
    auto new_prod =
        Producer(new_input, prod_node->name() + "_constant_of_shape", true);
    new_graph->add(new_prod);

    const auto success = GraphView::replace(graph_to_replace, new_graph);
    if (!success) {
      Log::warn("Could not replace Producer({})->ConstantOfShape({}) with"
                "Producer",
                prod_node->name(), constantofshape_node->name());
    } else {
      ++nbReplaced;
    }
  }

  Log::info("Replaced {} (out of {}) matching Producer->ConstantOfShape with "
            "Producers",
            nbReplaced, matches.size());
  return nbReplaced;
}
} // namespace Aidge

