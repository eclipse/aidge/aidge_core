/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>

#include "aidge/graph/Node.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/recipes/Recipes.hpp"
#include "aidge/graph/Matching.hpp"

namespace Aidge {
    void removeFlatten(std::shared_ptr<GraphView> graphView){
        const auto matches = SinglePassGraphMatching(graphView).match(
            "(FC|MatMul)<-(Flatten)+"
        );

        for (const auto& solution : matches) {
            auto flattenNodes(solution.graph->getNodes());
            flattenNodes.erase(solution.graph->rootNode());
            GraphView::replace(flattenNodes, {});
        }
    }
}
