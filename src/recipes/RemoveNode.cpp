/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>

#include "aidge/graph/Node.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Matching.hpp"
#include "aidge/recipes/Recipes.hpp"

size_t Aidge::removeNode(std::shared_ptr<GraphView> graphView, const std::string& type, bool incProducers) {
    auto matches = SinglePassGraphMatching(graphView).match(type);
    for (const auto& match : matches) {
        std::set<NodePtr> nodesToRemove = {match.graph->rootNode()};
        if (incProducers) {
            for (const auto& nodePtr: match.graph->rootNode()->getParents()) {
                if (nodePtr != nullptr && nodePtr->type() == "Producer") {
                    nodesToRemove.insert(nodePtr);
                }
            }
        }
        GraphView::replace(nodesToRemove, {});
    }

    return matches.size();
}

size_t Aidge::removeDropout(std::shared_ptr<GraphView> graphView) {
    return removeNode(graphView, "Dropout", true);
}

size_t Aidge::removeIdentity(std::shared_ptr<GraphView> graphView) {
    return removeNode(graphView, "Identity");
}
