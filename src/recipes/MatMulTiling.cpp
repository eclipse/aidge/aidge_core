/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
#include <cassert>
#include <memory>
#include <set>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/operator/MatMul.hpp"
#include "aidge/operator/Slice.hpp"
#include "aidge/operator/Identity.hpp"
#include "aidge/operator/Concat.hpp"
#include "aidge/recipes/Recipes.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"

// see https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm
void Aidge::matMulTiling(NodePtr matMul, const std::vector<DimSize_t>& maxDims) {
    if (matMul->getOperator()->type() != "MatMul") {
        AIDGE_INTERNAL_ASSERT("Operator should be a MatMul.");
    }
    AIDGE_ASSERT(matMul->getOperator()->operatorType() == OperatorType::Tensor, "Operator must be of Tensor type.");
    const auto& op = std::static_pointer_cast<OperatorTensor>(matMul->getOperator());
    if (!op->dimsForwarded()) {
        AIDGE_INTERNAL_ASSERT("Dimensions must be forwarded before any tiling");
    }

    const auto& in0Tensor = op->getInput(0);
    const auto& in1Tensor = op->getInput(1);
    const auto& outTensor = op->getOutput(0);
    const auto& input0Dims = in0Tensor->dims();
    const auto& input1Dims = in1Tensor->dims();
    const auto& outputDims = outTensor->dims();
    const auto& outputMatDims = std::vector<std::size_t>(outputDims.end() - 2, outputDims.end());;

    if (outputMatDims[0] > maxDims[0] || outputMatDims[1] > maxDims[1]) {
        const auto sliceDims = (outputMatDims[0] > maxDims[0]) ? input0Dims : input1Dims;
        std::int32_t axis;
        std::int64_t splitIndex0_end = static_cast<std::int64_t>(sliceDims.end()[-2]);
        std::int64_t splitIndex0_start = 0;
        std::int64_t splitIndex1_end = static_cast<std::int64_t>(sliceDims.end()[-1]);
        std::int64_t splitIndex1_start = 0;

        if (outputMatDims[0] > maxDims[0]) {
            splitIndex0_end = maxDims[0];
            splitIndex0_start = maxDims[0];
            axis = -2;
        }
        else {
            splitIndex1_end = maxDims[1];
            splitIndex1_start = maxDims[1];
            axis = -1;
        }

        auto identity0 = Identity();
        auto sliceX0 = Slice();
        auto sliceX0_starts = Producer(std::make_shared<Tensor>(Vector<std::int64_t>{{0, 0}}), "", true);
        sliceX0_starts->addChild(sliceX0, 0, 1);
        auto sliceX0_ends = Producer(std::make_shared<Tensor>(Vector<std::int64_t>{{splitIndex0_end, splitIndex1_end}}), "", true);
        sliceX0_ends->addChild(sliceX0, 0, 2);
        auto sliceX0_axes = Producer(std::make_shared<Tensor>(Vector<std::int8_t>{{-2, -1}}), "", true);
        sliceX0_axes->addChild(sliceX0, 0, 3);
        auto sliceX0_steps = Producer(std::make_shared<Tensor>(Vector<std::int64_t>{{1, 1}}), "", true);
        sliceX0_steps->addChild(sliceX0, 0, 4);
        auto matMulX0 = MatMul();
        auto identity1 = Identity();
        auto sliceX1 = Slice();
        auto sliceX1_starts = Producer(std::make_shared<Tensor>(Vector<std::int64_t>{{splitIndex0_start, splitIndex1_start}}), "", true);
        sliceX1_starts->addChild(sliceX1, 0, 1);
        auto sliceX1_ends = Producer(std::make_shared<Tensor>(Vector<std::int64_t>{{static_cast<std::int64_t>(sliceDims.end()[-2]), static_cast<std::int64_t>(sliceDims.end()[-1])}}), "", true);
        sliceX1_ends->addChild(sliceX1, 0, 2);
        auto sliceX1_axes = Producer(std::make_shared<Tensor>(Vector<std::int8_t>{{-2, -1}}), "", true);
        sliceX1_axes->addChild(sliceX1, 0, 3);
        auto sliceX1_steps = Producer(std::make_shared<Tensor>(Vector<std::int64_t>{{1, 1}}), "", true);
        sliceX1_steps->addChild(sliceX1, 0, 4);
        auto matMulX1 = MatMul();
        auto concat = Concat(2, axis);

        if (outputMatDims[0] > maxDims[0]) {
            identity0->addChild(sliceX0, 0, 0);
            identity0->addChild(sliceX1, 0, 0);
            identity1->addChild(matMulX0, 0, 1);
            identity1->addChild(matMulX1, 0, 1);
            sliceX0->addChild(matMulX0, 0, 0);
            sliceX1->addChild(matMulX1, 0, 0);
        }
        else {
            identity0->addChild(matMulX0, 0, 0);
            identity0->addChild(matMulX1, 0, 0);
            identity1->addChild(sliceX0, 0, 0);
            identity1->addChild(sliceX1, 0, 0);
            sliceX0->addChild(matMulX0, 0, 1);
            sliceX1->addChild(matMulX1, 0, 1);
        }
    
        matMulX0->addChild(concat, 0, 0);
        matMulX1->addChild(concat, 0, 1);

        auto gMatMul = std::make_shared<GraphView>();
        gMatMul->add({matMul});

        auto g = std::make_shared<GraphView>();
        g->add({identity0});
        g->add({identity1});
        g->add({sliceX0, sliceX0_starts, sliceX0_ends, sliceX0_axes, sliceX0_steps, matMulX0, matMulX1, sliceX1, sliceX1_starts, sliceX1_ends, sliceX1_axes, sliceX1_steps, concat});

        auto replaced = GraphView::replace(gMatMul, g);

        if (replaced) {
            g->forwardDims({}, true);

            // Recursive tiling
            matMulTiling(matMulX1, maxDims);
            matMulTiling(matMulX0, maxDims);
        }
        else {
            Log::warn("Unable to split MatMul {}", matMul->name());
        }
    }
}
