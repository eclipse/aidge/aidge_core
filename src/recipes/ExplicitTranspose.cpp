/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/recipes/Recipes.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Transpose.hpp"

void Aidge::explicitTranspose(std::shared_ptr<GraphView> graph) {
    // First, remove existing Transpose operators, if not needed anymore
    auto nodes = graph->getNodes();
    for (auto node : nodes) {
        AIDGE_ASSERT(node->getOperator()->operatorType() == OperatorType::Tensor, "Operator must be of Tensor type.");
        const auto& output = std::static_pointer_cast<OperatorTensor>(node->getOperator())->getOutput(0);

        if (node->type() == Transpose_Op::Type) {
            // Remove existing Transpose operators, if not needed anymore
            AIDGE_INTERNAL_ASSERT(node->inputs().size() == 1);
            const auto parent = node->inputs()[0];
            // Check parent is not nullptr, as this Operator may be an entry point of the graph without parent
            if (parent.first != nullptr) {
                AIDGE_ASSERT(parent.first->getOperator()->operatorType() == OperatorType::Tensor, "Operator must be of Tensor type.");
                const auto& input = std::static_pointer_cast<OperatorTensor>(parent.first->getOperator())->getOutput(parent.second);

                if (input->dataFormat() != DataFormat::Default
                    && output->dataFormat() != DataFormat::Default
                    && input->dataFormat() == output->dataFormat())
                {
                    // Add direct connection bypassing Transpose node
                    const auto childs = node->outputs()[0];
                    for (const auto& child : childs) {
                        parent.first->addChild(child.first, parent.second, child.second);
                    }

                    // Remove all node connections
                    node->resetConnections();
                    // Remove node from view
                    graph->remove(node);
                }
            }
        }
    }

    // Second, insert Transpose operator between node inputs and parent output, if needed
    nodes = graph->getNodes();
    for (auto node : nodes) {
        // TODO: currently, Operator data type is only reflected in its output tensor data type.
        // But an Operator might have multiple outputs of different data type(?)
        const auto& output = std::static_pointer_cast<OperatorTensor>(node->getOperator())->getOutput(0);

        IOIndex_t inputIdx = 0;
        for (auto parent : node->inputs()) {
            // TODO: possible optimization: currently, a Transpose Operator may
            // be added several time to the same output, if it has multiple childs,
            // even if it is the same conversion each time.
            if (parent.first != nullptr) {
                const auto& input = std::static_pointer_cast<OperatorTensor>(parent.first->getOperator())->getOutput(parent.second);

                if ((node->type() != Transpose_Op::Type
                    && input->dataFormat() != DataFormat::Default
                    && output->dataFormat() != DataFormat::Default
                    && input->dataFormat() != output->dataFormat()))
                {
                    // Allow Transpose fuse only if data format is fully specified before and after in this recipe
                    bool fuseTranspose = false;
                    if (parent.first->type() == Transpose_Op::Type && parent.first->getChildren().size() == 1) {
                        const auto& parentInput = std::static_pointer_cast<OperatorTensor>(parent.first->getOperator())->getInput(0);
                        if (parentInput->dataFormat() != DataFormat::Default) {
                            fuseTranspose = true;
                        }
                    }

                    if (fuseTranspose) {
                        // Do not insert a new Transpose if the only parent is already a Transpose!
                        const auto& parentInput = std::static_pointer_cast<OperatorTensor>(parent.first->getOperator())->getInput(0);
                        if (parentInput->dataFormat() == output->dataFormat()) {
                            // Case 1: same data format than before parent Transpose
                            // => remove Transpose altogether
                            const auto parentParent = parent.first->inputs()[0];
                            // Add direct connection bypassing Transpose node
                            parentParent.first->addChild(node, parentParent.second, 0);
                            // Remove all node connections
                            parent.first->resetConnections();
                            // Remove node from view
                            graph->remove(parent.first);
                        }
                        else {
                            // Case 2: change of format
                            // => compute the new permutation array
                            const auto transpose = getDataFormatTranspose(parentInput->dataFormat(), output->dataFormat());
                            auto transposeOp = std::static_pointer_cast<Transpose_Op>(parent.first->getOperator());
                            transposeOp->setDataFormat(output->dataFormat());
                            transposeOp->outputDimsOrder() = std::vector<DimSize_t>(transpose.begin(), transpose.end());
                        }
                    }
                    else {
                        const auto transpose = getDataFormatTranspose(input->dataFormat(), output->dataFormat());
                        auto transposeOp = Transpose(std::vector<DimSize_t>(transpose.begin(), transpose.end()));
                        transposeOp->getOperator()->setDataFormat(output->dataFormat());
                        transposeOp->getOperator()->setDataType(output->dataType());
                        if (output->getImpl()) {
                            const auto& device = output->getImpl()->device();
                            transposeOp->getOperator()->setBackend(device.first, device.second);
                        }
                        transposeOp->addChild(node, 0, inputIdx);
                        parent.first->addChild(transposeOp, parent.second, 0);

                        graph->add(transposeOp);
                        graph->add(parent.first);
                        graph->add(node);
                    }
                }
            }

            ++inputIdx;
        }
    }
}
