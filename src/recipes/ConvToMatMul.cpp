/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>

#include "aidge/graph/Node.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Matching.hpp"
#include "aidge/operator/Add.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/Unfold.hpp"
#include "aidge/operator/Fold.hpp"
#include "aidge/operator/Reshape.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/operator/MatMul.hpp"
#include "aidge/recipes/Recipes.hpp"

size_t Aidge::convToMatMul(std::shared_ptr<GraphView> graphView) {
    const auto matches = SinglePassGraphMatching(graphView).match("Conv2D");

    size_t nbReplaced = 0;
    for (const auto& match : matches) {
        const auto convNode = match.startNode;
        const std::shared_ptr<Conv_Op<2>> convOp =
            std::static_pointer_cast<Conv_Op<2>>(convNode->getOperator());

        AIDGE_ASSERT(convOp->getOutput(0) && !convOp->getOutput(0)->empty(),
            "Output dims must have been forwarded in order to apply convToMatMul for Conv {}", convNode->name());

        //const auto nbDims = convOp->getOutput(0)->dims().size();
        //const std::array<DimSize_t, 2> outputDims = {convOp->getOutput(0)->dims()[nbDims - 2], convOp->getOutput(0)->dims()[nbDims - 1]};
        const auto wShape = convOp->getInput(1)->dims();
        const auto wFlattenSize = std::accumulate(wShape.cbegin() + 1, wShape.cend(), DimSize_t(1), std::multiplies<DimSize_t>());

        auto microGraph = std::make_shared<GraphView>();
        auto unfold = Unfold(convOp->kernelDims(),
            (!convNode->name().empty()) ? convNode->name() + "_unfold" : "",
            convOp->strideDims(),
            convOp->dilationDims());
        auto wReshapeProd = Producer(std::make_shared<Tensor>(Vector<int64_t>{{static_cast<int64_t>(convOp->getInput(1)->dims()[0]), static_cast<int64_t>(wFlattenSize)}}),
            (!convNode->name().empty()) ? convNode->name() + "_w_reshape_shape_prod" : "",
            true);
        auto wReshape = Reshape({},
            false,
            (!convNode->name().empty()) ? convNode->name() + "_w_reshape" : "");
        auto matMul = MatMul((!convNode->name().empty()) ? convNode->name() + "_matmul" : "");
        auto reshapeProd = Producer(std::make_shared<Tensor>(Vector<int64_t>(convOp->getOutput(0)->dims())),
            (!convNode->name().empty()) ? convNode->name() + "_reshape_shape_prod" : "",
            true);
        auto reshape = Reshape({},
            false,
            (!convNode->name().empty()) ? convNode->name() + "_reshape" : "");
        //auto fold = Fold(outputDims,
        //    convOp->kernelDims(),
        //    (!convNode->name().empty()) ? convNode->name() + "_unfold" : "",
        //    convOp->strideDims(),
        //    convOp->dilationDims());

        wReshapeProd->addChild(wReshape, 0, 1);
        wReshape->addChild(matMul, 0, 0);
        unfold->addChild(matMul, 0, 1);
        reshapeProd->addChild(reshape, 0, 1);
        matMul->addChild(reshape, 0, 0);
        //matMul->addChild(fold, 0, 0);
        microGraph->add({unfold, wReshapeProd, wReshape, matMul, reshapeProd, reshape}, false);
        //microGraph->add({unfold, wReshapeProd, wReshape, matMul, fold}, false);

        // Handle bias
        if (convOp->getInput(2) && !convOp->getInput(2)->empty()) {
            auto add = Add((!convNode->name().empty()) ? convNode->name() + "_add" : "");
            auto bReshapeProd = Producer(std::make_shared<Tensor>(Vector<int64_t>{{1, static_cast<int64_t>(convOp->getInput(2)->size()), 1, 1}}),
                (!convNode->name().empty()) ? convNode->name() + "_b_reshape_shape_prod" : "",
                true);
            auto bReshape = Reshape({},
                false,
                (!convNode->name().empty()) ? convNode->name() + "_b_reshape" : "");

            bReshapeProd->addChild(bReshape, 0, 1);
            bReshape->addChild(add, 0, 1);
            reshape->addChild(add, 0, 0);
            //fold->addChild(add, 0, 0);
            microGraph->add({reshape, add, bReshapeProd, bReshape}, false);
            //microGraph->add({fold, add}, false);
            microGraph->setOrderedInputs({{unfold, 0}, {wReshape, 0}, {bReshape, 0}});
        }
        else {
            // Add a dummy 3rd input in order for replace() to work
            microGraph->setOrderedInputs({{unfold, 0}, {wReshape, 0}, {nullptr, 0}});
        }

        auto gConv = std::make_shared<GraphView>();
        gConv->add(convNode, false);

        const auto success = GraphView::replace(gConv, microGraph);

        if (!success) {
            Log::notice("Could not replace Conv {} with MatMul", convNode->name());
        }
        else {
            ++nbReplaced;
        }
    }

    Log::info("Replaced {} (out of {}) matching Conv with MatMul", nbReplaced, matches.size());
    return nbReplaced;
}
