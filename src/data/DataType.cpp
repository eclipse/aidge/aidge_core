/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/data/DataType.hpp"

#include <cstddef>  // std::size_t

std::size_t Aidge::getDataTypeBitWidth(const Aidge::DataType& type) {
    switch (type) {
        case DataType::Float64:   return 64;
        case DataType::Float32:   return 32;
        case DataType::Float16:   return 16;
        case DataType::BFloat16:  return 16;
        case DataType::Binary:    return 1;
        case DataType::Ternary:   return 2;
        case DataType::Int2:      return 2;
        case DataType::Int3:      return 3;
        case DataType::Int4:      return 4;
        case DataType::Int5:      return 5;
        case DataType::Int6:      return 6;
        case DataType::Int7:      return 7;
        case DataType::Int8:      return 8;
        case DataType::Int16:     return 16;
        case DataType::Int32:     return 32;
        case DataType::Int64:     return 64;
        case DataType::UInt2:     return 2;
        case DataType::UInt3:     return 3;
        case DataType::UInt4:     return 4;
        case DataType::UInt5:     return 5;
        case DataType::UInt6:     return 6;
        case DataType::UInt7:     return 7;
        case DataType::UInt8:     return 8;
        case DataType::UInt16:    return 16;
        case DataType::UInt32:    return 32;
        case DataType::UInt64:    return 64;
        default:                  return 0;
    }
}