/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/data/Tensor.hpp"

#include <cstddef>
#include <vector>

#include "aidge/data/half.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/operator/Abs.hpp"
#include "aidge/operator/Add.hpp"
#include "aidge/operator/Div.hpp"
#include "aidge/operator/Mul.hpp"
#include "aidge/operator/ReduceMean.hpp"
#include "aidge/operator/Sub.hpp"
#include "aidge/operator/Sqrt.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

Tensor::Tensor(const Tensor& other) = default;
Tensor::Tensor(Tensor&& other) = default;

Tensor& Tensor::operator=(const Tensor& other) = default;
Tensor& Tensor::operator=(Tensor&& other) = default;

Tensor::~Tensor() noexcept = default;


Tensor Tensor::operator+(const Tensor& other) const {
    AIDGE_ASSERT(hasImpl() && other.hasImpl(), "At least one Tensor cannot perform any binary operation because it has no implementation.");
    AIDGE_ASSERT(mImpl->backend() == other.mImpl->backend(), "Tensors must have the same backend");
    AIDGE_ASSERT(dataType() == other.dataType(), "Tensors must have the same data type");
    AIDGE_ASSERT(dataFormat() == other.dataFormat(), "Tensors must have the same data format");
    auto add_ = Add_Op();
    add_.associateInput(0, std::make_shared<Tensor>(*this));
    add_.associateInput(1, std::make_shared<Tensor>(other));
    add_.setDataType(dataType());
    add_.setDataFormat(dataFormat());
    add_.setBackend(mImpl->backend());
    add_.forward();
    // using add_backend = std::remove_reference_t<decltype(*Registrar<Add_Op>::create("cpu")(std::declval<const Add_Op&>()))>;
    return *add_.getOutput(0);
}

Tensor& Tensor::operator+=(const Tensor& other) {
    AIDGE_ASSERT(hasImpl() && other.hasImpl(), "At least one Tensor cannot perform any binary operation because it has no implementation.");
    AIDGE_ASSERT(mImpl->backend() == other.mImpl->backend(), "Tensors must have the same backend");
    AIDGE_ASSERT(dataType() == other.dataType(), "Tensors must have the same data type");
    AIDGE_ASSERT(dataFormat() == other.dataFormat(), "Tensors must have the same data format");
    auto add_ = Add_Op();
    const auto thisPtr = std::make_shared<Tensor>(*this);
    add_.associateInput(0, thisPtr);
    add_.associateInput(1, std::make_shared<Tensor>(other));
    add_.setOutput(0, thisPtr);
    add_.setDataType(dataType());
    add_.setDataFormat(dataFormat());
    add_.setBackend(mImpl->backend());
    add_.forward();
    return *this;
}


Tensor Tensor::operator-(const Tensor& other) const {
    AIDGE_ASSERT(hasImpl() && other.hasImpl(), "At least one Tensor cannot perform any binary operation because it has no implementation.");
    AIDGE_ASSERT(mImpl->backend() == other.mImpl->backend(), "Tensors must have the same backend");
    AIDGE_ASSERT(dataType() == other.dataType(), "Tensors must have the same data type");
    AIDGE_ASSERT(dataFormat() == other.dataFormat(), "Tensors must have the same data format");
    auto sub_ = Sub_Op();
    sub_.associateInput(0, std::make_shared<Tensor>(*this));
    sub_.associateInput(1, std::make_shared<Tensor>(other));
    sub_.setDataType(dataType());
    sub_.setDataFormat(dataFormat());
    sub_.setBackend(mImpl->backend());
    sub_.forward();
    // using add_backend = std::remove_reference_t<decltype(*Registrar<Add_Op>::create("cpu")(std::declval<const Add_Op&>()))>;
    return *sub_.getOutput(0);
}

Tensor& Tensor::operator-=(const Tensor& other) {
    AIDGE_ASSERT(hasImpl() && other.hasImpl(), "At least one Tensor cannot perform any binary operation because it has no implementation.");
    AIDGE_ASSERT(mImpl->backend() == other.mImpl->backend(), "Tensors must have the same backend");
    AIDGE_ASSERT(dataType() == other.dataType(), "Tensors must have the same data type");
    AIDGE_ASSERT(dataFormat() == other.dataFormat(), "Tensors must have the same data format");
    auto sub_ = Sub_Op();
    const auto thisPtr = std::make_shared<Tensor>(*this);
    sub_.associateInput(0, thisPtr);
    sub_.associateInput(1, std::make_shared<Tensor>(other));
    sub_.setOutput(0, thisPtr);
    sub_.setDataType(dataType());
    sub_.setDataFormat(dataFormat());
    sub_.setBackend(mImpl->backend());
    sub_.forward();
    // using add_backend = std::remove_reference_t<decltype(*Registrar<Add_Op>::create("cpu")(std::declval<const Add_Op&>()))>;
    return *this;
}


Tensor Tensor::operator*(const Tensor& other) const {
    AIDGE_ASSERT(hasImpl() && other.hasImpl(), "At least one Tensor cannot perform any binary operation because it has no implementation.");
    AIDGE_ASSERT(mImpl->backend() == other.mImpl->backend(), "Tensors must have the same backend");
    AIDGE_ASSERT(dataType() == other.dataType(), "Tensors must have the same data type");
    AIDGE_ASSERT(dataFormat() == other.dataFormat(), "Tensors must have the same data format");
    auto mul_ = Mul_Op();
    mul_.associateInput(0, std::make_shared<Tensor>(*this));
    mul_.associateInput(1, std::make_shared<Tensor>(other));
    mul_.setDataType(dataType());
    mul_.setDataFormat(dataFormat());
    mul_.setBackend(mImpl->backend());
    mul_.forward();
    // using add_backend = std::remove_reference_t<decltype(*Registrar<Add_Op>::create("cpu")(std::declval<const Add_Op&>()))>;
    return mul_.getOutput(0)->clone();
}

Tensor& Tensor::operator*=(const Tensor& other) {
    AIDGE_ASSERT(hasImpl() && other.hasImpl(), "At least one Tensor cannot perform any binary operation because it has no implementation.");
    AIDGE_ASSERT(mImpl->backend() == other.mImpl->backend(), "Tensors must have the same backend");
    AIDGE_ASSERT(dataType() == other.dataType(), "Tensors must have the same data type");
    AIDGE_ASSERT(dataFormat() == other.dataFormat(), "Tensors must have the same data format");
    auto mul_ = Mul_Op();
    const auto thisPtr = std::make_shared<Tensor>(*this);
    mul_.associateInput(0, thisPtr);
    mul_.associateInput(1, std::make_shared<Tensor>(other));
    mul_.setOutput(0, thisPtr);
    mul_.setDataType(dataType());
    mul_.setDataFormat(dataFormat());
    mul_.setBackend(mImpl->backend());
    mul_.forward();
    // using add_backend = std::remove_reference_t<decltype(*Registrar<Add_Op>::create("cpu")(std::declval<const Add_Op&>()))>;
    return *this;
}


Tensor Tensor::operator/(const Tensor& other) const {
    AIDGE_ASSERT(hasImpl() && other.hasImpl(), "At least one Tensor cannot perform any binary operation because it has no implementation.");
    AIDGE_ASSERT(mImpl->backend() == other.mImpl->backend(), "Tensors must have the same backend");
    AIDGE_ASSERT(dataType() == other.dataType(), "Tensors must have the same data type");
    AIDGE_ASSERT(dataFormat() == other.dataFormat(), "Tensors must have the same data format");
    auto div_ = Div_Op();
    div_.associateInput(0, std::make_shared<Tensor>(*this));
    div_.associateInput(1, std::make_shared<Tensor>(other));
    div_.setDataType(dataType());
    div_.setDataFormat(dataFormat());
    div_.setBackend(mImpl->backend());
    div_.forward();
    // using add_backend = std::remove_reference_t<decltype(*Registrar<Add_Op>::create("cpu")(std::declval<const Add_Op&>()))>;
    return div_.getOutput(0)->clone();
}

Tensor& Tensor::operator/=(const Tensor& other) {
    AIDGE_ASSERT(hasImpl() && other.hasImpl(), "At least one Tensor cannot perform any binary operation because it has no implementation.");
    AIDGE_ASSERT(mImpl->backend() == other.mImpl->backend(), "Tensors must have the same backend");
    AIDGE_ASSERT(dataType() == other.dataType(), "Tensors must have the same data type");
    AIDGE_ASSERT(dataFormat() == other.dataFormat(), "Tensors must have the same data format");
    auto div_ = Div_Op();
    const auto thisPtr = std::make_shared<Tensor>(*this);
    div_.associateInput(0, thisPtr);
    div_.associateInput(1, std::make_shared<Tensor>(other));
    div_.setOutput(0, thisPtr);
    div_.setDataType(dataType());
    div_.setDataFormat(dataFormat());
    div_.setBackend(mImpl->backend());
    div_.forward();
    // using add_backend = std::remove_reference_t<decltype(*Registrar<Add_Op>::create("cpu")(std::declval<const Add_Op&>()))>;
    return *this;
}

Tensor Tensor::sqrt() const {
    AIDGE_ASSERT(hasImpl(), "Tensor has no implementation.");
    auto sqrt_ = Sqrt_Op();
    sqrt_.associateInput(0, std::make_shared<Tensor>(*this));
    sqrt_.setDataType(dataType());
    sqrt_.setDataFormat(dataFormat());
    sqrt_.setBackend(mImpl->backend());
    sqrt_.forward();
    return sqrt_.getOutput(0)->clone();
}

Tensor Tensor::abs() const {
    AIDGE_ASSERT(hasImpl(), "Tensor has no implementation.");
    auto abs_ = Abs_Op();
    abs_.associateInput(0, std::make_shared<Tensor>(*this));
    abs_.setDataType(dataType());
    abs_.setDataFormat(dataFormat());
    abs_.setBackend(mImpl->backend());
    abs_.forward();
    return abs_.getOutput(0)->clone();
}

Tensor Tensor::mean() const {
    AIDGE_ASSERT(hasImpl(), "Tensor has no implementation.");
    // TODO: should be the default behavior of ReduceMean_Op
    // No need to specify the list of all axes!
    std::vector<std::int32_t> axes(nbDims());
    std::iota(std::begin(axes), std::end(axes), 0);
    auto mean_ = ReduceMean_Op(axes, false, false);
    mean_.associateInput(0, std::make_shared<Tensor>(*this));
    mean_.setDataType(dataType());
    mean_.setDataFormat(dataFormat());
    mean_.setBackend(mImpl->backend());
    mean_.forward();
    return mean_.getOutput(0)->clone();
}

// Tensor& Tensor::operator=(const Tensor& other) {
//     if (this == &other) {
//         return *this;
//     }
//     resize(other.dims(), other.strides());
//     setDataType(other.dataType(), false);  // do not convert existing data
//     if (other.hasImpl()) {
//         if (hasImpl()) {
//         //     copyFrom(other);
//         // } else {
//             // Perform a shallow copy only
//             setImpl(other.mImpl, other.mImplOffset);
//         }
//     } else {
//         setImpl(nullptr);
//     }
//     return *this;
// }


void Tensor::setBackend(const std::string &name, DeviceIdx_t device, bool copyFrom) {
    if (mImpl) {
        if (mImpl->device() != std::make_pair(name, device)) {
            // Backend change: create new impl, copy from old to new and replace
            // impl
            std::shared_ptr<TensorImpl> newImpl = Registrar<Tensor>::create({name, mDataType})(device, mDims);
            if (copyFrom) {
                newImpl->copyFrom(*mImpl, mImpl->size(), mImplOffset, 0);
            }
            setImpl(newImpl);
        }
    }
    else {
        mImpl = Registrar<Tensor>::create({name, mDataType})(device, mDims);
    }
    }

void Tensor::resize(const std::vector<DimSize_t>& dims,
                           std::vector<DimSize_t> strides) {
    if (dims.empty()) {  // scalar
        mDims = std::vector<DimSize_t>(0);
        mStrides = std::vector<DimSize_t>({1});
        mContiguous = true;

        computeSize(); // will set mSize to 1
        if (mImpl) {
            mImpl->resize(mDims);
        }
        return;
    }

    bool checkContiguous = true;
    if (strides.empty()) {
        strides.resize(dims.size());
        size_t expectedStride = 1;
        for (int dim = dims.size() - 1; dim >= 0; --dim) {
            strides[dim] = expectedStride;
            expectedStride *= dims[dim];
        }
        checkContiguous = false;
    } else {
        AIDGE_ASSERT(strides.size() == dims.size(),
                     "Number of strides must match number of dims");
    }

    if (mImpl && mImpl.use_count() > 1) {
        // Here we could also create a new storage for this tensor in this case
        // But, is it more likely that the user really wants this, or that he
        // did a mistake?
        AIDGE_ASSERT(dims == mDims && strides == mStrides,
                     "Cannot resize Tensor with shared storage");
    } else {
        mDims = dims;
        mStrides = strides;

        mContiguous = true;
        if (checkContiguous) {
            std::size_t expectedStride = 1;
            // std::size_t i = dims.size();
            // while ((i-- > 0) && (strides[i] == expectedStride)) {
            //     mContiguous&= (strides[i] == expectedStride);
            //     expectedStride*= dims[i];
            // }
            for (std::size_t i = dims.size() - 1; i > 0; --i) {
                if (strides[i] != expectedStride) {
                    mContiguous = false;
                    break;
                }
                expectedStride *= dims[i];
            }
            mContiguous &= (strides[0] == expectedStride);
        }

        computeSize();
        if (mImpl) {
            mImpl->resize(mDims);
        }
    }
}

std::string Tensor::toString(int precision, std::size_t offset) const {
    if (!hasImpl() || undefined()) {
        return "{}";
    }

    // Use default precision if no override provided
    precision = (precision >= 0) ? precision : Log::getPrecision();

   // Create a type-specific formatter function upfront
    std::function<std::string(void*, std::size_t)> formatter;

    switch (mDataType) {
        case DataType::Float64:
            formatter = [precision](void* ptr, std::size_t idx) {
                return fmt::format("{:.{}f}", static_cast<cpptype_t<DataType::Float64>*>(ptr)[idx], precision);
            };
            break;
        case DataType::Float32:
            formatter = [precision](void* ptr, std::size_t idx) {
                return fmt::format("{:.{}f}", static_cast<cpptype_t<DataType::Float32>*>(ptr)[idx], precision);
            };
            break;
        case DataType::Float16:
            formatter = [precision](void* ptr, std::size_t idx) {
                return fmt::format("{:.{}f}", static_cast<cpptype_t<DataType::Float16>*>(ptr)[idx], precision);
            };
            break;
        case DataType::Binary:
        case DataType::Octo_Binary:
        case DataType::Dual_Int4:
        case DataType::Dual_Int3:
        case DataType::Dual_UInt3:
        case DataType::Quad_Int2:
        case DataType::Quad_UInt2:
        case DataType::Int4:
        case DataType::UInt4:
        case DataType::Int3:
        case DataType::UInt3:
        case DataType::Int2:
        case DataType::UInt2:
        case DataType::Int8:
            formatter = [](void* ptr, std::size_t idx) {
                return fmt::format("{}", static_cast<cpptype_t<DataType::Int32>>(static_cast<cpptype_t<DataType::Int8>*>(ptr)[idx]));
            };
            break;
        case DataType::Dual_UInt4:
        case DataType::UInt8:
            formatter = [](void* ptr, std::size_t idx) {
                return fmt::format("{}", static_cast<cpptype_t<DataType::UInt32>>(static_cast<cpptype_t<DataType::UInt8>*>(ptr)[idx]));
            };
            break;
        case DataType::Int16:
            formatter = [](void* ptr, std::size_t idx) {
                return fmt::format("{}", static_cast<cpptype_t<DataType::Int16>*>(ptr)[idx]);
            };
            break;
        case DataType::Int32:
            formatter = [](void* ptr, std::size_t idx) {
                return fmt::format("{}", static_cast<cpptype_t<DataType::Int32>*>(ptr)[idx]);
            };
            break;
        case DataType::Int64:
            formatter = [](void* ptr, std::size_t idx) {
                return fmt::format("{}", static_cast<cpptype_t<DataType::Int64>*>(ptr)[idx]);
            };
            break;
        case DataType::UInt16:
            formatter = [](void* ptr, std::size_t idx) {
                return fmt::format("{}", static_cast<cpptype_t<DataType::UInt16>*>(ptr)[idx]);
            };
            break;
        case DataType::UInt32:
            formatter = [](void* ptr, std::size_t idx) {
                return fmt::format("{}", static_cast<cpptype_t<DataType::UInt32>*>(ptr)[idx]);
            };
            break;
        case DataType::UInt64:
            formatter = [](void* ptr, std::size_t idx) {
                return fmt::format("{}", static_cast<cpptype_t<DataType::UInt64>*>(ptr)[idx]);
            };
            break;
        default:
            AIDGE_ASSERT(true, "unsupported type to convert to string");
            return "{}";
    }

    if (dims().empty()) {
        return formatter(mImpl->hostPtr(), 0);
    }

    void* dataPtr = mImpl->hostPtr(mImplOffset);

    // Calculate maximum width across all elements
    std::size_t maxWidth = 0;
    for (std::size_t i = 0; i < mSize; ++i) {
        std::string value = formatter(dataPtr, i);
        maxWidth = std::max(maxWidth, value.length());
    }

    // Initialize variables similar to Python version
    std::vector<std::size_t> indexCoord(nbDims(), 0);
    const std::size_t initialDepth = nbDims() > 1 ? nbDims() - 2 : 0;
    std::size_t depth = initialDepth;
    std::size_t nbBrackets = nbDims() - 1;
    std::size_t index = 0;

    // Calculate number of lines (product of all dimensions except last)
    std::size_t nbLines = 1;
    for (std::size_t d = 0; d < nbDims() - 1; ++d) {
        nbLines *= dims()[d];
    }

    std::string result = "{";  // Using { instead of [ for C++ style

    for (std::size_t l = 0; l < nbLines; ++l) {
        // Add spacing and opening braces
        if (l != 0) {
            result += std::string(1 + offset, ' ');
        }
        result += std::string(nbDims() - 1 - nbBrackets, ' ') +
                 std::string(nbBrackets, '{');

        // Print numbers of a line
        for (DimSize_t i = 0; i < dims().back(); ++i) {
            std::string value = formatter(dataPtr, index);
            result += std::string(1 + maxWidth - value.length(), ' ') + value;
            if (i + 1 < dims().back()) {
                result += ',';
            }
            ++index;
        }

        // Check for end
        if (index == mSize) {
            result += std::string(nbDims(), '}');
            return result;
        } else {
            // Update coordinates and depth
            while (indexCoord[depth] + 1 >= static_cast<std::size_t>(dims()[depth])) {
                indexCoord[depth] = 0;
                --depth;
            }
            ++indexCoord[depth];
            nbBrackets = initialDepth - depth + 1;
            depth = initialDepth;
        }

        // Add closing braces and newlines
        result += std::string(nbBrackets, '}') + ",\n";
        if (nbBrackets > 1) {
            result += '\n';
        }
    }

    return result;
}


Tensor Tensor::extract(
    const std::vector<std::size_t>& fixedCoord) const {
    AIDGE_ASSERT(isContiguous(), "Tensor must be contiguous");
    AIDGE_ASSERT(fixedCoord.size() <= mDims.size(),
                 "Number of coordinates is higher than number of dimensions");

    Tensor subTensor(mDataType);
    subTensor.resize(
        std::vector<size_t>(mDims.cbegin() + fixedCoord.size(), mDims.cend()),
        std::vector<size_t>(mStrides.cbegin() + fixedCoord.size(),
                            mStrides.cend()));
    subTensor.setBackend(mImpl->backend(), mImpl->device().second);
    subTensor.setImpl(mImpl, mImplOffset + getStorageIdx(fixedCoord));
    return subTensor;
}

Tensor Tensor::extract(
    const std::vector<std::size_t>& startCoord,
    const std::vector<std::size_t>& dims) const {
    AIDGE_ASSERT(isContiguous(), "Tensor must be contiguous");
    AIDGE_ASSERT(startCoord.size() == mDims.size(),
                 "Coordinates does not match number of dimensions");

    Tensor subTensor(mDataType);
    subTensor.resize(dims, mStrides);
    subTensor.setBackend(mImpl->backend(), mImpl->device().second);
    subTensor.setImpl(mImpl, mImplOffset + getStorageIdx(startCoord));
    return subTensor;
}

void Tensor::makeContiguous() {
    if (!mImpl || isContiguous()) {
        return;
    }

    // Block so that mImpl ref count is 1 for resize()
    {
        // Create a new storage that will be contiguous
        std::shared_ptr<TensorImpl> newImpl = Registrar<Tensor>::create(
            {mImpl->backend(), mDataType})(mImpl->device().second, mDims);
        // Copy elements from old to new storage
        std::size_t idx = 0;
        while (idx < mSize) {
            const std::size_t storageIdx = getStorageIdx(getCoord(idx));

            // Determine the size of the contiguous chunk
            std::size_t copySize = 1;
            while (idx + copySize < mSize &&
                   getStorageIdx(getCoord(idx + copySize)) ==
                       storageIdx + copySize) {
                ++copySize;
            }

            // Perform a single copy for the contiguous chunk
            newImpl->copy(mImpl->rawPtr(mImplOffset + storageIdx), copySize,
                          idx);

            // Move to the next index after the contiguous chunk
            idx += copySize;
        }
        // Replace old storage by new, contiguous, storage
        setImpl(newImpl);
    }

    // Resize tensor without strides => tensor is now contiguous
    resize(mDims);
}

void Tensor::copyCast(const Tensor& src) {
    if (&src == this) {
        return;
    }

    AIDGE_ASSERT(src.isContiguous(), "cannot copy-cast non-contiguous tensor");

    // Current Tensor has necessarily a data type, but may not have backend
    if (!hasImpl()) {
        // If no backend was set for the current tensor, use the same as src
        const auto deviceSrc = src.getImpl()->device();
        setBackend(deviceSrc.first, deviceSrc.second);
    }
    resize(src.dims());

    AIDGE_ASSERT(src.getImpl()->device() == getImpl()->device(),
                 "cannot copy-cast from a different backend/device");
    getImpl()->copyCast(src.getImpl()->rawPtr(src.mImplOffset), src.dataType(),
                        src.size(), mImplOffset);
}

void Tensor::copyFrom(const Tensor& src) {
    if (&src == this) {
        return;
    }

    AIDGE_ASSERT(src.isContiguous(), "cannot copy from non-contiguous tensor");

    // Current Tensor has necessarily a data type, but may not have backend
    if (!hasImpl()) {
        // If no backend was set for the current tensor, use the same as src
        const auto deviceSrc = src.getImpl()->device();
        setBackend(deviceSrc.first, deviceSrc.second);
    }
    resize(src.dims());

    AIDGE_ASSERT(src.dataType() == dataType(),
                 "cannot copy from a different data type");
    getImpl()->copyFrom(*(src.getImpl()), src.size(), src.mImplOffset,
                        mImplOffset);
}

void Tensor::copyTranspose(const Tensor& src, const std::vector<DimSize_t>& transpose) {
    std::vector<DimSize_t> newDims;
    for (std::size_t i = 0; i < src.dims().size(); ++i) {
        newDims.push_back(src.dims()[transpose[i]]);
    }

    std::vector<std::size_t> newStrides(newDims.size(), 1);
    for (size_t i = 0; i < newDims.size(); ++i) {
        for (size_t j = i + 1; j < newDims.size(); ++j) {
            newStrides[i] *= newDims[j];
        }
    }

    AIDGE_ASSERT(src.getImpl(), "Tensor::copyTranspose(): an implementation is required for src Tensor!");
    AIDGE_ASSERT(mImpl, "Tensor::copyTranspose(): an implementation is required, use setBackend() first!");
    std::shared_ptr<TensorImpl> newImpl = Registrar<Tensor>::create({mImpl->backend(), mDataType})(mImpl->device().second, newDims);

    std::vector<size_t> indices(newDims.size(), 0);
    for (size_t i = 0; i < src.size(); ++i) {
        size_t idx = 0;
        // Permute indices based on OutputDimsOrder attr
        for (int j = newDims.size() -1; j >=0; --j) {
            idx += indices[transpose[j]] * newStrides[j];
        }

        // Copy the value in output
        newImpl->copy(src.getImpl()->rawPtr(i), 1, idx);

        // Update indices for the next iteration
        for (int j = newDims.size() - 1; j >= 0; --j) {
            if (indices[j] < src.dims()[j] - 1) {
                indices[j]++;
                break;
            }
            else {
                indices[j] = 0;
            }
        }
    }

    resize(newDims);
    setImpl(newImpl);
}

void Tensor::copyTranspose(const Tensor& src, const DataFormatTranspose& transpose) {
    copyTranspose(src, std::vector<DimSize_t>(transpose.begin(), transpose.end()));
}

void Tensor::copyCastFrom(const Tensor& src,
                                 std::shared_ptr<Tensor>& movedSrcPtr) {
    if (&src == this) {
        return;
    }

    AIDGE_ASSERT(src.isContiguous(),
                 "cannot copy-cast from non-contiguous tensor");

    // Current Tensor has necessarily a data type, but may not have backend
    if (!getImpl()) {
        // If no backend was set for the current tensor, use the same as src
        const auto deviceSrc = src.getImpl()->device();
        setBackend(deviceSrc.first, deviceSrc.second);
    }
    resize(src.dims());

    if (dataType() != src.dataType()) {
        // First move data to the target device (only if needed)
        const auto device = getImpl()->device();
        const Tensor& movedSrc =
            src.refFrom(movedSrcPtr, device.first, device.second);
        // Second, copy-cast data (necessary)
        getImpl()->copyCast(movedSrc.getImpl()->rawPtr(movedSrc.mImplOffset),
                            movedSrc.dataType(), movedSrc.size(), mImplOffset);
    } else {
        // Directly copy, no conversion necessary
        // Avoid making a double copy if both data type and device are the same
        getImpl()->copyFrom(*(src.getImpl()), src.size(), src.mImplOffset,
                            mImplOffset);
    }
}

Tensor& Tensor::refContiguous(std::shared_ptr<Tensor>& fallback) {
    // Scott Meyers' solution to avoid code duplication
    return const_cast<Tensor&>(
        static_cast<const Tensor&>(*this).refContiguous(fallback));
}

const Tensor& Tensor::refContiguous(
    std::shared_ptr<Tensor>& fallback) const {
    AIDGE_ASSERT(getImpl(),
                 "no backend was set for tensor, cannot refCast() it");

    if (isContiguous()) {
        return *this;
    } else {
        if (this != fallback.get()) {
            // Shallow copy to fallback
            *fallback = *this;
        }

        // Make fallback contiguous
        fallback->makeContiguous();
        return *fallback;
    }
}

Tensor& Tensor::refCast(std::shared_ptr<Tensor>& fallback,
                                      const DataType& dt) {
    // Scott Meyers' solution to avoid code duplication
    return const_cast<Tensor&>(
        static_cast<const Tensor&>(*this).refCast(fallback, dt));
}

const Tensor& Tensor::refCast(std::shared_ptr<Tensor>& fallback,
                                            const DataType& dt) const {
    AIDGE_ASSERT(getImpl(),
                 "no backend was set for tensor, cannot refCast() it");

    if (dt == dataType()) {
        return *this;
    } else {
        if (this == fallback.get()) {
            // if refFrom() was called before, just change the type
            fallback->setDataType(dt);
        } else {
            AIDGE_ASSERT(isContiguous(),
                         "cannot refCast non-contiguous tensor");

            if (!fallback) {
                fallback = std::make_shared<Tensor>(dt);
            } else {
                fallback->setDataType(
                    dt, false);  // don't keep previous data (no copy)
            }

            const auto device = getImpl()->device();
            fallback->setBackend(device.first, device.second,
                                 false);  // don't keep previous data (no copy)
            fallback->resize(dims());
            fallback->getImpl()->copyCast(getImpl()->rawPtr(mImplOffset),
                                          dataType(), size(),
                                          fallback->mImplOffset);
        }
        return *fallback;
    }
}

Tensor& Tensor::refFrom(std::shared_ptr<Tensor>& fallback,
                                      const std::string& backend,
                                      DeviceIdx_t device) {
    // Scott Meyers' solution to avoid code duplication
    return const_cast<Tensor&>(
        static_cast<const Tensor&>(*this).refFrom(fallback, backend, device));
}

const Tensor& Tensor::refFrom(std::shared_ptr<Tensor>& fallback,
                                            const std::string& backend,
                                            DeviceIdx_t device) const {
    AIDGE_ASSERT(getImpl(),
                 "no backend was set for tensor, cannot refFrom() it");

    if (std::make_pair(backend, device) == getImpl()->device()) {
        return *this;
    } else {
        if (this == fallback.get()) {
            // if refCast() was called before, just change the backend
            fallback->setBackend(backend, device);
        } else {
            AIDGE_ASSERT(isContiguous(),
                         "cannot refFrom non-contiguous tensor");

            if (!fallback) {
                fallback = std::make_shared<Tensor>(dataType());
            } else {
                fallback->setDataType(
                    dataType(), false);  // don't keep previous data (no copy)
            }

            fallback->setBackend(backend, device,
                                 false);  // don't keep previous data (no copy)
            fallback->resize(dims());
            fallback->getImpl()->copyFrom(*getImpl(), size(), mImplOffset,
                                          fallback->mImplOffset);
        }
        return *fallback;
    }
}

Tensor& Tensor::ref(std::shared_ptr<Tensor>& fallback,
                                  const DataType& dt,
                                  const std::string& backend,
                                  DeviceIdx_t device) {
    // Scott Meyers' solution to avoid code duplication
    return const_cast<Tensor&>(
        static_cast<const Tensor&>(*this).ref(fallback, dt, backend, device));
}

const Tensor& Tensor::ref(std::shared_ptr<Tensor>& fallback,
                                        const DataType& dt,
                                        const std::string& backend,
                                        DeviceIdx_t device) const {
    AIDGE_ASSERT(getImpl(), "no backend was set for tensor, cannot ref() it");

    if (dt == dataType() &&
        std::make_pair(backend, device) == getImpl()->device()) {
        return *this;
    } else {
        // Change fallback type, backend & device, without any data copy
        if (!fallback) {
            fallback = std::make_shared<Tensor>(dt);
        } else {
            fallback->setDataType(dt,
                                  false);  // don't keep previous data (no copy)
        }

        fallback->setBackend(backend, device,
                             false);  // don't keep previous data (no copy)
        fallback->resize(dims());
        return *fallback;
    }
}


std::vector<std::size_t>
Tensor::toCoord(const std::vector<DimSize_t>& dimensions, std::size_t index) {
    std::vector<std::size_t> coord(dimensions.size());
    std::size_t i = dimensions.size();

    while (i-- > 0) {
        coord[i] = (index % dimensions[i]);
        index /= dimensions[i];
    }
    return coord;
}


std::size_t Tensor::toIndex(const std::vector<DimSize_t> &dimensions, const std::vector<std::size_t>& coords) {
    AIDGE_ASSERT(coords.size() == dimensions.size(), "Tensor::getIdx(): Coordinates does not match number of dimensions.\n\tCoords : {}\n\tDimensions: {}",coords, dimensions);
    std::size_t index = 0;
    std::size_t dimensions_s = 1; // stride
    std::size_t i = dimensions.size();
    while (i-- > 0) {
        index += coords[i] * dimensions_s;
        dimensions_s *= dimensions[i];
    }
    return index;
}

template<typename T>
bool Tensor::isInBounds(const std::vector<DimSize_t>& dimensions, const std::vector<T>& coords){
    AIDGE_ASSERT(coords.size() == dimensions.size(),
                 "Coordinates({}) to compare have not "
                 "the same number of dimension as tensor dimensions({}), aborting.",
                 coords,
                 dimensions);
    bool isInBound {true};
    for(std::size_t i = 0 ; i < coords.size() && isInBound; ++i ){
        isInBound = coords[i] >= 0 && coords[i] < static_cast<T>(dimensions[i]) ;
    }
    return isInBound;
}


bool Tensor::isInBounds(const std::vector<DimSize_t>& dimensions, const std::size_t index){
    return index < std::accumulate(dimensions.cbegin(), dimensions.cend(), std::size_t(1), std::multiplies<std::size_t>());
}

template bool Tensor::isInBounds(const std::vector<DimSize_t>& dimensions, const std::vector<std::int16_t>& coords);
template bool Tensor::isInBounds(const std::vector<DimSize_t>& dimensions, const std::vector<std::int32_t>& coords);
template bool Tensor::isInBounds(const std::vector<DimSize_t>& dimensions, const std::vector<std::int64_t>& coords);
template bool Tensor::isInBounds(const std::vector<DimSize_t>& dimensions, const std::vector<std::size_t>& coords);
template bool Tensor::isInBounds(const std::vector<DimSize_t>& dimensions, const std::vector<float>& coords);
template bool Tensor::isInBounds(const std::vector<DimSize_t>& dimensions, const std::vector<double>& coords);


std::set<std::string> Tensor::getAvailableBackends() {
    std::set<std::string> backendsList;
    for (const auto& tupleKey : Registrar<Tensor>::getKeys()) {
        backendsList.insert(std::get<0>(tupleKey));
    }
    return backendsList;
}
}  // namespace Aidge
