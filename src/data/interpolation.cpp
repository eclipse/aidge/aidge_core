/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/data/Interpolation.hpp"

#include <algorithm>  // std::clamp
#include <bitset>
#include <cmath>      // std::ceil, std::floor
#include <cstddef>    // std::size_t
#include <cstdint>    // std::int64_t
#include <stdexcept>  // std::runtime_error
#include <utility>    // std::make_pair, std::set
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/data/half.hpp"
#include "aidge/operator/Pad.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Log.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

template <typename T>
[[noreturn]] T
Interpolation::interpolate(const std::vector<float> & /*originalIndex*/,
                           const std::vector<Point<T>> & /*points*/,
                           const Mode /*interpMode*/) {
    AIDGE_THROW_OR_ABORT(
        std::runtime_error,
        "interpolate() is backend dependendant and should be"
        "called from derived classes: Interpolation<Backend>::interpolate(...)"
        "Meaning that for CPU backend, InterpolationCPU::interpolate() should "
        "be called.");
}

std::vector<float> Interpolation::untransformCoordinates(
    const std::vector<DimSize_t> &transformedCoords,
    const std::vector<DimSize_t> &inputDims,
    const std::vector<DimSize_t> &outputDims,
    const Interpolation::CoordinateTransformation coordTransfoMode) {
    AIDGE_ASSERT(
        inputDims.size() == outputDims.size(),
        "Interpolate::untransformCoordinates: input and output coordinates "
        "dimension number mismatch, they should be equal."
        "Got inputDims({}) and outputDims ({}).",
        inputDims,
        outputDims);
    AIDGE_ASSERT(
        transformedCoords.size() == outputDims.size(),
        "Interpolate::untransformCoordinates: coordinates dimension mismatch, "
        "transformed coords number should be equal to output dimension number."
        "Got coords to transform ({}) and outputDims ({})",
        transformedCoords,
        outputDims);
    std::vector<float> originalCoords(transformedCoords.size());

    for (DimIdx_t i = 0; i < transformedCoords.size(); ++i) {
        float scale = static_cast<float>(outputDims[i]) /
                      static_cast<float>(inputDims[i]);

        switch (coordTransfoMode) {
        case CoordinateTransformation::AlignCorners:
            AIDGE_THROW_OR_ABORT(
                std::runtime_error,
                "Interpolation::untransformCoords: Unsupported Coordinate "
                "transform : AlignCorners");
            break;
        case CoordinateTransformation::Asymmetric:
            originalCoords[i] = transformedCoords[i] / scale;
            break;
        case CoordinateTransformation::HalfPixel:
            originalCoords[i] = (transformedCoords[i] + 0.5) / scale - 0.5;
            break;
        case CoordinateTransformation::HalfPixelSymmetric:
            AIDGE_THROW_OR_ABORT(
                std::runtime_error,
                "Interpolation::untransformCoords: Unsupported Coordinate "
                "transform : HalfPixelSymmetric");
            break;
        case Interpolation::CoordinateTransformation::PytorchHalfPixel:

            AIDGE_THROW_OR_ABORT(
                std::runtime_error,
                "Interpolation::untransformCoords: Unsupported Coordinate "
                "transform : PytorchHalfPixel");
            break;
        }
    }
    return originalCoords;
}

/**
 * @details Generates a list of all neighbours of a given coordinate.
 * Since the coordinates are floating points as they are the result of
 * Interpolation::untransformCoords, they are approximation of coordinates in
 * originalTensor frame from coordinates in interpolatedTensor frame.
 *
 * So to retrieve the neghbouring values, we must apply either floor() or
 * ceil() to each coordinate.
 *
 * In order to generate the list of all combinations
 * available, we simply iterate through the bits of each values from 0 to
 * tensorDims.
 * @example : in 2 dimensions , we  have the point (1.3, 3.4)
 * we iterate up to 2^2 - 1 and
 * 0 = 0b00 -> (floor(x), floor(y)) = (1,3)
 * 1 = 0b01 -> (floor(x), ceil(y))  = (1,4)
 * 2 = 0b10 -> (ceil(x) , floor(y)) = (2,3)
 * 3 = 0b11 -> (ceil(x) , ceil(y))  = (2,4)
 */
template <typename T>
std::set<Interpolation::Point<T>>
Interpolation::retrieveNeighbours(const T *tensorValues,
                                  const std::vector<DimSize_t> &tensorDims,
                                  const std::vector<float> &coords,
                                  const PadBorderType paddingMode) {

    Log::debug("retrieveNeighbours: TensorDims : {}", tensorDims);
    Log::debug("retrieveNeighbours: coords to interpolate : {}", coords);

    // Will retrieve out of bound values depending on given padding mode.
    // auto retrieveOutOfBoundValue =
    //     [&tensorValues, &tensorDims, &paddingMode](Coords coord) -> T {
    //     std::vector<DimSize_t> rectifiedCoord;
    //     rectifiedCoord.reserve(coord.size());
    //     switch (paddingMode) {
    //     case Aidge::PadBorderType::Edge: {
    //         for (DimSize_t i = 0; i < coord.size(); ++i) {
    //             rectifiedCoord[i] = coord[i] < 0 ? 0 : tensorDims[i] - 1;
    //         }
    //         return tensorValues[Tensor::getIdx(tensorDims, rectifiedCoord)];
    //     }
    //     case Aidge::PadBorderType::Zero: {
    //         return static_cast<T>(0);
    //     }
    //     default: {
    //         AIDGE_THROW_OR_ABORT(
    //             std::runtime_error,
    //             "Unsupported padding mode as of now for interpolation.");
    //     }
    //     }
    // };

    std::set<Point<T>> neighbours;
    const std::size_t nbNeighbours = std::size_t(1) << tensorDims.size();
    Coords neighbourCoords(tensorDims.size());

    for (std::size_t i = 0; i < nbNeighbours; ++i) {
        const std::bitset<MaxDim> bits = std::bitset<MaxDim>{i};
        for (size_t j = 0; j < tensorDims.size(); ++j) {
            neighbourCoords[j] =
                bits[j] == 0 ? std::ceil(coords[j]) : std::floor(coords[j]);
        }

        T value;
        if (Tensor::isInBounds(tensorDims, neighbourCoords)) {
            // cast from unsigned to signed won't create problem as we ensured
            // that all neighboursCoords values are > 0 with isInBounds
            value = tensorValues[Tensor::toIndex(
                tensorDims,
                std::vector<DimSize_t>(neighbourCoords.begin(),
                                       neighbourCoords.end()))];
        } else {
            switch (paddingMode) {
                case PadBorderType::Edge:
                    for (DimSize_t j = 0; j < tensorDims.size(); ++j) {
                        neighbourCoords[j] = (neighbourCoords[j] < 0) ? 0 :
                                                ((neighbourCoords[j] >= static_cast<std::int64_t>(tensorDims[j])) ? (tensorDims[j] - 1) :
                                                    neighbourCoords[j]);
                    }
                    value = tensorValues[Tensor::toIndex(
                                tensorDims,
                                std::vector<DimSize_t>(neighbourCoords.begin(),
                                                    neighbourCoords.end()))];
                    break;
                case PadBorderType::Zero:
                    value = static_cast<T>(0);
                    break;
                default:
                    AIDGE_THROW_OR_ABORT(
                        std::runtime_error,
                        "Unsupported padding mode as of now for interpolation.");
            }
        }
        neighbours.insert(std::make_pair(neighbourCoords, value));
    }
    Log::debug("Interpolation::retrieveNeighbours(): neighbourCoords: {}",
               neighbours);
    return neighbours;
}

template std::set<Interpolation::Point<int16_t>>
Interpolation::retrieveNeighbours(const int16_t *tensorValues,
                                  const std::vector<DimSize_t> &tensorDims,
                                  const std::vector<float> &coords,
                                  const PadBorderType paddingMode);

template std::set<Interpolation::Point<int32_t>>
Interpolation::retrieveNeighbours(const int32_t *tensorValues,
                                  const std::vector<DimSize_t> &tensorDims,
                                  const std::vector<float> &coords,
                                  const PadBorderType paddingMode);

template std::set<Interpolation::Point<int64_t>>
Interpolation::retrieveNeighbours(const int64_t *tensorValues,
                                  const std::vector<DimSize_t> &tensorDims,
                                  const std::vector<float> &coords,
                                  const PadBorderType paddingMode);

template std::set<Interpolation::Point<half_float::half>>
Interpolation::retrieveNeighbours(const half_float::half *tensorValues,
                                  const std::vector<DimSize_t> &tensorDims,
                                  const std::vector<float> &coords,
                                  const PadBorderType paddingMode);

template std::set<Interpolation::Point<float>>
Interpolation::retrieveNeighbours(const float *tensorValues,
                                  const std::vector<DimSize_t> &tensorDims,
                                  const std::vector<float> &coords,
                                  const PadBorderType paddingMode);

template std::set<Interpolation::Point<double>>
Interpolation::retrieveNeighbours(const double *tensorValues,
                                  const std::vector<DimSize_t> &tensorDims,
                                  const std::vector<float> &coords,
                                  const PadBorderType paddingMode);

} // namespace Aidge
