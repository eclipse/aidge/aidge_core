/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/data/DataFormat.hpp"

Aidge::DataFormatTranspose Aidge::getDataFormatTranspose(const DataFormat& src, const DataFormat& dst) {
    // Permutation array from default format to src format
    const auto srcDefToFormat = DataFormatTransposeDict[static_cast<int>(src)];
    // Permutation array from default format to dst format
    const auto dstDefToFormat = DataFormatTransposeDict[static_cast<int>(dst)];
    // Compute permutation array from src format to default format:
    DataFormatTranspose srcFormatToDef{};
    for (size_t i = 0; i < srcDefToFormat.size(); ++i) {
        if (srcDefToFormat[i] > 0) {
            srcFormatToDef[srcDefToFormat[i] - 1] = i;
        }
        else {
            srcFormatToDef[i] = i;
        }
    }

    // Compute permutation array from src format to dst format:
    DataFormatTranspose srcToDst{};
    for (size_t i = 0; i < dstDefToFormat.size(); ++i) {
        if (dstDefToFormat[srcFormatToDef[i]] > 0) {
            srcToDst[i] = dstDefToFormat[srcFormatToDef[i]] - 1;
        }
        else {
            srcToDst[i] = srcFormatToDef[i];
        }
    }

    return srcToDst;
}
