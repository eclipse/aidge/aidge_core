/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/graph/Matching.hpp"

#include <algorithm>   // std::find_if
#include <cctype>      // std::isspace
#include <cstddef>     // std::size_t
#include <memory>
#include <set>
#include <string>      // std::stoi
#include <utility>     // std::pair
#include <vector>

#include <fmt/color.h>

#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/utils/Log.hpp"

static void removeLeadingWhitespace(std::string& str) {
    str.erase(str.begin(),
        std::find_if(str.begin(),
                    str.end(),
                    [](char c) { return !std::isspace(c); }));
}

////////////////////////////////////////////////////////////

Aidge::SinglePassGraphMatching::Context::Context() = default;
Aidge::SinglePassGraphMatching::Context::Context(const Context& other) = default;
Aidge::SinglePassGraphMatching::Context& Aidge::SinglePassGraphMatching::Context::operator=(const Context& other) = default;
Aidge::SinglePassGraphMatching::Context::~Context() noexcept = default;

////////////////////////////////////////////////////////////

Aidge::SinglePassGraphMatching::MatchingResult::MatchingResult() : graph(std::make_shared<GraphView>()), startNode(nullptr) {}
Aidge::SinglePassGraphMatching::MatchingResult::MatchingResult(const Aidge::SinglePassGraphMatching::MatchingResult& other) {
    graph = std::make_shared<GraphView>(*(other.graph.get()));
    anchors = other.anchors;
    startNode = other.startNode;
}
Aidge::SinglePassGraphMatching::MatchingResult& Aidge::SinglePassGraphMatching::MatchingResult::operator=(const Aidge::SinglePassGraphMatching::MatchingResult& other) {
    graph = std::make_shared<GraphView>(*(other.graph.get()));
    anchors = other.anchors;
    startNode = other.startNode;
    return *this;
}
Aidge::SinglePassGraphMatching::MatchingResult::~MatchingResult() noexcept = default;

//////////////////////////////////////////////////////////

Aidge::SinglePassGraphMatching::SinglePassGraphMatching(const Aidge::SinglePassGraphMatching& other) = default;
Aidge::SinglePassGraphMatching& Aidge::SinglePassGraphMatching::operator=(const Aidge::SinglePassGraphMatching& other) = default;
Aidge::SinglePassGraphMatching::~SinglePassGraphMatching() noexcept = default;

std::set<Aidge::SinglePassGraphMatching::MatchingResult> Aidge::SinglePassGraphMatching::match(const std::string& query, bool disjoint) {
    Context ctx;
    ctx.query = query;
    std::set<MatchingResult> matches;

    while (matchSequence(ctx, matches) || matchNodeOrBlock(ctx, matches)) {
        removeLeadingWhitespace(ctx.query);
        if (!ctx.query.empty() && ctx.query[0] == ';') {
            ctx.query.erase(0, 1);
        }
        else {
            break;
        }
    }

    removeLeadingWhitespace(ctx.query);
    if (!ctx.query.empty()) {
        Log::warn("Syntax error, unable to parse remaining query: {}", ctx.query);
    }

    if (disjoint) {
        matches = filterLonguestDisjoint(matches);
    }
    Log::info("Graph matching complete.\nFound {} matches for the query", matches.size());
    return matches;
}

Aidge::SinglePassGraphMatching::MatchingResult Aidge::SinglePassGraphMatching::matchFrom(std::shared_ptr<Node> startNode, const std::string& query) {
    Context ctx;
    ctx.query = query;
    ctx.startNode = startNode;
    std::set<MatchingResult> matches;

    while (matchSequence(ctx, matches) || matchNodeOrBlock(ctx, matches)) {
        removeLeadingWhitespace(ctx.query);
        if (!ctx.query.empty() && ctx.query[0] == ';') {
            ctx.query.erase(0, 1);
        }
        else {
            break;
        }
    }

    removeLeadingWhitespace(ctx.query);
    if (!ctx.query.empty()) {
        Log::warn("Syntax error, unable to parse remaining query: {}", ctx.query);
    }

    AIDGE_INTERNAL_ASSERT(matches.size() <= 1);
    return (!matches.empty()) ? *matches.begin() : MatchingResult();
}

std::set<Aidge::SinglePassGraphMatching::MatchingResult> Aidge::SinglePassGraphMatching::filterLonguestDisjoint(const std::set<MatchingResult>& matches) {
    // Sort matches by highest number of nodes first, thanks to the CompareMatchingResultSize function
    std::set<MatchingResult, CompareMatchingResultSize> sortedMatches(matches.begin(), matches.end());
    // Keep all the nodes that are already in previous (selected) matches
    std::set<NodePtr> selectedNodes;
    std::set<MatchingResult> filteredMatches;

    for (const auto& match : sortedMatches) {
        const auto& nodes = match.graph->getNodes();
        bool isNonOverlapping = true;
        for (const auto& node : nodes) {
            if (selectedNodes.find(node) != selectedNodes.end()) {
                isNonOverlapping = false;
                break;
            }
        }

        if (isNonOverlapping) {
            // If no node of the current match is already in a previous match,
            // the match is disjoint from previous matches and can be kept!
            filteredMatches.insert(match);
            selectedNodes.insert(nodes.begin(), nodes.end());
        }
    }

    return filteredMatches;
}

bool Aidge::SinglePassGraphMatching::matchNodeOrBlock(Context& ctx, std::set<MatchingResult>& matches) {
    auto newCtx = ctx;
    Log::debug("{}node-or-block", std::string(2*newCtx.depth, ' '));
    auto newMatches = matches;
    ++newCtx.depth;

    // (BLOCK | NODE)
    if (!matchBlock(newCtx, newMatches) && !matchNode(newCtx, newMatches)) {
        Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
        return false;
    }

    // QUANTIFIER?
    bool matchMore = false;
    std::size_t matchQuantity = 0;
    removeLeadingWhitespace(newCtx.query);
    if (!newCtx.query.empty() && (newCtx.query[0] == '?' || newCtx.query[0] == '*')) {
        AIDGE_ASSERT(!(ctx.firstSequence && ctx.firstNode),
            "Ill-formed query; the root node cannot be optional in query at: {}", ctx.query);

        for (const auto& match : matches) {
            bool found = false;
            for (const auto& newMatch : newMatches) {
                if (match.graph->rootNode() == newMatch.graph->rootNode()) {
                    found = true;
                }
            }

            if (!found) {
                newMatches.insert(match);
            }
        }

        if (newCtx.query[0] == '*') {
            matchMore = true;
        }

        newCtx.query.erase(0, 1);
    }
    else if (!newCtx.query.empty() && newCtx.query[0] == '+') {
        newCtx.query.erase(0, 1);
        matchMore = true;
    }
    else if (!newCtx.query.empty() && newCtx.query[0] == '{') {
        newCtx.query.erase(0, 1);

        removeLeadingWhitespace(newCtx.query);
        const auto endQuantity = std::find_if(newCtx.query.begin(), newCtx.query.end(),
            [](char c) { return !isdigit(c); });
        if (endQuantity != newCtx.query.begin()) {
            matchQuantity = std::stoi(newCtx.query.substr(0, endQuantity - newCtx.query.begin()));
            newCtx.query = newCtx.query.substr(endQuantity - newCtx.query.begin());
        }
        else {
            Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
            return false;
        }

        if (matchQuantity == 0) {
            Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
            return false;
        }

        removeLeadingWhitespace(newCtx.query);
        if (!newCtx.query.empty() && newCtx.query[0] == '}') {
            newCtx.query.erase(0, 1);
        }
        else {
            Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
            return false;
        }

        if (matchQuantity > 1) {
            matchMore = true;
        }
    }

    if (matchMore) {
        std::set<MatchingResult> additionalMatches;

        do {
            auto additionalCtx = ctx;
            additionalCtx.firstNode = newCtx.firstNode;
            additionalCtx.firstSequence = newCtx.firstSequence;
            additionalCtx.anchors = newCtx.anchors;
            ++additionalCtx.depth;
            additionalMatches = newMatches;

            if (!matchBlock(additionalCtx, additionalMatches) && !matchNode(additionalCtx, additionalMatches)) {
                Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
                return false;
            }

            for (const auto& additionalMatch : additionalMatches) {
                for (auto& match : newMatches) {
                    if (match.graph->rootNode() == additionalMatch.graph->rootNode()) {
                        match.graph = std::make_shared<GraphView>(*(additionalMatch.graph.get()));
                        match.anchors = additionalMatch.anchors;
                        match.startNode = additionalMatch.startNode;
                        break;
                    }
                }
            }

            --matchQuantity;
        }
        while (!additionalMatches.empty() && matchQuantity > 1);
    }

    --newCtx.depth;
    ctx = newCtx;
    matches = newMatches;
    return true;
}

bool Aidge::SinglePassGraphMatching::matchBlock(Context& ctx, std::set<MatchingResult>& matches) {
    auto newCtx = ctx;
    Log::debug("{}block", std::string(2*newCtx.depth, ' '));
    auto newMatches = matches;
    ++newCtx.depth;

    // '('
    removeLeadingWhitespace(newCtx.query);
    if (!newCtx.query.empty() && newCtx.query[0] == '(') {
        newCtx.query.erase(0, 1);
    }
    else {
        Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
        return false;
    }

    // SEQ | PAR | BLOCK | ALT | NODE
    if (!matchSequence(newCtx, newMatches)
        && !matchParallel(newCtx, newMatches)
        && !matchAlternative(newCtx, newMatches)
        && !matchBlock(newCtx, newMatches)
        && !matchNode(newCtx, newMatches))
    {
        Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
        return false;
    }

    // ')'
    removeLeadingWhitespace(newCtx.query);
    if (!newCtx.query.empty() && newCtx.query[0] == ')') {
        newCtx.query.erase(0, 1);
    }
    else {
        Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
        return false;
    }

    --newCtx.depth;
    ctx = newCtx;
    matches = newMatches;
    return true;
}

bool Aidge::SinglePassGraphMatching::matchSequence(Context& ctx, std::set<MatchingResult>& matches) {
    auto newCtx = ctx;
    Log::debug("{}sequence", std::string(2*newCtx.depth, ' '));
    auto newMatches = matches;
    ++newCtx.depth;

    if (!ctx.inSequence) {
        newCtx.inSequence = true;
        newCtx.firstNode = true;
    }

    // NODE_OR_BLOCK
    if (!matchNodeOrBlock(newCtx, newMatches)) {
        Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
        return false;
    }

    newCtx.firstNode = false;

    bool found = false;
    while (true) {
        // (EDGE NODE_OR_BLOCK)+
        //   EDGE
        if (matchEdge(newCtx, newMatches)) {
            found = true;
        }
        else {
            break;
        }

        //   NODE_OR_BLOCK
        if (!matchNodeOrBlock(newCtx, newMatches)) {
            Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
            return false;
        }
    }

    if (!found) {
        Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
        return false;
    }

    if (!ctx.inSequence) {
        newCtx.inSequence = false;
    }

    --newCtx.depth;
    ctx = newCtx;
    matches = newMatches;
    return true;
}

bool Aidge::SinglePassGraphMatching::matchParallel(Context& ctx, std::set<MatchingResult>& matches) {
    auto newCtx = ctx;
    Log::debug("{}parallel", std::string(2*newCtx.depth, ' '));
    ++newCtx.depth;
    auto newMatches = matches;

    // NODE_OR_BLOCK
    auto parCtx = newCtx;
    if (!matchNodeOrBlock(parCtx, newMatches)) {
        Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
        return false;
    }
    newCtx.query = parCtx.query;

    bool found = false;
    while (true) {
        // ('&' NODE_OR_BLOCK)+
        //   '&'
        removeLeadingWhitespace(newCtx.query);
        if (!newCtx.query.empty() && newCtx.query[0] == '&') {
            newCtx.query.erase(0, 1);
            found = true;
        }
        else {
            break;
        }

        //   NODE_OR_BLOCK
        // reset the ctx to the beginning
        parCtx = newCtx;
        // reset the startNode to the beginning
        for (auto& newMatch : newMatches) {
            for (const auto& match : matches) {
                if (match.graph->rootNode() == newMatch.graph->rootNode()) {
                    newMatch.startNode = match.startNode;
                }
            }
        }

        if (!matchNodeOrBlock(parCtx, newMatches)) {
            Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
            return false;
        }
        newCtx.query = parCtx.query;
    }

    // Keep the last context for further query
    newCtx = parCtx;

    if (!found) {
        Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
        return false;
    }

    --newCtx.depth;
    ctx = newCtx;
    matches = newMatches;
    return true;
}

bool Aidge::SinglePassGraphMatching::matchAlternative(Context& ctx, std::set<MatchingResult>& matches) {
    auto newCtx = ctx;
    Log::debug("{}alternative", std::string(2*newCtx.depth, ' '));
    ++newCtx.depth;
    std::set<MatchingResult> newMatches;

    // NODE_OR_BLOCK
    auto altCtx = newCtx;
    auto altMatches = matches;
    if (!matchNodeOrBlock(altCtx, altMatches)) {
        Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
        return false;
    }
    newCtx.query = altCtx.query;
    newCtx.anchors.insert(altCtx.anchors.begin(), altCtx.anchors.end());
    bool firstSequence = altCtx.firstSequence;
    bool firstNode = altCtx.firstNode;
    newMatches.insert(altMatches.begin(), altMatches.end());

    bool found = false;
    while (true) {
        // ('|' NODE_OR_BLOCK)+
        //    '|'
        removeLeadingWhitespace(newCtx.query);
        if (!newCtx.query.empty() && newCtx.query[0] == '|') {
            newCtx.query.erase(0, 1);
            found = true;
        }
        else {
            break;
        }

        //    NODE_OR_BLOCK
        altCtx = newCtx;
        altMatches = matches;
        if (!matchNodeOrBlock(altCtx, altMatches)) {
            Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
            return false;
        }
        newCtx.query = altCtx.query;
        newCtx.anchors.insert(altCtx.anchors.begin(), altCtx.anchors.end());
        AIDGE_ASSERT(firstSequence == altCtx.firstSequence,
            "Ill-formed query; inconsistency between alternatives regarding first sequence in query at: {}", ctx.query);
        AIDGE_ASSERT(firstNode == altCtx.firstNode,
            "Ill-formed query; inconsistency between alternatives regarding first node in query at: {}", ctx.query);
        newMatches.insert(altMatches.begin(), altMatches.end());
    }

    if (!found) {
        Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
        return false;
    }

    newCtx.firstSequence = firstSequence;
    newCtx.firstNode = firstNode;

    --newCtx.depth;
    ctx = newCtx;
    matches = newMatches;
    return true;
}

bool Aidge::SinglePassGraphMatching::matchEdge(Context& ctx, std::set<MatchingResult>& /*matches*/) {
    auto newCtx = ctx;
    Log::debug("{}edge", std::string(2*newCtx.depth, ' '));

    // ('-' | '~') or '<'
    removeLeadingWhitespace(newCtx.query);
    if (!newCtx.query.empty() && (newCtx.query[0] == '-' || newCtx.query[0] == '~')) {
        newCtx.singleOutput = (newCtx.query[0] == '-');
        newCtx.query.erase(0, 1); // drop '-'
        newCtx.lookForChild = true;
    }
    else if (!newCtx.query.empty() && newCtx.query[0] == '<') {
        newCtx.query.erase(0, 1); // drop '<'
        newCtx.lookForChild = false;
    }
    else {
        Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
        return false;
    }

    // optional first IO_INDEX
    int firstIdx = 0;
    bool foundFirst = false;
    const auto endOutputIdx = std::find_if(newCtx.query.begin(), newCtx.query.end(),
        [](char c) { return !isdigit(c); });
    if (endOutputIdx != newCtx.query.begin()) {
        firstIdx = std::stoi(newCtx.query.substr(0, endOutputIdx - newCtx.query.begin()));
        newCtx.query = newCtx.query.substr(endOutputIdx - newCtx.query.begin());
        foundFirst = true;
    }
    else if (newCtx.query[0] == '*') {
        newCtx.query.erase(0, 1); // drop '*'
        firstIdx = -1;
        foundFirst = true;
    }

    // optional second IO_INDEX, preceded by '-'
    int secondIdx = 0;
    bool foundSecond = false;
    if (foundFirst && !newCtx.query.empty() && newCtx.query[0] == '-') {
        auto query = newCtx.query;
        query.erase(0, 1); // drop '-'

        const auto endInputIdx = std::find_if(query.begin(), query.end(),
            [](char c) { return !isdigit(c); });
        if (endInputIdx != query.begin()) {
            secondIdx = std::stoi(query.substr(0, endInputIdx - query.begin()));
            query = query.substr(endInputIdx - query.begin());
            foundSecond = true;
        }
        else if (query[0] == '*') {
            query.erase(0, 1); // drop '*'
            secondIdx = -1;
            foundSecond = true;
        }

        if (foundSecond) {
            newCtx.query = query;
        }
    }

    // '>' or ('-' | '~')
    if (newCtx.lookForChild && !newCtx.query.empty() && newCtx.query[0] == '>') {
        newCtx.query.erase(0, 1); // drop '>'
    }
    else if (!newCtx.lookForChild && !newCtx.query.empty() && (newCtx.query[0] == '-' || newCtx.query[0] == '~')) {
        newCtx.singleOutput = (newCtx.query[0] == '-');
        newCtx.query.erase(0, 1);
    }
    else {
        Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
        return false;
    }

    // Parsing is done, update the remaining context
    newCtx.edgeLeftIdx = 0;
    newCtx.edgeRightIdx = 0;
    if (foundFirst && foundSecond) {
        newCtx.edgeLeftIdx = firstIdx;
        newCtx.edgeRightIdx = secondIdx;
    }
    else if (foundFirst) {
        if (newCtx.lookForChild) {
            newCtx.edgeRightIdx = firstIdx;
        }
        else {
            newCtx.edgeLeftIdx = firstIdx;
        }
    }

    if (newCtx.lookForChild) {
        Log::debug("{}-{}-{}>", std::string(2*newCtx.depth + 2, ' '),
            newCtx.edgeLeftIdx, newCtx.edgeRightIdx);
    }
    else {
        Log::debug("{}<{}-{}-", std::string(2*newCtx.depth + 2, ' '),
            newCtx.edgeLeftIdx, newCtx.edgeRightIdx);
    }

    ctx = newCtx;
    return true;
}

bool Aidge::SinglePassGraphMatching::matchNode(Context& ctx, std::set<MatchingResult>& matches) {
    auto newCtx = ctx;
    Log::debug("{}node", std::string(2*newCtx.depth, ' '));
    auto newMatches = matches;

    // (TYPE | '.' | '$')
    removeLeadingWhitespace(newCtx.query);
    if (newCtx.query.empty()) {
        Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
        return false;
    }

    std::string type;
    bool unconnected = false;
    if (newCtx.query[0] == '.') {
        // '.'
        newCtx.query.erase(0, 1); // drop '.'
    }
    else if (newCtx.query[0] == '$') {
        // '$'
        newCtx.query.erase(0, 1); // drop '$'
        unconnected = true;
    }
    else {
        // TYPE
        const auto endIdentifier = std::find_if(newCtx.query.begin(), newCtx.query.end(),
            [](char c) { return (!isalnum(c) && c != '_'); });

        if (endIdentifier == newCtx.query.begin()) {
            Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
            return false;
        }

        type = newCtx.query.substr(0, endIdentifier - newCtx.query.begin());
        newCtx.query = newCtx.query.substr(endIdentifier - newCtx.query.begin());
    }

    // ('#' ANCHOR)?
    std::string anchor = "";
    if (!newCtx.query.empty() && newCtx.query[0] == '#') {
        AIDGE_ASSERT(!unconnected,
            "Ill-formed query; an anchor cannot be specified for end of graph ($) in query at: {}", ctx.query);

        // '#'
        newCtx.query.erase(0, 1); // drop '#'

        // ANCHOR
        const auto endAnchor = std::find_if(newCtx.query.begin(), newCtx.query.end(),
            [](char c) { return (!isalnum(c) && c != '_'); });
        anchor = "#" + newCtx.query.substr(0, endAnchor - newCtx.query.begin());
        newCtx.query = newCtx.query.substr(endAnchor - newCtx.query.begin());
    }

    // ('[' LAMBDA ']')?
    std::string lambda = "";
    if (!newCtx.query.empty() && newCtx.query[0] == '[') {
        AIDGE_ASSERT(!unconnected,
            "Ill-formed query; a lambda cannot be specified for end of graph ($) in query at: {}", ctx.query);

        // '['
        newCtx.query.erase(0, 1);

        // LAMBDA
        const auto endIdentifier = std::find_if(newCtx.query.begin(), newCtx.query.end(),
            [](char c) { return (!isalnum(c) && c != '_'); });

        if (endIdentifier == newCtx.query.begin()) {
            Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
            return false;
        }

        lambda = newCtx.query.substr(0, endIdentifier - newCtx.query.begin());
        newCtx.query = newCtx.query.substr(endIdentifier - newCtx.query.begin());

        // ']'
        if (!newCtx.query.empty() && newCtx.query[0] == ']') {
            newCtx.query.erase(0, 1);
        }
        else {
            Log::debug("{}{}", std::string(2*ctx.depth, ' '), fmt::styled("×", fmt::fg(fmt::color::red)));
            return false;
        }
    }

    // Parsing is done, try to match the node
    if (unconnected) {
        for (auto it = newMatches.begin(); it != newMatches.end(); ) {
            bool found = false;

            if (newCtx.lookForChild) {
                const auto outputs = (newCtx.edgeLeftIdx != gk_IODefaultIndex)
                    ? ((newCtx.edgeLeftIdx < it->startNode->nbOutputs())
                        ? std::vector<std::vector<std::pair<NodePtr, IOIndex_t>>>(1, std::vector<std::pair<NodePtr, IOIndex_t>>(it->startNode->output(newCtx.edgeLeftIdx)))
                        : std::vector<std::vector<std::pair<NodePtr, IOIndex_t>>>())
                    : it->startNode->outputs();

                for (const auto& output : outputs) {
                    for (const auto& node : output) {
                        if (!node.first) {
                            continue;
                        }

                        if (newCtx.edgeRightIdx == gk_IODefaultIndex || node.second == newCtx.edgeRightIdx) {
                            if (mGraph->inView(node.first) && !it->graph->inView(node.first)) {
                                found = true;
                                break;
                            }
                        }
                    }

                    if (found) {
                        break;
                    }
                }
            }
            else {
                const auto inputs = (newCtx.edgeLeftIdx != gk_IODefaultIndex)
                    ? ((newCtx.edgeLeftIdx < it->startNode->nbInputs())
                        ? std::vector<std::pair<NodePtr, IOIndex_t>>(1, it->startNode->input(newCtx.edgeLeftIdx))
                        : std::vector<std::pair<NodePtr, IOIndex_t>>())
                    : it->startNode->inputs();

                for (const auto& input : inputs) {
                    if (!input.first) {
                        continue;
                    }

                    if (newCtx.edgeRightIdx == gk_IODefaultIndex || input.second == newCtx.edgeRightIdx) {
                        if (mGraph->inView(input.first) && !it->graph->inView(input.first)) {
                            found = true;
                            break;
                        }
                    }
                }
            }

            if (found) {
                it = newMatches.erase(it);
            }
            else {
                ++it;
            }
        }

        Log::debug("{}node $, found: {}", std::string(2*newCtx.depth + 2, ' '), newMatches.size());
    }
    else if (newCtx.firstSequence && newCtx.firstNode) {
        // First node of first sequence = root node
        const auto nodes = (newCtx.startNode) ? std::set<NodePtr>{newCtx.startNode} : mGraph->getNodes();

        for (auto node : nodes) {
            if ((type.empty() || node->type() == type)
                && (lambda.empty() || mLambda.at(lambda)(node)))
            {
                MatchingResult result;
                result.graph->add(node, false);
                if (!anchor.empty()) {
                    result.anchors[type][anchor] = node;
                }
                result.startNode = node;
                newMatches.insert(result);
            }
        }
        newCtx.firstSequence = false;

        Log::debug("{}root node {}{}, found: {}", std::string(2*newCtx.depth + 2, ' '), fmt::styled(type.empty() ? "." : type, fmt::fg(fmt::color::yellow)), anchor, newMatches.size());
    }
    else if (newCtx.firstNode) {
        // First node of a (new) sequence: it has to be an existing anchor
        AIDGE_ASSERT(!anchor.empty(),
            "Ill-formed query; an anchor is expected in query at: {}", ctx.query);
        AIDGE_ASSERT(newCtx.anchors.find(type + anchor) != newCtx.anchors.end(),
            "Ill-formed query; the node anchor {} has to be an existing anchor in query at: {}", type + anchor, ctx.query);

        for (auto it = newMatches.begin(); it != newMatches.end(); ) {
            const auto anchors = it->anchors[type];
            const auto anchorNode = anchors.find(anchor);
            if (anchorNode != anchors.end()) {
                it->startNode = anchorNode->second;
                ++it;
            }
            else {
                it = newMatches.erase(it);
            }
        }

        Log::debug("{}anchor node {}{}, found: {}", std::string(2*newCtx.depth + 2, ' '), fmt::styled(type.empty() ? "." : type, fmt::fg(fmt::color::yellow)), anchor, newMatches.size());
    }
    else {
        for (auto it = newMatches.begin(); it != newMatches.end(); ) {
            bool found = false;

            if (newCtx.lookForChild) {
                const auto outputs = (newCtx.edgeLeftIdx != gk_IODefaultIndex)
                    ? ((newCtx.edgeLeftIdx < it->startNode->nbOutputs())
                        ? std::vector<std::vector<std::pair<NodePtr, IOIndex_t>>>(1, std::vector<std::pair<NodePtr, IOIndex_t>>(it->startNode->output(newCtx.edgeLeftIdx)))
                        : std::vector<std::vector<std::pair<NodePtr, IOIndex_t>>>())
                    : it->startNode->outputs();

                for (const auto& output : outputs) {
                    if (newCtx.singleOutput && output.size() > 1) {
                        continue;
                    }

                    for (const auto& node : output) {
                        if (!node.first) {
                            continue;
                        }

                        if ((type.empty() || node.first->type() == type)
                            && (lambda.empty() || mLambda.at(lambda)(node.first))
                            && (newCtx.edgeRightIdx == gk_IODefaultIndex || node.second == newCtx.edgeRightIdx))
                        {
                            if (mGraph->inView(node.first) && !it->graph->inView(node.first)) {
                                it->graph->add(node.first, false);
                                if (!anchor.empty()) {
                                    it->anchors[type][anchor] = node.first;
                                }
                                it->startNode = node.first;
                                found = true;
                                break;
                            }
                            else if (!anchor.empty() && it->anchors[type].find(anchor) != it->anchors[type].end()) {
                                it->startNode = node.first;
                                found = true;
                                break;
                            }
                        }
                    }

                    if (found) {
                        break;
                    }
                }
            }
            else {
                const auto inputs = (newCtx.edgeLeftIdx != gk_IODefaultIndex)
                    ? ((newCtx.edgeLeftIdx < it->startNode->nbInputs())
                        ? std::vector<std::pair<NodePtr, IOIndex_t>>(1, it->startNode->input(newCtx.edgeLeftIdx))
                        : std::vector<std::pair<NodePtr, IOIndex_t>>())
                    : it->startNode->inputs();

                for (const auto& input : inputs) {
                    if (!input.first) {
                        continue;
                    }

                    if ((type.empty() || input.first->type() == type)
                        && (lambda.empty() || mLambda.at(lambda)(input.first))
                        && (newCtx.edgeRightIdx == gk_IODefaultIndex || input.second == newCtx.edgeRightIdx))
                    {
                        if (newCtx.singleOutput && input.first->getChildren(input.second).size() > 1) {
                            continue;
                        }

                        if (mGraph->inView(input.first) && !it->graph->inView(input.first)) {
                            it->graph->add(input.first, false);
                            if (!anchor.empty()) {
                                it->anchors[type][anchor] = input.first;
                            }
                            it->startNode = input.first;
                            found = true;
                            break;
                        }
                        else if (!anchor.empty() && it->anchors[type].find(anchor) != it->anchors[type].end()) {
                            it->startNode = input.first;
                            found = true;
                            break;
                        }
                    }
                }
            }

            if (found) {
                ++it;
            }
            else {
                it = newMatches.erase(it);
            }
        }

        Log::debug("{}node {}{}, found: {}", std::string(2*newCtx.depth + 2, ' '), fmt::styled(type.empty() ? "." : type, fmt::fg(fmt::color::yellow)), anchor, newMatches.size());
    }

    newCtx.anchors.insert(type + anchor);
    ctx = newCtx;
    matches = newMatches;
    return true;
}

bool Aidge::operator<(const Aidge::SinglePassGraphMatching::MatchingResult& lhs, const Aidge::SinglePassGraphMatching::MatchingResult& rhs) {
    // Matching rootNode are guaranteed to be different!
    return lhs.graph->rootNode() < rhs.graph->rootNode();
}