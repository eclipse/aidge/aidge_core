/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/graph/StaticAnalysis.hpp"

#include <cstddef>  // std::size_t
#include <memory>
#include <numeric>  // std::accumulate
#include <set>

#include <fmt/core.h>  // fmt::println
#include <fmt/format.h>
#include <fmt/ranges.h>

#include "aidge/data/DataType.hpp"  // Aidge::isFloatingPoint
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/Operator.hpp"
#include "aidge/operator/OperatorTensor.hpp"

Aidge::OperatorStats::OperatorStats(const Operator& op)
  : mOp(op)
{
    //ctor
}

Aidge::OperatorStats::~OperatorStats() = default;

std::size_t Aidge::OperatorStats::getNbArithmIntOps() const {
    const auto opTensor = dynamic_cast<const OperatorTensor*>(&mOp);
    if (opTensor) {
        if (!isFloatingPoint(opTensor->getOutput(0)->dataType())) {
            return getNbArithmOps();
        }
    }
    return 0;
}

////////////////////////////////////////////////////////////////////////////////

Aidge::StaticAnalysis::StaticAnalysis(std::shared_ptr<GraphView> graph)
  : mGraph(graph)
{
    //ctor
}

Aidge::StaticAnalysis::~StaticAnalysis() = default;

void Aidge::StaticAnalysis::summary(bool incProducers) const {
    fmt::println("--------------------------------------------------------------------------------");
    fmt::println("                        Layer (type)               Output Shape         Param #");
    fmt::println("================================================================================");

    std::size_t nbParams = 0;
    std::size_t paramsSize = 0;  // Size in bits
    std::size_t fwdBwdSize = 0;  // Size in bits

    const auto namePtrTable = mGraph->getRankedNodesName("{0} ({1}#{3})");
    for (const auto &node : mGraph->getOrderedNodes()) {
        if (node->type() == Producer_Op::Type && !incProducers) {
            continue;
        }

        auto opTensor = std::dynamic_pointer_cast<OperatorTensor>(node->getOperator());
        std::string outputDimsStr = fmt::format("{: >27}", "?");
        if (opTensor) {
            const auto outputDims = opTensor->getOutput(0)->dims();
            outputDimsStr = fmt::format("{: >27}", fmt::format("{}", outputDims));

            for (std::size_t out = 0; out < node->nbOutputs(); ++out) {
                const auto output = opTensor->getOutput(out);
                if (output && node->type() != Producer_Op::Type) {
                    fwdBwdSize += output->size()
                        * getDataTypeBitWidth(output->dataType());
                }
            }
        }

        nbParams += getNbParams(node);
        paramsSize += getParamsSize(node);
        fmt::println("{: >36}{}{: >16}",
          namePtrTable.at(node), outputDimsStr, getNbParams(node));
    }

    std::size_t inputSize = 0;  // Size in bits
    for (const auto& input : mGraph->getOrderedInputs()) {
        if (input.first) {
            auto opTensor = std::dynamic_pointer_cast<OperatorTensor>(input.first->getOperator());
            if (opTensor && opTensor->getInput(input.second)) {
                inputSize += opTensor->getInput(input.second)->size()
                    * getDataTypeBitWidth(opTensor->getInput(input.second)->dataType());
            }
        }
    }

    fmt::println("================================================================================");
    fmt::println("Total params: {}", nbParams);
    fmt::println("--------------------------------------------------------------------------------");
    fmt::println("Input size (MB): {}", inputSize / 8.0 / 1024 / 1024);
    fmt::println("Forward/backward pass size (MB): {}", fwdBwdSize / 8.0 / 1024 / 1024);
    fmt::println("Params size (MB): {}", paramsSize / 8.0 / 1024 / 1024);
    fmt::println("Estimated Total Size (MB): {}", (inputSize + fwdBwdSize + paramsSize) / 8.0 / 1024 / 1024);
    fmt::println("--------------------------------------------------------------------------------");
}

std::size_t Aidge::StaticAnalysis::getNbParams(std::shared_ptr<Node> node) const {
    const auto opTensor = std::dynamic_pointer_cast<OperatorTensor>(node->getOperator());

    std::size_t nbParams = 0;

    // Look for Producers directly attached to the node's inputs.
    std::size_t i = 0;
    for (auto parent : node->inputs()) {
        if (parent.first && mGraph->inView(parent.first)) {
            if (parent.first->type() == Producer_Op::Type && opTensor->getInput(i)) {
                nbParams += opTensor->getInput(i)->size();
            }
        }
        ++i;
    }

    // Look for internal Producers, in case of meta-op.
    if (!node->getOperator()->isAtomic()) {
        const auto microGraph = std::dynamic_pointer_cast<MetaOperator_Op>(node->getOperator())->getMicroGraph();
        for (const auto &internalNode : microGraph->getNodes()) {
            if (internalNode->type() == Producer_Op::Type) {
                const auto internalOpTensor = std::dynamic_pointer_cast<OperatorTensor>(internalNode->getOperator());
                nbParams += internalOpTensor->getOutput(0)->size();
            }
        }
    }

    return nbParams;
}

std::size_t Aidge::StaticAnalysis::getParamsSize(std::shared_ptr<Node> node) const {
    const auto opTensor = std::dynamic_pointer_cast<OperatorTensor>(node->getOperator());

    std::size_t paramsSize = 0;

    // Look for Producers directly attached to the node's inputs.
    std::size_t i = 0;
    for (const auto& parent : node->inputs()) {
        if (parent.first && mGraph->inView(parent.first)) {
            if (parent.first->type() == Producer_Op::Type && opTensor->getInput(i)) {
                paramsSize += opTensor->getInput(i)->size()
                    * getDataTypeBitWidth(opTensor->getInput(i)->dataType());
            }
        }
        ++i;
    }

    // Look for internal Producers, in case of meta-op.
    if (!node->getOperator()->isAtomic()) {
        const auto microGraph = std::dynamic_pointer_cast<MetaOperator_Op>(node->getOperator())->getMicroGraph();
        for (const auto &internalNode : microGraph->getNodes()) {
            if (internalNode->type() == Producer_Op::Type) {
                const auto internalOpTensor = std::dynamic_pointer_cast<OperatorTensor>(internalNode->getOperator());
                paramsSize += internalOpTensor->getOutput(0)->size()
                    * getDataTypeBitWidth(internalOpTensor->getOutput(0)->dataType());
            }
        }
    }

    return paramsSize;
}

std::shared_ptr<Aidge::OperatorStats> Aidge::StaticAnalysis::getOpStats(std::shared_ptr<Node> node) const {
    return (Registrar<OperatorStats>::exists(node->type()))
        ? Registrar<OperatorStats>::create(node->type())(*(node->getOperator()))
        : (node->getOperator()->isAtomic())
            ? std::make_shared<OperatorStats>(*(node->getOperator()))
            : std::make_shared<MetaOpStats>(*(node->getOperator()));
}

std::size_t Aidge::StaticAnalysis::getNbArithmOps() const { return accumulate(&OperatorStats::getNbArithmOps); }
std::size_t Aidge::StaticAnalysis::getNbLogicOps() const { return accumulate(&OperatorStats::getNbLogicOps); }
std::size_t Aidge::StaticAnalysis::getNbCompOps() const { return accumulate(&OperatorStats::getNbCompOps); }
std::size_t Aidge::StaticAnalysis::getNbNLOps() const { return accumulate(&OperatorStats::getNbNLOps); }
std::size_t Aidge::StaticAnalysis::getNbOps() const { return accumulate(&OperatorStats::getNbOps); }
std::size_t Aidge::StaticAnalysis::getNbArithmIntOps() const { return accumulate(&OperatorStats::getNbArithmIntOps); }
std::size_t Aidge::StaticAnalysis::getNbArithmFpOps() const { return accumulate(&OperatorStats::getNbArithmFpOps); }
std::size_t Aidge::StaticAnalysis::getNbMACOps() const { return accumulate(&OperatorStats::getNbMACOps); }

std::size_t Aidge::StaticAnalysis::accumulate(std::size_t (OperatorStats::*func)() const) const {
    return std::accumulate(
        mGraph->getNodes().cbegin(),
        mGraph->getNodes().cend(),
        std::size_t(0),
        [this, func](const std::size_t& lhs, const std::shared_ptr<Node>& rhs) {
            return lhs + (this->getOpStats(rhs).get()->*func)();
        });
}

////////////////////////////////////////////////////////////////////////////////

Aidge::MetaOpStats::~MetaOpStats() = default;

std::size_t Aidge::MetaOpStats::getNbArithmOps() const { return StaticAnalysis(dynamic_cast<const MetaOperator_Op&>(mOp).getMicroGraph()).getNbArithmOps(); }
std::size_t Aidge::MetaOpStats::getNbLogicOps() const { return StaticAnalysis(dynamic_cast<const MetaOperator_Op&>(mOp).getMicroGraph()).getNbLogicOps(); }
std::size_t Aidge::MetaOpStats::getNbCompOps() const { return StaticAnalysis(dynamic_cast<const MetaOperator_Op&>(mOp).getMicroGraph()).getNbCompOps(); }
std::size_t Aidge::MetaOpStats::getNbNLOps() const { return StaticAnalysis(dynamic_cast<const MetaOperator_Op&>(mOp).getMicroGraph()).getNbNLOps(); }
std::size_t Aidge::MetaOpStats::getNbArithmIntOps() const { return StaticAnalysis(dynamic_cast<const MetaOperator_Op&>(mOp).getMicroGraph()).getNbArithmIntOps(); }
std::size_t Aidge::MetaOpStats::getNbMACOps() const { return StaticAnalysis(dynamic_cast<const MetaOperator_Op&>(mOp).getMicroGraph()).getNbMACOps(); }
