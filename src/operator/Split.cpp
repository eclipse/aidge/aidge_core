/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Split.hpp"

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <string>
#include <utility>
#include <vector>

#include <fmt/format.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"

void Aidge::Split_OpImpl::forward() {
    const Split_Op& op = dynamic_cast<const Split_Op&>(mOp);
    const auto axis = op.axis();
    const auto splits = op.split();
    const auto dims = op.getInput(0)->dims();

    //Compute pre/post axis strides
    const std::size_t stride_pre = std::accumulate(dims.cbegin(), dims.cbegin() + axis, 1, std::multiplies<std::size_t>());
    const std::size_t stride_post = std::accumulate(dims.crbegin(), dims.crbegin() + dims.size() -1 - axis, 1, std::multiplies<std::size_t>());
    for (auto i = 0; i < op.nbOutputs(); ++i)
    {
        DimSize_t chunkIdxOnAxis = std::accumulate(splits.cbegin(), splits.cbegin() + i, 0) * stride_post;
        DimSize_t offset = 0;
        for (std::size_t j = 0; j < stride_pre; ++j)
        {
            // Compute chunk position in input tensor
            DimSize_t idx = j * stride_post * dims[axis] + chunkIdxOnAxis;
            // Copy chunk in output
            op.getOutput(i)->getImpl()->copy(op.getInput(0)->getImpl()->rawPtr(idx),
                                            splits[i] * stride_post, offset);
            offset += splits[i] * stride_post;
        }

    }
}

/////////////////////////////////////////////////////

const std::string Aidge::Split_Op::Type = "Split";

Aidge::Split_Op::Split_Op(std::int8_t axis,
                        Aidge::DimSize_t nbOutputs,
                        const std::vector<Aidge::DimSize_t>& split)
    : OperatorTensor(Type, {InputCategory::Data, InputCategory::OptionalData}, nbOutputs),
    mAttributes(std::make_shared<Attributes_>(
        attr<SplitAttr::Axis>(axis),
        attr<SplitAttr::Split>(split)))
{
    mImpl = std::make_shared<Split_OpImpl>(*this);
}

Aidge::Split_Op::Split_Op(const Aidge::Split_Op &op)
    : OperatorTensor(op),
    mAttributes(op.mAttributes)
{
    if (!op.backend().empty()) {
        SET_IMPL_MACRO(Split_Op, *this, op.backend());
    }
    else {
        mImpl = std::make_shared<Split_OpImpl>(*this);
    }
}

std::shared_ptr<Aidge::Operator> Aidge::Split_Op::clone() const {
    return std::make_shared<Split_Op>(*this);
}

bool Aidge::Split_Op::dimsForwarded() const {
    if ((getInput(1) && !getInput(1)->undefined()))
    {
        // output dims are data dependent
        return false;
    }

    return OperatorTensor::dimsForwarded();
}

bool Aidge::Split_Op::forwardDims(bool allowDataDependency) {
    if (inputsAssociated()) {
        // Copy optional input #1, if present, to attribute Split
        if (getInput(1)) {
            if (!this->split().empty()) {
                Log::notice("Split_Op: ignoring non-empty Split attribute because input#1 takes precedence");
            }

            if (!allowDataDependency) {
                Log::warn("Split_Op: unable to forwardDims() because output dims are data dependent on input#1");
                return false;
            }

            std::shared_ptr<Tensor> fallback;
            this->split().clear(); // If both are provided input would override attrs
            this->split().reserve(getInput(1)->size());
            const auto& splits = getInput(1)->refCastFrom(fallback, NativeType_v<DimSize_t>, "cpu");
            std::copy_n(static_cast<DimSize_t*>(splits.getImpl()->hostPtr()),
                        splits.size(),
                        std::back_inserter(this->split()));
        }

        // Compute output dims
        if (this->axis() < 0)
            this->axis() += static_cast<std::int8_t>(getInput(0)->nbDims());

        DimSize_t dimToSplit = getInput(0)->dims()[this->axis()];
        DimSize_t nbOutput = this->nbOutputs();
        // Fill Split attr if empty
        if(this->split().empty()) {
            // In case the input Split is not provided, divide the dimension of Axis into equal slices
            AIDGE_ASSERT(dimToSplit > nbOutput, "Split_Op: Output number {} mustn't be bigger than dimension {}.", nbOutput, dimToSplit);
            DimSize_t baseSliceSize = dimToSplit / nbOutput;

            DimSize_t remainder = dimToSplit % nbOutput;

            for (DimSize_t i = 0; i < static_cast<DimSize_t>(nbOutput -1); ++i) {
                    this->split().push_back(baseSliceSize);
            }
            this->split().push_back(baseSliceSize + remainder);
        }

        const auto splits = this->split();
        AIDGE_ASSERT(splits.size() == nbOutput, "Split_Op: number of slices {} must be equal to number of outputs {}", splits, nbOutput);
        DimSize_t totalSplitSize = std::accumulate(splits.cbegin(), splits.cend(), 0);
        AIDGE_ASSERT(totalSplitSize == dimToSplit, "Split_Op: Total chunks size {} is different from dimension size {}.", totalSplitSize, dimToSplit);

        std::vector<DimSize_t> outDims = getInput(0)->dims();
        for (std::size_t i = 0; i < nbOutput; ++i)
        {
            outDims[this->axis()] = this->split()[i];
            mOutputs[i]->resize(outDims);
        }

        return true;
    }

    return false;
}

void Aidge::Split_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    if (Registrar<Split_Op>::exists({name})) {
        SET_IMPL_MACRO(Split_Op, *this, name);
    }
    else {
        mImpl = std::make_shared<Split_OpImpl>(*this);
    }
    for (std::size_t i = 0; i < this->nbOutputs(); i++)
    {
        mOutputs[i]->setBackend(name, device);
    }

}

std::set<std::string> Aidge::Split_Op::getAvailableBackends() const {
    return Registrar<Split_Op>::getKeys();
}

////////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Split(Aidge::DimSize_t nbOutput,
                                   std::int8_t axis,
                                   const std::vector<Aidge::DimSize_t>& split,
                                   const std::string &name) {
    return std::make_shared<Node>(std::make_shared<Split_Op>(axis, nbOutput, split), name);
}