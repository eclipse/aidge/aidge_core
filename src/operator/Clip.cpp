/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Clip.hpp"

#include <memory>
#include <string>
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/operator/Clip.hpp"

const std::string Aidge::Clip_Op::Type = "Clip";

bool Aidge::Clip_Op::dimsForwarded() const {
    if ((getInput(1) && !getInput(1)->undefined())
        || (getInput(2) && !getInput(2)->undefined()))
    {
        // output dims are data dependent
        return false;
    }

    return OperatorTensor::dimsForwarded();
}


bool Aidge::Clip_Op::forwardDims(bool allowDataDependency)
{
    if (getInput(1) )
    {
        if( this->min() != std::numeric_limits<float>::lowest())
        {
            Log::notice("{} : ignoring non-empty min attribute because input#1 "
                  "take precedence",
                  type());
        }
        if (!allowDataDependency) {
        Log::warn("{} : unable to forwardDims() because output dims are data "
                    "dependent on input#1",
                    type());
        return false;
        }
        std::shared_ptr<Tensor> fallback;
        const auto& minV = mInputs[1]->refCastFrom(fallback, NativeType_v<float>, "cpu");
        this->min() = *(static_cast<float*>(minV.getImpl()->hostPtr()));
    }
    if (getInput(2))
    {
       if( this->max() != std::numeric_limits<float>::max())
        {
            Log::notice("{} : ignoring non-empty max attribute because input#2 "
                  "take precedence",
                  type());
        }
        if (!allowDataDependency) {
        Log::warn("{} : unable to forwardDims() because output dims are data "
                    "dependent on input#2",
                    type());
        return false;
        }
        std::shared_ptr<Tensor> fallback;
        const auto& maxV = mInputs[2]->refCastFrom(fallback, NativeType_v<float>, "cpu");
        this->max() = *(static_cast<float*>(maxV.getImpl()->hostPtr()));
    }
    if (!inputsAssociated(false)) {
        return false;
    }
    else if ((getInput(1) && !getInput(1)->empty())  || (getInput(2) && !getInput(2)->empty()))
    {
        AIDGE_THROW_OR_ABORT(std::runtime_error,"Expected Input#1 and Input#2 to be scalar (Tensors of empty shapes)");
    }
    mOutputs[0] -> resize(getInput(0)->dims());
    return true;
}
void Aidge::Clip_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    mImpl = Registrar<Clip_Op>::create(name)(*this);
    mOutputs[0]->setBackend(name, device);
}
std::set<std::string> Aidge::Clip_Op::getAvailableBackends() const {
    return Registrar<Clip_Op>::getKeys();
}
std::shared_ptr<Aidge::Node> Aidge::Clip(const std::string &name,float min,float max)
{
    return std::make_shared<Node>(std::make_shared<Clip_Op>(min, max), name);
}