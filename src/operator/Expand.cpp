/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Expand.hpp"

#include <cstdint>
#include <fmt/core.h>

#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Log.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

const std::string Expand_Op::Type = "Expand";

Expand_Op::Expand_Op(const Expand_Op &op) : OperatorTensor(op) {
    if (op.mImpl) {
        SET_IMPL_MACRO(Expand_Op, *this, op.backend());
    } else {
        mImpl = nullptr;
    }
}

std::shared_ptr<Aidge::Operator> Expand_Op::clone() const {
    return std::make_shared<Expand_Op>(*this);
}

bool Expand_Op::forwardDims(bool allowDataDependency) {
    /////////////////
    // Error checking
    if (!inputsAssociated(false)) {
        return false;
    }
    if (!allowDataDependency) {
        Log::warn("{}: cannot execute forwardDims() as the output "
                  "dimensions are computed from some input data.",
                  type());
        return false;
    }

    AIDGE_ASSERT(getInput(1)->nbDims() == 1,
                 "{}: data input (#0) defines output shape. Must have only 1 "
                 "dimension.",
                 type());
    AIDGE_ASSERT(getInput(1)->dataType() == DataType::Int64,
                 "{}: shape input (#1) Data type must be int64.",
                 type());

    const std::vector<std::size_t> &inDataDims = getInput(0)->dims();
    std::vector<DimSize_t> inShape(getInput(1)->size());
    for (DimSize_t i = 0; i < getInput(1)->size(); ++i) {
        inShape[i] = getInput(1)->get<std::int64_t>(i);
    }

    ////////////////////////////////////////
    // Dimension broadcasting
    // copy pasted code from Mul forwardDims
    std::vector<std::size_t> outDims =
        (inDataDims.size() >= inShape.size()) ? inDataDims : inShape;
    const std::vector<std::size_t> &lowDims =
        (inDataDims.size() < inShape.size()) ? inDataDims : inShape;

    std::size_t outId = outDims.size() - 1;
    std::size_t lowId = lowDims.size() - 1;
    std::size_t i = 0;
    while (i++ < lowDims.size()) {
        if (outDims[outId] == 1) {
            outDims[outId] = lowDims[lowId];
        } else if ((lowDims[lowId] != 1) &&
                   (lowDims[lowId] != outDims[outId])) {
            AIDGE_THROW_OR_ABORT(std::runtime_error,
                                 "{}: Incompatible Tensor shape "
                                 "{} for input#0 vs {} for input#1",
                                 type(),
                                 inDataDims,
                                 inShape);
        }
        --outId;
        --lowId;
    }
    mOutputs[0]->resize(outDims);
    return true;
}

void Expand_Op::setBackend(const std::string &name,
                           Aidge::DeviceIdx_t device) {
    SET_IMPL_MACRO(Expand_Op, *this, name);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Expand_Op::getAvailableBackends() const {
    return Registrar<Expand_Op>::getKeys();
}

std::shared_ptr<Aidge::Node> Expand(const std::string &name) {
    return std::make_shared<Node>(std::make_shared<Expand_Op>(), name);
}
} // namespace Aidge
