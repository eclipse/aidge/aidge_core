/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/LRN.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::LRN_Op::Type = "LRN";

Aidge::LRN_Op::LRN_Op(std::int32_t size)
    : OperatorTensor(Type, {InputCategory::Data}, 1),
    mAttributes(std::make_shared<Attributes_>(
        attr<LRNAttr::Alpha>(0.0001),
        attr<LRNAttr::Beta>(0.75),
        attr<LRNAttr::Bias>(1.0),
        attr<LRNAttr::Size>(size)))
{}

Aidge::LRN_Op::LRN_Op(const Aidge::LRN_Op& op)
    : OperatorTensor(op),
        mAttributes(op.mAttributes)
{
    if (op.mImpl){
        SET_IMPL_MACRO(LRN_Op, *this, op.backend());
    }else{
        mImpl = nullptr;
    }
}

std::shared_ptr<Aidge::Operator> Aidge::LRN_Op::clone() const {
    return std::make_shared<LRN_Op>(*this);
}

void Aidge::LRN_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    mImpl = Registrar<LRN_Op>::create(name)(*this);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::LRN_Op::getAvailableBackends() const {
    return Registrar<LRN_Op>::getKeys();
}

////////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::LRN(std::int32_t size, const std::string& name) {
    return std::make_shared<Node>(std::make_shared<LRN_Op>(size), name);
}