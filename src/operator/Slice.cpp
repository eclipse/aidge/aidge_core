/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Slice.hpp"

#include <cstddef>
#include <cstdint>
#include <string>
#include <utility>
#include <vector>

#include <fmt/format.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"
#include "aidge/data/Data.hpp"
#include "aidge/utils/Registrar.hpp"


const std::string Aidge::Slice_Op::Type = "Slice";

Aidge::Slice_Op::Slice_Op(const std::vector<std::int64_t>& starts,
                        const std::vector<std::int64_t>& ends,
                        const std::vector<std::int8_t>& axes,
                        const std::vector<std::int64_t>& steps)
    : OperatorTensor(Type,
        {InputCategory::Data,
            InputCategory::OptionalData,
            InputCategory::OptionalData,
            InputCategory::OptionalData,
            InputCategory::OptionalData},
        1),
    mAttributes(std::make_shared<Attributes_>(
        attr<SliceAttr::Starts>(starts),
        attr<SliceAttr::Ends>(ends),
        attr<SliceAttr::Axes>(axes),
        attr<SliceAttr::Steps>(steps)))
{
    mImpl = std::make_shared<Slice_OpImpl>(*this);
}

Aidge::Slice_Op::Slice_Op(const Aidge::Slice_Op& op)
    : OperatorTensor(op), mAttributes(op.mAttributes)
{
    if (!op.backend().empty()) {
        SET_IMPL_MACRO(Slice_Op, *this, op.backend());
    }
    else {
        mImpl = std::make_shared<Slice_OpImpl>(*this);
    }
}

std::shared_ptr<Aidge::Operator> Aidge::Slice_Op::clone() const {
    return std::make_shared<Slice_Op>(*this);
}

// Helper function to calculate the linear index for multi-dimensional data
size_t getLinearIndex(const std::vector<size_t>& dims, const std::vector<size_t>& indices) {
    size_t linearIndex = 0;
    size_t stride = 1;
    for (int i = dims.size() - 1; i >= 0; --i) {
        linearIndex += indices[i] * stride;
        stride *= dims[i];
    }
    return linearIndex;
}

void Aidge::Slice_OpImpl::forward() {
    const Slice_Op& op = dynamic_cast<const Slice_Op&>(mOp);

    if (!op.getInput(0)) {
        AIDGE_THROW_OR_ABORT(std::runtime_error, "{}: input #0 should be associated with a Tensor", op.Type);
    }
    AIDGE_ASSERT((op.axes().size() == op.ends().size()) &&
                 (op.axes().size() == op.starts().size()),
                 "Starts, Ends and Axes arguments should be the same size.");

    const std::vector<size_t> inputDims = op.getInput(0)->dims();
    std::vector<size_t> indices(inputDims.size(), 0); // Initialize indices for each dimension

    // Create an array of ranges for each axis
    std::vector<std::vector<int>> ranges(inputDims.size());

    // Generate ranges dynamically for each dimension
    for (size_t axisIdx = 0; axisIdx < inputDims.size(); ++axisIdx) {
        if (std::find(op.axes().begin(), op.axes().end(), axisIdx) != op.axes().end()) {
            // This axis is being sliced
            int start = op.starts()[axisIdx];
            int end = op.ends()[axisIdx];
            int step = op.steps()[axisIdx];

            start = start >= 0 ? start: start + inputDims[axisIdx];
            start = std::max(0, std::min(start, static_cast<int>(inputDims[axisIdx])));
            end = end >= 0 ? end: end + inputDims[axisIdx];
            end = std::max(0, std::min(end, static_cast<int>(inputDims[axisIdx])));
            // Generate the range of indices for this axis
            for (int idx = start; (step > 0) ? (idx < end) : (idx > end); idx += step) {
                ranges[axisIdx].push_back(idx);
            }
        } else {
            // This axis is not being sliced, keep its full range (just one index in the range)
            ranges[axisIdx].push_back(0);
        }
    }

    // Use iterative stack to handle all dimensions dynamically
    std::vector<size_t> currentIndex(inputDims.size(), 0); // Track current index in each dimension
    std::vector<size_t> stackPointer(inputDims.size(), 0); // Pointers to ranges for each dimension
    size_t dim = 0; // Start at the first dimension
    size_t offset = 0; // Offset in the output tensor

    while (dim < inputDims.size()) {
        if (stackPointer[dim] < ranges[dim].size()) {
            // Set the current index for this dimension
            currentIndex[dim] = ranges[dim][stackPointer[dim]];
            stackPointer[dim]++;

            if (dim == inputDims.size() - 1) {
                // We've reached the last dimension, process this index combination
                size_t linearIndex = getLinearIndex(inputDims, currentIndex);
                op.getOutput(0)->getImpl()->copy(op.getInput(0)->getImpl()->rawPtr(linearIndex), 1, offset);
                offset++;
            } else {
                // Move to the next dimension
                dim++;
            }
        } else {
            // Reset this dimension and move back to the previous one
            stackPointer[dim] = 0;
            dim--;
        }
    }
}

bool Aidge::Slice_Op::dimsForwarded() const {
    if ((getInput(1) && !getInput(1)->undefined())
        || (getInput(2) && !getInput(2)->undefined())
        || (getInput(3) && !getInput(3)->undefined())
        || (getInput(4) && !getInput(4)->undefined()))
    {
        // output dims are data dependent
        return false;
    }

    return OperatorTensor::dimsForwarded();
}

bool Aidge::Slice_Op::forwardDims(bool allowDataDependency) {
    if (inputsAssociated()) {
        std::shared_ptr<Tensor> fallback;
        // Copy optional input #1, if present, to attribute Starts
        if (getInput(1)) {
            if (!this->starts().empty()) {
                Log::notice("Slice_Op: ignoring non-empty Starts attribute because input#1 takes precedence");
            }

            if (!allowDataDependency) {
                Log::warn("Slice_Op: unable to forwardDims() because output dims are data dependent on input#1");
                return false;
            }

            this->starts().clear(); // If both are provided input would override attrs
            this->starts().reserve(getInput(1)->size());
            const auto& starts = getInput(1)->refCastFrom(fallback, NativeType_v<int64_t>, "cpu");
            std::copy_n(static_cast<int64_t*>(starts.getImpl()->hostPtr()),
                        starts.size(),
                        std::back_inserter(this->starts()));
        }

        AIDGE_ASSERT(!this->starts().empty(), "Missing input#1 or Starts attribute");

        // Copy optional input #2, if present, to attribute Ends
        if (getInput(2)) {
            if (!this->ends().empty()) {
                Log::notice("Slice_Op: ignoring non-empty Ends attribute because input#2 takes precedence");
            }

            if (!allowDataDependency) {
                Log::warn("Slice_Op: unable to forwardDims() because output dims are data dependent on input#2");
                return false;
            }

            this->ends().clear(); // If both are provided input would override attrs
            this->ends().reserve(getInput(2)->size());
            const auto& ends = getInput(2)->refCastFrom(fallback, NativeType_v<int64_t>, "cpu");
            std::copy_n(static_cast<int64_t*>(ends.getImpl()->hostPtr()),
                        ends.size(),
                        std::back_inserter(this->ends()));
        }

        AIDGE_ASSERT(!this->ends().empty(), "Missing input#2 or Ends attribute");

        // Copy optional input #3, if present, to attribute Axes
        if (getInput(3)) {
            if (!this->axes().empty()) {
                Log::notice("Slice_Op: ignoring non-empty Axes attribute because input#3 takes precedence");
            }

            if (!allowDataDependency) {
                Log::warn("Slice_Op: unable to forwardDims() because output dims are data dependent on input#3");
                return false;
            }

            this->axes().clear(); // If both are provided input would override attrs
            this->axes().reserve(getInput(3)->size());
            const auto& axes = getInput(3)->refCastFrom(fallback, NativeType_v<int8_t>, "cpu");
            std::copy_n(static_cast<int8_t*>(axes.getImpl()->hostPtr()),
                        axes.size(),
                        std::back_inserter(this->axes()));
        }

        AIDGE_ASSERT(!this->axes().empty(), "Missing input#3 or Axes attribute");

        // Copy optional input #4, if present, to attribute Steps
        if (getInput(4)) {
            if (!this->steps().empty()) {
                Log::notice("Slice_Op: ignoring non-empty Steps attribute because input#4 takes precedence");
            }

            if (!allowDataDependency) {
                Log::warn("Slice_Op: unable to forwardDims() because output dims are data dependent on input#4");
                return false;
            }

            this->steps().clear(); // If both are provided input would override attrs
            this->steps().reserve(getInput(4)->size());
            const auto& steps = getInput(4)->refCastFrom(fallback, NativeType_v<int64_t>, "cpu");
            std::copy_n(static_cast<int64_t*>(steps.getImpl()->hostPtr()),
                        steps.size(),
                        std::back_inserter(this->steps()));
        }

        // Fill Steps attr if empty
        if(this->steps().empty()) {
            // In case the input Steps is not provided, default value is 1
            this->steps() = std::vector<std::int64_t>(this->axes().size(), 1);
        }

        // Compute output dims
        const DimSize_t nbAxes = this->axes().size();
        std::vector<DimSize_t> outDims = getInput(0)->dims();
        for (std::size_t i = 0; i < nbAxes; ++i) {
            const DimIdx_t axis = this->axes()[i] >= 0 ?
                            static_cast<DimIdx_t>(this->axes()[i]) :
                            static_cast<DimIdx_t>(this->axes()[i] + static_cast<DimIdx_t>(getInput(0)->nbDims()));
            DimSize_t start = this->starts()[i] >= 0 ?
                                static_cast<DimSize_t>(this->starts()[i]) :
                                static_cast<DimSize_t>(this->starts()[i] + static_cast<DimSize_t>(getInput(0)->dims()[axis]));
            // Clamp start to the range [0, axis_dim]
            start = std::max(static_cast<DimSize_t>(0), std::min(start, getInput(0)->dims()[axis]-1));

            DimSize_t end = this->ends()[i] >= 0 ?
                            static_cast<DimSize_t>(this->ends()[i]) :
                            static_cast<DimSize_t>(this->ends()[i] + static_cast<DimSize_t>(getInput(0)->dims()[axis]));
            // Clamp end to the range [0, axis_dim]
            end = std::max(static_cast<DimSize_t>(0), std::min(end, getInput(0)->dims()[axis]));
            const std::int64_t step = this->steps()[i];

            AIDGE_ASSERT(step != 0, "Slice_Op: Step ({}) must have a non-zero value on axis {}!", this->steps(), axis);
            if(step * (static_cast<int64_t>(end) - static_cast<int64_t>(start)) < 0) {
                if(step < 0) {
                    AIDGE_THROW_OR_ABORT(std::runtime_error, "{}: Step ({}) is negative, we must have End ({}) < Start ({}) on axis {}", type(), step, end, start, axis);
                }
                else {
                    AIDGE_THROW_OR_ABORT(std::runtime_error, "{}: Step ({}) is positive, we must have Start ({}) < End ({}) on axis {}", type(), step, start, end, axis);
                }
            }

            const std::size_t sliceLength = static_cast<std::size_t>(std::ceil((static_cast<float>(end) - static_cast<float>(start)) / static_cast<float>((step))));
            // Check if slice length is valid
            if (sliceLength > getInput(0)->dims()[axis])
            {
                AIDGE_THROW_OR_ABORT(std::runtime_error, "Slice_Op: ROI ({}) of Slice operator out of bounds ({}) on axis {}, with (Start, End, Step) = ({}, {}, {})",
                    sliceLength, getInput(0)->dims()[axis], axis, start, end, step);
            }
            outDims[axis] = sliceLength;
        }
        mOutputs[0]->resize(outDims);
        return true;
    }

    return false;
}

void Aidge::Slice_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    if (Registrar<Slice_Op>::exists({name})){
        SET_IMPL_MACRO(Slice_Op, *this, name);
    }
    else {
        mImpl = std::make_shared<Slice_OpImpl>(*this);
    }
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Slice_Op::getAvailableBackends() const {
    return Registrar<Slice_Op>::getKeys();
}

////////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Slice(const std::vector<std::int64_t>& starts,
                                   const std::vector<std::int64_t>& ends,
                                   const std::vector<std::int8_t>& axes,
                                   const std::vector<std::int64_t>& steps,
                                   const std::string &name) {
    return std::make_shared<Node>(std::make_shared<Slice_Op>(starts, ends, axes, steps), name);
}
