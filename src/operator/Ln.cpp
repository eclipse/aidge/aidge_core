/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Ln.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::Ln_Op::Type = "Ln";

Aidge::Ln_Op::Ln_Op(const Aidge::Ln_Op& op)
    : OperatorTensor(op)
{
    if (op.mImpl){
        SET_IMPL_MACRO(Ln_Op, *this, op.backend());
    } else {
        mImpl = nullptr;
    }
}

std::shared_ptr<Aidge::Operator> Aidge::Ln_Op::clone() const {
    return std::make_shared<Ln_Op>(*this);
}

void Aidge::Ln_Op::setBackend(const std::string& name, DeviceIdx_t device) {
    mImpl = Registrar<Ln_Op>::create(name)(*this);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Ln_Op::getAvailableBackends() const {
    return Registrar<Ln_Op>::getKeys();
}

/////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Ln(const std::string& name) {
    return std::make_shared<Node>(std::make_shared<Ln_Op>(), name);
}