/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Softmax.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::Softmax_Op::Type = "Softmax";

Aidge::Softmax_Op::Softmax_Op(std::int32_t axis)
    : OperatorTensor(Type, {InputCategory::Data}, 1),
    mAttributes(std::make_shared<Attributes_>(
        attr<SoftmaxAttr::Axis>(axis)))
{}

Aidge::Softmax_Op::Softmax_Op(const Aidge::Softmax_Op& op)
    : OperatorTensor(op),
        mAttributes(op.mAttributes)
{
    if (op.mImpl){
        SET_IMPL_MACRO(Softmax_Op, *this, op.backend());
    }else{
        mImpl = nullptr;
    }
}

std::shared_ptr<Aidge::Operator> Aidge::Softmax_Op::clone() const {
    return std::make_shared<Softmax_Op>(*this);
}

void Aidge::Softmax_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    mImpl = Registrar<Softmax_Op>::create(name)(*this);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Softmax_Op::getAvailableBackends() const {
    return Registrar<Softmax_Op>::getKeys();
}

////////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Softmax(std::int32_t axis, const std::string& name) {
    return std::make_shared<Node>(std::make_shared<Softmax_Op>(axis), name);
}