/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/ArgMax.hpp"

#include <cstddef>    // std::size_t
#include <cstdint>    // std::int32_t
#include <memory>
#include <string>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::ArgMax_Op::Type = "ArgMax";

Aidge::ArgMax_Op::ArgMax_Op(const Aidge::ArgMax_Op& op)
    : OperatorTensor(op),
      mAttributes(op.mAttributes)
{
    if (op.mImpl){
        SET_IMPL_MACRO(ArgMax_Op, *this, op.backend());
    } else {
        mImpl = nullptr;
    }
}

std::shared_ptr<Aidge::Operator> Aidge::ArgMax_Op::clone() const {
    return std::make_shared<ArgMax_Op>(*this);
}

bool Aidge::ArgMax_Op::forwardDims(bool /*allowDataDependency*/) {
    if (inputsAssociated()) {
        // make Axis attribute positive
        std::int32_t axis = mAttributes->template getAttr<ArgMaxAttr::Axis>();
        axis = axis >= 0 ? axis: axis+static_cast<std::int32_t>(getInput(0)->nbDims());

        // build output dimensions
        std::vector<DimSize_t> outDims = getInput(0)->dims();
        if (mAttributes->template getAttr<ArgMaxAttr::KeepDims>()) {
            outDims[axis] = 1;
        }
        else {
            outDims.erase(outDims.begin() + static_cast<std::size_t>(axis));
        }

        // TODO: change {1} for {} when scalar Tensors are better handled.
        mOutputs[0]->resize((outDims.size()>0) ? outDims : std::vector<DimSize_t>({1}));
        return true;
    }
    return false;
}

void Aidge::ArgMax_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    SET_IMPL_MACRO(ArgMax_Op, *this, name);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::ArgMax_Op::getAvailableBackends() const {
    return Registrar<ArgMax_Op>::getKeys();
}

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::ArgMax(std::int32_t axis,
                                    bool keep_dims,
                                    bool select_last_index,
                                    const std::string& name) {
    return std::make_shared<Node>(std::make_shared<ArgMax_Op>(axis, keep_dims, select_last_index), name);
}