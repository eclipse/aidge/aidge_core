/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Pad.hpp"

#include <array>
#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"

template <Aidge::DimIdx_t DIM>
const std::string Aidge::Pad_Op<DIM>::Type = "Pad" + std::to_string(DIM) + "D";

template <Aidge::DimIdx_t DIM>
std::shared_ptr<Aidge::Operator> Aidge::Pad_Op<DIM>::clone() const {
    return std::make_shared<Pad_Op<DIM>>(*this);
}

template <Aidge::DimIdx_t DIM>
bool Aidge::Pad_Op<DIM>::forwardDims(bool /*allowDataDependency*/) {
    if (inputsAssociated()) {
        std::array<DimSize_t, DIM + 2> outputDims{};
        const std::array<DimSize_t, DIM + 2> inputDims = getInput(0)->template dims<DIM+2>();

        for (std::size_t dim = 0; dim < DIM; ++dim) {
            outputDims[dim+2] = mAttributes->template getAttr<PadAttr::BeginEndBorders>()[dim]
                                + inputDims[dim+2]
                                + mAttributes->template getAttr<PadAttr::BeginEndBorders>()[DIM+dim];
        }
        outputDims[1] = inputDims[1];
        outputDims[0] = inputDims[0];
        mOutputs[0]->resize(outputDims);
        return true;
    }

    return false;
}

template <Aidge::DimIdx_t DIM>
void Aidge::Pad_Op<DIM>::setBackend(const std::string &name, Aidge::DeviceIdx_t device) {
    SET_IMPL_MACRO(Pad_Op<DIM>, *this, name);
    mOutputs[0]->setBackend(name, device);
}

template <Aidge::DimIdx_t DIM>
std::set<std::string> Aidge::Pad_Op<DIM>::getAvailableBackends() const {
    return Registrar<Pad_Op<DIM>>::getKeys();
}

template class Aidge::Pad_Op<1>;
template class Aidge::Pad_Op<2>;

////////////////////////////////////////////////////////////////////////////////

template <std::array<Aidge::DimSize_t, 1>::size_type DIM>
std::shared_ptr<Aidge::Node> Aidge::Pad(const std::array<Aidge::DimSize_t, 2*DIM> &beginEndTuples,
                                           const std::string& name,
                                           PadBorderType borderType,
                                           double borderValue)
{
    AIDGE_ASSERT(DIM<=MaxDim, "Too many kernel dimensions required by {}, not supported", Pad_Op<DIM>::Type);
    return std::make_shared<Node>(std::make_shared<Pad_Op<static_cast<DimIdx_t>(DIM)>>(beginEndTuples, borderType, borderValue), name);
}

template std::shared_ptr<Aidge::Node> Aidge::Pad<1>(const std::array<Aidge::DimSize_t, 2> &beginEndTuples, const std::string&, PadBorderType, double borderValue);
template std::shared_ptr<Aidge::Node> Aidge::Pad<2>(const std::array<Aidge::DimSize_t, 4> &beginEndTuples, const std::string&, PadBorderType, double borderValue);
template std::shared_ptr<Aidge::Node> Aidge::Pad<3>(const std::array<Aidge::DimSize_t, 6> &beginEndTuples, const std::string&, PadBorderType, double borderValue);
