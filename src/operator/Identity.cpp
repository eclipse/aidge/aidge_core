/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <string>

#include "aidge/operator/Identity.hpp"

void Aidge::Identity_OpImpl::forward() {
    const Identity_Op& op = dynamic_cast<const Identity_Op&>(mOp);
    op.getOutput(0)->setBackend(op.getInput(0)->backend(), op.getInput(0)->device());
    op.getOutput(0)->setDataType(op.getInput(0)->dataType());
    op.getOutput(0)->setDataFormat(op.getInput(0)->dataFormat());
    op.getOutput(0)->getImpl()->copy(op.getInput(0)->getImpl()->rawPtr(), op.getInput(0)->size());
}

//////////////////////////////////////////////////

const std::string Aidge::Identity_Op::Type = "Identity";

Aidge::Identity_Op::Identity_Op()
    : OperatorTensor(Type, {InputCategory::Data}, 1)
{
    mImpl = std::make_shared<Identity_OpImpl>(*this);
}

Aidge::Identity_Op::Identity_Op(const Aidge::Identity_Op& op)
    : OperatorTensor(op)
{
    mImpl = std::make_shared<Identity_OpImpl>(*this, op.backend());
}

std::shared_ptr<Aidge::Operator> Aidge::Identity_Op::clone() const {
    return std::make_shared<Identity_Op>(*this);
}

void Aidge::Identity_Op::setBackend(const std::string& name, DeviceIdx_t device) {
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Identity_Op::getAvailableBackends() const {
    return Registrar<Identity_Op>::getKeys();
}

std::shared_ptr<Aidge::Node> Aidge::Identity(const std::string& name) {
    return std::make_shared<Node>(std::make_shared<Identity_Op>(), name);
}
