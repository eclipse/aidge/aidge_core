/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Pop.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

Aidge::Elts_t Aidge::Pop_ProdConso::getNbRequiredData(const Aidge::IOIndex_t inputIdx) const {
    assert(mOp.getRawInput(inputIdx) && "requires valid input");

    const Pop_Op& op = dynamic_cast<const Pop_Op&>(mOp);
    AIDGE_ASSERT(!op.getInput(inputIdx)->empty(), "Pop operator requires known, non-empty, input dims for scheduling. You might have an unresolved data dependency upstream in the computing graph.");
    return Elts_t::DataElts(op.getInput(inputIdx)->size()
        / op.getInput(inputIdx)->dims()[0]);
}

void Aidge::Pop_OpImpl::forward() {
    const Pop_Op& op = dynamic_cast<const Pop_Op&>(mOp);

    assert(op.getInput(0) && "missing input #0");
    *op.getOutput(0) = op.getInput(0)->extract({op.forwardStep()}).clone();
}

//////////////////////////////////////////////////////////

const std::string Aidge::Pop_Op::Type = "Pop";

Aidge::Pop_Op::Pop_Op()
    : OperatorTensor(Type, {InputCategory::Data}, 1),
    mAttributes(std::make_shared<Attributes_>(attr<PopAttr::ForwardStep>(0)))
{
    mImpl = std::make_shared<Pop_OpImpl>(*this);
}

Aidge::Pop_Op::Pop_Op(const Aidge::Pop_Op& op)
    : OperatorTensor(op),
    mAttributes(op.mAttributes)
{
    if (!op.backend().empty()) {
        SET_IMPL_MACRO(Pop_Op, *this, op.backend());
    }
    else {
        mImpl = std::make_shared<Pop_OpImpl>(*this);
    }
}

std::shared_ptr<Aidge::Operator> Aidge::Pop_Op::clone() const {
    return std::make_shared<Pop_Op>(*this);
}

bool Aidge::Pop_Op::forwardDims(bool /*allowDataDependency*/) {
    if (inputsAssociated()) {
        auto inputDims = getInput(0)->dims();
        inputDims.erase(inputDims.begin());
        getOutput(0)->resize(inputDims);
        return true;
    }

    return false;
}

void Aidge::Pop_Op::updateConsummerProducer() {
    Operator::updateConsummerProducer();
    mAttributes->template getAttr<PopAttr::ForwardStep>() = 0;
}

void Aidge::Pop_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    if (Registrar<Pop_Op>::exists({name})){
        SET_IMPL_MACRO(Pop_Op, *this, name);
    }
    else {
        mImpl = std::make_shared<Pop_OpImpl>(*this);
    }
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Pop_Op::getAvailableBackends() const {
    return Registrar<Pop_Op>::getKeys();
}

void Aidge::Pop_Op::forward() {
    OperatorTensor::forward();
    ++mAttributes->template getAttr<PopAttr::ForwardStep>();
}

///////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Pop(const std::string& name) {
    return std::make_shared<Node>(std::make_shared<Pop_Op>(), name);
}