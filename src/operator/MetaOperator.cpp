/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/MetaOperator.hpp"

#include <cstddef>  // std::size_t
#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/DynamicAttributes.hpp"

Aidge::MetaOperator_Op::MetaOperator_Op(const std::string& type, const std::shared_ptr<GraphView>& graph, const std::vector<InputCategory>& forcedInputsCategory)
    : OperatorTensor(type, [graph, forcedInputsCategory]() {
        IOIndex_t inputIdx = 0;
        std::vector<InputCategory> inputsCategory;
        for (const auto& in : graph->getOrderedInputs()) {
            if (inputIdx < forcedInputsCategory.size()) {
                inputsCategory.push_back(forcedInputsCategory[inputIdx]);
            }
            else if (in.first) {
                inputsCategory.push_back(in.first->getOperator()->inputCategory(in.second));
            }
            else {
                // Dummy input, default to OptionalData
                inputsCategory.push_back(InputCategory::OptionalData);
            }
            ++inputIdx;
        }
        return inputsCategory;
    }(), graph->getOrderedOutputs().size()),
        mGraph(graph)
{
    // Associate outputs to micro-graph outputs for custom implementation
    for (size_t outputIdx = 0; outputIdx < mOutputs.size(); ++outputIdx) {
        const auto& outputOp = mGraph->getOrderedOutputs()[outputIdx];
        if (outputOp.first) {
            mOutputs[outputIdx] = std::dynamic_pointer_cast<Tensor>(outputOp.first->getOperator()->getRawOutput(outputOp.second));
        }
    }

    for (const auto& node : mGraph->getRankedNodesName("{1}_{3}")) {
        mAttributes->addAttr(node.second, node.first->getOperator()->attributes());
    }
}

Aidge::MetaOperator_Op::MetaOperator_Op(const MetaOperator_Op& op)
    : OperatorTensor(op),
        mGraph(op.mGraph->clone()), // Clone the micro-graph for isolation
        mAttributes(std::make_shared<DynamicAttributes>(*op.mAttributes)) // Clone attributes
{
    // Associate outputs to micro-graph outputs for custom implementation
    for (size_t outputIdx = 0; outputIdx < mOutputs.size(); ++outputIdx) {
        const auto& outputOp = mGraph->getOrderedOutputs()[outputIdx];
        if (outputOp.first) {
            mOutputs[outputIdx] = std::dynamic_pointer_cast<Tensor>(outputOp.first->getOperator()->getRawOutput(outputOp.second));
        }
    }

    // Attributes are already cloned.
}

std::shared_ptr<Aidge::Operator> Aidge::MetaOperator_Op::clone() const {
    auto metaOp = std::make_shared<MetaOperator_Op>(*this);
    if (mImpl) {
        // Only setBackend() is mImpl is not nullptr.
        // The inner-graph backend is already set in MetaOperator_Op copy
        // construtor, when the graph is cloned.
        metaOp->setBackend(mImpl->backend());
    }
    return metaOp;
}

void Aidge::MetaOperator_Op::associateInput(const IOIndex_t inputIdx, const std::shared_ptr<Data>& data) {
    AIDGE_ASSERT(data->type() == Tensor::Type, "input data must be of Tensor type");
    AIDGE_ASSERT(inputIdx < mGraph->getOrderedInputs().size(), "associateInput(): inputIdx ({}) out of bound for MetaOperator", inputIdx);

    const auto& inputOp = mGraph->getOrderedInputs()[inputIdx];
    AIDGE_ASSERT(inputOp.first, "associateInput(): inputIdx ({}) is a dummy input for this MetaOperator, cannot associate data!", inputIdx);
    inputOp.first->getOperator()->associateInput(inputOp.second, data);

    // Associate inputs for custom implementation
    mInputs[inputIdx] = std::dynamic_pointer_cast<Tensor>(inputOp.first->getOperator()->getRawInput(inputOp.second));
}

void Aidge::MetaOperator_Op::setInput(const Aidge::IOIndex_t inputIdx, const std::shared_ptr<Data>& data) {
    AIDGE_ASSERT(data->type() == Tensor::Type, "{} Operator only accepts Tensors as inputs", type());

    const auto& inputOp = mGraph->getOrderedInputs()[inputIdx];
    AIDGE_ASSERT(inputOp.first, "setInput(): inputIdx ({}) is a dummy input for this MetaOperator, cannot associate data!", inputIdx);
    inputOp.first->getOperator()->setInput(inputOp.second, data);

    // Associate inputs for custom implementation
    mInputs[inputIdx] = std::dynamic_pointer_cast<Tensor>(inputOp.first->getOperator()->getRawInput(inputOp.second));
}

std::string Aidge::MetaOperator_Op::backend() const noexcept {
    return (mImpl)
        ? mImpl->backend()
        : mGraph->rootNode()->getOperator()->backend();
}

void Aidge::MetaOperator_Op::setBackend(const std::string &name, Aidge::DeviceIdx_t device) {
    if (Registrar<MetaOperator_Op>::exists({name, type()})) {
        // A custom implementation exists for this meta operator
        mImpl = Registrar<MetaOperator_Op>::create({name, type()})(*this);

        // Set backend for in/out tensor of the MetaOp
        for(auto i: mGraph->inputNodes()){
            auto op_i = std::static_pointer_cast<OperatorTensor>(i->getOperator());
            for(std::size_t in_idx=0; in_idx < op_i->nbInputs(); ++in_idx){
                if (op_i->getInput(in_idx)) {
                    op_i->getInput(in_idx)->setBackend(name, device);
                }
            }
        }
        for(auto o: mGraph->outputNodes()){
            auto op_o = std::static_pointer_cast<OperatorTensor>(o->getOperator());
            for(std::size_t out_idx=0; out_idx < op_o->nbOutputs(); ++out_idx){
                op_o->getOutput(out_idx)->setBackend(name, device);
            }
        }
    }else{
        // Input/output tensors backend are also updated here.
        mGraph->setBackend(name, device);
    }


}

std::set<std::string> Aidge::MetaOperator_Op::getAvailableBackends() const {
    std::set<std::string> backendsList;
    for (const auto& tupleKey : Registrar<MetaOperator_Op>::getKeys()) {
        if (std::get<1>(tupleKey) == type()) {
            backendsList.insert(std::get<0>(tupleKey));
        }
    }
    return backendsList;
}


Aidge::Elts_t Aidge::MetaOperator_Op::getNbRequiredData(const IOIndex_t inputIdx) const {
    if (mImpl) {
        return mImpl->prodConso()->getNbRequiredData(inputIdx);
    }
    else {
        const auto& inputOp = mGraph->getOrderedInputs()[inputIdx];
        if (inputOp.first) {
            return inputOp.first->getOperator()->getNbRequiredData(inputOp.second);
        }
        else {
            return Elts_t::NoneElts();
        }
    }
}

Aidge::Elts_t Aidge::MetaOperator_Op::getNbRequiredProtected(const IOIndex_t inputIdx) const {
    if (mImpl) {
        return mImpl->prodConso()->getNbRequiredProtected(inputIdx);
    }
    else {
        const auto& inputOp = mGraph->getOrderedInputs()[inputIdx];
        if (inputOp.first) {
            return inputOp.first->getOperator()->getNbRequiredProtected(inputOp.second);
        }
        else {
            return Elts_t::NoneElts();
        }
    }
}

Aidge::Elts_t Aidge::MetaOperator_Op::getRequiredMemory(const IOIndex_t outputIdx, const std::vector<DimSize_t> &inputsSize) const {
    if (mImpl) {
        return mImpl->prodConso()->getRequiredMemory(outputIdx, inputsSize);
    }
    else {
        const auto& outputOp = mGraph->getOrderedOutputs()[outputIdx];
        if (outputOp.first) {
            return outputOp.first->getOperator()->getRequiredMemory(outputOp.second, inputsSize);
        }
        else {
            return Elts_t::NoneElts();
        }
    }
}

Aidge::Elts_t Aidge::MetaOperator_Op::getNbConsumedData(IOIndex_t inputIdx) const {
    if (mImpl) {
        return mImpl->prodConso()->getNbConsumedData(inputIdx);
    }
    else {
        const auto& inputOp = mGraph->getOrderedInputs()[inputIdx];
        if (inputOp.first) {
            return inputOp.first->getOperator()->getNbConsumedData(inputOp.second);
        }
        else {
            return Elts_t::NoneElts();
        }
    }
}

Aidge::Elts_t Aidge::MetaOperator_Op::getNbProducedData(IOIndex_t outputIdx) const {
    if (mImpl) {
        return mImpl->prodConso()->getNbProducedData(outputIdx);
    }
    else {
        const auto& outputOp = mGraph->getOrderedOutputs()[outputIdx];
        if (outputOp.first) {
            return outputOp.first->getOperator()->getNbProducedData(outputOp.second);
        }
        else {
            return Elts_t::NoneElts();
        }
    }
}

void Aidge::MetaOperator_Op::resetConsummerProducer() {
    if (mImpl) {
        mImpl->prodConso()->resetConsummerProducer();
    }
    else {
        if (!mScheduler) {
            // Lazy initialization
            mScheduler = std::make_shared<SequentialScheduler>(mGraph, mUpperNode.lock());
        }

        mScheduler->resetScheduling();
    }
}

void Aidge::MetaOperator_Op::updateConsummerProducer() {
    if (mImpl) {
        mImpl->prodConso()->updateConsummerProducer();
    }
    else {
        if (!mScheduler) {
            // Lazy initialization
            mScheduler = std::make_shared<SequentialScheduler>(mGraph, mUpperNode.lock());
        }

        // TODO: check that generateScheduling() can be called multiple time to iteratively update the schedule.
        // It could be a good idea to unify updateConsummerProducer() and generateScheduling() into a "updateScheduling()"
        mScheduler->generateScheduling();
    }
}

void Aidge::MetaOperator_Op::forward() {
    if (mImpl) {
        // A custom implementation exists for this meta operator
        mImpl->forward();
    }
    else {
        // No custom implementation, use the individual operators implementations
        if (!mScheduler) {
            // Lazy initialization
            // TODO: should we assert that a scheduler already exists at this point?
            // => should be created in updateConsummerProducer()
            mScheduler = std::make_shared<SequentialScheduler>(mGraph, mUpperNode.lock());
            mScheduler->generateScheduling();
        }

        mScheduler->forward(false);
    }
}

/////////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::MetaOperator(const char *type,
                                  const std::shared_ptr<Aidge::GraphView>& graph,
                                  const std::vector<InputCategory>& forcedInputsCategory,
                                  const std::string& name)
{
    auto op = std::make_shared<MetaOperator_Op>(type, graph, forcedInputsCategory);
    auto node = std::make_shared<Node>(op, name);
    op->setUpperNode(node);
    return node;
}
