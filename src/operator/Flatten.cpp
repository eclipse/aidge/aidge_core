/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Flatten.hpp"

#include <cstddef>    // std::size_t
#include <cstdint>    // std::int64_t
#include <memory>
#include <stdexcept>  // std::runtime_error
#include <string>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

void Aidge::Flatten_OpImpl::forward() {
    const Flatten_Op& op = dynamic_cast<const Flatten_Op&>(mOp);
    op.getOutput(0)->getImpl()->copy(op.getInput(0)->getImpl()->rawPtr(), op.getInput(0)->size());
}

//////////////////////////////////////////////////

const std::string Aidge::Flatten_Op::Type = "Flatten";

Aidge::Flatten_Op::Flatten_Op(const std::int64_t axis)
    : OperatorTensor(Type, {InputCategory::Data}, 1),
        mAttributes(std::make_shared<Attributes_>(
        attr<FlattenAttr::Axis>(axis)))
{
    mImpl = std::make_shared<Flatten_OpImpl>(*this);
}

Aidge::Flatten_Op::Flatten_Op(const Aidge::Flatten_Op& op)
    : OperatorTensor(op),
        mAttributes(op.mAttributes)
{
    if (!op.backend().empty()) {
        SET_IMPL_MACRO(Flatten_Op, *this, op.backend());
    }
    else {
        mImpl = std::make_shared<Flatten_OpImpl>(*this);
    }
}

std::shared_ptr<Aidge::Operator> Aidge::Flatten_Op::clone() const {
    return std::make_shared<Flatten_Op>(*this);
}

bool Aidge::Flatten_Op::forwardDims(bool /*allowDataDependency*/) {
    if (inputsAssociated()) {
        const auto inDims(getInput(0)->dims());
        const auto firstDim = std::accumulate(inDims.begin(), inDims.begin() + axis(), 1ULL, std::multiplies<DimSize_t>());
        mOutputs[0]->resize({firstDim, getInput(0)->size() / firstDim});
        return true;
    }

    return false;
}

void Aidge::Flatten_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    if (Registrar<Flatten_Op>::exists({name})){
        SET_IMPL_MACRO(Flatten_Op, *this, name);
    }
    else {
        mImpl = std::make_shared<Flatten_OpImpl>(*this);
    }
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Flatten_Op::getAvailableBackends() const {
    return Registrar<Flatten_Op>::getKeys();
}

//////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Flatten(std::int64_t axis,
                            const std::string &name)
{
    return std::make_shared<Node>(std::make_shared<Flatten_Op>(axis), name);
}