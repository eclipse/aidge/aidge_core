/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Round.hpp"

#include <memory>
#include <string>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::Round_Op::Type = "Round";

Aidge::Round_Op::Round_Op(const Aidge::Round_Op& op)
    : OperatorTensor(op)
{
    if (op.mImpl){
        SET_IMPL_MACRO(Round_Op, *this, op.backend());
    }else{
        mImpl = nullptr;
    }
}


std::shared_ptr<Aidge::Operator> Aidge::Round_Op::clone() const {
    return std::make_shared<Round_Op>(*this);
}

void Aidge::Round_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    mImpl = Registrar<Round_Op>::create(name)(*this);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Round_Op::getAvailableBackends() const {
    return Registrar<Round_Op>::getKeys();
}

std::shared_ptr<Aidge::Node> Aidge::Round(const std::string& name) {
    return std::make_shared<Node>(std::make_shared<Round_Op>(), name);
}