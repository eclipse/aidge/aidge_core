/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Cast.hpp"

#include <memory>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

void Aidge::Cast_OpImpl::forward() {
    const Cast_Op& op = dynamic_cast<const Cast_Op&>(mOp);
    op.getOutput(0)->copyCast(*(op.getInput(0)));
}

const std::string Aidge::Cast_Op::Type = "Cast";

Aidge::Cast_Op::Cast_Op(const DataType targetType)
    : OperatorTensor(Type, {InputCategory::Data}, 1),
      mAttributes(std::make_shared<Attributes_>(
        attr<CastAttr::TargetType>(targetType)))
{
    mImpl = std::make_shared<Cast_OpImpl>(*this);
    mOutputs[0]->setDataType(targetType);
}


void Aidge::Cast_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    if (Registrar<Cast_Op>::exists({name})) {
        SET_IMPL_MACRO(Cast_Op, *this, name);
    }
    else {
        mImpl = std::make_shared<Cast_OpImpl>(*this);
    }
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Cast_Op::getAvailableBackends() const {
    return Registrar<Cast_Op>::getKeys();
}

std::shared_ptr<Aidge::Node> Aidge::Cast(const Aidge::DataType targetType, const std::string& name) {
    return std::make_shared<Node>(std::make_shared<Cast_Op>(targetType), name);
}