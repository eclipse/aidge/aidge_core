/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/LeakyReLU.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"

const std::string Aidge::LeakyReLU_Op::Type = "LeakyReLU";

Aidge::LeakyReLU_Op::LeakyReLU_Op(const Aidge::LeakyReLU_Op& op)
    : OperatorTensor(op),
        mAttributes(op.mAttributes)
{
    if (op.mImpl){
        SET_IMPL_MACRO(LeakyReLU_Op, *this, op.backend());
    } else {
        mImpl = nullptr;
    }
}

std::shared_ptr<Aidge::Operator> Aidge::LeakyReLU_Op::clone() const {
    return std::make_shared<LeakyReLU_Op>(*this);
}

void Aidge::LeakyReLU_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    SET_IMPL_MACRO(LeakyReLU_Op, *this, name);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::LeakyReLU_Op::getAvailableBackends() const {
    return Registrar<LeakyReLU_Op>::getKeys();
}

/////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::LeakyReLU(float negativeSlope, const std::string& name) {
    return std::make_shared<Node>(std::make_shared<LeakyReLU_Op>(negativeSlope), name);
}