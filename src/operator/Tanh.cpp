/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Tanh.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::Tanh_Op::Type = "Tanh";

Aidge::Tanh_Op::Tanh_Op() : OperatorTensor(Type, {InputCategory::Data}, 1) {}

Aidge::Tanh_Op::Tanh_Op(const Aidge::Tanh_Op& op)
    : OperatorTensor(op)
{
    if (op.mImpl){
        SET_IMPL_MACRO(Tanh_Op, *this, op.backend());
    } else {
        mImpl = nullptr;
    }
}

std::shared_ptr<Aidge::Operator> Aidge::Tanh_Op::clone() const {
    return std::make_shared<Tanh_Op>(*this);
}

void Aidge::Tanh_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    mImpl = Registrar<Tanh_Op>::create(name)(*this);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Tanh_Op::getAvailableBackends() const {
    return Registrar<Tanh_Op>::getKeys();
}

////////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Tanh(const std::string& name) {
    return std::make_shared<Node>(std::make_shared<Tanh_Op>(), name);
}