/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Scaling.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"


//Caution: This operator is now deprecated and should no longer be used. 
//It has been replaced by the MetaOperator "Quantizer" (located directly in aidge_quantization).

const std::string Aidge::Scaling_Op::Type = "Scaling";

Aidge::Scaling_Op::Scaling_Op(float scalingFactor, std::size_t nbBits, bool isOutputUnsigned)
    : OperatorTensor(Type, {InputCategory::Data}, 1),
        mAttributes(std::make_shared<Attributes_>(
        attr<ScalingAttr::ScalingFactor>(scalingFactor),
        attr<ScalingAttr::QuantizedNbBits>(nbBits),
        attr<ScalingAttr::IsOutputUnsigned>(isOutputUnsigned)))
{
    Log::warn("Caution: The [Scaling] operator is now deprecated and should no longer be used.\nIt has been replaced by the MetaOperator [Quantizer] (located directly in aidge_quantization).");
} 

Aidge::Scaling_Op::Scaling_Op(const Aidge::Scaling_Op& op)
    : OperatorTensor(op),
    mAttributes(op.mAttributes)
{
    Log::warn("Caution: The [Scaling] operator is now deprecated and should no longer be used. \nIt has been replaced by the MetaOperator [Quantizer] (located directly in aidge_quantization).");
    if (op.mImpl){
        SET_IMPL_MACRO(Scaling_Op, *this, op.backend());
    } else {
        mImpl = nullptr;
    }
}

std::shared_ptr<Aidge::Operator> Aidge::Scaling_Op::clone() const {
    return std::make_shared<Scaling_Op>(*this);
}

void Aidge::Scaling_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    SET_IMPL_MACRO(Scaling_Op, *this, name);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Scaling_Op::getAvailableBackends() const {
    return Registrar<Scaling_Op>::getKeys();
}

////////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Scaling(float scalingFactor,
                                     std::size_t quantizedNbBits,
                                     bool isOutputUnsigned,
                                     const std::string& name)
{
    return std::make_shared<Node>(std::make_shared<Scaling_Op>(scalingFactor,quantizedNbBits, isOutputUnsigned), name);
}