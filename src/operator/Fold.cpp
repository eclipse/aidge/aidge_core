/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Fold.hpp"

#include <cmath>      // std::floor
#include <cstddef>    // std::size_t
#include <stdexcept>  // std::runtime_error
#include <string>
#include <utility>    // std::pair
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

template <Aidge::DimIdx_t DIM>
const std::string Aidge::Fold_Op<DIM>::Type = "Fold" + std::to_string(DIM) + "D";

template <Aidge::DimIdx_t DIM>
Aidge::Fold_Op<DIM>::Fold_Op(const Aidge::Fold_Op<DIM> &op)
    : OperatorTensor(op),
        mAttributes(op.mAttributes)
{
    if (!op.backend().empty()) {
        SET_IMPL_MACRO(Fold_Op<DIM>, *this, op.backend());
    }
    else {
        mImpl = nullptr;
    }
}

template <Aidge::DimIdx_t DIM>
std::shared_ptr<Aidge::Operator> Aidge::Fold_Op<DIM>::clone() const {
    return std::make_shared<Fold_Op<DIM>>(*this);
}

template <Aidge::DimIdx_t DIM>
bool Aidge::Fold_Op<DIM>::forwardDims(bool /*allowDataDependency*/) {
    if (inputsAssociated()) {
        auto dims(getInput(0)->dims());
        DimSize_t k = 1;
        DimSize_t l = 1;

        for (std::size_t dim = 0; dim < this->kernelDims().size() ; ++dim) {
            const DimSize_t kernelExtent = this->dilationDims()[dim] *
                                                    (this->kernelDims()[dim] - 1) + 1;

            k *= this->kernelDims()[dim];
            l *= 1 + static_cast<DimSize_t>(
                    floor(static_cast<float>(this->outputDims()[dim] - kernelExtent) /
                            static_cast<float>(this->strideDims()[dim])));
        }

        AIDGE_ASSERT(dims[dims.size() - 2] % k == 0 , "Fold: input number of channels ({}) is not divisible by the product of provided kernel dims ({})!",
            dims[dims.size() - 2], k);
        AIDGE_ASSERT(dims[dims.size() - 1] == l, "Fold: mismatch between expected input 3rd dim {} and provided input 3rd dim {}",
            dims[dims.size() - 1], l);

        dims[dims.size() - 2] /= k;
        dims.pop_back();
        dims.insert(dims.end(), this->outputDims().begin(), this->outputDims().end());
        mOutputs[0]->resize(dims);
        return true;
    }

    return false;
}

template <Aidge::DimIdx_t DIM>
void Aidge::Fold_Op<DIM>::setBackend(const std::string &name, Aidge::DeviceIdx_t device) {
    SET_IMPL_MACRO(Fold_Op<DIM>, *this, name);
    mOutputs[0]->setBackend(name, device);
}

template <Aidge::DimIdx_t DIM>
std::set<std::string> Aidge::Fold_Op<DIM>::getAvailableBackends() const {
    return Registrar<Fold_Op<DIM>>::getKeys();
}

template class Aidge::Fold_Op<2>;

///////////////////////////////////////

template <std::array<Aidge::DimSize_t, 1>::size_type DIM>
std::shared_ptr<Aidge::Node> Aidge::Fold(const std::array<Aidge::DimSize_t, DIM> &outputDims,
                                  const std::array<Aidge::DimSize_t, DIM> &kernelDims,
                                  const std::string& name,
                                  const std::array<Aidge::DimSize_t, DIM> &strideDims,
                                  const std::array<Aidge::DimSize_t, DIM> &dilationDims) {
    // FIXME: properly handle default w&b initialization in every cases
    AIDGE_ASSERT(DIM<=MaxDim, "Too many kernel dimensions required by Fold, not supported", Fold_Op<DIM>::Type);
    return std::make_shared<Node>(std::make_shared<Fold_Op<static_cast<DimIdx_t>(DIM)>>(outputDims, kernelDims, strideDims, dilationDims), name);
}

template std::shared_ptr<Aidge::Node> Aidge::Fold<2>(const std::array<Aidge::DimSize_t, 2>&, const std::array<Aidge::DimSize_t, 2>&, const std::string&, const std::array<Aidge::DimSize_t, 2>&, const std::array<Aidge::DimSize_t, 2>&);
