/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cstddef>   // std::size_t
#include <memory>
#include <stdexcept> // std::runtime_error
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Heaviside.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
// ----------------------------------------------------------- Heaviside_Op
// class

const std::string Heaviside_Op::Type = "Heaviside";

Heaviside_Op::Heaviside_Op(float value)
    : OperatorTensor(Type, {InputCategory::Data}, 1),
      mAttributes(
          std::make_shared<Attributes_>(attr<HeavisideAttr::Value>(value))) {}

Heaviside_Op::Heaviside_Op(const Heaviside_Op &op)
    : OperatorTensor(op), mAttributes(op.mAttributes) {
    if (op.mImpl) {
        SET_IMPL_MACRO(Heaviside_Op, *this, op.backend());
    } else {
        mImpl = nullptr;
    }
}

std::shared_ptr<Aidge::Operator> Aidge::Heaviside_Op::clone() const {
    return std::make_shared<Heaviside_Op>(*this);
}

void Heaviside_Op::setBackend(const std::string &name, DeviceIdx_t device) {
    SET_IMPL_MACRO(Heaviside_Op, *this, name);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Heaviside_Op::getAvailableBackends() const {
    return Registrar<Heaviside_Op>::getKeys();
}

// --------------------------------------------------------------- Free
// functions

NodePtr Heaviside(float value, const std::string &name) {
    return std::make_shared<Node>(std::make_shared<Heaviside_Op>(value), name);
}

} // namespace Aidge
