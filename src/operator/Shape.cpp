/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cstddef>  // std::size_t
#include <cstdint>  // std::int64_t
#include <string>
#include <vector>

#include "aidge/operator/Shape.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"
#include "aidge/utils/Log.hpp"

void Aidge::Shape_OpImpl::forward() {
    // Output is already valid after forwardDims()
    // But it may be with the wrong device (default cpu)
    // This can happen if forwardDims is called before setBackend
    const Shape_Op& op = dynamic_cast<const Shape_Op&>(mOp);
    op.getOutput(0)->setBackend(op.getInput(0)->backend(), op.getInput(0)->device());
}

///////////////////////////////////////////////

const std::string Aidge::Shape_Op::Type = "Shape";

Aidge::Shape_Op::Shape_Op(const std::int64_t start, const std::int64_t end)
    : OperatorTensor(Type, {InputCategory::Data}, 1),
        mAttributes(std::make_shared<Attributes_>(
        attr<ShapeAttr::Start>(start),
        attr<ShapeAttr::End>(end)))
{
    mImpl = std::make_shared<Shape_OpImpl>(*this);
}

Aidge::Shape_Op::Shape_Op(const Aidge::Shape_Op& op)
    : OperatorTensor(op),
        mAttributes(op.mAttributes)
{
    if (!op.backend().empty()) {
        SET_IMPL_MACRO(Shape_Op, *this, op.backend());
    }
    else {
        mImpl = std::make_shared<Shape_OpImpl>(*this);
    }
}

std::shared_ptr<Aidge::Operator> Aidge::Shape_Op::clone() const {
    return std::make_shared<Shape_Op>(*this);
}

bool Aidge::Shape_Op::forwardDims(bool /*allowDataDependency*/) {
    if (inputsAssociated()) {
        if (this->start() < 0)
            this->start() += static_cast<std::int64_t>(getInput(0)->nbDims());
        if (this->end() < 0)
            this->end() += static_cast<std::int64_t>(getInput(0)->nbDims());

        const auto start = this->start();
        const auto end = this->end();
        const auto nbDims = static_cast<std::int64_t>(getInput(0)->nbDims());
        const DimSize_t roi = end - start + 1;

        AIDGE_ASSERT(start < nbDims && end < nbDims, "'start' and 'end' must be < {}", nbDims);
        AIDGE_ASSERT(roi> 1, "Invalid ROI for Shape");

        mOutputs[0]->resize({roi});
        if (!mOutputs[0]->getImpl()){
            Log::debug("Shape::forwardDims, no implementation set for output, defaulting to CPU.");
            mOutputs[0]->setBackend("cpu");
        }
        // Ensure the output of this operator is valid after forwardDims():
        mOutputs[0]->getImpl()->copyCast(std::next(getInput(0)->dims().data(),
                                                    start),
                                            DataType::UInt64,
                                            end - start + 1);
        return true;
    }

    return false;
}

void Aidge::Shape_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    if (Registrar<Shape_Op>::exists({name})) {
        SET_IMPL_MACRO(Shape_Op, *this, name);
    }
    else {
        mImpl = std::make_shared<Shape_OpImpl>(*this);
    }
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Shape_Op::getAvailableBackends() const {
    return Registrar<Shape_Op>::getKeys();
}

//////////////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Shape(const std::int64_t start, const std::int64_t end, const std::string& name) {
    return std::make_shared<Node>(std::make_shared<Shape_Op>(start, end), name);
}
