/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Sigmoid.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::Sigmoid_Op::Type = "Sigmoid";

Aidge::Sigmoid_Op::Sigmoid_Op() : OperatorTensor(Type, {InputCategory::Data}, 1) {}

Aidge::Sigmoid_Op::Sigmoid_Op(const Aidge::Sigmoid_Op& op)
    : OperatorTensor(op)
{
    if (op.mImpl){
        SET_IMPL_MACRO(Sigmoid_Op, *this, op.backend());
    } else {
        mImpl = nullptr;
    }
}

std::shared_ptr<Aidge::Operator> Aidge::Sigmoid_Op::clone() const {
    return std::make_shared<Sigmoid_Op>(*this);
}


void Aidge::Sigmoid_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    mImpl = Registrar<Sigmoid_Op>::create(name)(*this);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Sigmoid_Op::getAvailableBackends() const {
    return Registrar<Sigmoid_Op>::getKeys();
}

///////////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Sigmoid(const std::string& name) {
    return std::make_shared<Node>(std::make_shared<Sigmoid_Op>(), name);
}