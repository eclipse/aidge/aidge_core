/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Atan.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::Atan_Op::Type = "Atan";

Aidge::Atan_Op::Atan_Op() : OperatorTensor(Type, {InputCategory::Data}, 1) {}

Aidge::Atan_Op::Atan_Op(const Aidge::Atan_Op& op)
    : OperatorTensor(op)
{
    if (op.mImpl){
        SET_IMPL_MACRO(Atan_Op, *this, op.backend());
    } else {
        mImpl = nullptr;
    }
}

std::shared_ptr<Aidge::Operator> Aidge::Atan_Op::clone() const {
    return std::make_shared<Atan_Op>(*this);
}


void Aidge::Atan_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    mImpl = Registrar<Atan_Op>::create(name)(*this);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Atan_Op::getAvailableBackends() const {
    return Registrar<Atan_Op>::getKeys();
}

///////////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Atan(const std::string& name) {
    return std::make_shared<Node>(std::make_shared<Atan_Op>(), name);
}
