/********************************************************************************
 * Copyright (c) 2024 Thales
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * Author: Lucas RAKOTOARIVONY, Thales Research & Technology France
 * Date: 25.06.2024
 *
 ********************************************************************************/

#include "aidge/operator/ShiftGELU.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::ShiftGELU_Op::Type = "ShiftGELU";

Aidge::ShiftGELU_Op::ShiftGELU_Op() : OperatorTensor(Type, {InputCategory::Data}, 1) {}

Aidge::ShiftGELU_Op::ShiftGELU_Op(const Aidge::ShiftGELU_Op& op)
    : OperatorTensor(op)
{
    if (op.mImpl){
        SET_IMPL_MACRO(ShiftGELU_Op, *this, op.backend());
    }else{
        mImpl = nullptr;
    }
}

std::shared_ptr<Aidge::Operator> Aidge::ShiftGELU_Op::clone() const {
    return std::make_shared<ShiftGELU_Op>(*this);
}

void Aidge::ShiftGELU_Op::setBackend(const std::string& name, DeviceIdx_t device) {
    SET_IMPL_MACRO(ShiftGELU_Op, *this, name);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::ShiftGELU_Op::getAvailableBackends() const {
    return Registrar<ShiftGELU_Op>::getKeys();
}

///////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::ShiftGELU(const std::string& name) {
    return std::make_shared<Node>(std::make_shared<ShiftGELU_Op>(), name);
}