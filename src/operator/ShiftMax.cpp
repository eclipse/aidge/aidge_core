/********************************************************************************
 * Copyright (c) 2024 Thales
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * Author: Lucas RAKOTOARIVONY, Thales Research & Technology France
 * Date: 25.06.2024
 *
 ********************************************************************************/

#include "aidge/operator/ShiftMax.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::ShiftMax_Op::Type = "ShiftMax";

Aidge::ShiftMax_Op::ShiftMax_Op() : OperatorTensor(Type, {InputCategory::Data}, 1) {}

Aidge::ShiftMax_Op::ShiftMax_Op(const Aidge::ShiftMax_Op& op)
    : OperatorTensor(op)
{
    if (op.mImpl){
        SET_IMPL_MACRO(ShiftMax_Op, *this, op.backend());
    }else{
        mImpl = nullptr;
    }
}

/**
 * @brief Clone the operator using its copy-constructor.
 * @see Operator::ShiftMax_Op
 */
std::shared_ptr<Aidge::Operator> Aidge::ShiftMax_Op::clone() const {
    return std::make_shared<ShiftMax_Op>(*this);
}

void Aidge::ShiftMax_Op::setBackend(const std::string& name, DeviceIdx_t device) {
    SET_IMPL_MACRO(ShiftMax_Op, *this, name);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::ShiftMax_Op::getAvailableBackends() const {
    return Registrar<ShiftMax_Op>::getKeys();
}

/////////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::ShiftMax(const std::string& name) {
    return std::make_shared<Node>(std::make_shared<ShiftMax_Op>(), name);
}