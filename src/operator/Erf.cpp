/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Erf.hpp"

#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::Erf_Op::Type = "Erf";

Aidge::Erf_Op::Erf_Op(const Aidge::Erf_Op& op)
    : OperatorTensor(op)
{
    if (op.mImpl) {
        SET_IMPL_MACRO(Erf_Op, *this, op.backend());
    } else {
        mImpl = nullptr;
    }
}

std::shared_ptr<Aidge::Operator> Aidge::Erf_Op::clone() const {
    return std::make_shared<Erf_Op>(*this);
}

void Aidge::Erf_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    SET_IMPL_MACRO(Erf_Op, *this, name);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Erf_Op::getAvailableBackends() const {
    return Registrar<Erf_Op>::getKeys();
}

/////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Erf(const std::string& name) {
    return std::make_shared<Node>(std::make_shared<Erf_Op>(), name);
}