/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/GridSample.hpp"

#include <cstddef>    // std::size_t
#include <memory>
#include <string>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"


const std::string Aidge::GridSample_Op::Type = "GridSample";


Aidge::GridSample_Op::GridSample_Op(
    typename Aidge::GridSample_Op::Mode mode,
    typename Aidge::GridSample_Op::PaddingMode paddingMode,
    bool alignCorners)
    : OperatorTensor(Type, {InputCategory::Data, InputCategory::Param}, 1),
      mAttributes(std::make_shared<Attributes_>(
        attr<GridSampleAttr::Mode>(mode),
        attr<GridSampleAttr::PaddingMode>(paddingMode),
        attr<GridSampleAttr::AlignCorners>(alignCorners)))
{
    // ctor
}


Aidge::GridSample_Op::GridSample_Op(const Aidge::GridSample_Op& other)
    : OperatorTensor(other),
      mAttributes(other.mAttributes)
{
    if (other.mImpl) {
        SET_IMPL_MACRO(GridSample_Op, *this, other.backend());
    } else {
        mImpl = nullptr;
    }
}


Aidge::GridSample_Op::~GridSample_Op() noexcept = default;


std::shared_ptr<Aidge::Operator> Aidge::GridSample_Op::clone() const {
    return std::make_shared<GridSample_Op>(*this);
}


bool Aidge::GridSample_Op::forwardDims(bool /*allowDataDependency*/) {
    // TODO: adapt for other formats than NCHW
    if (inputsAssociated()) {
        // check data has batch and channel dimensions: (N, C, D0, D1, ..., DN)
        AIDGE_ASSERT(getInput(0)->nbDims() > 2, "Input should have at least one spatial dimension.");
        const std::size_t nbSpatialFeat = getInput(0)->nbDims() -2; // all except channels and batchs
        // check grid field
        // should be (N, D0_out, D1_out, ..., DN_out, N+1)
        AIDGE_ASSERT(((getInput(1)->nbDims() == nbSpatialFeat + 2) &&
            (getInput(1)->dims()[nbSpatialFeat+1] == nbSpatialFeat) &&
            (getInput(1)->dims()[0] == getInput(0)->dims()[0])),
            "Wrong grid size {} for {} operator.", getInput(1)->dims(), type());

        std::vector<DimSize_t> outputDims{};
        outputDims.reserve(nbSpatialFeat+2);
        const std::vector<DimSize_t>& inputDims(getInput(1)->dims());
        outputDims.push_back(inputDims[0]);
        outputDims.push_back(getInput(0)->dims()[1]);
        for (std::size_t i = 2; i < nbSpatialFeat+2; ++i) {
            outputDims.push_back(inputDims[i-1]);
        }

        mOutputs[0]->resize(outputDims);
        return true;
    }

    return false;
}



void Aidge::GridSample_Op::setBackend(const std::string &name, Aidge::DeviceIdx_t device) {
    SET_IMPL_MACRO(GridSample_Op, *this, name);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::GridSample_Op::getAvailableBackends() const {
    return Registrar<GridSample_Op>::getKeys();
}


////////////////////////////////////////////////


std::shared_ptr<Aidge::Node> Aidge::GridSample(
                        typename Aidge::GridSample_Op::Mode mode,
                        typename Aidge::GridSample_Op::PaddingMode paddingMode,
                        bool alignCorners,
                        const std::string& name)
{
    return std::make_shared<Node>(
        std::make_shared<GridSample_Op>(
                mode,
                paddingMode,
                alignCorners),
            name);
}
