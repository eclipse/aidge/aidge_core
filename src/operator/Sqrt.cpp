/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Sqrt.hpp"

#include <memory>
#include <string>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::Sqrt_Op::Type = "Sqrt";

Aidge::Sqrt_Op::Sqrt_Op(const Aidge::Sqrt_Op& op)
    : OperatorTensor(op)
{
    if (op.mImpl){
        SET_IMPL_MACRO(Sqrt_Op, *this, op.backend());
    }else{
        mImpl = nullptr;
    }
}


std::shared_ptr<Aidge::Operator> Aidge::Sqrt_Op::clone() const {
    return std::make_shared<Sqrt_Op>(*this);
}

void Aidge::Sqrt_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    mImpl = Registrar<Sqrt_Op>::create(name)(*this);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Sqrt_Op::getAvailableBackends() const {
    return Registrar<Sqrt_Op>::getKeys();
}

////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Sqrt(const std::string& name) {
    return std::make_shared<Node>(std::make_shared<Sqrt_Op>(), name);
}