/********************************************************************************
 * Copyright (c) 2024 Thales
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * Author: Lucas RAKOTOARIVONY, Thales Research & Technology France
 * Date: 10.09.2024
 *
 ********************************************************************************/

#include "aidge/operator/ILayerNorm.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::ILayerNorm_Op::Type = "ILayerNorm";

void Aidge::ILayerNorm_Op::associateInput(const Aidge::IOIndex_t inputIdx, const std::shared_ptr<Aidge::Data>& data) {
    AIDGE_ASSERT(inputIdx < 3, "Operators {} supports only {} inputs", type(), nbInputs());
    AIDGE_ASSERT(data->type() == Tensor::Type, "input data must be of Tensor type");
    mInputs[inputIdx] = std::dynamic_pointer_cast<Tensor>(data);
    if (inputIdx == 0 && getInput(0)->nbDims() == 1)
        mInputs[inputIdx]->resize({1, getInput(inputIdx)->size()});
}

bool Aidge::ILayerNorm_Op::forwardDims(bool /*allowDataDependency*/) {
    if (inputsAssociated()) {
        const DimSize_t nbFeatures =  getInput(0)->dims()[1];
        for (std::size_t i = 0; i < nbInputs(); ++i) {
            if(inputCategory(i) == InputCategory::Param && getInput(i)->size() != nbFeatures) {
                getInput(i)->resize({getInput(0)->dims()[1]});
            }
        }
        mOutputs[0]->resize(getInput(0)->dims());
        return true;
    }
    return false;
}


void Aidge::ILayerNorm_Op::setBackend(const std::string& name, DeviceIdx_t device) {
    SET_IMPL_MACRO(ILayerNorm_Op, *this, name);
    mOutputs[0]->setBackend(name, device);
    getInput(1)->setBackend(name, device);
    getInput(2)->setBackend(name, device);
}

std::set<std::string> Aidge::ILayerNorm_Op::getAvailableBackends() const {
    return Registrar<ILayerNorm_Op>::getKeys();
}
