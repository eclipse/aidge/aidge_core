/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Move.hpp"

void Aidge::Move_OpImpl::forward() {
    const Move_Op& op = dynamic_cast<const Move_Op&>(mOp);
    op.getOutput(0)->copyFrom(*(op.getInput(0)));
}

const std::string Aidge::Move_Op::Type = "Move";

Aidge::Move_Op::Move_Op()
    : OperatorTensor(Type, {InputCategory::Data}, 1)
{
    mImpl = std::make_shared<Move_OpImpl>(*this);
}

Aidge::Move_Op::Move_Op(const Aidge::Move_Op& op)
    : OperatorTensor(op)
{
    if (!op.backend().empty()) {
        SET_IMPL_MACRO(Move_Op, *this, {op.getInput(0)->getImpl()->backend(), op.backend()});
    }
    else {
        mImpl = std::make_shared<Move_OpImpl>(*this);
    }
}

std::shared_ptr<Aidge::Operator> Aidge::Move_Op::clone() const {
    return std::make_shared<Move_Op>(*this);
}

void Aidge::Move_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    if (Registrar<Move_Op>::exists({mInputs[0]->getImpl()->backend(), name})) {
        SET_IMPL_MACRO(Move_Op, *this, {mInputs[0]->getImpl()->backend(), name});
    }
    else {
        mImpl = std::make_shared<Move_OpImpl>(*this);
    }
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Move_Op::getAvailableBackends() const {
    std::set<std::string> backendsList;
    for (const auto& tupleKey : Registrar<Move_Op>::getKeys()) {
        backendsList.insert(std::get<0>(tupleKey));
        backendsList.insert(std::get<1>(tupleKey));
    }
    return backendsList;
}

////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Move(const std::string& name) {
    return std::make_shared<Node>(std::make_shared<Move_Op>(), name);
}
