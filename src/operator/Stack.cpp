/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Stack.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

// TODO: Check why getRequiredMemory is always called with empty vector as
// inputSize
Elts_t StackProdConso::getRequiredMemory(
    const Aidge::IOIndex_t inputIdx,
    const std::vector<DimSize_t> &/*inputsSize*/) const {
    assert(mOp.getRawInput(inputIdx) && "requires valid input");

    const StackOp &op = dynamic_cast<const StackOp &>(mOp);
    // The produced data after one forward pass is simply the input size,
    // we do not produce the whole output tensor every time.
    if (op.forwardStep() <= op.maxElements()) {
        return Elts_t::DataElts(op.getInput(inputIdx)->size());
    } else {
        return Elts_t::NoneElts();
    }
}

void StackProdConso::resetConsummerProducer() {
    ProdConso::resetConsummerProducer();

    const StackOp &op = dynamic_cast<const StackOp &>(mOp);
    op.forwardStep() = 0;
}

const std::string StackOp::s_type = "Stack";

void StackOpImpl::forward() {
    const StackOp &op = dynamic_cast<const StackOp &>(mOp);
    AIDGE_ASSERT(op.getInput(0), "missing input #0");
    AIDGE_ASSERT((op.forwardStep() < op.maxElements()),
                 "cannot forward anymore, maximum number of elements to stack "
                 "exceeded");

    op.getOutput(0)->getImpl()->copy(
        op.getInput(0)->getImpl()->rawPtr(),
        op.getInput(0)->size(),
        op.forwardStep() * op.getInput(0)->size());
}

StackOp::StackOp(std::uint32_t maxElements)
    : OperatorTensor(s_type, {InputCategory::Data, InputCategory::OptionalData}, 1),
      mAttributes(std::make_shared<Attributes_>(
          attr<StackAttr::MaxElements>(maxElements),
          attr<StackAttr::ForwardStep>(0))) {
    mImpl = std::make_shared<StackOpImpl>(*this);
}

StackOp::StackOp(const Aidge::StackOp &op)
    : OperatorTensor(op), mAttributes(op.mAttributes) {
    if (!op.backend().empty()) {
        SET_IMPL_MACRO(StackOp, *this, op.backend());
    } else {
        mImpl = std::make_shared<StackOpImpl>(*this);
    }
}

std::shared_ptr<Aidge::Operator> Aidge::StackOp::clone() const {
    return std::make_shared<StackOp>(*this);
}

bool Aidge::StackOp::dimsForwarded() const {
    if ((getInput(1) && !getInput(1)->undefined()))
    {
        // output dims are data dependent
        return false;
    }

    return OperatorTensor::dimsForwarded();
}

bool Aidge::StackOp::forwardDims(bool allowDataDependency) {
    if (inputsAssociated()) {
        // Copy optional input #1 first dimension, if present, to attribute MaxElements
        if (getInput(1)) {
            if (!allowDataDependency) {
                Log::warn("StackOp: unable to forwardDims() because output dims are data dependent on input#1");
                return false;
            }

            std::shared_ptr<Tensor> fallback;
            const auto& maxElements = getInput(1)->refCastFrom(fallback, NativeType_v<std::uint32_t>, "cpu");
            AIDGE_ASSERT(maxElements.size() > 0, "Input#1 size should be > 0");
            this->maxElements() = static_cast<std::uint32_t*>(maxElements.getImpl()->hostPtr())[0];
        }

        AIDGE_ASSERT(this->maxElements() > 0, "Input#1 first element or MaxElements attribute should be > 0");

        auto inputDims = getInput(0)->dims();
        inputDims.insert(inputDims.begin(), maxElements());
        getOutput(0)->resize(inputDims);
        return true;
    }

    return false;
}

void StackOp::setBackend(const std::string &name, DeviceIdx_t device) {
    if (Registrar<StackOp>::exists({name})) {
        SET_IMPL_MACRO(StackOp, *this, name);
    } else {
        mImpl = std::make_shared<StackOpImpl>(*this);
    }
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> StackOp::getAvailableBackends() const {
    return Registrar<StackOp>::getKeys();
}

void StackOp::forward() {
    Operator::forward();
    ++forwardStep();
}

std::shared_ptr<Node> Stack(std::uint32_t maxElements,
                            const std::string &name) {
    return std::make_shared<Node>(std::make_shared<StackOp>(maxElements),
                                  name);
}
} // namespace Aidge
