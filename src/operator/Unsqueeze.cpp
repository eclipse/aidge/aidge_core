/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Unsqueeze.hpp"

#include <cstdint>
#include <fmt/core.h>
#include <functional>
#include <memory>
#include <string>
#include <vector>

#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Log.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
const std::string Unsqueeze_Op::Type = "Unsqueeze";

bool Aidge::Unsqueeze_Op::dimsForwarded() const {
  if ((getInput(1) && !getInput(1)->undefined())) {
    // output dims are data dependent
    return false;
  }

  return OperatorTensor::dimsForwarded();
}

bool Unsqueeze_Op::forwardDims(bool allowDataDependency) {
  // error checking
  if (!inputsAssociated(true)) {
    return false;
  }
  std::shared_ptr<Tensor> fallback;
  // Copy optional input #1, if present, to attribute Axes
  if (getInput(1)) {
    if (!this->axes().empty()) {
      Log::notice("{} : ignoring non-empty \"axes\" attribute because input#1 "
                  "takes precedence",
                  type());
    }

    if (!allowDataDependency) {
      Log::warn("{} : unable to forwardDims() because output dims are data "
                "dependent on input#1",
                type());
      return false;
    }

    this->axes().clear(); // If both are provided input would override attrs
    this->axes().reserve(getInput(1)->size());
    const auto &axes =
        getInput(1)->refCastFrom(fallback, NativeType_v<int8_t>, "cpu");
    std::copy_n(static_cast<int8_t *>(axes.getImpl()->hostPtr()),
                axes.size(), std::back_inserter(this->axes()));
  }
  AIDGE_ASSERT(!this->axes().empty(),
               "{} : Axes to unsqueeze can be defined via input#1 or axes "
               "attribute. None of them were provided.",
               type());

  std::vector<DimSize_t> input_dims = getInput(0)->dims();
  std::vector<DimIdx_t> axes_rectified_idx;
  axes_rectified_idx.reserve(this->axes().size());
  DimIdx_t output_nb_dims = input_dims.size() + this->axes().size();

  for (const int8_t &axis : this->axes()) {
    AIDGE_ASSERT(axis >= static_cast<int8_t>(-output_nb_dims) &&
                     axis < static_cast<int8_t>(output_nb_dims),
                 "{} : Axis index OutOfBounds enrror, expected value "
                 "within size limits of input tensor : "
                 "[-{},{}], got {}.",
                 type(), output_nb_dims, output_nb_dims - 1, axis);
    axes_rectified_idx.push_back(
        static_cast<DimIdx_t>(axis >= 0 ? axis : axis + output_nb_dims));
  }
  // sort by descending order
  std::sort(axes_rectified_idx.begin(), axes_rectified_idx.end());
  // Raise error if duplicate indexes are found
  const auto &it = std::adjacent_find(axes_rectified_idx.begin(), axes_rectified_idx.end());
  AIDGE_ASSERT(
      it == axes_rectified_idx.end(),
      "{} : The index {} appears multiple times in list of input dims. "
      "Check positive and negative indexes.\nRaw indexes :\t{}\nRectified "
      "indexes :\t{}",
      type(), *it, this->axes(), axes_rectified_idx);

  // computation
  std::vector<DimSize_t> output_dims(input_dims);
  output_dims.reserve(input_dims.size() + this->axes().size());
  for (const DimIdx_t &axis : axes_rectified_idx) {
    output_dims.insert(output_dims.begin() + axis, 1);
  }
  mOutputs[0]->resize(output_dims);
  return true;
}

void Unsqueeze_Op::setBackend(const std::string &name,
                              Aidge::DeviceIdx_t device) {
  if (Registrar<Unsqueeze_Op>::exists({name})) {
    SET_IMPL_MACRO(Unsqueeze_Op, *this, name);
  } else {
    mImpl = std::make_shared<Unsqueeze_OpImpl>(*this);
  }
  mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Unsqueeze_Op::getAvailableBackends() const {
  return Registrar<Unsqueeze_Op>::getKeys();
}

void Aidge::Unsqueeze_OpImpl::forward() {
  const Unsqueeze_Op &op_ = static_cast<const Unsqueeze_Op &>(mOp);
  // Check if input is provided
  AIDGE_ASSERT(op_.getInput(0), "Unsqueeze : missing input 0");
  op_.getOutput(0)->getImpl()->copy(op_.getInput(0)->getImpl()->rawPtr(),
                                    op_.getInput(0)->size());
}

} // namespace Aidge
