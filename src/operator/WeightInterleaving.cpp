/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/WeightInterleaving.hpp"

#include <memory>
#include <string>
#include <vector>

#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::WeightInterleaving_Op::Type = "WeightInterleaving";

/**
 * @brief Copy-constructor.
 * @param op WeightInterleaving_Op to copy.
 * @details Copies the operator attributes and its output tensor(s), but not
 * its input tensors. The new operator has no associated input.
 */
Aidge::WeightInterleaving_Op::WeightInterleaving_Op(const WeightInterleaving_Op& op)
    : OperatorTensor(op)
{
    if (op.mImpl) {
        SET_IMPL_MACRO(WeightInterleaving_Op, *this, op.backend());
    } else {
        mImpl = nullptr;
    }
}


std::shared_ptr<Aidge::Operator> Aidge::WeightInterleaving_Op::clone() const {
    return std::make_shared<WeightInterleaving_Op>(*this);
}


bool Aidge::WeightInterleaving_Op::forwardDims(bool /*allowDataDependency*/) {
    
    if (inputsAssociated()) {

        // check input data format is NHWC
        AIDGE_ASSERT((getInput(0)->dataFormat() == DataFormat::NHWC),
                    "Wrong Input tensor Data Format : {} for WeightInterleaving operator (should be DataFormat::NHWC for STM32).", getInput(0)->dataFormat());
        
        // Take the last dimension of the tensor : It is the Channel dimension in format NHWC
        // The weights will be compacted along side the channel dimension only
        const DimSize_t& lastDim = getInput(0)->dims().back();

        // Compute the last dimension size of the tensor after the weight interleaving compression
        // TO DO : implement a mechanism to get the number of bits of the DataType
        const DataType& dt = getInput(0)->dataType();

        std::uint8_t nbBits = 0;

        switch (dt) {
            case DataType::Int4:
                nbBits=4;
                break;
            case DataType::Int3:
                nbBits=3;
                break;
            case DataType::Int2:
                nbBits=2;
                break;
            default:
                AIDGE_ASSERT(true, "Unsupport type for WeightInterleaving {}", dt);
        }


        const auto lastDimCompression = compactDataSize(lastDim, nbBits);

        std::vector<DimSize_t> outputDims = getInput(0)->dims();
        outputDims.back() = lastDimCompression;

        // <batch, OutChannels>
        mOutputs[0]->resize(outputDims);

        return true;
    }

    return false;
}


void Aidge::WeightInterleaving_Op::setBackend(const std::string& name, DeviceIdx_t device) {
    SET_IMPL_MACRO(WeightInterleaving_Op, *this, name);
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::WeightInterleaving_Op::getAvailableBackends() const {
    return Registrar<WeightInterleaving_Op>::getKeys();
}

std::shared_ptr<Aidge::Node> Aidge::WeightInterleaving(const std::string& name) {
    return std::make_shared<Node>(std::make_shared<WeightInterleaving_Op>(), name);
}


std::size_t Aidge::WeightInterleaving_Op::compactDataSize(std::size_t dataSize, std::uint8_t nbBits) {
    AIDGE_ASSERT(nbBits > 0 && nbBits < 8, "nbBits must be between 1 and 4"); // Ensure valid bit width

    // Calculate the number of `nbBits` segments that can fit in an 8-bit byte.
    const unsigned int nbSlot = 8 / nbBits;

    // Calculate the number of compacted bytes needed to store all data elements.
    // The formula (dataSize + nbSlot - 1) / nbSlot effectively rounds up the division, ensuring that any remaining elements that don't fully fill a byte are accounted for.
    std::size_t requiredSize = (dataSize + nbSlot - 1) / nbSlot;

    return requiredSize;
}