/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Resize.hpp"

#include <algorithm>
#include <cstddef>   // std::size_t
#include <cstdint>   // std::int64_t
#include <fmt/core.h>
#include <stdexcept> // std::runtime_error
#include <string>
#include <vector>

#include "aidge/data/Data.hpp"
#include "aidge/data/Interpolation.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

const std::string Resize_Op::Type = "Resize";

bool Resize_Op::dimsForwarded() const {
    // in case of ROI add getInput(1) condition
    if ((getInput(1) && !getInput(1)->undefined()) ||
        (getInput(2) && !getInput(2)->undefined()) ||
        (getInput(3) && !getInput(3)->undefined())) {
        // output dims are data dependent
        return false;
    }

    return OperatorTensor::dimsForwarded();
}

bool Resize_Op::forwardDims(bool allowDataDependency) {
    if (!allowDataDependency) {
        Log::warn("{}: cannot execute forwardDims() as the output "
                    "dimensions are computed from some input data.",
                    type());
        return false;
    }

    // Some optional input may be linked but undefined because of ONNX import
    if (!inputsAssociated(false)) {
        return false;
    }

    /** @brief input #0 */
    constexpr IOIndex_t inDataIdx = 0;
    /** @brief input #1 */
    constexpr IOIndex_t inROIIdx = 1;
    /** @brief input #2 */
    constexpr IOIndex_t inScalesIdx = 2;
    /** @brief input #3 */
    constexpr IOIndex_t inSizesIdx = 3;

    std::vector<DimSize_t> outDims = getInput(inDataIdx)->dims();
    /////////////////////////////////////////////////////
    // Ensuring operator is connected properly
    const bool inputROIPresent =
        getInput(inROIIdx) && !getInput(inROIIdx)->undefined();
    if (inputROIPresent) {
            AIDGE_THROW_OR_ABORT(
                std::runtime_error,
                "{}: input ROI(#{}) is present but it is not supported.",
                type(),
                inROIIdx);
        }

    const bool inputScalesPresent =
        getInput(inScalesIdx) && !getInput(inScalesIdx)->undefined();
    const bool inputSizesPresent =
        getInput(inSizesIdx) && !getInput(inSizesIdx)->undefined();

    AIDGE_ASSERT(inputScalesPresent ^ inputSizesPresent,
                 "{}: Only one of the two inputs must be defined between input "
                 "Scales(#2) "
                 "and Sizes(#3). They cannot be specified at the same time.",
                 type())

    std::shared_ptr<Tensor> resizeParam = inputScalesPresent ? getInput(inScalesIdx) : getInput(inSizesIdx);
    AIDGE_ASSERT(getInput(inDataIdx)->nbDims() == resizeParam->size(),
        "{}: data input #0 and resizing parameter input #{} must have the "
        "same dimensions.",
        type(), inputScalesPresent ? inScalesIdx :inSizesIdx);


    ////////////////////////////////////////////
    // Case resize is done using Scales formula
    if (inputScalesPresent) {

        std::shared_ptr<Tensor> fallback;
        const auto &scales =
            resizeParam
                ->refCastFrom(fallback,
                              DataType::Float32,
                              resizeParam->backend());

        const std::vector<DimSize_t> inDims = getInput(inDataIdx)->dims();
        for (std::size_t dim = 0; dim < getInput(inScalesIdx)->size(); ++dim) {
            const auto scaleAlongDim = scales.get<cpptype_t<DataType::Float32>>(dim);
            AIDGE_ASSERT(scaleAlongDim > 0,
                         "{}: all scales values must be sctricly positive, "
                         "got {}.",
                         type(),
                         scaleAlongDim);
            outDims[dim] =
                static_cast<DimSize_t>(inDims[dim] * scaleAlongDim);
        }

        ///////////////////////////////////////////////////////////////
        // case where resize output dims are given via the Size input
    } else {
        std::shared_ptr<Tensor> fallback;
        const auto &sizes = resizeParam
                                ->refCastFrom(fallback,
                                              NativeType_v<DimSize_t>,
                                              resizeParam->backend());

        for (std::size_t dim = 0; dim < getInput(inSizesIdx)->size(); ++dim) {
            outDims[dim] = sizes.get<DimSize_t>(dim);
        }
    }
    mOutputs[0]->resize(outDims);
    return true;
}

void Resize_Op::setBackend(const std::string &name, DeviceIdx_t device) {
    SET_IMPL_MACRO(Resize_Op, *this, name);
    mOutputs[0]->setBackend(name, device);

    // By default, automatically set backend for all optional inputs: roi, scales and
    // sizes
    if (getInput(1)) {
        getInput(1)->setBackend(name, device);
    }
    if (getInput(2)) {
        getInput(2)->setBackend(name, device);
    }
    if (getInput(3)) {
        getInput(3)->setBackend(name, device);
    }
}

std::shared_ptr<Node>
Resize(std::vector<float> scale,
        std::vector<std::size_t> size,
       Interpolation::CoordinateTransformation coordTransfoMode,
       Interpolation::Mode interpolMode,
       float cubicCoefA,
       const std::string &name) {
    std::shared_ptr<Node> node_resize = std::make_shared<Node>(std::make_shared<Resize_Op>(coordTransfoMode,
                                                              interpolMode,
                                                              cubicCoefA),
                                  name);
    if (scale.size()) {
        std::shared_ptr<Node> prod_scale = Producer(std::make_shared<Tensor>(Vector<float>(scale)));
        prod_scale->addChild(node_resize, 0, 2);
    }
    if (size.size())
    {
        std::shared_ptr<Node> prod_size = Producer(std::make_shared<Tensor>(Vector<std::size_t>(size)));
        prod_size->addChild(node_resize, 0, 3);
    }
    return node_resize;

}
} // namespace Aidge
