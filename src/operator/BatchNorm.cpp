/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/BatchNorm.hpp"

#include <cstddef>    // std::size_t
#include <stdexcept>  // std::runtime_error
#include <string>
#include <utility>    // std::pair
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

template <Aidge::DimIdx_t DIM>
const std::string Aidge::BatchNorm_Op<DIM>::Type = "BatchNorm" + std::to_string(DIM) + "D";

template <Aidge::DimIdx_t DIM>
Aidge::BatchNorm_Op<DIM>::BatchNorm_Op(const BatchNorm_Op<DIM>& op)
    : OperatorTensor(op),
      mAttributes(op.mAttributes)
{
    if (op.mImpl) {
        SET_IMPL_MACRO(BatchNorm_Op<DIM>, *this, op.backend());
    } else {
        mImpl = nullptr;
    }
}

template <Aidge::DimIdx_t DIM>
std::shared_ptr<Aidge::Operator> Aidge::BatchNorm_Op<DIM>::clone() const {
    return std::make_shared<BatchNorm_Op<DIM>>(*this);
}

template <Aidge::DimIdx_t DIM>
bool Aidge::BatchNorm_Op<DIM>::forwardDims(bool /*allowDataDependency*/) {
    if (inputsAssociated()) {
        const DimSize_t nbFeatures =  getInput(0)->dims()[1];
        for (std::size_t i = 0; i < nbInputs(); ++i) {
            if(inputCategory(i) == InputCategory::Param && getInput(i)->size() != nbFeatures) {
                // /!\ Input size should be handled BEFORE calling this function
                // This should raise an error
                getInput(i)->resize({getInput(0)->dims()[1]});
            }
        }
        mOutputs[0]->resize(getInput(0)->dims());
        return true;
    }
    return false;
}

template <Aidge::DimIdx_t DIM>
void Aidge::BatchNorm_Op<DIM>::setBackend(const std::string &name, Aidge::DeviceIdx_t device) {
    SET_IMPL_MACRO(BatchNorm_Op<DIM>, *this, name);
    mOutputs[0]->setBackend(name, device);

    // By default, automatically set backend for scale, shift, mean and variance
    if (getInput(1)) {
        getInput(1)->setBackend(name, device);
    }
    else {
        Log::notice("BatchNorm_Op::setBackend(): could not set backend for scale input, because input is not connected");
    }

    if (getInput(2)) {
        getInput(2)->setBackend(name, device);
    }
    else {
        Log::notice("BatchNorm_Op::setBackend(): could not set backend for shift input, because input is not connected");
    }

    if (getInput(3)) {
        getInput(3)->setBackend(name, device);
    }
    else {
        Log::notice("BatchNorm_Op::setBackend(): could not set backend for variance input, because input is not connected");
    }

    if (getInput(4)) {
        getInput(4)->setBackend(name, device);
    }
    else {
        Log::notice("BatchNorm_Op::setBackend(): could not set backend for mean input, because input is not connected");
    }
}

template <Aidge::DimIdx_t DIM>
std::set<std::string> Aidge::BatchNorm_Op<DIM>::getAvailableBackends() const {
    return Registrar<BatchNorm_Op<DIM>>::getKeys();
}

template class Aidge::BatchNorm_Op<2>;
template class Aidge::BatchNorm_Op<3>;
template class Aidge::BatchNorm_Op<4>;

template <Aidge::DimSize_t DIM>
inline std::shared_ptr<Aidge::Node> Aidge::BatchNorm(const Aidge::DimSize_t nbFeatures,
                                       const float epsilon,
                                       const float momentum,
                                       const bool trainingMode,
                                       const std::string& name) {
    static_assert(DIM<=MaxDim,"Too many kernel dimensions required by BatchNorm, not supported");
    auto batchNorm = std::make_shared<Node>(std::make_shared<BatchNorm_Op<static_cast<DimIdx_t>(DIM)>>(epsilon, momentum, trainingMode), name);
    addProducer(batchNorm, 1, {nbFeatures}, "scale");
    addProducer(batchNorm, 2, {nbFeatures}, "shift");
    addProducer(batchNorm, 3, {nbFeatures}, "batch_mean");
    addProducer(batchNorm, 4, {nbFeatures}, "batch_variance");
    return batchNorm;
}

template std::shared_ptr<Aidge::Node> Aidge::BatchNorm<2>(const DimSize_t, const float, const float, const bool, const std::string&);
template std::shared_ptr<Aidge::Node> Aidge::BatchNorm<3>(const DimSize_t, const float, const float, const bool, const std::string&);
template std::shared_ptr<Aidge::Node> Aidge::BatchNorm<4>(const DimSize_t, const float, const float, const bool, const std::string&);
