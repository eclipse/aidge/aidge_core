/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Memorize.hpp"

#include <memory>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"

Aidge::Elts_t Aidge::Memorize_ProdConso::getNbRequiredData(
    Aidge::IOIndex_t inputIdx) const
{
    const Memorize_Op& op = dynamic_cast<const Memorize_Op&>(mOp);

    if (op.scheduleStep() == 0 && inputIdx == 0) {
        // No data input is required for the initial step.
        // Initialization data is required however.
        return Elts_t::NoneElts();
    }
    else if (op.scheduleStep() > 0 && inputIdx == 1) {
        // No initialization data is required after the initial step.
        return Elts_t::NoneElts();
    }
    else {
        return ProdConso::getNbRequiredData(inputIdx);
    }
}

Aidge::Elts_t Aidge::Memorize_ProdConso::getRequiredMemory(const Aidge::IOIndex_t outputIdx,
                                                         const std::vector<Aidge::DimSize_t> &/*inputsSize*/) const {
    assert(mOp.getRawOutput(outputIdx) && "requires valid output");

    const Memorize_Op& op = dynamic_cast<const Memorize_Op&>(mOp);

    if ((op.endStep() > 0) && (outputIdx == 1) && (op.scheduleStep() >= op.endStep())) {
        return Elts_t::NoneElts();
    }
    else {
        return Elts_t::DataElts(op.getOutput(outputIdx)->size());
    }
}

void Aidge::Memorize_ProdConso::updateConsummerProducer() {
    ProdConso::updateConsummerProducer();

    const Memorize_Op& op = dynamic_cast<const Memorize_Op&>(mOp);
    AIDGE_ASSERT(op.endStep() == 0 || op.scheduleStep() <= op.endStep(), "cannot update consumer producer anymore, number of cycles exceeded");
}

void Aidge::Memorize_OpImpl::forward() {
    const Memorize_Op& op = dynamic_cast<const Memorize_Op&>(mOp);

    AIDGE_ASSERT((op.endStep() == 0) || (op.forwardStep() <= op.endStep()), "cannot forward anymore, number of cycles exceeded");

    if (op.forwardStep() == 0) {
        op.getOutput(0)->getImpl()->copy(op.getInput(1)->getImpl()->rawPtr(), op.getInput(1)->size());
    }
    else {
        op.getOutput(0)->getImpl()->copy(op.getInput(0)->getImpl()->rawPtr(), op.getInput(0)->size());
    }
}

const std::string Aidge::Memorize_Op::Type = "Memorize";

Aidge::Memorize_Op::Memorize_Op(const std::uint32_t endStep)
    : OperatorTensor(Type, {InputCategory::Data, InputCategory::Data}, 2),
        mAttributes(std::make_shared<Attributes_>(
                    attr<MemorizeAttr::ScheduleStep>(0),
                    attr<MemorizeAttr::ForwardStep>(0),
                    attr<MemorizeAttr::EndStep>(endStep)))
{
    // The input idx 0 is a back edge for Memorize where inputs are (back, init)
    setBackEdges({0});
    mOutputs[1] = mOutputs[0];
}

Aidge::Memorize_Op::Memorize_Op(const Aidge::Memorize_Op& op)
    : OperatorTensor(op),
        mAttributes(op.mAttributes)
{
    if (op.mImpl) {
        SET_IMPL_MACRO(Memorize_Op, *this, op.backend());
    } else {
        mImpl = nullptr;
    }
    mOutputs[1] = mOutputs[0];
}

std::shared_ptr<Aidge::Operator> Aidge::Memorize_Op::clone() const {
    return std::make_shared<Memorize_Op>(*this);
}


void Aidge::Memorize_Op::updateConsummerProducer() {
    Operator::updateConsummerProducer();
    ++mAttributes->template getAttr<MemorizeAttr::ScheduleStep>();
    mAttributes->template getAttr<MemorizeAttr::ForwardStep>() = 0;
}

bool Aidge::Memorize_Op::forwardDims(bool /*allowDataDependency*/) {
    if (inputsAssociated(false)) {
        // Only require one of the input to have dims defined
        // Otherwise, forwardDims() won't converge!
        if (!(getInput(0)->undefined())) {
            const auto expectedDims =  getInput(0)->dims();
            mOutputs[0]->resize(expectedDims);
            return true;
        }
        else if (!(getInput(1)->undefined())) {
            const auto expectedDims =  getInput(1)->dims();
            mOutputs[0]->resize(expectedDims);
            return true;
        }
    }

    return false;
}

bool Aidge::Memorize_Op::dimsForwarded() const {
    // Only check the output dims
    bool forwarded = true;
    // check outputs have been filled
    for (IOIndex_t i = 0; i < nbOutputs(); ++i) {
        forwarded &= !(getOutput(i)->undefined());
    }
    return forwarded;
}

void Aidge::Memorize_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    if (Registrar<Memorize_Op>::exists({name})){
        SET_IMPL_MACRO(Memorize_Op, *this, name);
    }
    else {
        mImpl = std::make_shared<Memorize_OpImpl>(*this);
    }
    mOutputs[0]->setBackend(name, device);
}

void Aidge::Memorize_Op::forward() {
    OperatorTensor::forward();
    ++mAttributes->template getAttr<MemorizeAttr::ForwardStep>();
    mAttributes->template getAttr<MemorizeAttr::ScheduleStep>() = 0;
}

std::set<std::string> Aidge::Memorize_Op::getAvailableBackends() const {
    return Registrar<Memorize_Op>::getKeys();
}

/////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Memorize(const std::uint32_t endStep, const std::string& name) {
    return std::make_shared<Node>(std::make_shared<Memorize_Op>(endStep), name);
}
