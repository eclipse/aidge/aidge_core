/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/ConstantOfShape.hpp"

#include <cstdint>
#include <fmt/format.h>
#include <memory>
#include <stdexcept> // std::runtime_error
#include <string>
#include <vector>

#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/data/half.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

const std::string ConstantOfShape_Op::Type = "ConstantOfShape";

bool ConstantOfShape_Op::forwardDims(bool allowDataDependency) {
  if (!inputsAssociated()) {
    return false;
  }

  if (!allowDataDependency) {
    Log::warn("{} : unable to forwardDims() because output dims are data "
              "dependent on input#0",
              type());
    return false;
  }

  AIDGE_ASSERT(getInput(0)->nbDims() == 1,
               "{} : Input tensor should have only 1 dimension. {} dimensions"
               "received : {}",
               __func__, getInput(0)->nbDims(), getInput(0)->dims());
  AIDGE_ASSERT(getInput(0)->dataType() == DataType::Int64,
               "{} : Input tensor data type should be int64t, received : {}",
               __func__, getInput(0)->nbDims(), getInput(0)->dims());
  std::vector<DimSize_t> output_dims;
  output_dims.reserve(getInput(0)->size());
  for (std::size_t i = 0; i < getInput(0)->size(); ++i) {
    auto temp = getInput(0)->template get<std::int64_t>(i);
    output_dims.push_back(temp);
  }
  mOutputs[0]->resize(output_dims);
  return true;
}

void ConstantOfShape_Op::setBackend(const std::string &name,
                                       Aidge::DeviceIdx_t device) {
  SET_IMPL_MACRO(ConstantOfShape_Op, *this, name);
  mOutputs[0]->setBackend(name, device);
  value().setBackend(name,device);
}

std::set<std::string> Aidge::ConstantOfShape_Op::getAvailableBackends() const {
  return Registrar<ConstantOfShape_Op>::getKeys();
}

} // namespace Aidge

