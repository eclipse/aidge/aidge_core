/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Reshape.hpp"

#include <cstddef>    // std::size_t
#include <cstdint>    // std::int64_t
#include <memory>
#include <stdexcept>  // std::runtime_error
#include <string>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

void Aidge::Reshape_OpImpl::forward() {
    const Reshape_Op& op = dynamic_cast<const Reshape_Op&>(mOp);
    AIDGE_ASSERT(op.getInput(0), "missing input#0");
    // const auto& input = op.getInput(0)->refCastFrom(mInputFallback, *op.getOutput(0));
    op.getOutput(0)->getImpl()->copy(op.getInput(0)->getImpl()->rawPtr(), op.getInput(0)->size());
}

void Aidge::Reshape_OpImpl::backward() {
    const Reshape_Op& op = dynamic_cast<const Reshape_Op&>(mOp);
    AIDGE_ASSERT(op.getOutput(0)->grad(), "missing gradient for output#0");
    // const auto& output_grad = op.getOutput(0)->grad()->refCastFrom(mOutputGradFallback, *op.getOutput(0)->grad());
    op.getInput(0)->grad()->getImpl()->copy(op.getOutput(0)->grad()->getImpl()->rawPtr(), op.getOutput(0)->size());
}

//////////////////////////////////////////////////

const std::string Aidge::Reshape_Op::Type = "Reshape";

Aidge::Reshape_Op::Reshape_Op(const std::vector<std::int64_t>& shape, bool allowzero)
    : OperatorTensor(Type, {InputCategory::Data, InputCategory::OptionalData}, 1),
        mAttributes(std::make_shared<Attributes_>(
        attr<ReshapeAttr::Shape>(shape),
        attr<ReshapeAttr::AllowZero>(allowzero)))
{
    mImpl = std::make_shared<Reshape_OpImpl>(*this);
}

Aidge::Reshape_Op::Reshape_Op(const Aidge::Reshape_Op& op)
    : OperatorTensor(op),
        mAttributes(op.mAttributes)
{
    if (!op.backend().empty()) {
        SET_IMPL_MACRO(Reshape_Op, *this, op.backend());
    }
    else {
        mImpl = std::make_shared<Reshape_OpImpl>(*this);
    }
}

std::shared_ptr<Aidge::Operator> Aidge::Reshape_Op::clone() const {
    return std::make_shared<Reshape_Op>(*this);
}

bool Aidge::Reshape_Op::dimsForwarded() const {
    if (getInput(1) && !getInput(1)->undefined()) {
        // output dims are data dependent
        return false;
    }

    return OperatorTensor::dimsForwarded();
}

bool Aidge::Reshape_Op::forwardDims(bool allowDataDependency) {
    if (inputsAssociated()) {
        // Copy optional input #1, if present, to attribute Shape
        if (getInput(1)) {
            if (!this->shape().empty()) {
                Log::notice("Reshape_Op: ignoring non-empty Shape attribute because input#1 takes precedence");
            }

            if (!allowDataDependency) {
                Log::warn("Reshape_Op: unable to forwardDims() because output dims are data dependent on input#1");
                return false;
            }

            std::shared_ptr<Tensor> fallback;
            this->shape().clear(); // If both are provided input would override attrs
            this->shape().reserve(getInput(1)->size());
            const auto& shape = mInputs[1]->refCastFrom(fallback, NativeType_v<int64_t>, "cpu");
            std::copy_n(static_cast<int64_t*>(shape.getImpl()->hostPtr()),
                        shape.size(),
                        std::back_inserter(this->shape()));
        }

        AIDGE_ASSERT(!this->shape().empty(), "Missing input#1 or Shape attribute");

        // Compute output dims
        std::vector<DimSize_t> outDims;
        // variables to handle a negative dimension
        bool foundNegativeDimension = false;
        std::size_t outSize = 1;
        DimIdx_t negativeIndex = 0;
        for(std::size_t i = 0; i < this->shape().size(); ++i)
        {
            int64_t dimSize = this->shape()[i];
            if (dimSize < 0) {
                AIDGE_ASSERT(!foundNegativeDimension, "Found more than one negative dimension in Reshape Operator: {}.", this->shape());
                foundNegativeDimension = true;
                dimSize = 1;
                negativeIndex = static_cast<DimIdx_t>(i);
            }
            else if (dimSize == 0 && !this->allowZero())
            {
                dimSize = getInput(0) -> dims()[i];
            }
            outDims.push_back(static_cast<DimSize_t>(dimSize));
            if (dimSize != 0) {
                outSize *= static_cast<DimSize_t>(dimSize);
            }
        }

        if (foundNegativeDimension) {
            outDims[negativeIndex] = (getInput(0) -> size()) / outSize;
        }

        mOutputs[0]->resize(outDims);
        return true;
    }

    return false;
}

void Aidge::Reshape_Op::setBackend(const std::string& name, Aidge::DeviceIdx_t device) {
    if (Registrar<Reshape_Op>::exists({name})){
        SET_IMPL_MACRO(Reshape_Op, *this, name);
    }
    else {
        mImpl = std::make_shared<Reshape_OpImpl>(*this);
    }
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Reshape_Op::getAvailableBackends() const {
    return Registrar<Reshape_Op>::getKeys();
}

//////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Reshape(const std::vector<std::int64_t>& shape,
                            bool allowzero,
                            const std::string &name)
{
    return std::make_shared<Node>(std::make_shared<Reshape_Op>(shape, allowzero), name);
}