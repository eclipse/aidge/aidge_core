/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Concat.hpp"

#include <string>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::Concat_Op::Type = "Concat";

Aidge::Concat_Op::Concat_Op(const Aidge::IOIndex_t nbIn, const std::int32_t axis)
    : OperatorTensor(Type, std::vector<InputCategory>(nbIn, InputCategory::Data), 1),
        mAttributes(std::make_shared<Attributes_>(
        attr<ConcatAttr::Axis>(axis)))
{
    if (nbIn == 0) {
        AIDGE_THROW_OR_ABORT(std::runtime_error, "Add operator should have at least one input.");
    }
    mImpl = std::make_shared<Concat_OpImpl>(*this);
}

Aidge::Concat_Op::Concat_Op(const Aidge::Concat_Op& op)
    : OperatorTensor(op),
        mAttributes(op.mAttributes)
{
    if (!op.backend().empty()) {
        SET_IMPL_MACRO(Concat_Op, *this, op.backend());
    }
    else {
        mImpl = std::make_shared<Concat_OpImpl>(*this);
    }
}

std::shared_ptr<Aidge::Operator> Aidge::Concat_Op::clone() const {
    return std::make_shared<Concat_Op>(*this);
}

void Aidge::Concat_OpImpl::forward() {
    const Concat_Op& op = dynamic_cast<const Concat_Op&>(mOp);
    auto axis = op.axis();
    const auto nbDimsInput0 = op.getInput(0)->nbDims();
    axis = (axis < 0) ? axis + static_cast<std::int32_t>(nbDimsInput0) : axis;

    assert(op.getInput(0) && "missing input in Concat operator");
    for (IOIndex_t i = 1; i < mOp.nbInputs(); ++i) {
        assert(op.getInput(i) && "missing input in Concat operator");
        assert(op.getInput(i)->dataType() == op.getInput(0)->dataType());
    }

    DimSize_t outputAxisValue = 0;
    for (IOIndex_t i = 0; i < mOp.nbInputs(); ++i) {
        outputAxisValue += op.getInput(i)->dims()[axis];
    }

    DimSize_t prodDimLower = 1;
    for (DimIdx_t i = 0; i < axis; ++i) {
        prodDimLower *= op.getInput(0)->dims()[i];
    }
    DimSize_t prodDimHigher = 1;
    for (DimIdx_t i = axis + 1; static_cast<std::size_t>(i) < op.getInput(0)->dims().size();
         ++i) {
        prodDimHigher *= op.getInput(0)->dims()[i];
    }

    std::size_t oIndexStart = 0;
    // std::size_t oIndex = 0;
    for (std::size_t inputId = 0; inputId < op.nbInputs(); ++inputId) {
        // oIndex = oIndexStart;
        const DimSize_t iOffset = prodDimHigher*op.getInput(inputId)->dims()[axis];
        for (std::size_t iIndex = 0, oIndex = oIndexStart; iIndex < prodDimLower; ++iIndex) {
            op.getOutput(0)->getImpl()->copy(op.getInput(inputId)->getImpl()->rawPtr(iIndex*iOffset), iOffset, oIndex);
            oIndex += prodDimHigher*outputAxisValue;
        }
        oIndexStart += op.getInput(inputId)->dims()[axis]*prodDimHigher;
    }
}


bool Aidge::Concat_Op::forwardDims(bool /*allowDataDependency*/) {
    if (!inputsAssociated()) {
        return false;
    }
    const std::size_t nbDimsInput0 = getInput(0)->nbDims();
    AIDGE_ASSERT(nbDimsInput0 > 0, "First input in {} Operator is scalar", type());
    for (IOIndex_t i = 1; i < nbInputs(); ++i) {
        AIDGE_ASSERT(nbDimsInput0 == getInput(i)->nbDims(),
            "Input 0 and input {} in {} Operator have different number of dimensions: {} / {}",
            i, type(), nbDimsInput0, getInput(i)->nbDims());
    }
    // Check validity of attributes with inputs
    // Axis
    std::int32_t axis = mAttributes->template getAttr<ConcatAttr::Axis>();
    axis = (axis < 0) ? axis + static_cast<std::int32_t>(nbDimsInput0) : axis;
    AIDGE_ASSERT(((axis >= 0) && (axis < static_cast<std::int32_t>(nbDimsInput0))),
                "'Axis' attribute not compatible with provided inputs.")
    const std::size_t axis_u64 = static_cast<std::size_t>(axis);

    // Check validity of inputs
    auto outputDims =  getInput(0)->dims();
    for (IOIndex_t i = 1; i < nbInputs(); ++i) {
        for (DimSize_t dim = 0; dim < nbDimsInput0; ++dim) {
            if (dim == axis_u64) {
                outputDims[axis_u64] += getInput(i)->dims()[axis_u64];
            }
            else {
                AIDGE_ASSERT(getInput(i)->dims()[dim] == outputDims[dim],
                    "Incomatible dimensions between input 0 {} and input {} {}",
                    getInput(0)->dims(), i, getInput(i)->dims());
            }
        }
    }

    getOutput(0)->resize(outputDims);
    return true;
}

void Aidge::Concat_Op::setBackend(const std::string& name, DeviceIdx_t device) {
    if (Registrar<Concat_Op>::exists({name})) {
        SET_IMPL_MACRO(Concat_Op, *this, name);
    }
    else {
        mImpl = std::make_shared<Concat_OpImpl>(*this);
    }
    mOutputs[0]->setBackend(name, device);
}

std::set<std::string> Aidge::Concat_Op::getAvailableBackends() const {
    return Registrar<Concat_Op>::getKeys();
}

/////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::Concat(const Aidge::IOIndex_t nbIn, const std::int32_t axis, const std::string& name) {
    return std::make_shared<Node>(std::make_shared<Concat_Op>(nbIn, axis), name);
}
