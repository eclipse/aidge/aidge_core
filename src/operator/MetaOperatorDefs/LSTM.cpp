/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/MetaOperatorDefs.hpp"

#include <memory>
#include <string>

#include "aidge/operator/Memorize.hpp"
#include "aidge/operator/Add.hpp"
#include "aidge/operator/Mul.hpp"
#include "aidge/operator/FC.hpp"
#include "aidge/operator/Identity.hpp"
#include "aidge/operator/Tanh.hpp"

namespace Aidge {

std::shared_ptr<MetaOperator_Op> LSTM_Op(const DimSize_t seqLength,
                                         const std::string& name)
{
    // Construct micro-graph
    auto input = Identity((!name.empty()) ? name + "_input" : "");
    auto hiddenState = Memorize(seqLength, (!name.empty()) ? name + "_hidden_state" : "");
    auto cellState = Memorize(seqLength, (!name.empty()) ? name + "_cell_state" : "");
    auto add = Add((!name.empty()) ? name + "_add" : "");

    // Forget gate
    auto forgetGateX = std::make_shared<Node>(std::make_shared<FC_Op>(), (!name.empty()) ? name + "_forgetGateX" : "");
    input->addChild(forgetGateX, 0, 0);
    auto forgetGateH = std::make_shared<Node>(std::make_shared<FC_Op>(), (!name.empty()) ? name + "_forgetGateH" : "");
    hiddenState->addChild(forgetGateH, 1, 0);
    auto forgetGate = Add((!name.empty()) ? name + "_forgetGate" : "");
    forgetGateX->addChild(forgetGate, 0, 0);
    forgetGateH->addChild(forgetGate, 0, 1);
    auto forgetGateAct = Sigmoid((!name.empty()) ? name + "_forgetGateAct" : "");
    auto forgetGateMul = Mul((!name.empty()) ? name + "_forgetGateMul" : "");
    forgetGate->addChild(forgetGateAct, 0, 0);
    forgetGateAct->addChild(forgetGateMul, 0, 0);
    forgetGateMul->addChild(add, 0, 0);
    cellState->addChild(forgetGateMul, 1, 1);

    // Input gate
    auto inputGateX = std::make_shared<Node>(std::make_shared<FC_Op>(), (!name.empty()) ? name + "_inputGateX" : "");
    input->addChild(inputGateX, 0, 0);
    auto inputGateH = std::make_shared<Node>(std::make_shared<FC_Op>(), (!name.empty()) ? name + "_inputGateH" : "");
    hiddenState->addChild(inputGateH, 1, 0);
    auto inputGate = Add((!name.empty()) ? name + "_inputGate" : "");
    inputGateX->addChild(inputGate, 0, 0);
    inputGateH->addChild(inputGate, 0, 1);
    auto inputGateAct = Sigmoid((!name.empty()) ? name + "_inputGateAct" : "");
    auto inputGateMul = Mul((!name.empty()) ? name + "_inputGateMul" : "");
    inputGate->addChild(inputGateAct, 0, 0);
    inputGateAct->addChild(inputGateMul, 0, 0);
    inputGateMul->addChild(add, 0, 1);

    // Candidate for cell update
    auto cellCandidateX = std::make_shared<Node>(std::make_shared<FC_Op>(), (!name.empty()) ? name + "_cellCandidateX" : "");
    input->addChild(cellCandidateX, 0, 0);
    auto cellCandidateH = std::make_shared<Node>(std::make_shared<FC_Op>(), (!name.empty()) ? name + "_cellCandidateH" : "");
    hiddenState->addChild(cellCandidateH, 1, 0);
    auto cellCandidate = Add((!name.empty()) ? name + "_cellCandidate" : "");
    cellCandidateX->addChild(cellCandidate, 0, 0);
    cellCandidateH->addChild(cellCandidate, 0, 1);
    auto cellCandidateAct = Tanh((!name.empty()) ? name + "_cellCandidateAct" : "");
    cellCandidate->addChild(cellCandidateAct, 0, 0);
    cellCandidateAct->addChild(inputGateMul, 0, 1);

    // Output gate
    auto outputGateX = std::make_shared<Node>(std::make_shared<FC_Op>(), (!name.empty()) ? name + "_outputGateX" : "");
    input->addChild(outputGateX, 0, 0);
    auto outputGateH = std::make_shared<Node>(std::make_shared<FC_Op>(), (!name.empty()) ? name + "_outputGateH" : "");
    hiddenState->addChild(outputGateH, 1, 0);
    auto outputGate = Add((!name.empty()) ? name + "_outputGate" : "");
    outputGateX->addChild(outputGate, 0, 0);
    outputGateH->addChild(outputGate, 0, 1);
    auto outputGateAct = Sigmoid((!name.empty()) ? name + "_outputGateAct" : "");
    auto outputGateMul = Mul((!name.empty()) ? name + "_outputGateMul" : "");
    outputGate->addChild(outputGateAct, 0, 0);
    outputGateAct->addChild(outputGateMul, 0, 0);

    // Updated cell state to help determine new hidden state
    auto cellUpdatedAct = Tanh((!name.empty()) ? name + "_cellUpdatedAct" : "");
    add->addChild(cellUpdatedAct, 0, 0);
    cellUpdatedAct->addChild(outputGateMul, 0, 1);
    outputGateMul->addChild(hiddenState, 0, 0);
    add->addChild(cellState, 0, 0);

    std::shared_ptr<GraphView> microGraph = std::make_shared<GraphView>();
    microGraph->add(input);
    microGraph->add({hiddenState, cellState, add,
        forgetGateX, forgetGateH, forgetGate, forgetGateAct, forgetGateMul,
        inputGateX, inputGateH, inputGate, inputGateAct, inputGateMul,
        cellCandidateX, cellCandidateH, cellCandidate, cellCandidateAct,
        outputGateX, outputGateH, outputGate, outputGateAct, outputGateMul,
        cellUpdatedAct}, false);

    microGraph->setOrderedInputs({{input, 0},
        {inputGateX, 1}, {outputGateX, 1}, {forgetGateX, 1}, {cellCandidateX, 1},
        {inputGateH, 1}, {outputGateH, 1}, {forgetGateH, 1}, {cellCandidateH, 1},
        {inputGateX, 2}, {outputGateX, 2}, {forgetGateX, 2}, {cellCandidateX, 2},
        {inputGateH, 2}, {outputGateH, 2}, {forgetGateH, 2}, {cellCandidateH, 2},
        {hiddenState, 1}, {cellState, 1}});
    microGraph->setOrderedOutputs({{hiddenState, 0}, {cellState, 0}});

    return std::make_shared<MetaOperator_Op>("LSTM", microGraph);
}

std::shared_ptr<Node> LSTM(const DimSize_t inChannel,
                           const DimSize_t hiddenChannel,
                           const DimSize_t seqLength,
                           bool noBias,
                           const std::string& name)
{
    auto op = LSTM_Op(seqLength, name);
    auto metaOp = std::make_shared<Node>(op, name);
    op->setUpperNode(metaOp);
    addProducer(metaOp, 1, {hiddenChannel, inChannel}, "wi");
    addProducer(metaOp, 2, {hiddenChannel, inChannel}, "wo");
    addProducer(metaOp, 3, {hiddenChannel, inChannel}, "wf");
    addProducer(metaOp, 4, {hiddenChannel, inChannel}, "wc");
    addProducer(metaOp, 5, {hiddenChannel, hiddenChannel}, "ri");
    addProducer(metaOp, 6, {hiddenChannel, hiddenChannel}, "ro");
    addProducer(metaOp, 7, {hiddenChannel, hiddenChannel}, "rf");
    addProducer(metaOp, 8, {hiddenChannel, hiddenChannel}, "rc");
    if (!noBias) {
        addProducer(metaOp, 9, {hiddenChannel}, "wbi");
        addProducer(metaOp, 10, {hiddenChannel}, "wbo");
        addProducer(metaOp, 11, {hiddenChannel}, "wbf");
        addProducer(metaOp, 12, {hiddenChannel}, "wbc");
        addProducer(metaOp, 13, {hiddenChannel}, "rbi");
        addProducer(metaOp, 14, {hiddenChannel}, "rbo");
        addProducer(metaOp, 15, {hiddenChannel}, "rbf");
        addProducer(metaOp, 16, {hiddenChannel}, "rbc");
    }
    return metaOp;
}

} // namespace Aidge
