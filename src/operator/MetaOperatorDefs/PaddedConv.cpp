/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/MetaOperatorDefs.hpp"

#include <array>
#include <memory>

#include "aidge/graph/Node.hpp"
#include "aidge/graph/OpArgs.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/MetaOperator.hpp"
#include "aidge/operator/Pad.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/Types.h"

template <std::array<Aidge::DimSize_t, 1>::size_type DIM>
std::shared_ptr<Aidge::Node> Aidge::PaddedConv(Aidge::DimSize_t in_channels,
                                  Aidge::DimSize_t out_channels,
                                  const std::array<Aidge::DimSize_t, DIM> &kernel_dims,
                                  const std::string& name,
                                  const std::array<Aidge::DimSize_t, DIM> &stride_dims,
                                  const std::array<Aidge::DimSize_t, 2*DIM> &padding_dims,
                                  const std::array<Aidge::DimSize_t, DIM> &dilation_dims,
                                  bool no_bias)
{
    // auto metaOp = PaddedConv_Op<DIM>(kernel_dims, stride_dims, padding_dims, dilation_dims);
    // if (!name.empty()) {
    //     metaOp->getMicroGraph()->setName(name);
    //     metaOp->getMicroGraph()->setNodesName();
    // }
    // auto metaOpNode = std::make_shared<Node>(metaOp, name);
    auto graph = Sequential({
        Pad<DIM>(padding_dims, (!name.empty()) ? name + "_pad" : ""),
        std::make_shared<Node>(std::make_shared<Conv_Op<static_cast<DimIdx_t>(DIM)>>(kernel_dims, stride_dims, dilation_dims), (!name.empty()) ? name + "_conv" : "")
    });
    auto metaOpNode = MetaOperator(("PaddedConv" + std::to_string(DIM) + "D").c_str(), graph, {}, name);
    addProducer(metaOpNode, 1, append(out_channels, append(in_channels, kernel_dims)), "w");
    if (!no_bias) {
        addProducer(metaOpNode, 2, {out_channels}, "b");
    }
    return metaOpNode;
}
template std::shared_ptr<Aidge::Node> Aidge::PaddedConv<1>(const Aidge::DimSize_t, const Aidge::DimSize_t, const std::array<Aidge::DimSize_t,1>&, const std::string&, const std::array<Aidge::DimSize_t, 1>&, const std::array<Aidge::DimSize_t, 2>&, const std::array<Aidge::DimSize_t, 1>&, bool);
template std::shared_ptr<Aidge::Node> Aidge::PaddedConv<2>(const Aidge::DimSize_t, const Aidge::DimSize_t, const std::array<Aidge::DimSize_t,2>&, const std::string&, const std::array<Aidge::DimSize_t, 2>&, const std::array<Aidge::DimSize_t, 4>&, const std::array<Aidge::DimSize_t, 2>&, bool);

template <std::array<Aidge::DimSize_t, 1>::size_type DIM>
std::shared_ptr<Aidge::MetaOperator_Op> Aidge::PaddedConv_Op(
                                  const std::array<Aidge::DimSize_t, DIM> &kernel_dims,
                                  const std::array<Aidge::DimSize_t, DIM> &stride_dims,
                                  const std::array<Aidge::DimSize_t, 2*DIM> &padding_dims,
                                  const std::array<Aidge::DimSize_t, DIM> &dilation_dims)
{
    auto pad = Pad<DIM>(padding_dims, "", PadBorderType::Constant, 0.0);
    auto conv = std::make_shared<Node>(std::make_shared<Conv_Op<static_cast<DimIdx_t>(DIM)>>(kernel_dims, stride_dims, dilation_dims), "");

    return std::make_shared<MetaOperator_Op>(("PaddedConv" + std::to_string(DIM) + "D").c_str(), Sequential({pad, conv}));
}
template std::shared_ptr<Aidge::MetaOperator_Op> Aidge::PaddedConv_Op<1>(const std::array<Aidge::DimSize_t, 1>&, const std::array<Aidge::DimSize_t, 1>&, const std::array<Aidge::DimSize_t, 2>&, const std::array<Aidge::DimSize_t, 1>&);
template std::shared_ptr<Aidge::MetaOperator_Op> Aidge::PaddedConv_Op<2>(const std::array<Aidge::DimSize_t, 2>&, const std::array<Aidge::DimSize_t, 2>&, const std::array<Aidge::DimSize_t, 4>&, const std::array<Aidge::DimSize_t, 2>&);

// helper with C-style array instead of std::array for kernel_dims to allow automatic template DIM deduction
template <Aidge::DimSize_t DIM>
std::shared_ptr<Aidge::Node> Aidge::PaddedConv(
    Aidge::DimSize_t in_channels,
    Aidge::DimSize_t out_channels,
    Aidge::DimSize_t const (&kernel_dims)[DIM],
    const std::string& name,
    const std::array<Aidge::DimSize_t, DIM> &stride_dims,
    const std::array<Aidge::DimSize_t, 2*DIM> &padding_dims,
    const std::array<Aidge::DimSize_t, DIM> &dilation_dims,
    bool no_bias)
{
    return PaddedConv(in_channels, out_channels, to_array(kernel_dims), name, stride_dims, padding_dims, dilation_dims, no_bias);
}
template std::shared_ptr<Aidge::Node> Aidge::PaddedConv<1>(const Aidge::DimSize_t, const Aidge::DimSize_t, const Aidge::DimSize_t (&)[1], const std::string&, const std::array<Aidge::DimSize_t, 1>&, const std::array<Aidge::DimSize_t, 2>&, const std::array<Aidge::DimSize_t, 1>&, bool);
template std::shared_ptr<Aidge::Node> Aidge::PaddedConv<2>(const Aidge::DimSize_t, const Aidge::DimSize_t, const Aidge::DimSize_t (&)[2], const std::string&, const std::array<Aidge::DimSize_t, 2>&, const std::array<Aidge::DimSize_t, 4>&, const std::array<Aidge::DimSize_t, 2>&, bool);
