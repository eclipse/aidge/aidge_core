#include "aidge/filler/Filler.hpp"
#include "aidge/operator/Add.hpp"
#include "aidge/operator/Heaviside.hpp"
#include "aidge/operator/Identity.hpp"
#include "aidge/operator/Memorize.hpp"
#include "aidge/operator/MetaOperatorDefs.hpp"
#include "aidge/operator/Mul.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/operator/Sub.hpp"

namespace Aidge {

constexpr auto memorizeOpDataOutputRecIndex = 1;
/* constexpr auto memorizeOpDataOutputIndex = 0; */ // unused variable

std::shared_ptr<Node> Leaky(const int nbTimeSteps,
                            const float beta,
                            const float threshold,
                            const std::string &name) {

    auto microGraph = std::make_shared<GraphView>();

    auto inputNode = Identity((!name.empty()) ? name + "_input" : "");
    auto addNode = Add(!name.empty() ? name + "_add" : "");
    auto mulNode = Mul(!name.empty() ? name + "_mul" : "");
    auto subNode = Sub(!name.empty() ? name + "_sub" : "");
    auto hsNode = Heaviside(0, !name.empty() ? name + "_hs" : "");
    auto subNode2 = Sub(!name.empty() ? name + "_threshold" : "");
    auto reset = Mul(!name.empty() ? name + "_reset" : "");

    auto betaTensor = std::make_shared<Tensor>(beta);
    auto uthTensor = std::make_shared<Tensor>(static_cast<float>(threshold));
    uniformFiller<float>(uthTensor, threshold, threshold);

    auto decayRate = Producer(betaTensor, "leaky_beta", true);
    auto uth = Producer(uthTensor, "leaky_uth", true);

    auto potentialMem =
        Memorize(nbTimeSteps, (!name.empty()) ? name + "_potential" : "");
    auto spikeMem =
        Memorize(nbTimeSteps, (!name.empty()) ? name + "_spike" : "");

    // U[t] = Input[T] + beta * U[T-1] - S[T-1] * U_th
    // with S[T] = | 1, if U[T] - U_th > 0
    //             | 0 otherwise

    // beta * U[T-1]
    decayRate->addChild(/*otherNode=*/mulNode, /*outId=*/0, /*otherInId=*/1);
    potentialMem->addChild(mulNode, 1, 0);

    // Input[T] + beta * U[T-1]
    mulNode->addChild(/*otherNode=*/addNode, /*outId=*/0, /*otherInId=*/1);
    inputNode->addChild(/*otherNode=*/addNode, /*outId=*/0, /*otherInId=*/0);

    // S[T-1] * U_th
    spikeMem->addChild(reset,
                       /*outId=*/memorizeOpDataOutputRecIndex,
                       /*otherInId=*/0);

    // TODO(#219) Handle hard/soft reset
    uth->addChild(reset, 0, 1);

    // Input[T] + beta * U[T-1] - S[T-1] * U_th
    addNode->addChild(subNode, 0, 0);
    reset->addChild(subNode, 0, 1);

    // U[t] = (Input[T] + beta * U[T-1]) - S[T-1]
    subNode->addChild(potentialMem, 0, 0);

    // U[T] - U_th
    subNode->addChild(subNode2, 0, 0);
    uth->addChild(subNode2, 0, 1);

    // with S[T] = | 1, if U[T] - U_th > 0
    subNode2->addChild(hsNode, 0, 0);
    hsNode->addChild(spikeMem, 0, 0);

    microGraph->add(inputNode);
    microGraph->add({addNode,
                     mulNode,
                     potentialMem,
                     decayRate,
                     uth,
                     spikeMem,
                     hsNode,
                     subNode,
                     subNode2,
                     reset},
                    false);

    microGraph->setOrderedInputs(
        {{inputNode, 0}, {potentialMem, 1}, {spikeMem, 1}});

    // NOTE: Outputs are NOT the memory nodes (as it is done in LSTM), to avoid
    // producing data during init. This way, we can plug an operator after
    // our node, and get correct results.
    microGraph->setOrderedOutputs({//{potentialMem, memorizeOpDataOutputIndex},
                                   //{spikeMem, memorizeOpDataOutputIndex}
                                   {subNode, 0},
                                   {hsNode, 0}});

    auto metaOp = MetaOperator(/*type*/ "Leaky",
                               /*graph*/ microGraph,
                               /*forcedInputsCategory=*/{},
                               /*name*/ "leaky");

    return metaOp;
}

std::shared_ptr<MetaOperator_Op> LeakyOp() {
    AIDGE_THROW_OR_ABORT(std::runtime_error, "Not implemented yet");
}
} // namespace Aidge
