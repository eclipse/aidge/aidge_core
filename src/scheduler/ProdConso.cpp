/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/scheduler/ProdConso.hpp"

#include <algorithm>  // std::fill
#include <cstddef>  // std::size_t
#include <memory>
#include <vector>

#include "aidge/data/Elts.hpp"
#include "aidge/operator/Operator.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"

Aidge::ProdConso::ProdConso(const Operator& op, bool inPlace):
    mOp(op),
    mInPlace(inPlace),
    mNbConsumedData(mOp.nbInputs(), Elts_t::NoneElts()),
    mNbProducedData(mOp.nbOutputs(), Elts_t::NoneElts())
{
    //ctor
}

Aidge::Elts_t Aidge::ProdConso::getNbRequiredData(const Aidge::IOIndex_t inputIdx) const {
    if (mOp.getRawInput(inputIdx)) {
        const auto input = std::static_pointer_cast<Tensor>(mOp.getRawInput(inputIdx));
        if (!input->undefined()) {
            // Known amount of data: requires the whole tensor by default
            return Elts_t::DataElts(input->size());
        }
        else {
            // Unknown amount of data: require a single token by default
            return Elts_t::TokenElts(1);
        }
    }

    // Input not connected, meaning it is an optional input: do no require anything!
    return Elts_t::NoneElts();
}

Aidge::Elts_t Aidge::ProdConso::getNbRequiredProtected(IOIndex_t inputIdx) const {
    if (mOp.getRawInput(inputIdx)) {
        const auto input = std::static_pointer_cast<Tensor>(mOp.getRawInput(inputIdx));
        if (!input->undefined()) {
            // Known amount of data: protect the whole tensor by default
            return Elts_t::DataElts((mInPlace) ? 0 : input->size());
        }
        else {
            // Unknown amount of data: protect a single token by default
            // (this does not really make sense for now, as getNbRequiredProtected()
            // is supposed to give a precise amount of data to protect for
            // memory management purpose...)
            return Elts_t::TokenElts((mInPlace) ? 0 : 1);
        }
    }

    // Input not connected, meaning it is an optional input: do no require anything!
    return Elts_t::NoneElts();
}

Aidge::Elts_t Aidge::ProdConso::getRequiredMemory(const Aidge::IOIndex_t outputIdx,
                                                         const std::vector<Aidge::DimSize_t> &/*inputsSize*/) const {
    if (mOp.getRawOutput(outputIdx)) {
        const auto output = std::static_pointer_cast<Tensor>(mOp.getRawOutput(outputIdx));
        if (!output->undefined()) {
            // Known amount of data: requires the whole tensor by default,
            // regardless of available data on inputs
            return Elts_t::DataElts(output->size());
        }
        else {
            // Unknown amount of data: require a single token by default
            // (this does not really make sense for now, as getRequiredMemory()
            // is supposed to give a precise amount of data to allocate for
            // memory management purpose...)
            return Elts_t::TokenElts(1);
        }
    }

    // Output not set, meaning it is an optional output: do no require anything!
    return Elts_t::NoneElts();
}

Aidge::Elts_t Aidge::ProdConso::getNbConsumedData(Aidge::IOIndex_t inputIdx) const {
    AIDGE_ASSERT(static_cast<std::size_t>(inputIdx) < mNbConsumedData.size(),
        "input index ({}) is out of bound ({}) for operator type {}",
        inputIdx, mNbConsumedData.size(), mOp.type());
    return mNbConsumedData[static_cast<std::size_t>(inputIdx)];
}

Aidge::Elts_t Aidge::ProdConso::getNbProducedData(Aidge::IOIndex_t outputIdx) const {
    AIDGE_ASSERT(static_cast<std::size_t>(outputIdx) < mNbProducedData.size(),
        "output index ({}) is out of bound ({}) for operator type {}",
        outputIdx, mNbProducedData.size(), mOp.type());
    return mNbProducedData[static_cast<std::size_t>(outputIdx)];
}

void Aidge::ProdConso::updateConsummerProducer(){
    // Update producer-consumer data
    for (std::size_t inputIdx = 0; inputIdx < mNbConsumedData.size(); ++inputIdx) {
        // each input is consumed by the minimum amount for a forward pass
        mNbConsumedData[inputIdx] += getNbRequiredData(static_cast<IOIndex_t>(inputIdx));
    }

    for (std::size_t outputIdx = 0; outputIdx < mNbProducedData.size(); ++outputIdx) {
        mNbProducedData[outputIdx] += getRequiredMemory(outputIdx, {});
    }
}

void Aidge::ProdConso::resetConsummerProducer(){
    std::fill(mNbConsumedData.begin(), mNbConsumedData.end(), Elts_t::NoneElts());
    std::fill(mNbProducedData.begin(), mNbProducedData.end(), Elts_t::NoneElts());
}
