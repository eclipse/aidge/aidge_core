/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/utils/DynamicAttributes.hpp"

std::map<std::type_index, std::unique_ptr<Aidge::DynamicAttributes::AnyUtils_>> Aidge::DynamicAttributes::mAnyUtils = []() {
    std::map<std::type_index, std::unique_ptr<Aidge::DynamicAttributes::AnyUtils_>> m;
    m.emplace(typeid(DynamicAttributes), std::unique_ptr<AnyUtils<DynamicAttributes>>(new AnyUtils<DynamicAttributes>()));
    return m;
}();

template<> void Aidge::DynamicAttributes::setAttr<future_std::any>(const std::string& name, const future_std::any& value)
{
    const auto dot = name.find('.');
    if (dot == name.npos) {
        AIDGE_ASSERT(mAnyUtils.find(value.type()) != mAnyUtils.end(), "DynamicAttributes::setAttr(): cannot set value to std::any of never seen type.");

        auto res = mAttrs.emplace(std::make_pair(name, value));
        if (!res.second)
            res.first->second = value;
    }
    else {
        const auto ns = name.substr(0, dot);
        const auto nsName = name.substr(dot + 1);
        auto res = mAttrs.emplace(std::make_pair(ns, future_std::any(DynamicAttributes())));
        future_std::any_cast<DynamicAttributes&>(res.first->second).setAttr<future_std::any>(nsName, value);
    }
}

bool future_std::operator<(const future_std::any& lhs, const future_std::any& rhs) {
    if (lhs.type() == rhs.type()) {
        return Aidge::DynamicAttributes::mAnyUtils.at(lhs.type())->compare(lhs, rhs);
    }
#ifdef PYBIND
    else if (lhs.type() == typeid(py::object)) {
        return Aidge::DynamicAttributes::mAnyUtils.at(rhs.type())->compare(lhs, rhs);
    }
    else if (rhs.type() == typeid(py::object)) {
        return Aidge::DynamicAttributes::mAnyUtils.at(lhs.type())->compare(lhs, rhs);
    }
#endif
    else {
        return (lhs.type().before(rhs.type()));
    }
}
