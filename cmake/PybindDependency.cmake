function(add_pybind_dependency target_name)

    # This function add dependencies on pybind/python in the
    # case where a target depends on it. This is orthogonal to
    # the creation of a pybind python module.

    # In this case we need to add additional dependencies and distinguish the two link time usage for the archive:

    #### 1. link for producing a python binding module, which must not include the python interpreter

    # For the case 1, the archive is bound to a python module which will provide the runtime,
    # hence we add dependency only on the pybind and python headers. Also we install the pybind headers
    # for backward compatibility of dependent build systems which may not depend upon pybind.

    #### 2. link for producing an executable (tests for instance) which must include the python interpreter

    # For the case 2, a library or executable must also depend on the embedded python libraries,
    # hence we add dependency on Python::Python when the target is not a module. Also we account for
    # the case where the python libraries are not present (such as on cibuildwheel). In this case
    # only python modules can be built, not standalone executables.

    # Make detection of Development.Embed optional, we need to separate the components detections
    # otherwise the variables set by the Interpreter components may be undefined.
    find_package(Python COMPONENTS Interpreter)
    find_package(Python COMPONENTS Development)
    if(NOT Python_Development.Embed_FOUND)
        message(WARNING "Could not find Python embed libraries, fall back to Python Module only mode. If you are running this from `cibuildwheel, this warning is nominal.")
        find_package(Python COMPONENTS Development.Module)
    endif()

    # Set these variables which are used in the package config (aidge_core-config.cmake.in)
    # and for conditional build on the presence on the python interpreter library
    set(AIDGE_REQUIRES_PYTHON TRUE PARENT_SCOPE)
    set(AIDGE_PYTHON_HAS_EMBED ${Python_Development.Embed_FOUND} PARENT_SCOPE)

    # Add pybind11 headers dependencies, the headers for the package interface are installed below
    target_include_directories(${target_name} SYSTEM PUBLIC
        $<INSTALL_INTERFACE:include/_packages_deps/${target_name}>
        $<BUILD_INTERFACE:${pybind11_INCLUDE_DIR}>)

    # Add include dirs for Python.h
    target_include_directories(${target_name} SYSTEM PUBLIC ${Python_INCLUDE_DIRS})

    # Add Python embedded interpreter when the target is not a module (tests executables for instance)
    # Also requires to have Development.Embed installed on the system
    if (Python_Development.Embed_FOUND)
         set(target_is_module $<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>)
         target_link_libraries(${target_name} INTERFACE $<$<NOT:${target_is_module}>:Python::Python>)
    endif()

    # Install pybind headers such that dependent modules can find them
    install(DIRECTORY ${pybind11_INCLUDE_DIR}/pybind11
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/_packages_deps/${target_name}
    )

endfunction()
