/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cstdlib>
#include <memory>
#include <string>
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_random.hpp>

#include "aidge/data/Data.hpp"
#include "aidge/data/Interpolation.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/filler/Filler.hpp"
#include "aidge/operator/Pad.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

TEST_CASE("[core/data] Interpolation", "[Interpolation][Data]") {
    Log::setConsoleLevel(Log::Debug);

    auto tensor = std::make_shared<Tensor>(std::vector<DimSize_t>({10, 10}));
    tensor->setDataType(DataType::Float32);
    tensor->setBackend("cpu");
    Aidge::constantFiller(tensor, 1337.F);

    SECTION("retrieveNeighbours") {
        std::set<Interpolation::Point<float>> neighbours;
        std::set<Interpolation::Point<float>> expectedResult;

        std::vector<float> coords;
        SECTION("Out of bounds") {
            coords = {-0.5, -0.5};
            expectedResult = {{{-1, -1}, 0.f},
                              {{0, -1}, 0.F},
                              {{-1, 0}, 0.F},
                              {{0, 0}, 1337.F}};
            neighbours = Interpolation::retrieveNeighbours<float>(
                reinterpret_cast<float *>(tensor->getImpl()->rawPtr()),
                tensor->dims(),
                coords,
                PadBorderType::Zero);

            CHECK(neighbours == expectedResult);
        }
        SECTION("Some coords are rounds hence duplicates are filtered out") {
            tensor = std::make_shared<Tensor>(
                std::vector<DimSize_t>({5, 10, 10, 10}));
            tensor->setDataType(DataType::Float32);
            tensor->setBackend("cpu");
            Aidge::constantFiller(tensor, 1337.F);

            expectedResult = {{{0, 0, -1, -1}, 0.F},
                              {{0, 0, 0, -1}, 0.F},
                              {{0, 0, -1, 0}, 0.F},
                              {{0, 0, 0, 0}, 1337.F}};

            neighbours = Interpolation::retrieveNeighbours(
                reinterpret_cast<float *>(tensor->getImpl()->rawPtr()),
                tensor->dims(),
                std::vector<float>({0, 0, -0.25, -0.25}));
            CHECK(expectedResult == neighbours);
        }
    }
}
} // namespace Aidge
