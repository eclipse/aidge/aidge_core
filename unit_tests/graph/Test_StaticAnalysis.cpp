/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include <fmt/chrono.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/OpArgs.hpp"
#include "aidge/graph/StaticAnalysis.hpp"
#include "aidge/operator/Add.hpp"
#include "aidge/operator/BatchNorm.hpp"
#include "aidge/operator/FC.hpp"
#include "aidge/operator/ReLU.hpp"
#include "aidge/operator/MetaOperatorDefs.hpp"
#include "aidge/operator/Producer.hpp"

using namespace Aidge;

TEST_CASE("[core/graph] StaticAnalysis") {
    SECTION("Conv") {
        auto g1 = Sequential({
            Conv(3, 4, {5, 5}, "conv1"),
            ReLU("relu1"),
            PaddedConv(4, 8, {5, 5}, "conv2", {1, 1}, {2, 2, 2, 2}),
            ReLU("relu2"),
            PaddedConv(8, 16, {3, 3}, "conv3", {1, 1}, {2, 2, 2, 2}),
            ReLU("relu3"),
            PaddedConv(16, 16, {5, 5}, "conv4", {1, 1}, {2, 2, 2, 2}),
            Add("add"),
            PaddedConv(16, 16, {5, 5}, "conv5", {1, 1}, {2, 2, 2, 2}),
            ReLU("relu5"),
            Add("add2")
        });

        g1->getNode("relu3")->addChild(g1->getNode("add"), 0, 1);
        g1->getNode("conv5")->addChild(g1->getNode("add2"), 0, 1);
        g1->updateInputsOutputs();

        g1->forwardDims({{16, 3, 512, 512}});

        StaticAnalysis stats(g1);
        REQUIRE(stats.getNbParams(g1->getNode("conv1")) == 3 * 4 * 5 * 5 + 4);
        REQUIRE(stats.getNbParams(g1->getNode("conv2")) == 4 * 8 * 5 * 5 + 8);
        REQUIRE(stats.getNbParams(g1->getNode("conv3")) == 8 * 16 * 3 * 3 + 16);

        const auto conv1Stats = stats.getOpStats(g1->getNode("conv1"));
        REQUIRE(conv1Stats->getNbMACOps() == 1LL * (16 * 508 * 508) * (5 * 5 * 3 * 4));
        REQUIRE(conv1Stats->getNbArithmOps() == 2LL * (16 * 508 * 508) * (5 * 5 * 3 * 4));
        REQUIRE(conv1Stats->getNbArithmFpOps() == 2LL * (16 * 508 * 508) * (5 * 5 * 3 * 4));
        REQUIRE(conv1Stats->getNbArithmIntOps() == 0);

        g1->getNode("conv1")->getOperator()->setDataType(DataType::Int8);
        REQUIRE(conv1Stats->getNbMACOps() == 1LL * (16 * 508 * 508) * (5 * 5 * 3 * 4));
        REQUIRE(conv1Stats->getNbArithmOps() == 2LL * (16 * 508 * 508) * (5 * 5 * 3 * 4));
        REQUIRE(conv1Stats->getNbArithmFpOps() == 0);
        REQUIRE(conv1Stats->getNbArithmIntOps() == 2LL * (16 * 508 * 508) * (5 * 5 * 3 * 4));
    }
}
