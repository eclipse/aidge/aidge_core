/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include <string>
#include <vector>

#include "aidge/utils/StaticAttributes.hpp"

using namespace Aidge;

enum class TestAttr { a, b, c, d };

namespace {
template <>
const char *const EnumStrings<TestAttr>::data[] = {
    "a",
    "b",
    "c",
    "d"
};
}

using Attributes_ = StaticAttributes<TestAttr, int, float, std::string, std::vector<bool>>;
template <TestAttr e>
using attr = typename Attributes_::template attr<e>;

TEST_CASE("[core/attributes] StaticAttribute") {
    SECTION("TestAttr") {
        StaticAttributes<TestAttr, int, float, std::string, std::vector<bool>> attrs(
            attr<TestAttr::a>(42),
            attr<TestAttr::b>(18.75),
            attr<TestAttr::c>("test"),
            attr<TestAttr::d>({true, false, true}));

        REQUIRE(attrs.getAttr<int>("a") == 42);
        REQUIRE_THROWS(attrs.getAttr<int>("inexistent"));
    }
}
