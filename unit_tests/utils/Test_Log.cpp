/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/utils/Log.hpp"

#include <fmt/color.h>

using namespace Aidge;

TEST_CASE("[core/log] Log", "[Logger]") {
    SECTION("TestLog") {
        Log::setConsoleLevel(Log::Level::Debug);
        Log::debug("this is a debug message");
        Log::debug("{}", fmt::styled("green debug", fmt::fg(fmt::color::green)));
        Log::info("this is an info message");
        Log::notice("this is a notice message");
        Log::warn("this is a warn message");
        Log::error("this is an error message");
        AIDGE_LOG_CONTEXT("Testing the context of logger");
        Log::fatal("fatal");
        Log::debug("Now debug messages are supposed to [{}].", fmt::styled("appear", fmt::emphasis::italic));
        Log::info("Current consol level is {}", Log::getConsoleLevel());
        Log::setConsoleLevel(Log::Level::Warn);
        Log::notice("This message should not appear.");


    }
}
