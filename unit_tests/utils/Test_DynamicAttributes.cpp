/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include <string>
#include <vector>

#include "aidge/utils/DynamicAttributes.hpp"

using namespace Aidge;

TEST_CASE("[core/attributes] DynamicAttributes") {
    SECTION("TestAttr") {
        DynamicAttributes attrs;
        attrs.addAttr("a", 1);
        attrs.addAttr("b", 1.0f);
        attrs.addAttr("c", std::string("test"));
        attrs.addAttr<std::vector<bool>>("d", {false, true, false});

        REQUIRE(attrs.getAttr<int>("a") == 1);
        REQUIRE(attrs.getAttr<float>("b") == 1.0f);
        REQUIRE(attrs.getAttr<std::string>("c") == "test");
        REQUIRE(attrs.getAttr<std::vector<bool>>("d") == std::vector<bool>{{false, true, false}});

        attrs.addAttr("e", DynamicAttributes());
        attrs.getAttr<DynamicAttributes>("e").addAttr("e1", 1.0f);
        attrs.getAttr<DynamicAttributes>("e").addAttr("e2", std::string("test"));

        REQUIRE(attrs.getAttr<DynamicAttributes>("e").getAttr<float>("e1") == 1.0f);
        REQUIRE(attrs.getAttr<DynamicAttributes>("e").getAttr<std::string>("e2") == "test");
    }

    SECTION("TestAttrNS") {
        DynamicAttributes attrs;
        attrs.addAttr("mem.a", 1);
        attrs.addAttr("mem.data.b", 1.0f);
        attrs.addAttr("impl.c", std::string("test"));
        attrs.addAttr<std::vector<bool>>("d", {false, true, false});

        REQUIRE(attrs.getAttr<int>("mem.a") == 1);
        REQUIRE(attrs.getAttr<float>("mem.data.b") == 1.0f);
        REQUIRE(attrs.getAttr<std::string>("impl.c") == "test");
        REQUIRE(attrs.getAttr<std::vector<bool>>("d") == std::vector<bool>{{false, true, false}});

        attrs.getAttr<DynamicAttributes>("mem.data").addAttr("e", 2.0f);
        attrs.getAttr<DynamicAttributes>("impl").addAttr("f", std::string("test2"));
        REQUIRE(attrs.getAttr<float>("mem.data.e") == 2.0f);
        REQUIRE(attrs.getAttr<std::string>("impl.f") == "test2");

        REQUIRE(attrs.getAttr<DynamicAttributes>("mem.data").getAttr<float>("b") == 1.0f);
        REQUIRE(attrs.getAttr<DynamicAttributes>("impl").getAttr<std::string>("c") == "test");
    }
}
