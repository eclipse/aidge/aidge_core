/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Gather.hpp"

#include <cstdint>
#include <memory>


using namespace Aidge;

TEST_CASE("[cpu/operator] Gather(forward)", "[Gather][CPU]") {
    SECTION("2D Tensor axis 0") {
        std::shared_ptr<Tensor> input = std::make_shared<Tensor>(Array2D<int,3,3> {
            {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
            }
        });
        std::shared_ptr<Tensor> indexes = std::make_shared<Tensor>(Array2D<int,1,2> {
            {
                {1, 2}
            }
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array3D<int,1,2,3> {
            {
                {
                    {4, 5, 6},
                    {7, 8, 9}
                }
            }
        });

        std::shared_ptr<Node> myGather = Gather(std::int8_t(0));
        auto op = std::static_pointer_cast<OperatorTensor>(myGather -> getOperator());
        op->associateInput(0,input);
        op->associateInput(1,indexes);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        myGather->forward();
        op->getOutput(0)->print();
        expectedOutput->print();

        REQUIRE(*(op->getOutput(0)) == *expectedOutput);

    }
    SECTION("2D Tensor axis 1") {
        std::shared_ptr<Tensor> input = std::make_shared<Tensor>(Array2D<int,3,3> {
            {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
            }
        });
        std::shared_ptr<Tensor> indexes = std::make_shared<Tensor>(Array2D<int,1,2> {
            {
                {0, 2}
            }
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array3D<int,3,1,2> {
            {
                {
                    {1, 3}
                },
                {
                    {4, 6}
                },
                {
                    {7, 9}
                }
            }
        });

        std::shared_ptr<Node> myGather = Gather(1);
        auto op = std::static_pointer_cast<OperatorTensor>(myGather -> getOperator());
        op->associateInput(0,input);
        op->associateInput(1,indexes);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        myGather->forward();

        REQUIRE(*(op->getOutput(0)) == *expectedOutput);

    }
    SECTION("Init with attributes") {
        std::shared_ptr<Tensor> input = std::make_shared<Tensor>(Array2D<int,3,3> {
            {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
            }
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array3D<int,3,1,2> {
            {
                {
                    {1, 3}
                },
                {
                    {4, 6}
                },
                {
                    {7, 9}
                }
            }
        });

        std::shared_ptr<Node> myGather = Gather(1, {0, 2}, {1, 2});
        auto op = std::static_pointer_cast<OperatorTensor>(myGather -> getOperator());
        op->associateInput(0,input);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        myGather->forward();

        REQUIRE(*(op->getOutput(0)) == *expectedOutput);

    }
}