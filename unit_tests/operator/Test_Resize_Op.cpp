/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Resize.hpp"
#include <catch2/catch_test_macros.hpp>
#include <cstddef> // std::size_t
#include <cstdint>
#include <memory>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Log.hpp"

namespace Aidge {


TEST_CASE("[core/operator] Resize_Op(forwardDims)",
          "[Resize][forwardDimsScales]") {
  std::vector<Aidge::DimSize_t> input_dims;
  std::vector<float> scales;
  std::vector<std::size_t> sizes;
  std::vector<Aidge::DimSize_t> expected_dims;

  SECTION("Un-connected input leads to failure.") {
    input_dims = std::vector<Aidge::DimSize_t>({1, 1, 2, 2});
    std::shared_ptr<Tensor> input_data = std::make_shared<Tensor>(input_dims);

    auto resize_node = Resize();
    auto op = std::static_pointer_cast<Resize_Op>(resize_node->getOperator());
    op->associateInput(0, input_data);

    REQUIRE_THROWS(op->forwardDims(true));
  }

  SECTION("Connecting both Scales & Sizes leads to failure") {
    input_dims = std::vector<Aidge::DimSize_t>({1, 1, 2, 2});
    std::shared_ptr<Tensor> input_data = std::make_shared<Tensor>(input_dims);

    scales = std::vector<float>({.5, 3.0f, 2.0f, 2.0f});
    sizes = std::vector<std::size_t>({1, 3, 4, 4});
    expected_dims = std::vector<Aidge::DimSize_t>({2, 3, 4, 4});

    auto resize_node = Resize(scales, sizes);
    auto op = std::static_pointer_cast<Resize_Op>(resize_node->getOperator());
    op->associateInput(0, input_data);

    REQUIRE_THROWS(op->forwardDims(true));
  }

  SECTION("Input Scales") {
    SECTION("TEST 1") {
      input_dims = std::vector<Aidge::DimSize_t>({1, 1, 2, 2});
      std::shared_ptr<Tensor> input_data = std::make_shared<Tensor>(input_dims);

      scales = std::vector<float>({1, 1, 2, 2});
      sizes = std::vector<std::size_t>({});
      expected_dims = std::vector<Aidge::DimSize_t>({1, 1, 4, 4});
      auto resize_node = Resize(scales, sizes);
      auto op = std::static_pointer_cast<Resize_Op>(resize_node->getOperator());
      op->associateInput(0, input_data);

      REQUIRE_NOTHROW(op->forwardDims(true));
      REQUIRE(op->getOutput(0)->dims() == expected_dims);
    }

    SECTION("TEST 2") {
      input_dims = std::vector<Aidge::DimSize_t>({4, 4, 10, 10});
      std::shared_ptr<Tensor> input_data = std::make_shared<Tensor>(input_dims);

      scales = std::vector<float>({1, 1, 2, 3});
      sizes = std::vector<std::size_t>({});
      expected_dims = std::vector<Aidge::DimSize_t>({4, 4, 20, 30});
      auto resize_node = Resize(scales, sizes);
      auto op = std::static_pointer_cast<Resize_Op>(resize_node->getOperator());
      op->associateInput(0, input_data);

      REQUIRE_NOTHROW(op->forwardDims(true));
      REQUIRE(op->getOutput(0)->dims() == expected_dims);
    }
    SECTION("TEST 3") {
      input_dims = std::vector<Aidge::DimSize_t>({4, 2, 10, 10});
      std::shared_ptr<Tensor> input_data = std::make_shared<Tensor>(input_dims);

      scales = std::vector<float>({1, 1, 0.5, 0.5});
      sizes = std::vector<std::size_t>({});
      expected_dims = std::vector<Aidge::DimSize_t>({4, 2, 5, 5});
      auto resize_node = Resize(scales, sizes);
      auto op = std::static_pointer_cast<Resize_Op>(resize_node->getOperator());
      op->associateInput(0, input_data);

      REQUIRE_NOTHROW(op->forwardDims(true));
      REQUIRE(op->getOutput(0)->dims() == expected_dims);
    }
    SECTION("TEST 4") {
      input_dims = std::vector<Aidge::DimSize_t>({11, 11, 4, 4});
      std::shared_ptr<Tensor> input_data = std::make_shared<Tensor>(input_dims);


      scales = std::vector<float>({1, 1, 0.3, 0.3});
      sizes = std::vector<std::size_t>({});
      expected_dims = std::vector<Aidge::DimSize_t>({11, 11, 1, 1});
      auto resize_node = Resize(scales, sizes);
      auto op = std::static_pointer_cast<Resize_Op>(resize_node->getOperator());
      op->associateInput(0, input_data);

      REQUIRE_NOTHROW(op->forwardDims(true));
      REQUIRE(op->getOutput(0)->dims() == expected_dims);
    }
  }

  SECTION("Input Sizes") {
    SECTION("TEST 1") {
      input_dims = std::vector<Aidge::DimSize_t>({1, 1, 2, 2});
      std::shared_ptr<Tensor> input_data = std::make_shared<Tensor>(input_dims);

      scales = std::vector<float>({});
      sizes = std::vector<std::size_t>({4, 5, 8, 8});
      expected_dims = std::vector<Aidge::DimSize_t>({4, 5, 8, 8});
      auto resize_node = Resize(scales, sizes);
      auto op = std::static_pointer_cast<Resize_Op>(resize_node->getOperator());
      op->associateInput(0, input_data);

      REQUIRE_NOTHROW(op->forwardDims(true));
      REQUIRE(op->getOutput(0)->dims() == expected_dims);
    }
    SECTION("TEST 2") {
      input_dims = std::vector<Aidge::DimSize_t>({60, 60, 30, 30});
      std::shared_ptr<Tensor> input_data = std::make_shared<Tensor>(input_dims);

      scales = std::vector<float>({});
      sizes = std::vector<std::size_t>({1, 1, 75, 75});
      expected_dims = std::vector<Aidge::DimSize_t>({1, 1, 75, 75});
      auto resize_node = Resize(scales, sizes);
      auto op = std::static_pointer_cast<Resize_Op>(resize_node->getOperator());
      op->associateInput(0, input_data);

      REQUIRE_NOTHROW(op->forwardDims(true));
      REQUIRE(op->getOutput(0)->dims() == expected_dims);
    }
    SECTION("TEST 3") {
      input_dims = std::vector<Aidge::DimSize_t>({11, 11, 20, 20});
      std::shared_ptr<Tensor> input_data = std::make_shared<Tensor>(input_dims);

      scales = std::vector<float>({});
      sizes = std::vector<std::size_t>({19, 6, 8, 8});
      expected_dims = std::vector<Aidge::DimSize_t>({19, 6, 8, 8});
      auto resize_node = Resize(scales, sizes);
      auto op = std::static_pointer_cast<Resize_Op>(resize_node->getOperator());
      op->associateInput(0, input_data);

      REQUIRE_NOTHROW(op->forwardDims(true));
      REQUIRE(op->getOutput(0)->dims() == expected_dims);
    }
    SECTION("TEST 4") {
      input_dims = std::vector<Aidge::DimSize_t>({43, 211, 22, 22});
      std::shared_ptr<Tensor> input_data = std::make_shared<Tensor>(input_dims);

      scales = std::vector<float>({});
      sizes = std::vector<std::size_t>({1, 1, 10, 10});
      expected_dims = std::vector<Aidge::DimSize_t>({1, 1, 10, 10});
      auto resize_node = Resize(scales, sizes);
      auto op = std::static_pointer_cast<Resize_Op>(resize_node->getOperator());
      op->associateInput(0, input_data);

      REQUIRE_NOTHROW(op->forwardDims(true));
      REQUIRE(op->getOutput(0)->dims() == expected_dims);
    }
  }
}

} // namespace Aidge
