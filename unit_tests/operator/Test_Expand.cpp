/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_random.hpp>
#include <cstddef> // std::size_t
#include <memory>
#include <random>  // std::mt19937, std::uniform_int_distribution
#include <vector>

#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Expand.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace Aidge {
////////////////////////////////////////////////////////////////////////////////////////
//                                  DISCLAIMER
// Since this is a broadcastable operator, most of the test has been copied
// from Mul
////////////////////////////////////////////////////////////////////////////////////////

TEST_CASE("[core/operator] Expand_Op(forwardDims)", "[Expand][forwardDims]") {
    constexpr std::uint16_t NBTRIALS = 10;

    // Create a random number generator
    auto rd = Catch::Generators::Detail::getSeed;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<std::size_t> dimsDist(1, 10);
    std::uniform_int_distribution<std::size_t> nbDimsDist(1, 5);

    // Create Mul Operator
    std::shared_ptr<Node> expand = Expand("expand_node");
    auto op = std::static_pointer_cast<Expand_Op>(expand->getOperator());

    // input_0
    std::shared_ptr<Tensor> dataTensor = std::make_shared<Tensor>();
    op->associateInput(0, dataTensor);
    // input_1
    std::shared_ptr<Tensor> shapeTensor = std::make_shared<Tensor>();
    op->associateInput(1, shapeTensor);

    shapeTensor->setDataType(DataType::Int64);
    shapeTensor->setBackend("cpu");

    /**
     * @todo Special case: scalar not handled yet by
     * ``OperatorTensor::forwardDims()``
     */
    SECTION("Scalar / Scalar") {
        // input_0
        dataTensor->resize({});

        // input_1
        shapeTensor->resize({});

        REQUIRE_THROWS(op->forwardDims(true));
    }
    SECTION("Scalar / +1-D") {
        // a scalar is compatible with any other Tensor
        // input_0
        dataTensor->resize({});

        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
            // input_1
            const std::size_t nbDims = nbDimsDist(gen);
            shapeTensor->resize({nbDims});
            std::vector<std::size_t> expandShape(nbDims);
            for (std::size_t i = 0; i < nbDims; ++i) {
                expandShape[i] = dimsDist(gen);
            }
            shapeTensor->getImpl()->setRawPtr(expandShape.data(),
                                              expandShape.size());

            REQUIRE_NOTHROW(op->forwardDims(true));
            REQUIRE((op->getOutput(0)->dims()) == expandShape);
        }
    }
    SECTION("Failure when shape to expand tensor has not 1 dimension.") {

        // input_0
        const std::size_t nbDims = nbDimsDist(gen);
        std::vector<std::size_t> dims(nbDims);
        for (std::size_t i = 0; i < nbDims; ++i) {
            dims[i] = dimsDist(gen);
        }
        dataTensor->resize(dims);

        // input_1
        shapeTensor->resize({});

        REQUIRE_THROWS(op->forwardDims(true));
    }
    SECTION("+1-D / +1-D") {
        // same size
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
            const std::size_t nbDims = nbDimsDist(gen) + 1;
            std::vector<std::size_t> dims0(nbDims);
            for (std::size_t i = 0; i < nbDims; ++i) {
                dims0[i] = dimsDist(gen) + 1;
            }

            dataTensor->resize(dims0);
            shapeTensor->resize({dims0.size()});
            shapeTensor->getImpl()->setRawPtr(dims0.data(), dims0.size());
            REQUIRE_NOTHROW(op->forwardDims(true));
            REQUIRE((op->getOutput(0)->dims()) == dims0);
        }

        // broadcast
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
            const std::size_t nbDims = nbDimsDist(gen) + 1;
            std::vector<std::size_t> dims0(nbDims);
            for (std::size_t i = 0; i < nbDims; ++i) {
                dims0[i] = dimsDist(gen) + 2;
            }
            std::vector<std::size_t> dimsOut = dims0;
            std::vector<std::size_t> dims1 = dims0;
            for (std::size_t i = 0; i < nbDims; ++i) {
                if (dimsDist(gen) <= 5) {
                    dims1[i] = 1;
                }
            }
            dims1.erase(
                dims1.cbegin(),
                dims1.cbegin() + std::min(nbDimsDist(gen), nbDims - 1));

            dataTensor->resize(dims0);
            shapeTensor->resize({dims1.size()});
            shapeTensor->getImpl()->setRawPtr(dims1.data(), dims1.size());

            REQUIRE_NOTHROW(op->forwardDims(true));
            REQUIRE((op->getOutput(0)->dims()) == dimsOut);

            // input_0 - wrong
            // T1->resize({dims[0] + 1});
            std::vector<std::size_t> dims1Wrong = dims1;
            for (std::size_t i = 0; i < dims1.size(); ++i) {
                ++dims1Wrong[i];
            }
            shapeTensor->resize({dims1Wrong.size()});
            shapeTensor->getImpl()->setRawPtr(dims1Wrong.data(),
                                              dims1Wrong.size());
            REQUIRE(dims0 != dims1Wrong);
            REQUIRE_THROWS(op->forwardDims(true));
        }
    }
}
} // namespace Aidge
