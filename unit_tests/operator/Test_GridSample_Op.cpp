/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cstddef>  // std::size_t
#include <memory>
#include <random>   // std::mt19937, std::uniform_int_distribution
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_random.hpp>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/GridSample.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace Aidge {

TEST_CASE("[core/operator] GridSample_Op(forwardDims)", "[GridSample][forwardDims]") {
    constexpr std::uint16_t NBTRIALS = 10;

    // Create a random number generator
    auto rd = Catch::Generators::Detail::getSeed;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<std::size_t> dimsDist(1, 10);
    std::uniform_int_distribution<std::size_t> nbDimsDist(1, 5);

    // Create GridSample Operator
    std::shared_ptr<Node> myGridSample = GridSample(GridSample_Op::Mode::Cubic, GridSample_Op::PaddingMode::Border, false);
    auto op = std::static_pointer_cast<OperatorTensor>(myGridSample -> getOperator());

    // input_0
    std::shared_ptr<Tensor> data_in0 = std::make_shared<Tensor>();
    op -> associateInput(0,data_in0);
    // input_1
    std::shared_ptr<Tensor> grid_in1 = std::make_shared<Tensor>();
    op -> associateInput(1,grid_in1);

    SECTION("Valid shape provided") {
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {

            std::size_t N = dimsDist(gen);
            std::size_t C = dimsDist(gen);
            std::size_t H_data_in0 = dimsDist(gen);
            std::size_t W_data_in0 = dimsDist(gen);
            std::size_t H_grid_in1 = dimsDist(gen);
            std::size_t W_grid_in1 = dimsDist(gen);

            data_in0->resize({N, C, H_data_in0, W_data_in0});
            grid_in1->resize({N, H_grid_in1, W_grid_in1, 2});

            REQUIRE_NOTHROW(op->forwardDims());
            REQUIRE((op->getOutput(0)->dims()) == std::vector<std::size_t>({N, C, H_grid_in1, W_grid_in1}));
        }
    }
    SECTION("Invalid shape provided") {
        std::size_t N_in = dimsDist(gen);
        std::size_t C = dimsDist(gen);
        std::size_t H_data_in0 = dimsDist(gen);
        std::size_t W_data_in0 = dimsDist(gen);
        std::size_t H_grid_in1 = dimsDist(gen);
        std::size_t W_grid_in1 = dimsDist(gen);

        // different batch number
        std::size_t N_out = N_in+1;
        data_in0->resize({N_in, C, H_data_in0, W_data_in0});
        grid_in1->resize({N_out, H_grid_in1, W_grid_in1, 2});
        REQUIRE_THROWS(op->forwardDims());

        // different number of dimensions
        data_in0->resize({N_in, C, H_data_in0, W_data_in0});
        grid_in1->resize({N_in, H_grid_in1, W_grid_in1, 2, 2});
        REQUIRE_THROWS(op->forwardDims());

        // wrong number of pixel coordinates
        data_in0->resize({N_in, C, H_data_in0, W_data_in0});
        grid_in1->resize({N_in, H_grid_in1, W_grid_in1, 2 + dimsDist(gen)});
        REQUIRE_THROWS(op->forwardDims());
    }
}

} // namespace Aidge
