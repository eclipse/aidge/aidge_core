/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_random.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>
#include <cstddef>
#include <memory>
#include <numeric>
#include <random>
#include <stdexcept>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Stack.hpp"
#include "aidge/utils/TensorUtils.hpp"

using Catch::Matchers::Equals;

namespace Aidge {

TEST_CASE("[core/operator] Stack(forward)", "[Stack]") {
    constexpr auto nbTrials = 10;
    auto rd = Catch::Generators::Detail::getSeed;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<std::size_t> nbDist(1, 100);
    std::uniform_int_distribution<std::size_t> dimsDist(1, 10);
    std::uniform_int_distribution<std::size_t> nbDimsDist(2, 5);
    std::uniform_real_distribution<float> valueDist(0.1f, 1.1f);
    // std::uniform_int_distribution<std::size_t> tensorNbDimsDist(2U, 5U);

    const std::size_t nbDimsTensor = nbDimsDist(gen);
    std::vector<std::size_t> dimsIn(nbDimsTensor);
    std::shared_ptr<Tensor> t1 = std::make_shared<Tensor>();

    SECTION("Constructors") {
        // Valid arguments
        for (auto i = 0; i < nbTrials; ++i) {
            auto maxElements = nbDist(gen);
            REQUIRE_NOTHROW(StackOp(maxElements));

            auto op1 = StackOp(maxElements);
            REQUIRE(op1.maxElements() == maxElements);
            REQUIRE(op1.forwardStep() == 0);

            // Copy Constructor
            auto op2 = op1;
            REQUIRE(op2.maxElements() == maxElements);
            REQUIRE(op2.forwardStep() == 0);
        }
    }

    SECTION("forwardDims") {
        for (auto i = 0; i < nbTrials; ++i) {
            auto maxElements = nbDist(gen);
            auto op = StackOp(maxElements);

            const std::size_t nbDims = nbDimsDist(gen);
            std::vector<std::size_t> dims(nbDims);
            for (std::size_t i = 0; i < nbDims; ++i) {
                dims[i] = dimsDist(gen);
            }
            t1->resize(dims);

            REQUIRE_THROWS_WITH(
                op.forwardDims(),
                Equals("Stack: input #0 should be associated with a Tensor"));
            op.associateInput(0, t1);
            REQUIRE_NOTHROW(op.forwardDims());
            REQUIRE(op.getOutput(0)->dims()[0] == maxElements);
        }
    }

    SECTION("forward") {

        std::generate(dimsIn.begin(), dimsIn.end(), [&gen, &dimsDist]() {
            return dimsDist(gen);
        });
        const std::size_t nbElems =
            std::accumulate(dimsIn.cbegin(),
                            dimsIn.cend(),
                            1u,
                            std::multiplies<std::size_t>());

        std::uniform_int_distribution<std::size_t> numTensorsDist(2, 10);
        const std::size_t numTensors = numTensorsDist(gen);

        std::vector<std::shared_ptr<Tensor>> tensors(numTensors);
        std::vector<float *> arrays(numTensors);

        for (std::size_t i = 0; i < numTensors; ++i) {
            arrays[i] = new float[nbElems];
            for (std::size_t j = 0; j < nbElems; ++j) {
                arrays[i][j] = valueDist(gen);
            }
            tensors[i] = std::make_shared<Tensor>();
            tensors[i]->resize(dimsIn);
            tensors[i]->setBackend("cpu");
            tensors[i]->setDataType(DataType::Float32);
            tensors[i]->getImpl()->setRawPtr(arrays[i], nbElems);
        }

        auto myStack = Stack(numTensors);
        myStack->getOperator()->setBackend("cpu");
        myStack->getOperator()->setDataType(DataType::Float32);
        auto op =
            std::static_pointer_cast<OperatorTensor>(myStack->getOperator());

        op->associateInput(0, tensors[0]);
        op->forwardDims();
        op->getOutput(0)->zeros();

        // Perform forward passes for each tensor
        for (std::size_t i = 0; i < numTensors; ++i) {
            if (i > 0) {
                op->associateInput(0, tensors[i]);
            }
            op->forward();
        }

        auto output = op->getOutput(0);

        std::vector<DimSize_t> expectedDims = dimsIn;
        expectedDims.insert(expectedDims.begin(), numTensors);

        REQUIRE(output->dims() == expectedDims);

        // Compare output slices with input tensors
        for (std::size_t i = 0; i < numTensors; ++i) {
            Tensor outputTensor = output->extract(
                {static_cast<std::uint64_t>(static_cast<int>(i))});
            Tensor inputTensor(DataType::Float32);
            inputTensor.resize(dimsIn);
            inputTensor.setBackend("cpu");
            inputTensor.getImpl()->setRawPtr(arrays[i], nbElems);

            REQUIRE(approxEq<float>(outputTensor, inputTensor));
        }

        // Attempt to exceed maxElements
        std::shared_ptr<Tensor> extraTensor = std::make_shared<Tensor>();
        extraTensor->resize(dimsIn);
        extraTensor->setBackend("cpu");
        extraTensor->setDataType(DataType::Float32);
        float *extraArray = new float[nbElems];
        for (std::size_t j = 0; j < nbElems; ++j) {
            extraArray[j] = valueDist(gen);
        }
        extraTensor->getImpl()->setRawPtr(extraArray, nbElems);

        REQUIRE_THROWS_AS((op->associateInput(0, extraTensor), op->forward()),
                          std::runtime_error);

        // Clean up
        delete[] extraArray;
        for (std::size_t i = 0; i < numTensors; ++i) {
            delete[] arrays[i];
        }
    }
}
} // namespace Aidge
