/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <algorithm>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_random.hpp>
#include <cstddef> // std::size_t
#include <cstdint>
#include <functional>
#include <memory>
#include <numeric>
#include <random> // std::mt19937, std::uniform_int_distribution
#include <system_error>
#include <vector>

#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/filler/Filler.hpp"
#include "aidge/operator/ConstantOfShape.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
TEST_CASE("[core/operator] ConstantOfShape_Op(forwardDims)",
          "[ConstantOfShape][forwardDims]") {
  constexpr std::uint16_t NBTRIALS = 10;

  // Create a random number generator
  auto random_seed = Catch::Generators::Detail::getSeed;
  std::mt19937 gen(random_seed());
  std::uniform_int_distribution<std::size_t> input_tensor_dims_dist(1, 10);
  std::uniform_int_distribution<std::size_t> input_tensor_value_dist(1, 9);
  std::uniform_real_distribution<float> op_value_attr_value_dist(1, 10000);

  std::uniform_int_distribution<std::size_t> op_value_attr_type_dist(
      0, static_cast<int>(Aidge::DataType::UInt64));
  // TENSORS
  std::shared_ptr<Tensor> input_T = std::make_shared<Tensor>();
  input_T->setDataType(Aidge::DataType::Int64);
  input_T->setBackend("cpu");

  SECTION("operator test") {
    // Create Operator
    for (int i = 0; i < NBTRIALS; ++i) {
      std::shared_ptr<Node> node =
          ConstantOfShape(Tensor(op_value_attr_value_dist(gen)));
      auto op =
          std::static_pointer_cast<ConstantOfShape_Op>(node->getOperator());
      op->associateInput(0, input_T);

      std::vector<DimSize_t> input_dims;
      input_dims.push_back(input_tensor_dims_dist(gen));

      Log::setConsoleLevel(Log::Debug);
      int input_nb_elems = input_dims.at(0);
      int output_nb_elems = 1;
      int64_t *array_in = new int64_t[input_nb_elems];
      for (std::size_t i = 0; i < input_nb_elems; ++i) {
        std::int64_t val = input_tensor_value_dist(gen);
        array_in[i] = val;
        output_nb_elems *= val;
      }

      input_T->resize(input_dims);
      op->setInput(0, input_T);
      input_T->getImpl()->setRawPtr(array_in, input_nb_elems);

      REQUIRE(op->forwardDims(true));
      REQUIRE(input_T->size() == op->getOutput(0)->nbDims());
      for (DimSize_t i = 0; i < op->getOutput(0)->nbDims(); ++i) {
        CHECK(array_in[i] == op->getOutput(0)->dims().at(i));
      }
      delete[] array_in;
    }
  }
}
} // namespace Aidge

