/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Flatten.hpp"

#include <memory>

using namespace Aidge;

TEST_CASE("[cpu/operator] Flatten(forward)") {
    std::shared_ptr<Tensor> input = std::make_shared<Tensor>(Array4D<int32_t,1,2,3,5> {
        {
            {
                {
                    { 1,  2,  3,  4,  5},
                    { 6,  7,  8,  9, 10},
                    {11, 12, 13, 14, 15}
                },
                {
                    {16, 17, 18, 19, 20},
                    {21, 22, 23, 24, 25},
                    {26, 27, 28, 29, 30}
                }
            }
        }
    });

    SECTION("Default (axis = 1)") {
        std::shared_ptr<Node> myFlatten = Flatten();
        auto op = std::static_pointer_cast<OperatorTensor>(myFlatten -> getOperator());
        op->associateInput(0, input);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        myFlatten->forward();

        auto expectedOutput = input->clone();
        expectedOutput.resize({1, input->size()});

        REQUIRE(op->getOutput(0)->dims() == expectedOutput.dims());
        REQUIRE(*(op->getOutput(0)) == expectedOutput);
    }

    SECTION("Axis = 0") {
        std::shared_ptr<Node> myFlatten = Flatten(0);
        auto op = std::static_pointer_cast<OperatorTensor>(myFlatten -> getOperator());
        op->associateInput(0, input);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        myFlatten->forward();

        auto expectedOutput = input->clone();
        expectedOutput.resize({1, input->size()});

        REQUIRE(op->getOutput(0)->dims() == expectedOutput.dims());
        REQUIRE(*(op->getOutput(0)) == expectedOutput);
    }

    SECTION("Axis = 2") {
        std::shared_ptr<Node> myFlatten = Flatten(2);
        auto op = std::static_pointer_cast<OperatorTensor>(myFlatten -> getOperator());
        op->associateInput(0, input);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        myFlatten->forward();

        auto expectedOutput = input->clone();
        expectedOutput.resize({2, input->size() / 2});

        REQUIRE(op->getOutput(0)->dims() == expectedOutput.dims());
        REQUIRE(*(op->getOutput(0)) == expectedOutput);
    }

    SECTION("Axis = 4") {
        std::shared_ptr<Node> myFlatten = Flatten(4);
        auto op = std::static_pointer_cast<OperatorTensor>(myFlatten -> getOperator());
        op->associateInput(0, input);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        myFlatten->forward();

        auto expectedOutput = input->clone();
        expectedOutput.resize({input->size(), 1});

        REQUIRE(op->getOutput(0)->dims() == expectedOutput.dims());
        REQUIRE(*(op->getOutput(0)) == expectedOutput);
    }
}