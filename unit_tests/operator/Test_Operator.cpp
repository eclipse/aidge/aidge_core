/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cstddef>
#include <memory>
#include <string>
#include <vector>

#include <catch2/catch_test_macros.hpp>

#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/Add.hpp"
#include "aidge/operator/ReLU.hpp"
#include "aidge/operator/Producer.hpp"

namespace Aidge {
// TEST_CASE("[core/operator] Operator(computeReceptiveField)", "[Operator][computeReceptiveFiled]") {
//     auto dataProvider1 = Producer({16, 3, 224, 224}, "dataProvider1");
//     auto dataProvider2 = Producer({16, 3, 224, 224}, "dataProvider2");
//     auto gen1 = Add();
//     auto gen2 = ReLU();

//     auto g = std::make_shared<GraphView>("TestGraph");

//     dataProvider1->addChild(gen1, 0);
//     dataProvider2->addChild(gen1, 0);
//     g->add(gen1);
//     g->addChild(gen2, gen1, 0);

//     g->forwardDims();

//     SECTION("Check individual receptive fields") {
//         auto res1 = gen1->getOperator()->computeReceptiveField(0, {16,3,10,10});
//         auto res2 = gen2->getOperator()->computeReceptiveField(gen2->getOperator()->output(0).getIdx({3,2,100,28}), {1,1,30,40});

//         REQUIRE(((res1[0].first == 0) && (res1[0].second == std::vector<DimSize_t>({16, 3, 10, 10}))));
//         REQUIRE(((res1[1].first == 0) && (res1[1].second == std::vector<DimSize_t>({16, 3, 10, 10}))));
//         REQUIRE(((res2[0].first == gen2->getOperator()->input(0).getIdx({3,2,100,28})) && (res2[0].second == std::vector<DimSize_t>({1, 1, 30, 40}))));
//     }
// }
}  // namespace Aidge