/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Split.hpp"

using namespace Aidge;

TEST_CASE("[cpu/operator] Split(forward)", "[Split][CPU]") {
    std::shared_ptr<Tensor> input = std::make_shared<Tensor>(Array4D<int,1,3,7,2> {
        {
            {
                {{ 1,  2},{ 3,  4},{ 5,  6},{ 7,  8},{ 9, 10},{11, 12},{13, 14}},
                {{15, 16},{17, 18},{19, 20},{21, 22},{23, 24},{25, 26},{27, 28}},
                {{30, 31},{32, 33},{34, 35},{36, 37},{38, 39},{40, 41},{42, 43}}
            }
        }
    });
    SECTION("Default split") {
        std::shared_ptr<Tensor> expectedOutput0 = std::make_shared<Tensor>(Array4D<int,1,3,2,2> {
            {
                {
                    {{ 1,  2},{ 3,  4}},
                    {{15, 16},{17, 18}},
                    {{30, 31},{32, 33}}
                }
            }
        });
        std::shared_ptr<Tensor> expectedOutput1 = std::make_shared<Tensor>(Array4D<int,1,3,2,2> {
            {
                {
                    {{ 5,  6},{ 7,  8}},
                    {{19, 20},{21, 22}},
                    {{34, 35},{36, 37}}
                }
            }
        });
        std::shared_ptr<Tensor> expectedOutput2 = std::make_shared<Tensor>(Array4D<int,1,3,3,2> {
            {
                {
                    {{ 9, 10},{11, 12},{13, 14}},
                    {{23, 24},{25, 26},{27, 28}},
                    {{38, 39},{40, 41},{42, 43}}
                }
            }
        });
        auto mySplit = Split(DimSize_t(3), int8_t(2)); // Split on axis 2 into 3 outputs
        mySplit->getOperator()->associateInput(0, input);
        mySplit->getOperator()->setBackend("cpu");
        mySplit->getOperator()->setDataType(DataType::Int32);
        mySplit->forward();

        REQUIRE(*std::static_pointer_cast<OperatorTensor>(mySplit->getOperator())->getOutput(0) == *expectedOutput0);
        REQUIRE(*std::static_pointer_cast<OperatorTensor>(mySplit->getOperator())->getOutput(1) == *expectedOutput1);
        REQUIRE(*std::static_pointer_cast<OperatorTensor>(mySplit->getOperator())->getOutput(2) == *expectedOutput2);
    }
    SECTION("Split with different chunk size") {
        std::shared_ptr<Tensor> expectedOutput0 = std::make_shared<Tensor>(Array4D<int,1,3,4,2> {
            {
                {
                    {{ 1,  2},{ 3,  4},{ 5,  6},{ 7,  8}},
                    {{15, 16},{17, 18},{19, 20},{21, 22}},
                    {{30, 31},{32, 33},{34, 35},{36, 37}}
                }
            }
        });
        std::shared_ptr<Tensor> expectedOutput1 = std::make_shared<Tensor>(Array4D<int,1,3,1,2> {
            {
                {
                    {{ 9, 10}},
                    {{23, 24}},
                    {{38, 39}}
                }
            }
        });
        std::shared_ptr<Tensor> expectedOutput2 = std::make_shared<Tensor>(Array4D<int,1,3,2,2> {
            {
                {
                    {{11, 12},{13, 14}},
                    {{25, 26},{27, 28}},
                    {{40, 41},{42, 43}}
                }
            }
        });
        auto mySplit = Split(DimSize_t(3), int8_t(2), {DimSize_t(4), DimSize_t(1), DimSize_t(2)});
        mySplit->getOperator()->associateInput(0, input);
        mySplit->getOperator()->setBackend("cpu");
        mySplit->getOperator()->setDataType(DataType::Int32);
        mySplit->forward();

        REQUIRE(*std::static_pointer_cast<OperatorTensor>(mySplit->getOperator())->getOutput(0) == *expectedOutput0);
        REQUIRE(*std::static_pointer_cast<OperatorTensor>(mySplit->getOperator())->getOutput(1) == *expectedOutput1);
        REQUIRE(*std::static_pointer_cast<OperatorTensor>(mySplit->getOperator())->getOutput(2) == *expectedOutput2);
    }
    SECTION("Split with bad split attribute") {
        auto mySplit = Split(DimSize_t(3), int8_t(2), {DimSize_t(4), DimSize_t(1), DimSize_t(3)});
        mySplit->getOperator()->associateInput(0, input);
        mySplit->getOperator()->setBackend("cpu");
        mySplit->getOperator()->setDataType(DataType::Int32);
        REQUIRE_THROWS(mySplit->forward());
    }
    SECTION("Split with bad outNumber") {
        auto mySplit = Split(DimSize_t(8), int8_t(2));
        mySplit->getOperator()->associateInput(0, input);
        mySplit->getOperator()->setBackend("cpu");
        mySplit->getOperator()->setDataType(DataType::Int32);
        REQUIRE_THROWS(mySplit->forward());
    }
}