/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_random.hpp>
#include <cstddef> // std::size_t
#include <memory>
#include <random>  // std::mt19937, std::uniform_int_distribution
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Heaviside.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace Aidge {

TEST_CASE("[core/operator] Heaviside_Op(forwardDims)",
          "[Heaviside][forwardDims]") {

    constexpr std::uint16_t NBTRIALS = 10;

    // Create a random number generator
    auto rd = Catch::Generators::Detail::getSeed;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<std::size_t> dimsDist(1, 10);
    std::uniform_int_distribution<std::size_t> nbDimsDist(1, 5);

    // Create Heaviside Operator
    std::shared_ptr<Node> myHeaviside = Heaviside(0.5);
    auto op =
        std::static_pointer_cast<OperatorTensor>(myHeaviside->getOperator());

    // input_0
    std::shared_ptr<Tensor> T0 = std::make_shared<Tensor>();
    op->associateInput(0, T0);

    SECTION("Scalar") {
        // input 0
        T0->resize({});

        REQUIRE_NOTHROW(op->forwardDims());
        REQUIRE((op->getOutput(0)->dims() == std::vector<std::size_t>()));
    }

    SECTION("+1-D Tensor") {

        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {

            const std::size_t nb_dims = nbDimsDist(gen);
            std::vector<std::size_t> dims(nb_dims);

            for (std::size_t i = 0; i < nb_dims; ++i) {
                dims[i] = dimsDist(gen);
            }
            T0->resize(dims);

            REQUIRE_NOTHROW(op->forwardDims());
            REQUIRE((op->getOutput(0)->dims()) == dims);
        }
    }
}
} // namespace Aidge
