/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Add.hpp"
#include "aidge/operator/Concat.hpp"

using namespace Aidge;

TEST_CASE("[cpu/operator] Concat(forward)", "[Concat][CPU]") {
    SECTION("Concat scalar inputs") {
        std::shared_ptr<Tensor> input1 = std::make_shared<Tensor>(2);
        std::shared_ptr<Tensor> input2 = std::make_shared<Tensor>(4);
        auto myConcat = Concat(2, 0);
        myConcat->getOperator()->associateInput(0, input1);
        myConcat->getOperator()->associateInput(1, input2);
        REQUIRE_THROWS(myConcat->forward());
    }
    SECTION("Concat 1D inputs") {
        std::shared_ptr<Tensor> input1 = std::make_shared<Tensor>(Array1D<int,2>{{ 2, 3 }});
        std::shared_ptr<Tensor> input2 = std::make_shared<Tensor>(Array1D<int,3>{{ 4, 5, 6 }});
        std::shared_ptr<Tensor> input3 = std::make_shared<Tensor>(Array1D<int,4>{{ 7, 8, 9, 10 }});
        std::shared_ptr<Tensor> input4 = std::make_shared<Tensor>(Array1D<int,5>{{ 11, 12, 13, 14, 15 }});
        std::shared_ptr<Tensor> input5 = std::make_shared<Tensor>(Array1D<int,6>{{ 16, 17, 18, 19, 20, 21 }});

        Tensor expectedOutput = Array1D<int,20>{
            { 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12,13,14,15,16,17,18,19,20,21 }};

        std::shared_ptr<Concat_Op> op = std::make_shared<Concat_Op>(5,0);
        op->associateInput(0, input1);
        op->associateInput(1, input2);
        op->associateInput(2, input3);
        op->associateInput(3, input4);
        op->associateInput(4, input5);
        op->setBackend("cpu");
        op->setDataType(DataType::Int32);
        fmt::print("{}\n", *(op->getInput(0)));
        fmt::print("{}\n", *(op->getInput(1)));
        fmt::print("{}\n", *(op->getInput(2)));
        fmt::print("{}\n", *(op->getInput(3)));
        fmt::print("{}\n", *(op->getInput(4)));
        op->forward();

        fmt::print("res: {}\n", *(op->getOutput(0)));

        REQUIRE(*(op->getOutput(0)) == expectedOutput);
    }
    SECTION("Concat 4D inputs on 1st axis") {
        std::shared_ptr<Tensor> input1 = std::make_shared<Tensor>(Array4D<int,1,3,3,2> {
            {                                       //
                {                                   //
                    {{20, 47},{21, 48},{22, 49}},   //
                    {{23, 50},{24, 51},{25, 52}},   //
                    {{26, 53},{27, 54},{28, 55}}    //
                },                                  //
            }                                       //
        });                                         //
        std::shared_ptr<Tensor> input2 = std::make_shared<Tensor>(Array4D<int,2,3,3,2> {
            {
                {                                   //
                    {{29, 56},{30, 57},{31, 58}},   //
                    {{32, 59},{33, 60},{34, 61}},   //
                    {{35, 62},{36, 63},{37, 64}}    //
                },                                  //
                {                                   //
                    {{38, 65},{39, 66},{40, 67}},   //
                    {{41, 68},{42, 69},{43, 70}},   //
                    {{44, 71},{45, 72},{46, 73}}    //
                }                                   //
            }                                       //
        });                                         //

        Tensor expectedOutput = Array4D<int,3,3,3,2> {
            {                                       //
                {                                   //
                    {{20, 47},{21, 48},{22, 49}},   //
                    {{23, 50},{24, 51},{25, 52}},   //
                    {{26, 53},{27, 54},{28, 55}}    //
                },                                  //
                {                                   //
                    {{29, 56},{30, 57},{31, 58}},   //
                    {{32, 59},{33, 60},{34, 61}},   //
                    {{35, 62},{36, 63},{37, 64}}    //
                },                                  //
                {                                   //
                    {{38, 65},{39, 66},{40, 67}},   //
                    {{41, 68},{42, 69},{43, 70}},   //
                    {{44, 71},{45, 72},{46, 73}}    //
                }                                   //
            }                                       //
        };                                         //

        auto myConcat = Concat(2, 0);
        std::shared_ptr<Concat_Op> op = std::static_pointer_cast<Concat_Op>(myConcat->getOperator());
        op->associateInput(0, input1);
        op->associateInput(1, input2);
        op->setBackend("cpu");
        op->setDataType(DataType::Int32);
        myConcat->forward();

        fmt::print("res: {}\n", *(op->getOutput(0)));

        REQUIRE(*(op->getOutput(0)) == expectedOutput);
    }

    SECTION("Concat 4D inputs on 3rd axis") {
        std::shared_ptr<Tensor> input1 = std::make_shared<Tensor>(Array4D<int,1,3,3,2> {
            {                                       //
                {                                   //
                    {{20, 47},{21, 48},{22, 49}},   //
                    {{23, 50},{24, 51},{25, 52}},   //
                    {{26, 53},{27, 54},{28, 55}}    //
                },                                  //
            }                                       //
        });                                         //
        std::shared_ptr<Tensor> input2 = std::make_shared<Tensor>(Array4D<int,1,3,6,2> {
            {
                {                                   //
                    {{29, 56},{30, 57},{31, 58},{38, 65},{39, 66},{40, 67}},   //
                    {{32, 59},{33, 60},{34, 61},{41, 68},{42, 69},{43, 70}},   //
                    {{35, 62},{36, 63},{37, 64},{44, 71},{45, 72},{46, 73}}    //
                },
            }
        });

        Tensor expectedOutput = Array4D<int,1,3,9,2> {
            {                                                                                             //
                {                                                                                         //
                    {{20, 47},{21, 48},{22, 49},{29, 56},{30, 57},{31, 58},{38, 65},{39, 66},{40, 67}},   //
                    {{23, 50},{24, 51},{25, 52},{32, 59},{33, 60},{34, 61},{41, 68},{42, 69},{43, 70}},   //
                    {{26, 53},{27, 54},{28, 55},{35, 62},{36, 63},{37, 64},{44, 71},{45, 72},{46, 73}}    //
                },                                                                                        //
            }                                                                                             //
        };                                                                                               //

        auto myConcat = Concat(2, 2);
        std::shared_ptr<Concat_Op> op = std::static_pointer_cast<Concat_Op>(myConcat->getOperator());
        op->associateInput(0, input1);
        op->associateInput(1, input2);
        op->setBackend("cpu");
        op->setDataType(DataType::Int32);
        myConcat->forward();

        std::static_pointer_cast<Tensor>(myConcat->getOperator()->getRawOutput(0))->print();

        REQUIRE(*(op->getOutput(0)) == expectedOutput);
    }
}
