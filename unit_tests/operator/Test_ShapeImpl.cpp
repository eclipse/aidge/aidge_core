/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Shape.hpp"

#include <cstdint>
#include <memory>


using namespace Aidge;

TEST_CASE("[cpu/operator] Shape(forward)", "[Shape][CPU]") {
    SECTION("Default attributes") {
        std::shared_ptr<Tensor> input = std::make_shared<Tensor>(Array4D<int,1,2,3,5> {
            {
                {
                    {
                        { 1,  2,  3,  4,  5},
                        { 6,  7,  8,  9, 10},
                        {11, 12, 13, 14, 15}
                    },
                    {
                        {16, 17, 18, 19, 20},
                        {21, 22, 23, 24, 25},
                        {26, 27, 28, 29, 30}
                    }
                }
            }
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array1D<int,4> {
            {1, 2, 3, 5}
        });

        std::shared_ptr<Shape_Op> op = std::make_shared<Shape_Op>();
        op->associateInput(0, input);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        op->forward();

        REQUIRE(*(op->getOutput(0)) == *expectedOutput);

    }
    SECTION("Using attributes") {
        std::shared_ptr<Tensor> input = std::make_shared<Tensor>(Array4D<int,1,2,3,5> {
            {
                {
                    {
                        { 1,  2,  3,  4,  5},
                        { 6,  7,  8,  9, 10},
                        {11, 12, 13, 14, 15}
                    },
                    {
                        {16, 17, 18, 19, 20},
                        {21, 22, 23, 24, 25},
                        {26, 27, 28, 29, 30}
                    }
                }
            }
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array1D<int,2> {
            {2, 3}
        });

        std::shared_ptr<Node> myShape = Shape(1, 2);
        auto op = std::static_pointer_cast<OperatorTensor>(myShape -> getOperator());
        op->associateInput(0,input);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        myShape->forward();

        REQUIRE(*(op->getOutput(0)) == *expectedOutput);

    }
}
