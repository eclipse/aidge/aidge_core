/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <algorithm>  // std::ajacent_find, std::all_of, std::sort, std::transform
#include <array>
#include <chrono>      // std::micro, std::chrono::time_point,
                       // std::chrono::system_clock
#include <cstddef>     // std::size_t
#include <cstdint>     // std::int8_t, std::uint16_t
#include <functional>  // std::multiplies
#include <memory>
#include <numeric>     // std::accumulate
#include <random>      // std::random_device, std::mt19937
                       // std::uniform_int_distribution, std::uniform_real_distribution
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_random.hpp>
#include <fmt/core.h>

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Unsqueeze.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/TensorUtils.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

bool ensure_axes_validity(std::vector<int8_t> dims_to_unsqueeze,
                          DimIdx_t nb_dims_input_tensor) {

  bool in_bounds =
      std::all_of(dims_to_unsqueeze.begin(), dims_to_unsqueeze.end(),
                  [&nb_dims_input_tensor,
                   &dims_to_unsqueeze](const int8_t &dim_to_unsqueeze) {
                    return (dim_to_unsqueeze <
                            nb_dims_input_tensor + dims_to_unsqueeze.size());
                  });

  std::sort(dims_to_unsqueeze.begin(), dims_to_unsqueeze.end());
  bool index_appear_twice =
      dims_to_unsqueeze.end() !=
      std::adjacent_find(dims_to_unsqueeze.begin(), dims_to_unsqueeze.end());

  return in_bounds && !index_appear_twice;
}

std::vector<DimSize_t>
generate_unsqueeze_output_dims(std::vector<size_t> dims_in,
                               std::vector<int8_t> dims_to_unsqueeze) {

  std::sort(dims_to_unsqueeze.begin(), dims_to_unsqueeze.end());
  std::vector<DimSize_t> dims_out(dims_in);
  dims_out.reserve(dims_in.size() + dims_to_unsqueeze.size());
  for (const DimIdx_t &dim : dims_to_unsqueeze) {
    dims_out.insert(dims_out.begin() + dim, 1);
  }
  return dims_out;
}

std::vector<int8_t> rectify_indexes(const std::vector<int8_t> & dims_to_unsqueeze,
                                    const int8_t offset) {
  std::vector<int8_t> output;
  output.reserve(dims_to_unsqueeze.size());
  for (int8_t dim : dims_to_unsqueeze) {
    output.push_back(dim >= 0 ? dim : dim + offset);
  }
  return output;
}

TEST_CASE("[core/operator] Unsqueeze(forwardDims)",
          "[Unsqueeze][forwardDims]") {
  constexpr std::uint16_t NB_TRIALS = 10;
  // Create a random number generator
  auto random_seed = Catch::Generators::Detail::getSeed;
  std::mt19937 gen(random_seed());

  std::uniform_real_distribution<float> valueDist(0.1f, 1.1f);
  std::uniform_int_distribution<std::size_t> tensor_dims_size_dist(
      std::size_t(1), std::size_t(10));
  std::uniform_int_distribution<std::size_t> tensor_nb_dims_dist(
      std::size_t(1), std::size_t(7));
  std::uniform_int_distribution<std::size_t> nb_dims_to_unsqueeze_dist(
      std::size_t(1), std::size_t(8));

  std::shared_ptr<Tensor> input_T = std::make_shared<Tensor>();
  std::shared_ptr<Tensor> axes_T = std::make_shared<Tensor>();

  SECTION("ERROR : Inputs not ready") {
    SECTION("unconnected input") {
      std::shared_ptr<Node> myUnsqueeze =
          Unsqueeze(std::vector<std::int8_t>({0}));
      auto op =
          std::static_pointer_cast<OperatorTensor>(myUnsqueeze->getOperator());
      REQUIRE_THROWS(op->forwardDims());
    }

    std::shared_ptr<Tensor> input_T = std::make_shared<Tensor>();

    SECTION("empty tensor") {
      // Create the Unsqueeze Operator
      std::shared_ptr<Node> myUnsqueeze =
          Unsqueeze(std::vector<std::int8_t>({0}));
      auto op =
          std::static_pointer_cast<OperatorTensor>(myUnsqueeze->getOperator());
      op->associateInput(0, input_T);

      CHECK(op->forwardDims() == false);
    }
  }
  SECTION("Compare with reference output") {
    int8_t nb_dims = 3;
    SECTION("axes is given via attribute") {
      SECTION("unsqueez(0)") {
        std::shared_ptr<Node> myUnsqueeze =
            Unsqueeze(std::vector<std::int8_t>({0}));
        auto op = std::static_pointer_cast<OperatorTensor>(
            myUnsqueeze->getOperator());
        op->associateInput(0, input_T);

        std::vector<DimSize_t> dims_in{2, 3, 4};
        input_T->resize(dims_in);

        CHECK(op->forwardDims() == true);
        CHECK(op->getOutput(0)->dims() == std::vector<DimSize_t>({1, 2, 3, 4}));
        CHECK((op->getOutput(0)->dims().size()) == nb_dims + 1);
      }
      SECTION("Unsqueeze(1)") {
        std::shared_ptr<Node> myUnsqueeze =
            Unsqueeze(std::vector<std::int8_t>({1}));
        auto op = std::static_pointer_cast<OperatorTensor>(
            myUnsqueeze->getOperator());
        op->associateInput(0, input_T);

        std::array<DimSize_t, 3> dims_in{2, 3, 4};
        input_T->resize(dims_in);

        CHECK(op->forwardDims() == true);
        CHECK(op->getOutput(0)->dims() == std::vector<DimSize_t>({2, 1, 3, 4}));
        CHECK((op->getOutput(0)->dims().size()) == nb_dims + 1);
      }
      SECTION("Unsqueeze(2)") {
        std::shared_ptr<Node> myUnsqueeze =
            Unsqueeze(std::vector<std::int8_t>({2}));
        auto op = std::static_pointer_cast<OperatorTensor>(
            myUnsqueeze->getOperator());
        op->associateInput(0, input_T);

        std::vector<DimSize_t> dims_in{2, 3, 4};
        input_T->resize(dims_in);

        CHECK(op->forwardDims() == true);
        CHECK(op->getOutput(0)->dims() == std::vector<DimSize_t>({2, 3, 1, 4}));
        CHECK((op->getOutput(0)->dims().size()) == nb_dims + 1);
      }
      SECTION("Unsqueeze({0,4})") {
        std::shared_ptr<Node> myUnsqueeze =
            Unsqueeze(std::vector<std::int8_t>({0, 4}));
        auto op = std::static_pointer_cast<OperatorTensor>(
            myUnsqueeze->getOperator());
        op->associateInput(0, input_T);

        std::vector<DimSize_t> dims_in{3, 4, 5};
        input_T->resize(dims_in);

        CHECK(op->forwardDims() == true);
        CHECK(op->getOutput(0)->dims() ==
              std::vector<DimSize_t>({1, 3, 4, 5, 1}));
      }
    }
    SECTION("axes is given via tensor") {
        // arguments here should be overridden by axes_T values
        std::shared_ptr<Node> myUnsqueeze =
            Unsqueeze(std::vector<std::int8_t>({0, 4}));
        auto op = std::static_pointer_cast<OperatorTensor>(
            myUnsqueeze->getOperator());
        op->associateInput(0, input_T);

        auto axes_T = std::make_shared<Aidge::Tensor>(
            Aidge::Array1D<int8_t, 3>({1, 3, 4}));
        axes_T->setDataType(Aidge::DataType::Int8);
        axes_T->setBackend("cpu");

        std::vector<DimSize_t> dims_in{3, 4, 5};
        input_T->resize(dims_in);
        op->associateInput(0, input_T);
        op->associateInput(1, axes_T);

        CHECK(op->forwardDims(true) == true);
        CHECK(op->getOutput(0)->dims() ==
              std::vector<DimSize_t>({3, 1, 4, 1, 1, 5}));
    }
  }
  SECTION("Random testing") {
    SECTION("Unsqueeze({N,...})") {
      int number_of_operation{0};
      for (uint16_t trial = 0; trial < NB_TRIALS; ++trial) {
        const size_t nb_dims_to_unsqueeze = nb_dims_to_unsqueeze_dist(gen);
        const size_t nb_dims_tensor = tensor_nb_dims_dist(gen);
        const size_t idx_dims_to_unsqueeze_max =
            nb_dims_to_unsqueeze + nb_dims_tensor;
        const size_t variance_error = 2;
        std::uniform_int_distribution<short> idx_dims_to_unsqueeze_dist(
            -idx_dims_to_unsqueeze_max - variance_error,
            idx_dims_to_unsqueeze_max - 1 + variance_error);
        // Create the Operator
        std::vector<int8_t> dims_to_unsqueeze(nb_dims_to_unsqueeze);
        std::generate(dims_to_unsqueeze.begin(), dims_to_unsqueeze.end(),
                      [&gen, &idx_dims_to_unsqueeze_dist]() {
                        return idx_dims_to_unsqueeze_dist(gen);
                      });
        std::shared_ptr<Node> unsqueeze_node = Unsqueeze(dims_to_unsqueeze);
        auto op = std::static_pointer_cast<OperatorTensor>(
            unsqueeze_node->getOperator());
        op->associateInput(0, input_T);

        // input tensor
        std::vector<std::size_t> dims_in(nb_dims_tensor);
        std::generate(dims_in.begin(), dims_in.end(),
                      [&gen, &tensor_dims_size_dist]() {
                        return tensor_dims_size_dist(gen);
                      });
        input_T->resize(dims_in);
        op->setInput(0, input_T);

        dims_to_unsqueeze = rectify_indexes(
            dims_to_unsqueeze, input_T->nbDims() + dims_to_unsqueeze.size());
        bool dims_to_unsqueeze_valid =
            ensure_axes_validity(dims_to_unsqueeze, input_T->nbDims());
        Log::warn("raw dims_to_unsqueeze : {}", dims_to_unsqueeze);
        Log::warn("dims_to_unsqueeze : {}", dims_to_unsqueeze);
        Log::warn("tensor dims : {}", input_T->dims());

        if (!dims_to_unsqueeze_valid) {
          ensure_axes_validity(dims_to_unsqueeze, input_T->nbDims());
          REQUIRE_THROWS(op->forwardDims(true));
        } else {
          // output tensor
          std::vector<DimSize_t> dims_out =
              generate_unsqueeze_output_dims(dims_in, dims_to_unsqueeze);
          Log::warn("dims_out : {}", dims_out);
          CHECK(op->forwardDims(true) == true);
          CHECK(op->getOutput(0)->dims() == dims_out);
          generate_unsqueeze_output_dims(dims_in, dims_to_unsqueeze);
        }
      }
    }
  }
}

TEST_CASE("[core/operator] Unsqueeze(forward)", "[Unsqueeze][forward]") {
  constexpr std::uint16_t NB_TRIALS = 10;
  // Create a random number generator
  std::random_device rd;
  auto random_seed = rd();
  fmt::print("True random seed: {}\n", random_seed);
  std::mt19937 gen(random_seed);
  // Random float distribution between 0 and 1
  std::uniform_real_distribution<float> valueDist(0.1f, 1.1f);
  std::uniform_int_distribution<std::size_t> tensor_dims_size_dist(
      std::size_t(1), std::size_t(10));
  std::size_t min_tensor_nb_dims{1};
  std::size_t max_tensor_nb_dims{7};
  std::uniform_int_distribution<std::size_t> tensor_nb_dims_dist(
      min_tensor_nb_dims, max_tensor_nb_dims);
  std::uniform_int_distribution<std::size_t> nb_dims_to_unsqueeze_dist(
      std::size_t(1), std::size_t(8));
  std::uniform_int_distribution<short> idx_dims_to_unsqueeze_dist(-9, 8);

  std::shared_ptr<Tensor> input_T = std::make_shared<Tensor>();
  input_T->setDataType(DataType::Float32);
  input_T->setBackend("cpu");
  std::shared_ptr<Tensor> result_T = std::make_shared<Tensor>();
  result_T->setDataType(DataType::Float32);
  result_T->setBackend("cpu");

  // BENCHMARKING
  std::chrono::time_point<std::chrono::system_clock> start;
  std::chrono::time_point<std::chrono::system_clock> end;
  std::chrono::duration<double, std::micro> duration{};

  int number_of_operation{0};
  for (uint16_t trial = 0; trial < NB_TRIALS; ++trial) {
    // Create the Operator
    size_t nb_dims_to_unsqueeze = nb_dims_to_unsqueeze_dist(gen);
    std::vector<int8_t> dims_to_unsqueeze(nb_dims_to_unsqueeze);
    std::generate(dims_to_unsqueeze.begin(), dims_to_unsqueeze.end(),
                  [&gen, &idx_dims_to_unsqueeze_dist]() {
                    return idx_dims_to_unsqueeze_dist(gen);
                  });
    std::shared_ptr<Node> unsqueeze_node = Unsqueeze(dims_to_unsqueeze);
    auto op =
        std::static_pointer_cast<OperatorTensor>(unsqueeze_node->getOperator());
    op->setDataType(DataType::Float32);
    op->setBackend("cpu");
    op->associateInput(0, input_T);

    // input tensor
    const std::size_t nb_dims_tensor = tensor_nb_dims_dist(gen);
    std::vector<std::size_t> dims_in(nb_dims_tensor);
    std::generate(dims_in.begin(), dims_in.end(),
                  [&gen, &tensor_dims_size_dist]() {
                    return tensor_dims_size_dist(gen);
                  });
    input_T->resize(dims_in);
    op->setInput(0, input_T);

    // rectifying indexes
    std::transform(
        dims_to_unsqueeze.begin(), dims_to_unsqueeze.end(),
        dims_to_unsqueeze.begin(),
        [&nb_dims_tensor, &nb_dims_to_unsqueeze](int8_t dim_to_unsqueeze) {
          return dim_to_unsqueeze < 0
                     ? dim_to_unsqueeze +
                           (nb_dims_tensor + nb_dims_to_unsqueeze)
                     : dim_to_unsqueeze;
        });

    // ensuring arguments given to Unsqueeze are good
    bool axes_to_unsqueeze_valid =
        ensure_axes_validity(dims_to_unsqueeze, input_T->nbDims());
    if (!axes_to_unsqueeze_valid) {
      REQUIRE_THROWS(op->forwardDims(true));
    } else {
      // output tensor
      std::vector<DimSize_t> dims_out =
          generate_unsqueeze_output_dims(dims_in, dims_to_unsqueeze);
      CHECK(op->forwardDims(true) == true);
      CHECK(op->getOutput(0)->dims() == dims_out);

      SECTION("forward") {
        const std::size_t nb_elems =
            std::accumulate(dims_in.cbegin(), dims_in.cend(), std::size_t(1),
                            std::multiplies<std::size_t>());
        float *array_in = new float[nb_elems];
        for (std::size_t i = 0; i < nb_elems; ++i) {
          array_in[i] = valueDist(gen);
        }
        number_of_operation += nb_elems; // Copying all values : 1
                                         // assignation / item in the tensor

        // input0
        input_T->resize(dims_in);
        input_T->getImpl()->setRawPtr(array_in, nb_elems);

        // results
        result_T->resize(dims_out);
        result_T->getImpl()->setRawPtr(array_in, nb_elems);

        CHECK(op->forwardDims(true) == true);
        start = std::chrono::system_clock::now();
        REQUIRE_NOTHROW(unsqueeze_node->forward());
        end = std::chrono::system_clock::now();
        duration +=
            std::chrono::duration_cast<std::chrono::microseconds>(end - start);

        CHECK(result_T->nbDims() == op->getOutput(0)->nbDims());
        for (DimSize_t i = 0; i < op->getOutput(0)->nbDims(); ++i) {
          CHECK(result_T->dims().at(i) == op->getOutput(0)->dims().at(i));
        }
        CHECK(approxEq<float>(*result_T, *(op->getOutput(0))));

        delete[] array_in;
      }
    }
    Log::info("GlobalAveragePooling total execution time: {}µs\n", duration.count());
    Log::info("Number of operations : {}\n", number_of_operation);
    Log::info("Operation / µs = {}\n", number_of_operation / duration.count());
  }
}

} // namespace Aidge
