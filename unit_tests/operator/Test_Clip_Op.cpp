/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_random.hpp>
#include <cstddef>  // std::size_t
#include <memory>
#include <random>   // std::mt19937, std::uniform_int_distribution
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Clip.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace Aidge {
TEST_CASE("[core/operator] Clip_Op(forwardDims)", "[Clip][forwardDims]") {
    constexpr std::uint16_t NBTRIALS = 10;

    // Create a random number generator
    auto rd = Catch::Generators::Detail::getSeed;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<std::size_t> dimsDist(1, 10);
    std::uniform_int_distribution<std::size_t> nbDimsDist(1, 5);

    // Create Clip Operator
    std::shared_ptr<Node> myClip = Clip();
    auto op = std::static_pointer_cast<OperatorTensor>(myClip -> getOperator());

    // Input tensor
    std::shared_ptr<Tensor> T0 = std::make_shared<Tensor>(Array2D<int,2,2> {
            {
                {1, 2},
                {3, 4}
            }
        });
    op -> associateInput(0,T0);
    // Tensor representign Min value
    std::shared_ptr<Tensor> Tmin = std::make_shared<Tensor>(2.0);
    op -> associateInput(1,Tmin);
    
    // Tensor representign Max value
    std::shared_ptr<Tensor> Tmax = std::make_shared<Tensor>(4.0);
    op -> associateInput(2,Tmax);
    /**
     * @todo Special case: scalar not handled yet by
     * ``OperatorTensor::forwardDims()``
     */
    SECTION("Scalar") {
        //We set every Input as a Scalar
        T0->resize({});
        Tmin->resize({});
        Tmax->resize({});

        REQUIRE_NOTHROW(op->forwardDims(true));
        REQUIRE((op->getOutput(0)->dims() == std::vector<std::size_t>()));
    }
    SECTION("Normal Input") {
        // a scalar is compatible with any other Tensor
        // input_0
        std::shared_ptr<Tensor> T0 = std::make_shared<Tensor>(Array2D<int,2,2> {
        {
            {1, 2},
            {3, 4}
        }
        });
        const std::size_t nb_dims = nbDimsDist(gen);
        std::vector<std::size_t> dims(nb_dims);
        for (std::size_t i = 0; i < nb_dims; ++i)
        {
            dims[i] = dimsDist(gen);
        }
        T0->resize(dims);
        op->associateInput(0,T0);
        REQUIRE_NOTHROW(op->forwardDims(true));
        REQUIRE((op->getOutput(0)->dims()) == dims);
        }
    
    SECTION("Min and max attributes ")
    {
        std::shared_ptr<Node> clip_attr = Clip("",-1.0,2.0);
        auto opc = std::static_pointer_cast<OperatorTensor>(clip_attr -> getOperator());
        std::shared_ptr<Tensor> T0 = std::make_shared<Tensor>();
        opc -> associateInput(0,T0);
        std::shared_ptr<Tensor> Tmin = std::make_shared<Tensor>(7);
        opc-> associateInput(1,Tmin);
        std::shared_ptr<Tensor> Tmax = std::make_shared<Tensor>(4);
        opc -> associateInput(2,Tmax);

        REQUIRE_NOTHROW(opc->forwardDims(true));
        REQUIRE((opc->getOutput(0)->dims() == std::vector<std::size_t>()));
    }
    SECTION("Min and max attributes (No Input for min and max)")
    {
        std::shared_ptr<Node> clip = Clip("",-1.0,2.0);
        auto opcl = std::static_pointer_cast<OperatorTensor>(clip -> getOperator());
        std::shared_ptr<Tensor> T0 = std::make_shared<Tensor>();
        opcl -> associateInput(0,T0);
        REQUIRE_NOTHROW(opcl->forwardDims());
        REQUIRE((opcl->getOutput(0)->dims() == std::vector<std::size_t>()));
    }
}
} // namespace Aidge
