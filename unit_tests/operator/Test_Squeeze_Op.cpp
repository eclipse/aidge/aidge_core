/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/Squeeze.hpp"

#include <algorithm>  // std::copy_if, std::count_if, std::generate, std::sort, std::transform
#include <array>
#include <chrono>
#include <cmath>
#include <cstddef> // std::size_t
#include <cstdint> // std::int8_t, std::uint16_t
#include <functional>  // std::multiplies
#include <iterator>  // std::back_inserter
#include <memory>
#include <numeric> // std::accumulate
#include <random> // std::random_device, std::mt19937, std::uniform_real_distribution
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_random.hpp>
#include <fmt/core.h>

#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/TensorUtils.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
TEST_CASE("[core/operator] Squeeze(forwardDims)", "[Squeeze][forwardDims]") {
  constexpr std::uint16_t NB_TRIALS = 10;
  // Create a random number generator
  auto random_seed = Catch::Generators::Detail::getSeed;
  std::mt19937 gen(random_seed());

  // Random float distribution between 0 and 1
  constexpr int8_t max_nb_dims = 7;
  std::uniform_real_distribution<float> tensor_value_dist(0.1f, 1.1f);
  std::uniform_int_distribution<std::size_t> tensor_nb_dims_dist(
      std::size_t(1), std::size_t(max_nb_dims));
  std::uniform_int_distribution<std::size_t> tensor_dims_size_dist(
      std::size_t(1), std::size_t(5));
  std::uniform_int_distribution<std::size_t> nb_dims_to_squeeze_dist(
      std::size_t(1), std::size_t(2));
  std::uniform_int_distribution<short> idx_dims_to_squeeze_dist(-9, 8);

  std::shared_ptr<Tensor> input_T = std::make_shared<Tensor>();

  SECTION("ERROR : Inputs not ready") {
    SECTION("unconnected input") {
      std::shared_ptr<Node> squeeze_node = Squeeze();
      auto op =
          std::static_pointer_cast<OperatorTensor>(squeeze_node->getOperator());
      REQUIRE_THROWS(op->forwardDims());
    }

    SECTION("empty tensor") {
      // Create the Squeeze Operator
      std::shared_ptr<Node> squeeze_node = Squeeze(std::vector<int8_t>({0}));
      auto op =
          std::static_pointer_cast<OperatorTensor>(squeeze_node->getOperator());
      op->associateInput(0, input_T);

      CHECK(op->forwardDims() == false);
    }
  }
  SECTION("ERROR : nb_dims_to_squeeze>input.size()") {
    constexpr size_t nb_dims_to_squeeze = 100;

    std::vector<std::int8_t> dims_to_squeeze(nb_dims_to_squeeze);
    std::generate(dims_to_squeeze.begin(), dims_to_squeeze.end(),
                  [&gen, &idx_dims_to_squeeze_dist]() {
                    return idx_dims_to_squeeze_dist(gen);
                  });
    Log::error("dims_to_sqeeze = {}", dims_to_squeeze);

    std::shared_ptr<Node> squeeze_node = Squeeze(dims_to_squeeze);
    auto op =
        std::static_pointer_cast<OperatorTensor>(squeeze_node->getOperator());

    // input tensor
    const std::size_t nb_dims = tensor_nb_dims_dist(gen);
    std::vector<std::size_t> dims_in(nb_dims);
    std::generate(dims_in.begin(), dims_in.end(),
                  [&tensor_dims_size_dist, &gen]() {
                    return tensor_dims_size_dist(gen);
                  });

    // Test
    input_T->resize(dims_in);
    op->setInput(0, input_T);
    REQUIRE_THROWS(op->forwardDims());
  }
  SECTION("Compare with reference output") {
    SECTION("axes is given via attribute") {
      SECTION("Squeeze a 1-sized-axis") {
        int8_t nb_dims = 4;
        std::shared_ptr<Node> squeeze_node = Squeeze(std::vector<std::int8_t>({0}));
        auto op = std::static_pointer_cast<OperatorTensor>(
            squeeze_node->getOperator());
        op->associateInput(0, input_T);

        std::vector<DimSize_t> dims_in{1, 2, 3, 4};
        input_T->resize(dims_in);

        CHECK(op->forwardDims());
        CHECK(op->getOutput(0)->dims() == std::vector<DimSize_t>({2, 3, 4}));
        CHECK((op->getOutput(0)->dims().size()) == 3);
      }
      SECTION("Squeeze multiple 1-sized axes") {
        // test should be successful
        std::shared_ptr<Node> squeeze_node =
            Squeeze(std::vector<int8_t>({1, -4}));
        auto op = std::static_pointer_cast<OperatorTensor>(
            squeeze_node->getOperator());
        op->associateInput(0, input_T);

        std::vector<DimSize_t> dims_in{1, 1, 13, 200};
        input_T->resize(dims_in);

        CHECK(op->forwardDims());
        CHECK(op->getOutput(0)->dims() == std::vector<DimSize_t>{13, 200});
        CHECK((op->getOutput(0)->dims().size()) == 2);
      }
      SECTION("Squeeze a non-1-Sized axis") {
        int8_t nb_dims = 4;
        std::shared_ptr<Node> squeeze_node = Squeeze(std::vector<std::int8_t>({3}));
        auto op = std::static_pointer_cast<OperatorTensor>(
            squeeze_node->getOperator());
        op->associateInput(0, input_T);

        std::vector<DimSize_t> dims_in{1, 2, 3, 4};
        input_T->resize(dims_in);

        REQUIRE_THROWS(op->forwardDims());
      }
      SECTION("Squeeze multiple non-sized-axes") {
        std::shared_ptr<Node> squeeze_node =
            Squeeze(std::vector<int8_t>({1, -2}));
        auto op = std::static_pointer_cast<OperatorTensor>(
            squeeze_node->getOperator());
        op->associateInput(0, input_T);

        std::array<DimSize_t, 3> dims_in{2, 3, 4};
        input_T->resize(dims_in);

        REQUIRE_THROWS((op->forwardDims()));
      }
    }
    SECTION("axes is given via tensor") {
      SECTION("tensor is empty") {
        // arguments here should be overridden by axes_T values
        std::shared_ptr<Node> myUnsqueeze =
            Squeeze(std::vector<std::int8_t>({0, 4}));
        auto op = std::static_pointer_cast<OperatorTensor>(
            myUnsqueeze->getOperator());
        op->associateInput(0, input_T);

        auto axes_T =
            std::make_shared<Aidge::Tensor>(std::vector<DimSize_t>({}));
        axes_T->setDataType(Aidge::DataType::Int8);
        axes_T->setBackend("cpu");

        std::vector<DimSize_t> dims_in{3, 1, 4, 1, 1, 5};
        input_T->resize(dims_in);
        op->associateInput(0, input_T);
        op->associateInput(1, axes_T);

        CHECK(op->forwardDims(true));
        CHECK(op->getOutput(0)->dims() == std::vector<DimSize_t>({3, 4, 5}));
      }
      SECTION("tensor not empty") {
        // arguments here should be overridden by axes_T values
        std::shared_ptr<Node> myUnsqueeze =
            Squeeze(std::vector<std::int8_t>({3, 1}));
        auto op = std::static_pointer_cast<OperatorTensor>(
            myUnsqueeze->getOperator());
        op->associateInput(0, input_T);

        auto axes_T =
            std::make_shared<Aidge::Tensor>(Aidge::Array1D<int8_t, 2>({0, 3}));
        axes_T->setDataType(Aidge::DataType::Int8);
        axes_T->setBackend("cpu");

        std::vector<DimSize_t> dims_in{1, 3, 4, 1, 5};
        input_T->resize(dims_in);
        op->associateInput(0, input_T);
        op->associateInput(1, axes_T);

        CHECK(op->forwardDims(true) == true);
        CHECK(op->getOutput(0)->dims() == std::vector<DimSize_t>({3, 4, 5}));
      }
    }
  }
  SECTION("Squeeze()") {
    // Create the Operator
    std::shared_ptr<Node> squeeze_node = Squeeze();
    auto op =
        std::static_pointer_cast<OperatorTensor>(squeeze_node->getOperator());
    op->associateInput(0, input_T);

    for (uint16_t trial = 0; trial < NB_TRIALS; ++trial) {
      // input tensor
      const std::size_t nb_dims = tensor_nb_dims_dist(gen);
      std::vector<std::size_t> dims_in(nb_dims);

      std::generate(dims_in.begin(), dims_in.end(),
                    [&gen, &tensor_dims_size_dist]() {
                      return tensor_dims_size_dist(gen);
                    });

      // output tensor
      std::vector<DimSize_t> dims_out;
      dims_out.reserve(dims_in.size());
      std::copy_if(dims_in.begin(), dims_in.end(), std::back_inserter(dims_out),
                   [](DimSize_t dim) { return dim != 1; });
      // Test
      input_T->resize(dims_in);
      op->setInput(0, input_T);
      CHECK(op->forwardDims() == true);
      CHECK(op->getOutput(0)->dims() == dims_out);

      int nb_ones = std::count_if(dims_in.begin(), dims_in.end(),
                                  [](std::int8_t dim) { return dim == 1; });
      CHECK((op->getInput(0)->dims().size() -
             op->getOutput(0)->dims().size()) == nb_ones);
    }
  }
  SECTION("Squeeze({N,...})") {
    int number_of_operation{0};
    for (uint16_t trial = 0; trial < NB_TRIALS; ++trial) {
      // Create the Operator
      size_t nb_dims_to_squeeze = nb_dims_to_squeeze_dist(gen);
      std::vector<int8_t> dims_to_squeeze(nb_dims_to_squeeze);
      std::generate(dims_to_squeeze.begin(), dims_to_squeeze.end(),
                    [&gen, &idx_dims_to_squeeze_dist]() {
                      return idx_dims_to_squeeze_dist(gen);
                    });
      std::shared_ptr<Node> squeeze_node = Squeeze({dims_to_squeeze});
      auto op =
          std::static_pointer_cast<OperatorTensor>(squeeze_node->getOperator());
      op->associateInput(0, input_T);

      // input tensor
      const std::size_t nb_dims_tensor = tensor_nb_dims_dist(gen);
      std::vector<std::size_t> dims_in(nb_dims_tensor);
      std::generate(dims_in.begin(), dims_in.end(),
                    [&gen, &tensor_dims_size_dist]() {
                      return tensor_dims_size_dist(gen);
                    });
      input_T->resize(dims_in);
      op->setInput(0, input_T);

      // rectifying indexes
      std::transform(dims_to_squeeze.begin(), dims_to_squeeze.end(),
                     dims_to_squeeze.begin(),
                     [&nb_dims_tensor](int8_t dim_to_squeeze) {
                       return dim_to_squeeze < 0
                                  ? dim_to_squeeze + nb_dims_tensor
                                  : dim_to_squeeze;
                     });
      std::sort(dims_to_squeeze.begin(), dims_to_squeeze.end());
      auto it = std::unique(dims_to_squeeze.begin(), dims_to_squeeze.end());
      dims_to_squeeze.erase(it, dims_to_squeeze.end());

      // ensuring arguments given to Squeeze are good
      bool not_in_bounds = false;
      bool dim_to_squeeze_not_1_sized = false;
      for (const auto dim_to_squeeze : dims_to_squeeze) {
        not_in_bounds = dim_to_squeeze >= nb_dims_tensor;
        if (not_in_bounds) {
          break;
        }
        dim_to_squeeze_not_1_sized = dims_in.at(dim_to_squeeze) != 1;
        if (dim_to_squeeze_not_1_sized) {
          break;
        }
      }

      if (nb_dims_tensor > max_nb_dims || not_in_bounds ||
          dim_to_squeeze_not_1_sized) {
        REQUIRE_THROWS(op->forwardDims());
      } else {
        // output tensor
        int i = 0;
        std::vector<DimSize_t> dims_out;
        dims_out.reserve(dims_in.size());
        std::copy_if(dims_in.begin(), dims_in.end(),
                     std::back_inserter(dims_out),
                     [&dims_to_squeeze, &i](DimSize_t dim) {
                       bool ok = dim != 1 ||
                                 !std::binary_search(dims_to_squeeze.begin(),
                                                     dims_to_squeeze.end(), i);
                       i++; // incrementing counter since C++ has not enumerate
                            // fctn (until C++23)
                       return ok;
                     });
        CHECK(op->forwardDims() == true);
        CHECK(op->getOutput(0)->dims() == dims_out);
      }
    }
  }
}

TEST_CASE("[core/operator] Squeeze(forward)", "[Squeeze][forward]") {
  constexpr std::uint16_t NB_TRIALS = 10;
  // Create a random number generator
  auto random_seed = Catch::Generators::Detail::getSeed;
  std::mt19937 gen(random_seed());

  constexpr int8_t max_nb_dims = 7;
  std::uniform_real_distribution<float> tensor_value_dist(0.1f, 1.1f);
  std::uniform_int_distribution<std::size_t> tensor_nb_dims_dist(
      std::size_t(1), std::size_t(max_nb_dims));
  std::uniform_int_distribution<std::size_t> tensor_dims_size_dist(
      std::size_t(1), std::size_t(5));
  std::uniform_int_distribution<std::size_t> nb_dims_to_squeeze_dist(
      std::size_t(1), std::size_t(2));
  std::uniform_int_distribution<short> idx_dims_to_squeeze_dist(-9, 8);

  std::shared_ptr<Tensor> input_T = std::make_shared<Tensor>();

  // BENCHMARKING
  std::chrono::time_point<std::chrono::system_clock> start;
  std::chrono::time_point<std::chrono::system_clock> end;
  std::chrono::duration<double, std::micro> duration{};

  int number_of_operation{0};
  for (uint16_t trial = 0; trial < NB_TRIALS; ++trial) {
    // Create the Operator
    size_t nb_dims_to_squeeze = nb_dims_to_squeeze_dist(gen);
    std::vector<int8_t> dims_to_squeeze(nb_dims_to_squeeze);
    std::generate(dims_to_squeeze.begin(), dims_to_squeeze.end(),
                  [&gen, &idx_dims_to_squeeze_dist]() {
                    return idx_dims_to_squeeze_dist(gen);
                  });
    std::shared_ptr<Node> squeeze_node = Squeeze({dims_to_squeeze});
    auto op =
        std::static_pointer_cast<OperatorTensor>(squeeze_node->getOperator());
    op->setDataType(DataType::Float32);
    op->setBackend("cpu");

    // input tensor
    const std::size_t nb_dims_tensor = tensor_nb_dims_dist(gen);
    std::vector<std::size_t> dims_in(nb_dims_tensor);
    std::generate(dims_in.begin(), dims_in.end(),
                  [&gen, &tensor_dims_size_dist]() {
                    return tensor_dims_size_dist(gen);
                  });
    input_T->resize(dims_in);
    op->setInput(0, input_T);

    // rectifying indexes
    std::transform(dims_to_squeeze.begin(), dims_to_squeeze.end(),
                   dims_to_squeeze.begin(),
                   [&nb_dims_tensor](int8_t dim_to_squeeze) {
                     return dim_to_squeeze < 0 ? dim_to_squeeze + nb_dims_tensor
                                               : dim_to_squeeze;
                   });

    // ensuring arguments given to Squeeze are good
    bool not_in_bounds = false;
    bool dim_to_squeeze_not_1_sized = false;
    for (const auto dim_to_squeeze : dims_to_squeeze) {
      not_in_bounds = dim_to_squeeze >= nb_dims_tensor;
      if (not_in_bounds) {
        break;
      }
      dim_to_squeeze_not_1_sized = dims_in.at(dim_to_squeeze) != 1;
      if (dim_to_squeeze_not_1_sized) {
        break;
      }
    }
    if (nb_dims_tensor > max_nb_dims || not_in_bounds ||
        dim_to_squeeze_not_1_sized) {
      REQUIRE_THROWS(op->forwardDims());
    } else {
      // output tensor
      int i = 0;
      std::vector<DimSize_t> dims_out;
      dims_out.reserve(dims_in.size());
      for (DimIdx_t i = 0; i < dims_in.size(); ++i) {
        if (dims_in[i] == 1 &&
            std::find(dims_to_squeeze.begin(), dims_to_squeeze.end(), i) !=
                dims_to_squeeze.end()) {
          continue;
        }
        dims_out.push_back(dims_in[i]);
      }
      CHECK(op->forwardDims());
      CHECK(op->getOutput(0)->dims() == dims_out);

      SECTION("forward") {
        // Create the input Tensor
        std::shared_ptr<Tensor> input_T = std::make_shared<Tensor>();
        input_T->setDataType(DataType::Float32);
        input_T->setBackend("cpu");
        op->associateInput(0, input_T);

        // Create results Tensor
        std::shared_ptr<Tensor> result_T = std::make_shared<Tensor>();
        result_T->setDataType(DataType::Float32);
        result_T->setBackend("cpu");

        const std::size_t nb_elems =
            std::accumulate(dims_in.cbegin(), dims_in.cend(), std::size_t(1),
                            std::multiplies<std::size_t>());
        float *array_in = new float[nb_elems];
        for (std::size_t i = 0; i < nb_elems; ++i) {
          float val = tensor_value_dist(gen);
          array_in[i] = val;
        }
        number_of_operation += nb_elems; // Copying all values : 1
                                         // assignation / item in the tensor
        // input0
        input_T->resize(dims_in);
        input_T->getImpl()->setRawPtr(array_in, nb_elems);

        result_T->resize(dims_out);
        result_T->getImpl()->setRawPtr(array_in, nb_elems);

        CHECK(op->forwardDims() == true);
        start = std::chrono::system_clock::now();
        REQUIRE_NOTHROW(squeeze_node->forward());
        end = std::chrono::system_clock::now();
        duration +=
            std::chrono::duration_cast<std::chrono::microseconds>(end - start);

        CHECK(approxEq<float>(*result_T, *(op->getOutput(0))));
        CHECK(result_T->nbDims() == op->getOutput(0)->nbDims());
        for (DimSize_t i = 0; i < op->getOutput(0)->nbDims(); ++i) {
          CHECK(result_T->dims().at(i) == op->getOutput(0)->dims().at(i));
        }
        CHECK(approxEq<float>(*result_T, *(op->getOutput(0))));

        delete[] array_in;
      }
    Log::info("GlobalAveragePooling total execution time: {}µs\n", duration.count());
    Log::info("Number of operations : {}\n", number_of_operation);
    Log::info("Operation / µs = {}\n", number_of_operation / duration.count());
    }
  }
}

} // namespace Aidge
