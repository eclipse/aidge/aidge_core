/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>
#include <random>   // std::random_device, std::mt19937, std::uniform_int_distribution

#include <catch2/catch_test_macros.hpp>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/DepthToSpace.hpp"


namespace Aidge {

TEST_CASE("[core/operator] DepthToSpace_Op", "[DepthToSpace][forwardDims]") {
    // Create a random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<std::size_t> dimsDist(1, 10);

    SECTION("Nb dimensions") {
        // Create DepthToSpace operator with block_size of 1 compatible with any size
        std::shared_ptr<Node> myDTS = DepthToSpace(1);
        auto op = std::static_pointer_cast<OperatorTensor>(myDTS -> getOperator());

        SECTION("Scalar") {
            // input_0
            std::shared_ptr<Tensor> T0 = std::make_shared<Tensor>(9);
            op -> associateInput(0,T0);
            REQUIRE_THROWS(op->forwardDims());
        }
        SECTION("+1-D") {
            // input_0
            std::shared_ptr<Tensor> T0 = std::make_shared<Tensor>();
            op -> associateInput(0,T0);

            for (std::uint16_t nb_dims = 0; nb_dims < 6; ++nb_dims) {

                std::vector<std::size_t> dims0(nb_dims);
                for (std::size_t i = 0; i < nb_dims; ++i) {
                    dims0[i] = dimsDist(gen);
                }
                T0->resize(dims0);
                if (nb_dims == 4) {
                    REQUIRE_NOTHROW(op->forwardDims());
                } else {
                    REQUIRE_THROWS(op->forwardDims());
                }
            }
        }
    }

    SECTION("Propagation") {
        // input_0 with 4-D in NCHW format
        std::shared_ptr<Tensor> T0 = std::make_shared<Tensor>(std::vector<DimSize_t>({1, 16, 100, 100}));

        DepthToSpace_Op myDTS_should_throw = DepthToSpace_Op(7);
        myDTS_should_throw.associateInput(0,T0);

        REQUIRE_THROWS(myDTS_should_throw.forwardDims());

        DepthToSpace_Op myDTS_should_not_throw = DepthToSpace_Op(4);
        myDTS_should_not_throw.associateInput(0,T0);

        REQUIRE_NOTHROW(myDTS_should_not_throw.forwardDims());
        REQUIRE(myDTS_should_not_throw.getOutput(0)->dims() == std::vector<std::size_t>({1,1,400,400}));
    }
}

TEST_CASE("[core/operator] DepthToSpace_Op impl", "[DepthToSpace][forward]") {
    // Create a random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<std::size_t> dimsDist(1, 10);


}

} // namespace Aidge
