/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_random.hpp>
#include <cstddef> // std::size_t
#include <memory>
#include <random> // std::mt19937, std::uniform_int_distribution
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/GlobalAveragePooling.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
TEST_CASE("[core/operator] GlobalAveragePooling_Op(forwardDims)",
          "[GlobalAveragePooling][forwardDims]") {
  constexpr std::uint16_t NB_TRIALS = 10;
  // Create a random number generator
  auto rd = Catch::Generators::Detail::getSeed;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<std::size_t> dimsDist(1, 10);
  std::uniform_int_distribution<std::size_t> inf3DimsDistribution(1, 2);
  std::uniform_int_distribution<std::size_t> sup3DimsDistribution(3, 10);

  // Create the GlobalAveragePooling Operator
  std::shared_ptr<Node> myGlobAvgPool = GlobalAveragePooling();
  auto op =
      std::static_pointer_cast<OperatorTensor>(myGlobAvgPool->getOperator());

  // input_0
  std::shared_ptr<Tensor> input_T = std::make_shared<Tensor>();
  SECTION("Un-connected input leads to failure.") {
    REQUIRE_THROWS(op->forwardDims());
  }
  op->associateInput(0, input_T);

  SECTION("Connected Inputs") {
    SECTION("empty tensor") {
      for (uint16_t trial = 0; trial < NB_TRIALS; ++trial) {
        // Test that on undefined input it does not fail
        REQUIRE_NOTHROW(op->forwardDims());
      }
    }
    SECTION("Full tensor") {
      SECTION("nbDim < 3") {
        for (uint16_t trial = 0; trial < NB_TRIALS; ++trial) {
          const std::size_t nb_dims = inf3DimsDistribution(gen);
          std::vector<std::size_t> dims(nb_dims);
          for (uint16_t i = 0; i < nb_dims; ++i) {
            dims[i] = dimsDist(gen);
          }
          input_T->resize(dims);
          REQUIRE_THROWS(op->forwardDims());
        }
      }
      SECTION("nbDim > 3") {
        for (uint16_t trial = 0; trial < NB_TRIALS; ++trial) {
          const std::size_t nb_dims = sup3DimsDistribution(gen);
          std::vector<std::size_t> dims(nb_dims);
          for (uint16_t i = 0; i < nb_dims; ++i) {
            dims[i] = dimsDist(gen) + 1;
          }
          std::vector<DimSize_t> dims_out(nb_dims, 1);
          dims_out[0] = dims[0];
          dims_out[1] = dims[1];
          input_T->resize(dims);
          op->setInput(0, input_T);
          REQUIRE_NOTHROW(op->forwardDims());
          REQUIRE(op->getOutput(0)->dims() == dims_out);
          REQUIRE((op->getOutput(0)->dims().size()) == nb_dims);
        }
      }
    }
  }
}
} // namespace Aidge
