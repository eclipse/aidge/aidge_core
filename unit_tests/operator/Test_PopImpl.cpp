/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <memory>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Pop.hpp"
#include "aidge/utils/TensorUtils.hpp"

using namespace Aidge;

TEST_CASE("[cpu/operator] Pop(forward)", "[Pop][CPU]") {
    std::shared_ptr<Tensor> pop1 = std::make_shared<Tensor>(Array1D<int,3>{{4,5,6}});
    std::shared_ptr<Tensor> pop2 = std::make_shared<Tensor>(Array1D<int,3>{{1,2,3}});
    std::shared_ptr<Tensor> input = std::make_shared<Tensor>(Array2D<int,2,3>{{{1,2,3}, {4,5,6}}});

    auto pop = Pop("pop");
    std::shared_ptr<Pop_Op> op = std::static_pointer_cast<Pop_Op>(pop->getOperator());
    op->associateInput(0, input);
    op->setBackend("cpu");
    op->setDataType(DataType::Int32);
    op->forwardDims();

    REQUIRE_NOTHROW(pop->forward());
    REQUIRE(*op->getOutput(0) == *pop2);
    REQUIRE_NOTHROW(pop->forward());
    REQUIRE(*op->getOutput(0) == *pop1);
}
