/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/recipes/Recipes.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/graph/OpArgs.hpp"
#include <cstddef>

using namespace Aidge;

TEST_CASE("[ExplicitTranspose] conv") {
    auto conv1 = Conv(3, 32, {3, 3}, "conv1");
    auto conv2 = Conv(32, 64, {3, 3}, "conv2");
    auto conv3 = Conv(64, 10, {1, 1}, "conv3", {2, 2});

    auto g1 = Sequential({
        Producer({16, 3, 224, 224}, "dataProvider"),
        conv1,
        conv2,
        conv3
    });

    g1->setDataFormat(DataFormat::NCHW);
    conv2->getOperator()->setDataFormat(DataFormat::NHWC);

    g1->save("explicitTranspose_before");
    REQUIRE(g1->getNodes().size() == 10);
    const auto initialNodes = g1->getNodes();

    g1->forwardDims();
    explicitTranspose(g1);

    // Check that Transpose were inserted
    g1->save("explicitTranspose_after");
    REQUIRE(g1->getNodes().size() == 12);

    // Check that Transpose are removed
    conv2->getOperator()->setDataFormat(DataFormat::NCHW);
    explicitTranspose(g1);

    REQUIRE(g1->getNodes().size() == 10);
    REQUIRE(g1->getNodes() == initialNodes);
}
