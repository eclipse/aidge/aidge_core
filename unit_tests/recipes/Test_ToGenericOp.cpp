/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <memory>
#include <set>
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/OpArgs.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/FC.hpp"
#include "aidge/operator/ReLU.hpp"
#include "aidge/operator/GenericOperator.hpp"
#include "aidge/recipes/Recipes.hpp"

namespace Aidge {

TEST_CASE("[graph/convert] toGenericOp", "[toGenericOp][recipies]") {
    // Create a convolution operator
    std::shared_ptr<GraphView> g =
                Sequential({
                    Conv(1, 3, {3, 3}, "conv1"),
                    ReLU(),
                    Conv(3, 4, {1, 1}, "conv2"),
                    ReLU(),
                    Conv(4, 3, {1, 1}, "conv3"),
                    ReLU(),
                    FC(2028, 256, false, "fc1"),
                    ReLU(),
                    FC(256, 10, false, "fc2")});
    
    // NCHW - MNIST DATA like
    g->forwardDims({{5, 1, 28, 28}});

    SECTION("Test Operator to Generic Operator") {
        auto convOp = g->getNode("conv2");

        // Convert to GenericOperator
        toGenericOp(convOp);

        auto newGenOp = g->getNode("conv2");

        // Ensure the conversion
        REQUIRE(newGenOp->type() == "Conv2D");

        const auto convOpAttr = convOp->getOperator()->attributes()->getAttrs();
        const auto newGenOpAttr = (newGenOp->getOperator()->attributes()->getAttrs());
        REQUIRE((!(newGenOpAttr < convOpAttr) && !(convOpAttr < newGenOpAttr)));
    }

    SECTION("Test MetaOperator to Generic Operator") {

        const auto nbFused = fuseToMetaOps(g, "Conv2D->ReLU->FC", "ConvReLUFC");

        REQUIRE(nbFused == 1);

        std::shared_ptr<Node> metaOpNode;

        const auto nodes = g->getNodes(); // g nodes gets modified in the loop!
        for (const auto& nodePtr : nodes) 
        {
            if (nodePtr->type() == "ConvReLUFC") 
            {
                nodePtr->setName("ConvReLUFC_0");
                metaOpNode = nodePtr;
                // Convert to GenericOperator
                toGenericOp(nodePtr);
            }
        }

        REQUIRE(metaOpNode);
        REQUIRE(!metaOpNode->getOperator()->isAtomic());
        auto newGenOp = g->getNode("ConvReLUFC_0");

        // Ensure the conversion
        REQUIRE(newGenOp->type() == "ConvReLUFC");
        REQUIRE(std::dynamic_pointer_cast<GenericOperator_Op>(newGenOp->getOperator()));

        const auto metaOpAttr = *std::static_pointer_cast<DynamicAttributes>(metaOpNode->getOperator()->attributes());
        const auto newGenOpAttr = *std::static_pointer_cast<DynamicAttributes>(newGenOp->getOperator()->attributes());
        REQUIRE((!(newGenOpAttr < metaOpAttr) && !(metaOpAttr < newGenOpAttr)));

    }

}

} // namespace Aidge
