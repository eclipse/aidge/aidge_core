/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/recipes/Recipes.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/graph/OpArgs.hpp"
#include <cstddef>

using namespace Aidge;

TEST_CASE("[ConvToMatMul] conv") {
    auto conv1 = Conv(3, 32, {3, 3}, "conv1");
    auto conv2 = Conv(32, 64, {3, 3}, "conv2", {1, 1}, {1, 1}, true);
    auto conv3 = Conv(64, 10, {1, 1}, "conv3", {2, 2});

    auto g1 = Sequential({
        Producer({16, 3, 224, 224}, "dataProvider"),
        conv1,
        conv2,
        conv3
    });

    g1->forwardDims();

    g1->save("convToMatMul_before");
    REQUIRE(convToMatMul(g1) == 3);
    g1->save("convToMatMul_after");
}
