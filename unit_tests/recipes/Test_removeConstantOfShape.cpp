/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/graph/GraphView.hpp"
#include "aidge/operator/Identity.hpp"
#include "aidge/recipes/Recipes.hpp"

#include <cstddef>
#include <cstdint>
#include <memory>
#include <vector>

#include <catch2/catch_test_macros.hpp>

#include "aidge/graph/OpArgs.hpp"
#include "aidge/operator/Add.hpp"
#include "aidge/operator/ConstantOfShape.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/MatMul.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/operator/ReLU.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/Types.h"

using namespace Aidge;

TEST_CASE("[cpu/recipes] removeConstantOfShape",
          "[ConstantOfShape][removeConstantOfShape][recipes]") {
  auto input_T = std::make_shared<Tensor>(Array1D<int64_t, 4>({1, 1, 3, 3}));

  auto model = std::make_shared<GraphView>();
  SECTION("Sequential model") {
    model = Sequential({Producer(input_T, "prod_0", true),
                        ConstantOfShape(3, "constantOfShape_0"),
                        Conv(1, 1, {3, 3}, "Conv_0"), ReLU("ReLU_1")});
    model->save("test_removeConstantOfShape_model_before_1");
    CHECK(removeConstantOfShape(model) == 1);
    CHECK(model->forwardDims());
    model->save("test_removeConstantOfShape_model_after_1");
  }
}

