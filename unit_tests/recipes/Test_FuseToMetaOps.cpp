/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <set>

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/OpArgs.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/ReLU.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/recipes/Recipes.hpp"

namespace Aidge {


TEST_CASE("[cpu/recipes] FuseToMetaOps", "[FuseToMetaOps][recipes]") {
    auto g1 = Sequential({
        Producer({16, 3, 224, 224}, "dataProvider"),
        Conv(3, 32, {3, 3}, "conv1"),
        ReLU("relu1"),
        Conv(32, 64, {3, 3}, "conv2"),
        ReLU("relu2"),
        Conv(64, 10, {1, 1}, "conv3")
    });
    g1->save("FuseToMetaOps_before");

    const auto nbFused = fuseToMetaOps(g1, "Conv2D->ReLU", "ConvReLU");
    g1->save("FuseToMetaOps_after", true);

    REQUIRE(nbFused == 2);
}

}  // namespace Aidge
