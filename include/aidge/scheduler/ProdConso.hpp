/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_SCHEDULER_PRODCONSO_H_
#define AIDGE_SCHEDULER_PRODCONSO_H_

#include <memory>
#include <string>
#include <vector>

#include "aidge/utils/Types.h"
#include "aidge/data/Elts.hpp"

namespace Aidge {
class Operator;

class ProdConso {
public:
    ProdConso(const Operator& op, bool inPlace = false);

    static std::unique_ptr<ProdConso> defaultModel(const Operator& op) {
        return std::make_unique<ProdConso>(op, false);
    }

    static std::unique_ptr<ProdConso> inPlaceModel(const Operator& op) {
        return std::make_unique<ProdConso>(op, true);
    }

    const Operator& getOperator() const noexcept {
        return mOp;
    }

    /**
     * @brief Minimum amount of data from a specific input required by the
     * implementation to be run.
     *
     * @param inputIdx Index of the input analyzed.
     * @return std::size_t
     */
    virtual Elts_t getNbRequiredData(const IOIndex_t inputIdx) const;

    /**
     * @brief Amount of input data that cannot be overwritten during the execution.
     */
    virtual Elts_t getNbRequiredProtected(const IOIndex_t inputIdx) const;

    /**
     * @brief Memory required at an output for a given input size.
     */
    virtual Elts_t getRequiredMemory(const IOIndex_t outputIdx, const std::vector<DimSize_t> &inputsSize) const;

    /**
     * @brief Total amount of consumed data from a specific input.
     *
     * @param inputIdx Index of the input analyzed.
     * @return DimSize_t
     */
    virtual Elts_t getNbConsumedData(const IOIndex_t inputIdx) const;

    /**
     * @brief Total amount of produced data ready to be used on a specific output.
     *
     * @param outputIdx Index of the output analyzed.
     * @return DimSize_t
     */
    virtual Elts_t getNbProducedData(const IOIndex_t outputIdx) const;

    /**
     * @brief Update the Consumer Producer system by simulating the consumption and production of i/o
     *
     */
    virtual void updateConsummerProducer();

    /**
     * @brief Reset the Consumer Producer system.
     *
     */
    virtual void resetConsummerProducer();

    virtual ~ProdConso() = default;

protected:
    const Operator &mOp;
    const bool mInPlace;
    std::vector<Elts_t> mNbConsumedData;
    std::vector<Elts_t> mNbProducedData;
};
} // namespace Aidge

#endif /* AIDGE_SCHEDULER_PRODCONSO_H_ */
