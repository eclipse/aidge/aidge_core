/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_SCHEDULER_MEMORY_MANAGER_H
#define AIDGE_CORE_SCHEDULER_MEMORY_MANAGER_H

#include <memory>
#include <vector>
#include <map>

#include "aidge/graph/Node.hpp"

namespace Aidge {
/**
 * @brief The MemoryManager can be used to generate an optimized static memory 
 * layout for a computing graph in a global memory space.
 * The are some assumptions:
 * - A MemoryManager represents a single global memory space, filled with 
 *   contiguous, non-overlapping MemorySpace chunks.
 * - A MemorySpace contains one or multiple MemoryPlane, each MemoryPlane
 *   corresponding to the allocation of a specific Tensor. When a Tensor can re-
 *   use the memory of the preceding one (for in-place or partially in-place
 *   operators), multiple overlapping MemoryPlane can be created in the same 
 *   MemorySpace (remember, MemorySpace **cannot** be overlapping!).
 * - A MemoryPlane is tailored for handling (N)HWC data with two properties:
 *   - Possibility of wrapping: on the H axis (each W*C block is contiguous).
 *   - Possibility of concatenation: on the C axis (C1+C2+...+Cn).
 * - All the sizes and offsets specified in a MemoryManager are expressed in
 *   number of data elements, or **words**, meaning currently a uniform data 
 *   precision is expected in a MemoryManager (for instance, if the precision is
 *   16-bits, each data element will be 2 bytes, which will be the size of a word).
 */
class MemoryManager {
public:
    typedef int Clock_T;

    enum OptimizeStrategy {
        None,
        OptimizeMaxLifetimeMinSizeFirst,
        OptimizeMaxLifetimeMaxSizeFirst,
        OptimizeMaxHoleMaxLifetimeFirst
    };

    // MemorySpace are contiguous, non-overlapping memory blocks, that can be
    // re-arranged freely.
    struct MemorySpace {
        MemorySpace(Clock_T clock_,
                    unsigned int offset_,
                    unsigned int size_,
                    std::set<std::shared_ptr<Node> > dependencies_
                        = std::set<std::shared_ptr<Node> >()
        ):
            offset(offset_),
            size(size_),
            dependencies(dependencies_),
            allocated(clock_),
            released(-1) {}

        /// Offset of the MemorySpace in the MemoryManager global memory space (in words)
        unsigned int offset;
        /// Size of the MemorySpace (in words)
        unsigned int size;
        std::set<std::shared_ptr<Node> > dependencies;
        Clock_T allocated;
        Clock_T released;
    };

    /**
     * @brief MemoryPlane belongs to a MemorySpace. Any number of potentiall
     * overlapping planes can be associated to a MemorySpace.
     * MemoryPlane can be non-contiguous (in case of stride, or wrapping, when
     * offset + size > memSpace.size).
     * MemoryPlane cannot be re-arranged inside a MemorySpace.
     * 
     * A MemoryPlane is tailored for handling (N)HWC data with two properties:
     * - Possibility of wrapping: on the H axis (each W*C block is contiguous).
     * - Possibility of concatenation: on the C axis (C1+C2+...+Cn).
     * 
     * Detail of (N)HWC data handling:
     * - \p length is the size of contiguous and non-breakable memory line (W in HWC);
     * - \p count is the number of memory lines of size \p length constituting a memory block (H in HWC);
     * - \p stride is the number of channels, or memory blocks, *in total*, 
     *   of \p count lines of size \p length (C in NHWC);
     * - \p size is the number of channels, or memory blocks, *in this MemoryPlane*,
     *   of \p count lines of size \p length.
     *   In the case of concatenation, there can be multiple overlapping MemoryPlane
     *   with different size, like NHWC = NHW(C1+C2):
     *   - MemoryPlane#1: \p size = C1 and \p stride = C=C1+C2
     *   - MemoryPlane#2: \p size = C2 and \p stride = C=C1+C2
     *                    (with an additional relative offset of +C1)
     * In this mode, wrapping can only occur on the H (\p count) axis. W*C chunks
     * are guaranteed to be contiguous (\p length * \p stride).
     * 
     * By default, \p stride = \p size, \p count = 1 and \p length = 1, meaning
     * there is no NHWC layout and the MemoryPlane can be wrapped **anywhere**.
     * In this case, \p size is the total size of the MemoryPlane (H*W*C, in words).
     */
    struct MemoryPlane {
        MemoryPlane(std::shared_ptr<MemorySpace> memSpace_,
                    Clock_T clock_,
                    unsigned int offset_,
                    unsigned int size_,
                    unsigned int stride_ = 0,
                    unsigned int length_ = 1,
                    unsigned int count_ = 1
        ):
            memSpace(memSpace_),
            allocated(clock_),
            offset(offset_),
            size(size_),
            stride(std::max(size_, stride_)),
            length(length_),
            count(count_)
        {
            assert(offset <= memSpace->size);
            // The preceding assert should allow offset == memSpace->size (see
            // issue #63). This means immediate wrapping.
            // It appends if the final offset computed in reallocate() is at
            // the end of the previous memPlane and is also at the end of the
            // memSpace (in case for example of in-place memory op.).
            // Instead of bringing the offset back to the beginning of the
            // memSpace, we stay attached to this offset in case the memSpace
            // grows when a new memPlane is added.

            assert(getContiguousOffset() >= memSpace->offset);
            assert(getWrappedOffset() >= memSpace->offset);
            assert(getContiguousOffset() + getContiguousSize()
                <= memSpace->offset + memSpace->size);
            assert(getWrappedOffset() + getWrappedSize()
                <= memSpace->offset + memSpace->size);
        }

        /**
         * @brief Get the total size of the MemoryPlane, including the stride.
         * 
         * @return unsigned int Total size in words
         */
        inline unsigned int getSize() const {
            return stride * length * count;
        }

        /**
         * @brief Get the useful size of the MemoryPlane, as if its memory blocks
         * were contiguous, without stride.
         * 
         * @return unsigned int Useful size in words
         */
        inline unsigned int getUsefulSize() const {
            return size * length * count;
        }

        /**
         * @brief Get the absolute offset of the beginning of the memory plane.
         * 
         * @return unsigned int Contiguous offset in words
         */
        inline unsigned int getContiguousOffset() const {
            return memSpace->offset + offset;
        }

        /**
         * @brief Get the size of the contiguous part of the memory plane, from
         * its beginning to the limit of the MemorySpace size.
         * If the MemoryPlane fill the MemorySpace without wrapping, the contiguous
         * size will be the same as the total size of the MemoryPlane.
         * 
         * @return unsigned int Contiguous size in words
         */
        inline unsigned int getContiguousSize() const {
            return std::min(getSize(), getLimit());
        }

        /**
         * @brief Get the absolute offset of the wrapped part of the memory plane.
         * Since the wrapped part of the memory plane begins at the beginning of
         * the MemorySpace, the returned offset is always the same as the MemorySpace
         * offset.
         * 
         * @return unsigned int Wrapped offset in words
         */
        inline unsigned int getWrappedOffset() const {
            return memSpace->offset;
        }

        /**
         * @brief Get the size of the wrapped part of the memory plane, from
         * the beginning of the MemorySpace to the total size of the MemoryPlane,
         * including the stride.
         * If the MemoryPlane fill the MemorySpace without wrapping, the wrapped
         * size will 0.
         * 
         * @return unsigned int Wrapped size in words
         */
        inline unsigned int getWrappedSize() const {
            return getSize() - getContiguousSize();
        }

        /**
         * @brief Get the absolute offset after the end of the memory plane (if it
         * is wrapped, the offset will correspond to the end of the wrapped part).
         * The word at the final offset is not included in the MemoryPlane.
         * 
         * @return unsigned int Final offset in words
         */
        inline unsigned int getFinalOffset() const {
            return (getWrappedSize() > 0)
                ? getWrappedOffset() + getWrappedSize()
                : getContiguousOffset() + getContiguousSize();
        }

        /**
         * @brief Get the absolute offset after the end of the contiguous part
         * of the memory plane.
         * The word at the upper offset is not included in the MemoryPlane.
         * 
         * @return unsigned int Upper offset in words
         */
        inline unsigned int getUpperOffset() const {
            return (getContiguousOffset() + getContiguousSize());
        }

        // Limit is computed dynamically, as memSpace->size may increase after
        // the creation of this memory space. This is actually necessary to
        // ensure that the memory wrapping works correctly, because when
        // computing the margin required for the wrapping, it is assumed that
        // the previous layer wrapping extends to the full memory space size.
        inline unsigned int getLimit() const {
            // limit must be a multiple of (stride * length) if count > 1
            // or stride if length > 1
            // uses floor() to stay below memSpace->size
            return (count > 1)
                ? std::floor((memSpace->size - offset)
                        / static_cast<double>(stride * length)) * (stride * length)
                : (length > 1)
                    ? std::floor((memSpace->size - offset)
                            / static_cast<double>(stride)) * stride
                    : memSpace->size - offset;
        }

        std::shared_ptr<MemorySpace> memSpace;
        Clock_T allocated;
        /// Relative offset of the MemoryPlane in the MemorySpace (in words)
        unsigned int offset;
        /// Number of channels, or memory blocks, *in this MemoryPlane*,
        /// of \p count lines of size \p length.
        /// In the case of concatenation, there can be multiple overlapping MemoryPlane
        /// with different size, like NHWC = NHW(C1+C2):
        /// - MemoryPlane#1: \p size = C1 and \p stride = C=C1+C2
        /// - MemoryPlane#2: \p size = C2 and \p stride = C=C1+C2
        ///                  (with an additional relative offset of +C1)
        /// By default, \p stride = \p size, \p count = 1 and \p length = 1, meaning
        /// there is no NHWC layout and the MemoryPlane can be wrapped **anywhere**.
        /// In this case, \p size is the total size of the MemoryPlane (H*W*C, in words).
        unsigned int size;
        /// Number of channels, or memory blocks *in total*,
        /// of \p count lines of size \p length (the C in NHWC).
        /// There should be C blocks of H*W size.
        unsigned int stride;
        /// Size of an elementary, contiguous and non-breakable, memory line 
        /// (the W in NHWC), in words. A MemoryPlane wrapping cannot occur in
        /// the middle of a memory line.
        unsigned int length;
        /// Number of memory lines of size \p length constituting a memory block
        /// (the H in NHWC). The size of a memory block is H*W.
        unsigned int count;
    };

    struct MaxLifetimeMinSizeFirst {
        MaxLifetimeMinSizeFirst(unsigned int maxLifetime_)
            : maxLifetime(maxLifetime_) {}
        const unsigned int maxLifetime;

        bool operator()(const std::shared_ptr<MemorySpace>& p0,
                        const std::shared_ptr<MemorySpace>& p1);
    };

    struct MaxLifetimeMaxSizeFirst {
        MaxLifetimeMaxSizeFirst(unsigned int maxLifetime_)
            : maxLifetime(maxLifetime_) {}
        const unsigned int maxLifetime;

        bool operator()(const std::shared_ptr<MemorySpace>& p0,
                        const std::shared_ptr<MemorySpace>& p1);
    };

    struct MaxHoleMaxLifetimeFirst {
        MaxHoleMaxLifetimeFirst(unsigned int maxLifetime_, MemoryManager* inst_)
            : maxLifetime(maxLifetime_),
              inst(inst_) {}
        const unsigned int maxLifetime;
        MemoryManager* inst;

        bool operator()(const std::shared_ptr<MemorySpace>& p0,
                        const std::shared_ptr<MemorySpace>& p1);
    };

    typedef std::map<std::shared_ptr<Node>, std::vector<MemoryPlane>> MemMap_T;

public:
    MemoryManager(): mClock(0) {}
    ~MemoryManager() noexcept;

public:
    /// Generates a new MemorySpace
    std::shared_ptr<MemorySpace> reserve(unsigned int size,
                                    const std::set<std::shared_ptr<Node> >&
                          dependencies = std::set<std::shared_ptr<Node> >());
    /// Expand an existing MemorySpace, without affecting its MemoryPlane
    /// This function rebuild the memory stack mMemStack
    void expand(std::shared_ptr<MemorySpace> memSpace,
                unsigned int requiredSize);
    /// Generates a MemoryPlane in a new MemorySpace
    MemoryPlane allocate(unsigned int size,
                         const std::set<std::shared_ptr<Node> >&
                          dependencies = std::set<std::shared_ptr<Node> >(),
                         unsigned int stride = 0,
                         unsigned int length = 1,
                         unsigned int count = 1);
    /// Generates a MemoryPlane in a new MemorySpace, associated to a Node
    unsigned int allocate(const std::shared_ptr<Node>& node,
                          unsigned int size,
                          const std::set<std::shared_ptr<Node> >&
                          dependencies = std::set<std::shared_ptr<Node> >(),
                          unsigned int stride = 0,
                          unsigned int length = 1,
                          unsigned int count = 1);
    bool isWrapAround(std::shared_ptr<MemorySpace> memSpace,
                      unsigned int offset,
                      unsigned int size,
                      unsigned int stride = 0,
                      unsigned int length = 1,
                      unsigned int count = 1) const;
    /// Generate a new MemoryPlane in an existing MemorySpace
    MemoryPlane reallocate(std::shared_ptr<MemorySpace> memSpace,
                           unsigned int offset,
                           unsigned int size,
                           bool wrapAround,
                           unsigned int extraSize = 0,
                           const std::set<std::shared_ptr<Node> >&
                additionalDependencies = std::set<std::shared_ptr<Node> >(),
                           unsigned int stride = 0,
                           unsigned int length = 1,
                           unsigned int count = 1);
    /// Generate a new MemoryPlane directly following an existing MemoryPlane
    /// memPlane with an additional offset extraOffset
    MemoryPlane reallocate(const MemoryPlane& memPlane,
                           unsigned int extraOffset,
                           unsigned int size,
                           bool wrapAround,
                           unsigned int extraSize = 0,
                           const std::set<std::shared_ptr<Node> >&
                additionalDependencies = std::set<std::shared_ptr<Node> >(),
                           unsigned int stride = 0,
                           unsigned int length = 1,
                           unsigned int count = 1);
    /// Generate a new MemoryPlane in an existing MemorySpace, associated to a
    /// Node
    unsigned int reallocate(std::shared_ptr<MemorySpace> memSpace,
                            const std::shared_ptr<Node>& node,
                            unsigned int offset,
                            unsigned int size,
                            bool wrapAround,
                            unsigned int extraSize = 0,
                            const std::set<std::shared_ptr<Node> >&
                additionalDependencies = std::set<std::shared_ptr<Node> >(),
                            unsigned int stride = 0,
                            unsigned int length = 1,
                            unsigned int count = 1);
    /// Generate a new MemoryPlane directly following an existing MemoryPlane
    /// memPlane with an additional offset extraOffset
    unsigned int reallocate(const MemoryPlane& memPlane,
                            const std::shared_ptr<Node>& node,
                            unsigned int extraOffset,
                            unsigned int size,
                            bool wrapAround,
                            unsigned int extraSize = 0,
                            const std::set<std::shared_ptr<Node> >&
                additionalDependencies = std::set<std::shared_ptr<Node> >(),
                            unsigned int stride = 0,
                            unsigned int length = 1,
                            unsigned int count = 1);

    unsigned int release(std::shared_ptr<MemorySpace> memSpace);
    unsigned int release(const std::shared_ptr<Node>& node);
    unsigned int releaseDependencies(const std::shared_ptr<Node>& node);
    void optimize(OptimizeStrategy strategy);
    unsigned int getOffset(const std::shared_ptr<Node>& node,
                           unsigned int plane = 0) const;
    unsigned int getSize(const std::shared_ptr<Node>& node,
                         unsigned int plane) const;
    unsigned int getSize(const std::shared_ptr<Node>& node) const;
    unsigned int getNbPlanes(const std::shared_ptr<Node>& node) const;
    unsigned int getPeakUsage() const;
    Clock_T getMaxLifetime() const;
    const std::vector<MemoryPlane>& getPlanes(const std::shared_ptr<Node>& node)
        const;
    const MemMap_T& getPlanes() const { return mMemPlanes; }
    MemMap_T getPlanes(std::shared_ptr<MemorySpace> memSpace) const;
    unsigned int getNbPlanes(std::shared_ptr<MemorySpace> memSpace) const;
    Clock_T getCurrentTick() const { return mClock; };
    void tick();
    void log(const std::string& fileName) const;

private:
    /// Find a valid offset in the memory stack that can fit a contiguous chunk
    /// of memory of size @size
    unsigned int onStack(unsigned int size);
    unsigned int offStack(unsigned int offset);
    std::map<unsigned int, unsigned int> getStack(
        std::shared_ptr<MemorySpace> memSpace,
        Clock_T clock) const;
    std::pair<Clock_T, unsigned int> getMaxHole(
        std::shared_ptr<MemorySpace> memSpace) const;

    std::map<unsigned int, unsigned int> mMemStack;
    std::vector<std::shared_ptr<MemorySpace> > mMemSpaces;
    MemMap_T mMemPlanes;
    Clock_T mClock;
};
}

namespace {
template <>
const char* const EnumStrings<Aidge::MemoryManager::OptimizeStrategy>::data[]
    = {"None",
       "OptimizeMaxLifetimeMinSizeFirst",
       "OptimizeMaxLifetimeMaxSizeFirst",
       "OptimizeMaxHoleMaxLifetimeFirst"};
}

#endif // AIDGE_CORE_SCHEDULER_MEMORY_MANAGER_H
