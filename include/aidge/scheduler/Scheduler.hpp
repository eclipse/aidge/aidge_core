/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_SCHEDULER_SCHEDULER_H_
#define AIDGE_CORE_SCHEDULER_SCHEDULER_H_

#include <cstddef>  // std::size_t
#include <chrono>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/scheduler/MemoryManager.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
class Node;
class GraphView;


/**
 * @class Scheduler
 * @brief Generate and manage the execution schedule order of nodes in a graph.
 * It provides functionality for static scheduling, memory
 * management, and visualization of the scheduling process.
 *
 * Key features:
 * - Static scheduling generation with early and late execution times
 * - Memory layout generation for scheduled nodes
 * - Input tensor connection to graph nodes
 * - Scheduling visualization through diagram generation
 *
 * @see GraphView
 * @see Node
 * @see MemoryManager
 */
class Scheduler {
protected:
    /**
     * @struct StaticSchedulingElement
     * @brief Represents a node in the static schedule.
     */
    struct StaticSchedulingElement {
        StaticSchedulingElement(
            std::shared_ptr<Node> node_,
            std::size_t early_ = static_cast<std::size_t>(-1),
            std::size_t late_ = static_cast<std::size_t>(-1))
            : node(node_), early(early_), late(late_) {}

        std::shared_ptr<Node> node; /** Scheduled `Node` */
        std::size_t early; /** Earliest possible execution time */
        std::size_t late; /** Latest possible execution time */
        std::vector<StaticSchedulingElement*> earlierThan; /** Nodes that must be executed earlier */
        std::vector<StaticSchedulingElement*> laterThan; /** Nodes that must be executed later */
    };

    /**
     * @struct SchedulingElement
     * @brief Represent a `Node` with its actual execution times.
     * @details Start and end times are stored for later display.
     */
    struct SchedulingElement {
        SchedulingElement(
            std::shared_ptr<Node> node_,
            std::chrono::time_point<std::chrono::high_resolution_clock> start_,
            std::chrono::time_point<std::chrono::high_resolution_clock> end_)
            : node(node_), start(start_), end(end_) {}
        ~SchedulingElement() noexcept = default;
        std::shared_ptr<Node> node; /** Executed `Node` */
        std::chrono::time_point<std::chrono::high_resolution_clock> start; /** Actual start time of execution */
        std::chrono::time_point<std::chrono::high_resolution_clock> end; /** Actual end time of execution */
    };
public:
    enum class AvailableDataStatus {
        Connected,
        UpperNodeInputFound,
        UpperNodeInputConnected,
        ValidTensor,
        NotConnected
    };

    enum class EarlyLateSort {
        Default,
        AsSoonAsPossible,
        AsLateAsPossible
    };

    /**
     * @struct PriorProducersConsumers
     * @brief Manages producer-consumer relationships for nodes.
     */
    struct PriorProducersConsumers {
        PriorProducersConsumers();
        PriorProducersConsumers(const PriorProducersConsumers&);
        ~PriorProducersConsumers() noexcept;
        bool isPrior = false; /** Indicates if this Node is a prior to another Node */
        std::set<std::shared_ptr<Aidge::Node>> requiredProducers; /** Set of required producer nodes */
        std::set<std::shared_ptr<Aidge::Node>> priorConsumers; /** Set of required prior consumer nodes */
    };

public:
    Scheduler() = delete;

    /**
     * @brief Constructor for the Scheduler class.
     * @param graphView Shared pointer to the GraphView to be scheduled.
     * @param upperNode Shared pointer to the upper node of the GraphView (optional).
     */
    Scheduler(std::shared_ptr<GraphView> graphView, std::shared_ptr<Node> upperNode = nullptr)
        : mGraphView(graphView),
          mUpperNode(upperNode)
    {
        // ctor
    };

    virtual ~Scheduler();

public:
    /**
     * @brief Get the static scheduling order of nodes.
     * @param step The step of the static schedule to retrieve (default is 0).
     * @param sorting Sorting mode.
     * @return Vector of shared pointers to Nodes in their scheduled order.
     */
    std::vector<std::shared_ptr<Node>> getStaticScheduling(std::size_t step = 0, EarlyLateSort sorting = EarlyLateSort::Default) const;

    /**
     * @brief Get the GraphView associated with this Scheduler.
     * @return Shared pointer to the GraphView.
     */
    inline std::shared_ptr<GraphView> graphView() const noexcept {
        return mGraphView;
    }

    /**
     * @brief Generate full static scheduling of the GraphView.
     * For each node, an earliest and latest possible execution logical step
     * is specified. Nodes that may be scheduled at the same logical step have
     * no data dependency and can be run in parallel.
    */
    void generateScheduling();

    /**
     * Reset all scheduling and associated nodes producer consumer.
    */
    void resetScheduling();

    /**
     * Generate the memory layout for the current static scheduling.
     * @param incProducers If true, include the producers in the memory layout.
     * @param wrapAroundBuffer If true, allow wrapping in memory planes.
    */
    MemoryManager generateMemory(bool incProducers = false, bool wrapAroundBuffer = false) const;

    /**
     * Generate the memory layout for the current static scheduling, with auto-
     * concatenation: the Concat operator is replaced by direct allocation
     * when possible.
     * @param incProducers If true, include the producers in the memory layout.
     * @param wrapAroundBuffer If true, allow wrapping in memory planes.
    */
    MemoryManager generateMemoryAutoConcat(bool incProducers = false, bool wrapAroundBuffer = false) const;

    /**
     * @brief Connect input tensors to the data input of the GraphView.
     * In case of multiple data input tensors, they are mapped to producers in
     * the order given by the graph.
     *
     * @param data data input tensors
     */
    void connectInputs(const std::vector<std::shared_ptr<Aidge::Tensor>>& data);

    /**
     * @brief Save the static scheduling diagram, with early and late relative
     * order of execution for the nodes, to a file in Mermaid format.
     * @param fileName Name of the file to save the diagram (without extension).
     */
    void saveStaticSchedulingDiagram(const std::string& fileName) const;
    void saveFactorizedStaticSchedulingDiagram(const std::string& fileName, size_t minRepeat = 2) const;

    /**
     * @brief Save in a Mermaid file the order of layers execution.
     * @param fileName Name of the generated file.
     */
    void saveSchedulingDiagram(const std::string& fileName) const;


protected:
    /**
     * @brief Getter for the set of children Nodes of the given input Nodes.
     * @param producers Set of Nodes for which we want to obtain the set of children Nodes.
     * @return std::set<std::shared_ptr<Node>> Children Nodes.
     */
    std::set<std::shared_ptr<Node>> getConsumers(const std::set<std::shared_ptr<Node>>& producers) const;

    Elts_t getNbAvailableData(const std::shared_ptr<Node>& node, const IOIndex_t inputIdx, AvailableDataStatus& status) const;

    /**
     * @brief Get the prior producers and consumers for a node.
     * @param node Shared pointer to the Node.
     * @return PriorProducersConsumers object containing prior information.
     */
    PriorProducersConsumers getPriorProducersConsumers(const std::shared_ptr<Node>& node) const;

    /**
     * @brief Generate an initial base scheduling for the GraphView.
     * The scheduling is entirely sequential and guaranteed to be valid w.r.t.
     * each node producer-consumer model.
     * @return Vector of pointers to `StaticSchedulingElement` representing the base schedule.
    */
    std::vector<StaticSchedulingElement*> generateBaseScheduling() const;

    /**
     * @brief Calculates early and late execution times for each node in an initial base scheduling.
     *
     * This method performs two passes over the schedule:
     * 1. Forward pass: Calculates the earliest possible execution time for each node
     * 2. Backward pass: Calculates the latest possible execution time for each node
     *
     * It also establishes 'earlierThan' and 'laterThan' relationships between nodes.
     *
     * @param schedule Vector of shared pointers to StaticSchedulingElements to be processed
     */
    void generateEarlyLateScheduling(std::vector<StaticSchedulingElement*>& schedule) const;

    /**
     * @brief Get the factorized scheduling, by identifying repetitive sequences
     * in the scheduling.
     *
     * @param schedule Vector of shared pointers to StaticSchedulingElements to be processed
     * @param size_t Minimum number repetitions to factorize the sequence
     * @return Vector containing the repetitive sequences, in order. The second
     * element of the pair is the number of repetitions.
     */
    std::vector<std::pair<std::vector<StaticSchedulingElement*>, size_t>>
        getFactorizedScheduling(const std::vector<StaticSchedulingElement*>& schedule, size_t minRepeat = 2) const;

private:
    /**
     * @brief Summarize the consumer state of a node for debugging purposes.
     * @param consumer Shared pointer to the consumer Node.
     * @param nodeName Name of the node.
     * @details Provide the amount of data consumed and required for each input
     * and the amount of data produced for each output.
     */
    void summarizeConsumerState(const std::shared_ptr<Node>& consumer, const std::string& nodeName) const;

protected:
    /** @brief Shared pointer to the scheduled GraphView */
    std::shared_ptr<GraphView> mGraphView;
    /** @brief Weak pointer to the upper node containing the graph view */
    std::weak_ptr<Node> mUpperNode;
    /** @brief List of SchedulingElement (i.e: Nodes with their computation time) */
    std::vector<SchedulingElement> mScheduling;
    /** @brief List of nodes ordered by their */
    std::vector<std::vector<StaticSchedulingElement*>> mStaticSchedule;
    std::size_t mStaticScheduleStep = 0;
    mutable std::map<std::shared_ptr<Node>, PriorProducersConsumers> mPriorCache;
};
} // namespace Aidge

namespace Aidge {
inline auto format_as(Scheduler::AvailableDataStatus status) {
    switch (status) {
    case Scheduler::AvailableDataStatus::Connected:
        return "The input is connected to a Node.";
    case Scheduler::AvailableDataStatus::UpperNodeInputFound:
        return "The input is an upper node input, but is not connected in any GraphView.";
    case Scheduler::AvailableDataStatus::UpperNodeInputConnected:
        return "The input is an upper node input and is connected to a Node.";
    case Scheduler::AvailableDataStatus::ValidTensor:
        return "The input is not connected in the current GraphView but has a valid tensor assigned.";
    case Scheduler::AvailableDataStatus::NotConnected:
        return "The input is not connected in the current GraphView.";
    default:
        return "UNKNOWN STATUS.";
    }
}
}

#endif /* AIDGE_CORE_SCHEDULER_SCHEDULER_H_ */
