
/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_GRAPH_STATICANALYSIS_H_
#define AIDGE_CORE_GRAPH_STATICANALYSIS_H_

#include <cstddef>  // std::size_t
#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/operator/AvgPooling.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/ConvDepthWise.hpp"
#include "aidge/operator/FC.hpp"
#include "aidge/operator/MatMul.hpp"
#include "aidge/operator/MaxPooling.hpp"
#include "aidge/operator/Operator.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/ReduceMean.hpp"
#include "aidge/operator/ReduceSum.hpp"
#include "aidge/operator/Softmax.hpp"
#include "aidge/operator/MetaOperator.hpp"
#include "aidge/utils/Registrar.hpp"

namespace Aidge {
/**
 * @brief Base class to compute statistics from an Operator.
 *
 */
class OperatorStats : public Registrable<OperatorStats, std::string, std::function<std::shared_ptr<OperatorStats>(const Operator&)>> {
public:
    OperatorStats() = delete;
    OperatorStats(const Operator& op);

    virtual ~OperatorStats();

    inline const Operator& getOperator() const noexcept { return mOp; }

    /**
     * @brief Get the worst case total number of arithmetic operations for the
     * operator data flow. This includes base arithmetic operations: +, -, / and *.
     * Control flow operations (loop counters, index computation...) and memory
     * accesses are not included.
     * A naive implementation is considered (more operations might be required
     * for numerical stability in an actual implementation).
     * Example of Operator with only arithmetic operations: Conv.
     *
     * @return std::size_t Number of arithmetic operations.
     */
    virtual std::size_t getNbArithmOps() const { return 2 * getNbMACOps(); };

    /**
     * @brief Get the worst case total number of logic operations for the
     * operator data flow. This includes operations like logical shift, or, and...
     * Control flow operations (loop counters, index computation...) and memory
     * accesses are not included.
     * A naive implementation is considered (more operations might be required
     * for numerical stability in an actual implementation).
     * Example of Operator with only logic operations: BitShift.
     *
     * @return std::size_t Number of logic operations.
     */
    virtual std::size_t getNbLogicOps() const { return 0; };

    /**
     * @brief Get the worst case total number of comparison operations for the
     * operator data flow. This includes operations like <, >, =...
     * Control flow operations (loop counters, index computation...) and memory
     * accesses are not included.
     * A naive implementation is considered (more operations might be required
     * for numerical stability in an actual implementation).
     * Example of Operator with only comparison operations: MaxPool.
     *
     * @return std::size_t Number of comparison operations.
     */
    virtual std::size_t getNbCompOps() const { return 0; };

    /**
     * @brief Get the worst case total number of non-linear (NL) operations for the
     * operator data flow. This includes operations like calls to tanh(), erf(), cos()...
     * Control flow operations (loop counters, index computation...) and memory
     * accesses are not included.
     * A naive implementation is considered (more operations might be required
     * for numerical stability in an actual implementation).
     * Example of Operator with only NL operations: Tanh.
     * Non-linear operations are necessarily of floating-point type.
     *
     * @return std::size_t Number of non-linear (NL) operations.
     */
    virtual std::size_t getNbNLOps() const { return 0; };

    /**
     * @brief Get the worst case total number of operations for the operator data flow.
     * Total number of operations = arithmetic ops + logic ops + comp ops + NL ops.
     * Control flow operations (loop counters, index computation...) and memory
     * accesses are not included.
     * A naive implementation is considered (more operations might be required
     * for numerical stability in an actual implementation).
     *
     * @return std::size_t Number of operations.
     */
    std::size_t getNbOps() const { return getNbArithmOps() + getNbLogicOps() + getNbCompOps() + getNbNLOps(); };

    /**
     * @brief Get the worst case total number of INT arithmetic operations for
     * the operator data flow.
     * Such that getNbArithmOps() = getNbArithmIntOps() + getNbArithmFpOps()
     * Control flow operations (loop counters, index computation...) and memory
     * accesses are not included.
     * A naive implementation is considered (more operations might be required
     * for numerical stability in an actual implementation).
     *
     * @return std::size_t Number of INT arithmetic operations.
     */
    virtual std::size_t getNbArithmIntOps() const;

    /**
     * @brief Get the worst case total number of FP arithmetic operations for
     * the operator data flow.
     * Such that getNbArithmOps() = getNbArithmIntOps() + getNbArithmFpOps()
     * Control flow operations (loop counters, index computation...) and memory
     * accesses are not included.
     * A naive implementation is considered (more operations might be required
     * for numerical stability in an actual implementation).
     *
     * @return std::size_t Number of FP arithmetic operations.
     */
    std::size_t getNbArithmFpOps() const { return getNbArithmOps() - getNbArithmIntOps(); };

    /**
     * @brief Get the worst case total number of MAC operations for the operator
     * data flow. MAC operations are included in getNbArithmOps(), with 1 MAC
     * operation counted as 2 arithmetic operations. MAC can be INT of FP.
     * Control flow operations (loop counters, index computation...) and memory
     * accesses are not included.
     * A naive implementation is considered (more operations might be required
     * for numerical stability in an actual implementation).
     *
     * @return std::size_t Number of MAC operations.
     */
    virtual std::size_t getNbMACOps() const { return 0; };

protected:
    const Operator &mOp;
};

/**
 * @brief Base class to compute statistics from a GraphView
 *
 */
class StaticAnalysis : public std::enable_shared_from_this<StaticAnalysis> {
public:
    StaticAnalysis() = delete;
    StaticAnalysis(std::shared_ptr<GraphView> graph);

    virtual ~StaticAnalysis();

    inline const std::shared_ptr<GraphView> getGraph() const noexcept { return mGraph; }

    /**
     * @brief Get the Operator Stats object corresponding to the given node.
     *
     * @param node Node
     * @return std::shared_ptr<OperatorStats> Node's Operator stats
     */
    std::shared_ptr<OperatorStats> getOpStats(std::shared_ptr<Node> node) const;

    /**
     * @brief Get the number of parameters associated to a node. This includes
     * all Producers directly connected to the node's inputs as well as all
     * internal Producers (in case of a meta operator).
     *
     * Note: this function does not check if parameters are shared between
     * several nodes or not. This means that simply adding parameters count from
     * several nodes may lead to a higher number of parameters than in reality
     * if some of them are shared.
     *
     * @param node Node
     * @return std::size_t Number of parameters
     */
    virtual std::size_t getNbParams(std::shared_ptr<Node> node) const;

    /**
     * @brief Get the total parameters memory size, in bits, associated to a node.
     * This includes all Producers directly connected to the node's inputs as
     * well as all internal Producers (in case of a meta operator).
     *
     * Note: this function does not check if parameters are shared between
     * several nodes or not. This means that simply adding parameters size from
     * several nodes may lead to a higher parameter size than in reality
     * if some of them are shared.
     *
     * @param node Node
     * @return std::size_t Total parameters memory, in bits
     */
    virtual std::size_t getParamsSize(std::shared_ptr<Node> node) const;

    std::size_t getNbArithmOps() const;
    std::size_t getNbLogicOps() const;
    std::size_t getNbCompOps() const;
    std::size_t getNbNLOps() const;
    std::size_t getNbOps() const;
    std::size_t getNbArithmIntOps() const;
    std::size_t getNbArithmFpOps() const;
    std::size_t getNbMACOps() const;
    virtual void summary(bool incProducers = false) const;

protected:
    const std::shared_ptr<GraphView> mGraph;

    std::size_t accumulate(std::size_t (OperatorStats::*func)() const) const;
};

////////////////////////////////////////////////////////////////////////////////

class MetaOpStats : public OperatorStats {
public:
    MetaOpStats() = delete;
    MetaOpStats(const Operator& op) : OperatorStats(op) {}

    ~MetaOpStats();

    static std::unique_ptr<MetaOpStats> create(const Operator& op) {
        return std::make_unique<MetaOpStats>(op);
    }

    std::size_t getNbArithmOps() const override;
    std::size_t getNbLogicOps() const override;
    std::size_t getNbCompOps() const override;
    std::size_t getNbNLOps() const override;
    std::size_t getNbArithmIntOps() const override;
    std::size_t getNbMACOps() const override;
};

template <class OP>
class ConvStats : public OperatorStats {
public:
    ConvStats(const Operator& op) : OperatorStats(op) {}

    static std::unique_ptr<ConvStats<OP>> create(const Operator& op) {
        return std::make_unique<ConvStats<OP>>(op);
    }

    std::size_t getNbMACOps() const override {
        const OP& op_ = dynamic_cast<const OP&>(mOp);
        AIDGE_ASSERT(op_.dimsForwarded(), "Dims must be forwarded for static analysis");
	    const std::size_t weightsSize = op_.getInput(1)->size();
        const std::size_t outputSize
            = std::accumulate(op_.getOutput(0)->dims().cbegin() + 2,
                              op_.getOutput(0)->dims().cend(),
                              1,
                              std::multiplies<std::size_t>()); // NCHW...
        const std::size_t batchSize = op_.getInput(0)->dims()[0]; // NCHW
        return batchSize * (weightsSize * outputSize);
    }
};

// Beware: cannot use Conv_Op<2>::Type as key because static variable initialization order is undefined!
REGISTRAR(OperatorStats, "Conv1D", ConvStats<Conv_Op<1>>::create);
REGISTRAR(OperatorStats, "ConvDepthWise1D", ConvStats<ConvDepthWise_Op<1>>::create);
REGISTRAR(OperatorStats, "Conv2D", ConvStats<Conv_Op<2>>::create);
REGISTRAR(OperatorStats, "ConvDepthWise2D", ConvStats<ConvDepthWise_Op<2>>::create);

template <class OP>
class MaxPoolingStats : public OperatorStats {
public:
    MaxPoolingStats(const Operator& op) : OperatorStats(op) {}

    static std::unique_ptr<MaxPoolingStats<OP>> create(const Operator& op) {
        return std::make_unique<MaxPoolingStats<OP>>(op);
    }

    std::size_t getNbCompOps() const override {
        const OP& op_ = dynamic_cast<const OP&>(mOp);
        AIDGE_ASSERT(op_.dimsForwarded(), "Dims must be forwarded for static analysis");
	    const std::size_t poolSize
            = std::accumulate(op_.kernelDims().cbegin(),
                              op_.kernelDims().cend(),
                              1,
                              std::multiplies<std::size_t>());
        const std::size_t outputSize
            = std::accumulate(op_.getOutput(0)->dims().cbegin() + 2,
                              op_.getOutput(0)->dims().cend(),
                              1,
                              std::multiplies<std::size_t>()); // NCHW...
        const std::size_t batchSize = op_.getInput(0)->dims()[0]; // NCHW
        return batchSize * ((poolSize - 1) * outputSize);
    }
};

REGISTRAR(OperatorStats, "MaxPooling1D", MaxPoolingStats<MaxPooling_Op<1>>::create);
REGISTRAR(OperatorStats, "MaxPooling2D", MaxPoolingStats<MaxPooling_Op<2>>::create);
REGISTRAR(OperatorStats, "MaxPooling3D", MaxPoolingStats<MaxPooling_Op<3>>::create);

template <class OP>
class AvgPoolingStats : public OperatorStats {
public:
    AvgPoolingStats(const Operator& op) : OperatorStats(op) {}

    static std::unique_ptr<AvgPoolingStats<OP>> create(const Operator& op) {
        return std::make_unique<AvgPoolingStats<OP>>(op);
    }

    std::size_t getNbArithmOps() const override {
        const OP& op_ = dynamic_cast<const OP&>(mOp);
        AIDGE_ASSERT(op_.dimsForwarded(), "Dims must be forwarded for static analysis");
	    const std::size_t poolSize
            = std::accumulate(op_.kernelDims().cbegin(),
                              op_.kernelDims().cend(),
                              1,
                              std::multiplies<std::size_t>());
        const std::size_t outputSize
            = std::accumulate(op_.getOutput(0)->dims().cbegin() + 2,
                              op_.getOutput(0)->dims().cend(),
                              1,
                              std::multiplies<std::size_t>()); // NCHW...
        const std::size_t batchSize = op_.getInput(0)->dims()[0]; // NCHW
        // (poolSize - 1) additions + 1 division for each output
        return batchSize * (poolSize * outputSize);
    }
};

REGISTRAR(OperatorStats, "AvgPooling1D", AvgPoolingStats<AvgPooling_Op<1>>::create);
REGISTRAR(OperatorStats, "AvgPooling2D", AvgPoolingStats<AvgPooling_Op<2>>::create);
REGISTRAR(OperatorStats, "AvgPooling3D", AvgPoolingStats<AvgPooling_Op<3>>::create);
REGISTRAR(OperatorStats, "AvgPooling4D", AvgPoolingStats<AvgPooling_Op<4>>::create);

class FCStats : public OperatorStats {
public:
    FCStats(const Operator& op) : OperatorStats(op) {}

    static std::unique_ptr<FCStats> create(const Operator& op) {
        return std::make_unique<FCStats>(op);
    }

    std::size_t getNbMACOps() const override {
        const FC_Op& op_ = dynamic_cast<const FC_Op&>(mOp);
        AIDGE_ASSERT(op_.dimsForwarded(), "Dims must be forwarded for static analysis");
	    const std::size_t weightsSize = op_.getInput(1)->size();
        const std::size_t batchSize = op_.getInput(0)->dims()[0]; // NCHW
        return batchSize * weightsSize;
    }
};

REGISTRAR(OperatorStats, "FC", FCStats::create);

class MatMulStats : public OperatorStats {
public:
    MatMulStats(const Operator& op) : OperatorStats(op) {}

    static std::unique_ptr<MatMulStats> create(const Operator& op) {
        return std::make_unique<MatMulStats>(op);
    }

    std::size_t getNbMACOps() const override {
        const MatMul_Op& op_ = dynamic_cast<const MatMul_Op&>(mOp);
        AIDGE_ASSERT(op_.dimsForwarded(), "Dims must be forwarded for static analysis");
        const std::size_t n = (op_.getInput(0)->dims().size() > 1)
            ? op_.getInput(0)->dims().end()[-2] : 1;
        const std::size_t k = op_.getInput(0)->dims().back();
        const std::size_t m = (op_.getInput(1)->dims().size() > 1)
            ? op_.getInput(1)->dims().back() : 1;
        const std::size_t nb = (op_.getInput(0)->dims().size() > 2)
            ? std::accumulate(op_.getInput(0)->dims().cbegin(),
                              op_.getInput(0)->dims().cend() - 2,
                              1,
                              std::multiplies<std::size_t>())
            : 1;

        return nb * n * m * k;
    }
};

REGISTRAR(OperatorStats, "MatMul", MatMulStats::create);

class ReLUStats : public OperatorStats {
public:
    ReLUStats(const Operator& op) : OperatorStats(op) {}

    static std::unique_ptr<ReLUStats> create(const Operator& op) {
        return std::make_unique<ReLUStats>(op);
    }

    std::size_t getNbCompOps() const override {
        const OperatorTensor& op_ = dynamic_cast<const OperatorTensor&>(mOp);
        AIDGE_ASSERT(op_.dimsForwarded(), "Dims must be forwarded for static analysis");
        return op_.getOutput(0)->size();
    }
};

REGISTRAR(OperatorStats, "ReLU", ReLUStats::create);

class AbsStats : public OperatorStats {
public:
    AbsStats(const Operator& op) : OperatorStats(op) {}

    static std::unique_ptr<AbsStats> create(const Operator& op) {
        return std::make_unique<AbsStats>(op);
    }

    std::size_t getNbCompOps() const override {
        const OperatorTensor& op_ = dynamic_cast<const OperatorTensor&>(mOp);
        AIDGE_ASSERT(op_.dimsForwarded(), "Dims must be forwarded for static analysis");
        return op_.getOutput(0)->size();
    }

    // This is in the worst case (all values are negative)
    std::size_t getNbArithmOps() const override {
        const OperatorTensor& op_ = dynamic_cast<const OperatorTensor&>(mOp);
        AIDGE_ASSERT(op_.dimsForwarded(), "Dims must be forwarded for static analysis");
        return op_.getOutput(0)->size();
    }
};

REGISTRAR(OperatorStats, "Abs", AbsStats::create);

class ReduceMeanStats : public OperatorStats {
public:
    ReduceMeanStats(const Operator& op) : OperatorStats(op) {}

    static std::unique_ptr<ReduceMeanStats> create(const Operator& op) {
        return std::make_unique<ReduceMeanStats>(op);
    }

    std::size_t getNbArithmOps() const override {
        const ReduceMean_Op& op_ = dynamic_cast<const ReduceMean_Op&>(mOp);
        AIDGE_ASSERT(op_.dimsForwarded(), "Dims must be forwarded for static analysis");
        const std::size_t nbIn = op_.getInput(0)->size();
        const std::size_t nbOut = op_.getOutput(0)->size();
        const std::size_t nbReduce = nbIn / nbOut;
        // (nbReduce - 1) additions + 1 division for each output
        return nbOut * nbReduce;
    }
};

REGISTRAR(OperatorStats, "ReduceMean", ReduceMeanStats::create);

class ReduceSumStats : public OperatorStats {
public:
    ReduceSumStats(const Operator& op) : OperatorStats(op) {}

    static std::unique_ptr<ReduceSumStats> create(const Operator& op) {
        return std::make_unique<ReduceSumStats>(op);
    }

    std::size_t getNbArithmOps() const override {
        const ReduceSum_Op& op_ = dynamic_cast<const ReduceSum_Op&>(mOp);
        AIDGE_ASSERT(op_.dimsForwarded(), "Dims must be forwarded for static analysis");
        const std::size_t nbIn = op_.getInput(0)->size();
        const std::size_t nbOut = op_.getOutput(0)->size();
        const std::size_t nbReduce = nbIn / nbOut;
        // (nbReduce - 1) additions for each output
        return nbOut * (nbReduce - 1);
    }
};

REGISTRAR(OperatorStats, "ReduceSum", ReduceSumStats::create);

class SoftmaxStats : public OperatorStats {
public:
    SoftmaxStats(const Operator& op) : OperatorStats(op) {}

    static std::unique_ptr<SoftmaxStats> create(const Operator& op) {
        return std::make_unique<SoftmaxStats>(op);
    }

    std::size_t getNbArithmOps() const override {
        const Softmax_Op& op_ = dynamic_cast<const Softmax_Op&>(mOp);
        AIDGE_ASSERT(op_.dimsForwarded(), "Dims must be forwarded for static analysis");
        const std::size_t axis = (op_.axis() >= 0) ? op_.axis() : op_.getInput(0)->nbDims() + op_.axis();
        const std::size_t nbReduce = op_.getInput(0)->dims()[axis];
        const std::size_t nbOut = op_.getOutput(0)->size();
        // nbOut divisions + (nbReduce - 1) additions
        return nbOut + (nbReduce - 1);
    }

    std::size_t getNbNLOps() const override {
        const Softmax_Op& op_ = dynamic_cast<const Softmax_Op&>(mOp);
        AIDGE_ASSERT(op_.dimsForwarded(), "Dims must be forwarded for static analysis");
        const std::size_t axis = (op_.axis() >= 0) ? op_.axis() : op_.getInput(0)->nbDims() + op_.axis();
        const std::size_t nbReduce = op_.getInput(0)->dims()[axis];
        const std::size_t nbOut = op_.getOutput(0)->size();
        // nbOut exp + nbReduce exp
        return nbOut + nbReduce;
    }
};

REGISTRAR(OperatorStats, "Softmax", SoftmaxStats::create);

class MemOpStats : public OperatorStats {
public:
    MemOpStats(const Operator& op) : OperatorStats(op) {}

    static std::unique_ptr<MemOpStats> create(const Operator& op) {
        return std::make_unique<MemOpStats>(op);
    }
};

REGISTRAR(OperatorStats, "Reshape", MemOpStats::create);
REGISTRAR(OperatorStats, "Transpose", MemOpStats::create);
REGISTRAR(OperatorStats, "Concat", MemOpStats::create);
REGISTRAR(OperatorStats, "Split", MemOpStats::create);
REGISTRAR(OperatorStats, "Slice", MemOpStats::create);
REGISTRAR(OperatorStats, "Squeeze", MemOpStats::create);
REGISTRAR(OperatorStats, "Unsqueeze", MemOpStats::create);
REGISTRAR(OperatorStats, "Gather", MemOpStats::create);
REGISTRAR(OperatorStats, "Identity", MemOpStats::create);

class ElemWiseOpStats : public OperatorStats {
public:
    ElemWiseOpStats(const Operator& op) : OperatorStats(op) {}

    static std::unique_ptr<ElemWiseOpStats> create(const Operator& op) {
        return std::make_unique<ElemWiseOpStats>(op);
    }

    std::size_t getNbArithmOps() const override {
        const OperatorTensor& op_ = dynamic_cast<const OperatorTensor&>(mOp);
        AIDGE_ASSERT(op_.dimsForwarded(), "Dims must be forwarded for static analysis");
        return op_.getOutput(0)->size();
    }
};

REGISTRAR(OperatorStats, "Add", ElemWiseOpStats::create);
REGISTRAR(OperatorStats, "Sub", ElemWiseOpStats::create);
REGISTRAR(OperatorStats, "Mul", ElemWiseOpStats::create);
REGISTRAR(OperatorStats, "Div", ElemWiseOpStats::create);

class ElemWiseLogicOpStats : public OperatorStats {
public:
    ElemWiseLogicOpStats(const Operator& op) : OperatorStats(op) {}

    static std::unique_ptr<ElemWiseLogicOpStats> create(const Operator& op) {
        return std::make_unique<ElemWiseLogicOpStats>(op);
    }

    std::size_t getNbArithmOps() const override {
        const OperatorTensor& op_ = dynamic_cast<const OperatorTensor&>(mOp);
        AIDGE_ASSERT(op_.dimsForwarded(), "Dims must be forwarded for static analysis");
        return op_.getOutput(0)->size();
    }
};

REGISTRAR(OperatorStats, "And", ElemWiseLogicOpStats::create);

class ElemWiseNLOpStats : public OperatorStats {
public:
    ElemWiseNLOpStats(const Operator& op) : OperatorStats(op) {}

    static std::unique_ptr<ElemWiseNLOpStats> create(const Operator& op) {
        return std::make_unique<ElemWiseNLOpStats>(op);
    }

    std::size_t getNbNLOps() const override {
        const OperatorTensor& op_ = dynamic_cast<const OperatorTensor&>(mOp);
        AIDGE_ASSERT(op_.dimsForwarded(), "Dims must be forwarded for static analysis");
        return op_.getOutput(0)->size();
    }
};

REGISTRAR(OperatorStats, "Atan", ElemWiseNLOpStats::create);
REGISTRAR(OperatorStats, "Sqrt", ElemWiseNLOpStats::create);
REGISTRAR(OperatorStats, "Erf", ElemWiseNLOpStats::create);
REGISTRAR(OperatorStats, "Ln", ElemWiseNLOpStats::create);
REGISTRAR(OperatorStats, "Sigmoid", ElemWiseNLOpStats::create);
REGISTRAR(OperatorStats, "Tanh", ElemWiseNLOpStats::create);
REGISTRAR(OperatorStats, "Pow", ElemWiseNLOpStats::create);
}

#endif /* AIDGE_CORE_GRAPH_STATICANALYSIS_H_ */
