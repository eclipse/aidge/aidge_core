/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_PARSING_TOKEN_H_
#define AIDGE_CORE_PARSING_TOKEN_H_

#include <string>
#include <type_traits>

#include <fmt/format.h>

namespace Aidge {

template <typename EnumType>
class ParsingToken: public std::enable_shared_from_this<ParsingToken<EnumType>>
{
    static_assert(std::is_enum<EnumType>::value, "ParsingToken EnumType must be an enum type");
public:
    /**
     * @brief Token container
     * @param type one of the token type
     * @param lexeme String representing additional information of the token
     */
    ParsingToken(const EnumType type , const std::string& lexeme )
        : mLexeme(lexeme), mType(type){}

    /**
     * @brief get the lexeme
     * @return std::string
     */
    const std::string getLexeme(void) const noexcept {
        return mLexeme;
    }

    /**
     * @brief get the token type
     *
     * @return ParsingToken
     */
    const EnumType getType(void) const noexcept {
        return mType;
    }

    /**
     * @brief copy the token
     * @return deep copy of the token
     */
    std::shared_ptr<ParsingToken> copy() const noexcept {
        return std::make_shared<ParsingToken<EnumType>>(mType, mLexeme);
    }

    //TODO
    std::string rep(void) const { return fmt::format(" Token ({})\n", mLexeme); }

private:
    /**
     * @brief additional information of the token
     */
    const std::string mLexeme;

    /**
     * @brief type of the token
     * @see ConditionalTokenTypes
     */
    const EnumType mType;

};

}  // namespace Aidge

#endif //AIDGE_CORE_PARSING_TOKEN_H_
