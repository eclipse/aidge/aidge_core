/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_DATA_DATAFORMAT_H_
#define AIDGE_CORE_DATA_DATAFORMAT_H_

#include <array>
#include <cstddef>  // std::size_t

#include "aidge/utils/logger/EnumString.hpp"

namespace Aidge {

/**
 * @brief Enumeration of supported tensor data layouts
 *
 * Represents different memory layout formats for multi-dimensional tensors:
 * - N: Batch size
 * - C: Channels
 * - H: Height
 * - W: Width
 * - D: Depth (for 3D tensors)
 */
enum class DataFormat {
    Default,    ///< Default format, implementation dependent
    NCHW,      ///< 4D format: [batch][channel][height][width]
    NHWC,      ///< 4D format: [batch][height][width][channel]
    CHWN,      ///< 4D format: [channel][height][width][batch]
    NCDHW,     ///< 5D format: [batch][channel][depth][height][width]
    NDHWC,     ///< 5D format: [batch][depth][height][width][channel]
    CDHWN,     ///< 5D format: [channel][depth][height][width][batch]
    Any        ///< Unspecified format
};

using DataFormatTranspose = std::array<std::size_t, 5>;

/**
 * @brief Dictionary of transpose operations between different formats
 *
 * Contains permutation arrays to convert between different data formats.
 * @warning In this array only, dimension index starts at 1
 * (0 is reserved as default value).
 */
constexpr std::array<DataFormatTranspose, 7> DataFormatTransposeDict = {{
    {},                 // Default
    {1, 2, 3, 4},      // NCHW
    {1, 3, 4, 2},      // NHWC
    {2, 3, 4, 1},      // CHWN
    {1, 2, 3, 4, 5},   // NCDHW
    {1, 3, 4, 5, 2},   // NDHWC
    {2, 3, 4, 5, 1}    // CDHWN
}};

/**
 * @brief Get the permutation array for converting between data formats
 *
 * @param src Source data format
 * @param dst Destination data format
 * @return DataFormatTranspose Permutation array to achieve the format conversion
 */
DataFormatTranspose getDataFormatTranspose(const DataFormat& src, const DataFormat& dst);

} // namespace Aidge

namespace {
template <>
const char* const EnumStrings<Aidge::DataFormat>::data[]
    = {"Default", "NCHW", "NHWC", "CHWN", "NCDHW", "NDHWC", "CDHWN", "Any"};
}

namespace Aidge {
inline auto format_as(DataFormat df) { return EnumStrings<DataFormat>::data[static_cast<int>(df)]; }
} // namespace Aidge

#endif /* AIDGE_CORE_DATA_DATAFORMAT_H_ */
