/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_DATA_H_
#define AIDGE_DATA_H_

#include <cstddef>  // std::size_t
#include <string>

#include "aidge/utils/ErrorHandling.hpp"

namespace Aidge {

class Data {
public:
    Data() = delete;
    Data(Data&& other) = default;
    Data(const Data& other) = default;
    Data(const std::string& type): mType(type) {};

    Data& operator=(const Data& other) {
        AIDGE_ASSERT(other.mType == mType, "Cannot copy a different type fo Data object.");
        return *this;
    };
    Data& operator=(Data&& other) {
        AIDGE_ASSERT(other.mType == mType, "Cannot copy a different type fo Data object.");
        return *this;
    };
    constexpr const std::string& type() const {
        return mType;
    }
    virtual ~Data() = default;
    virtual std::string toString(int precision = -1, std::size_t offset = 0) const = 0;

private:
    const std::string mType;
};
}

#endif /* AIDGE_DATA_H_ */
