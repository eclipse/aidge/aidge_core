/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_DATA_DATATYPE_H_
#define AIDGE_CORE_DATA_DATATYPE_H_

#include <cstddef>  // std::size_t
#include <cstdint>

#include "aidge/data/half.hpp"
#include "aidge/utils/logger/EnumString.hpp"

namespace Aidge {
/**
 * @brief Enumeration of data types supported by the framework
 *
 * Represents the various data types that can be used for computation and storage.
 * This includes standard types (floating point, integers), quantized types,
 * and specialized formats for neural network operations.
 */
enum class DataType {
    // Floating point types
    Float64,    ///< 64-bit floating point (double)
    Float32,    ///< 32-bit floating point (float)
    Float16,    ///< 16-bit floating point (half)
    BFloat16,   ///< 16-bit brain floating point

    // Quantized binary and ternary types
    Binary,     ///< 1-bit binary values
    Octo_Binary,///< 8x1-bit interleaved binary
    Ternary,    ///< Ternary values (-1,0,1)

    // Quantized integer types (2-bit)
    Int2,       ///< 2-bit signed integer
    Quad_Int2,  ///< 4x2-bit interleaved signed integer
    UInt2,      ///< 2-bit unsigned integer
    Quad_UInt2, ///< 4x2-bit interleaved unsigned integer

    // Quantized integer types (3-bit)
    Int3,       ///< 3-bit signed integer
    Dual_Int3,  ///< 2x3-bit interleaved signed integer
    UInt3,      ///< 3-bit unsigned integer
    Dual_UInt3, ///< 2x3-bit interleaved unsigned integer

    // Quantized integer types (4-bit)
    Int4,       ///< 4-bit signed integer
    Dual_Int4,  ///< 2x4-bit interleaved signed integer
    UInt4,      ///< 4-bit unsigned integer
    Dual_UInt4, ///< 2x4-bit interleaved unsigned integer

    // Standard integer types
    Int5,       ///< 5-bit signed integer
    Int6,       ///< 6-bit signed integer
    Int7,       ///< 7-bit signed integer
    Int8,       ///< 8-bit signed integer
    Int16,      ///< 16-bit signed integer
    Int32,      ///< 32-bit signed integer
    Int64,      ///< 64-bit signed integer

    UInt5,      ///< 5-bit unsigned integer
    UInt6,      ///< 6-bit unsigned integer
    UInt7,      ///< 7-bit unsigned integer
    UInt8,      ///< 8-bit unsigned integer
    UInt16,     ///< 16-bit unsigned integer
    UInt32,     ///< 32-bit unsigned integer
    UInt64,     ///< 64-bit unsigned integer

    Any ///< Unspecified type
};

namespace {
// Type trait for mapping C++ types to Aidge DataType
template<typename T>
struct NativeType {
    static constexpr DataType value = DataType::Any;
};
// Helper variable template
template<typename T>
constexpr DataType NativeType_v = NativeType<T>::value;
// Specializations for native types
template<> struct NativeType<double> { static constexpr DataType value = Aidge::DataType::Float64; };
template<> struct NativeType<float> { static constexpr DataType value = Aidge::DataType::Float32; };
template<> struct NativeType<half_float::half> { static constexpr DataType value = Aidge::DataType::Float16; };
template<> struct NativeType<std::int8_t> { static constexpr DataType value = Aidge::DataType::Int8; };
template<> struct NativeType<std::int16_t> { static constexpr DataType value = Aidge::DataType::Int16; };
template<> struct NativeType<std::int32_t> { static constexpr DataType value = Aidge::DataType::Int32; };
template<> struct NativeType<std::int64_t> { static constexpr DataType value = Aidge::DataType::Int64; };
template<> struct NativeType<std::uint8_t> { static constexpr DataType value = Aidge::DataType::UInt8; };
template<> struct NativeType<std::uint16_t> { static constexpr DataType value = Aidge::DataType::UInt16; };
template<> struct NativeType<std::uint32_t> { static constexpr DataType value = Aidge::DataType::UInt32; };
template<> struct NativeType<std::uint64_t> { static constexpr DataType value = Aidge::DataType::UInt64; };


// Type trait for mapping Aidge DataType to C++ types
template <DataType D>
struct cpptype {
    using type = void; // Placeholder
};
// Helper alias template
template <DataType D>
using cpptype_t = typename cpptype<D>::type;
// Specializations for data types
template <> struct cpptype<DataType::Float16> { using type = half_float::half; };
template <> struct cpptype<DataType::Float32> { using type = float; };
template <> struct cpptype<DataType::Float64> { using type = double; };
template <> struct cpptype<DataType::Int4> { using type = std::int8_t; };
template <> struct cpptype<DataType::UInt4> { using type = std::int8_t; };
template <> struct cpptype<DataType::Int3> { using type = std::int8_t; };
template <> struct cpptype<DataType::UInt3> { using type = std::int8_t; };
template <> struct cpptype<DataType::Int2> { using type = std::int8_t; };
template <> struct cpptype<DataType::UInt2> { using type = std::int8_t; };
template <> struct cpptype<DataType::Dual_Int4> { using type = std::int8_t; };
template <> struct cpptype<DataType::Dual_UInt4> { using type = std::int8_t; };
template <> struct cpptype<DataType::Dual_Int3> { using type = std::int8_t; };
template <> struct cpptype<DataType::Dual_UInt3> { using type = std::int8_t; };
template <> struct cpptype<DataType::Quad_Int2> { using type = std::int8_t; };
template <> struct cpptype<DataType::Quad_UInt2> { using type = std::int8_t; };
template <> struct cpptype<DataType::Binary> { using type = std::int8_t; };
template <> struct cpptype<DataType::Octo_Binary> { using type = std::int8_t; };
template <> struct cpptype<DataType::Int8> { using type = std::int8_t; };
template <> struct cpptype<DataType::Int16> { using type = std::int16_t; };
template <> struct cpptype<DataType::Int32> { using type = std::int32_t; };
template <> struct cpptype<DataType::Int64> { using type = std::int64_t; };
template <> struct cpptype<DataType::UInt8> { using type = std::uint8_t; };
template <> struct cpptype<DataType::UInt16> { using type = std::uint16_t; };
template <> struct cpptype<DataType::UInt32> { using type = std::uint32_t; };
template <> struct cpptype<DataType::UInt64> { using type = std::uint64_t; };


// Type trait for mapping DataType to their interleaved variants
template<DataType D>
struct WeightInterleavedType {
    static const DataType value = DataType::Any;
};
// Helper varible template
template<DataType D>
constexpr DataType WeightInterleavedType_v =  WeightInterleavedType<D>::value;
// Specializations for interleaved types
template<> struct WeightInterleavedType<DataType::Int4> { static constexpr DataType value = DataType::Dual_Int4; };
template<> struct WeightInterleavedType<DataType::UInt4> { static constexpr DataType value = DataType::Dual_UInt4; };
template<> struct WeightInterleavedType<DataType::Int3> { static constexpr DataType value = DataType::Dual_Int3; };
template<> struct WeightInterleavedType<DataType::UInt3> { static constexpr DataType value = DataType::Dual_UInt3; };
template<> struct WeightInterleavedType<DataType::Int2> { static constexpr DataType value = DataType::Quad_Int2; };
template<> struct WeightInterleavedType<DataType::UInt2> { static constexpr DataType value = DataType::Quad_UInt2; };
template<> struct WeightInterleavedType<DataType::Binary> { static constexpr DataType value = DataType::Octo_Binary; };

}

/**
 * @brief Check if a data type is floating point
 * @param type The type to check
 * @return true if the type is floating point, false otherwise
 */
constexpr bool isFloatingPoint(const DataType& type) noexcept {
    return type == DataType::Float64 ||
           type == DataType::Float32 ||
           type == DataType::Float16 ||
           type == DataType::BFloat16;
}

std::size_t getDataTypeBitWidth(const DataType& type);

} // namespace Aidge

namespace {
template <>
const char* const EnumStrings<Aidge::DataType>::data[]
    = {"Float64", "Float32", "Float16", "BFloat16", "Binary", "Octo_Binary",
       "Ternary", "Int2", "Quad_Int2", "UInt2", "Quad_UInt2", "Int3",
       "Dual_Int3", "UInt3", "Dual_UInt3", "Int4", "Dual_Int4", "UInt4",
       "Dual_UInt4", "Int5", "Int6", "Int7", "Int8", "Int16", "Int32", "Int64",
       "UInt5", "UInt6", "UInt7", "UInt8", "UInt16", "UInt32", "UInt64", "Any"};
}

namespace Aidge {
inline auto format_as(DataType dt) {
    return EnumStrings<DataType>::data[static_cast<int>(dt)];
}
} // namespace Aidge

#endif /* AIDGE_CORE_DATA_DATATYPE_H_ */
