/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_UTILS_INTERPOLATION_H_
#define AIDGE_CORE_UTILS_INTERPOLATION_H_

#include <cstdint>  // std::int64_t
#include <utility>  // std::pair
#include <vector>

#include "aidge/operator/Pad.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
/* @brief generic class to hold interpolation */
class Interpolation {
  public:
    /**
     * @brief simple type alias to describe a coordinates
     * @note the indexes are deliberately chosen to be signed values as some
     * points retrieved by interpolation are out of bound, hence their coords
     * can be < 0
     */
    using Coords = std::vector<std::int64_t>;
    /**
     * @brief type alias to designate a point of any type : hence coordinates &
     * associated value
     */
    template <class T> using Point = std::pair<Coords, T>;

    /**
     * @brief details how coordinates are transformed from interpolated tensor
     * to original tensor
     */
    enum CoordinateTransformation {
        HalfPixel,
        HalfPixelSymmetric,
        PytorchHalfPixel,
        AlignCorners,
        Asymmetric,
    };

    /**
     * @brief apply transformation to coords in interpolated Tensor to find
     * equivalent coordinates in original tensor reference frame.
     * @warning it is assumed that all parameters have the same
     * number of dimensions.
     * @param[in] transformedCoords : coords in interpolated tensor
     * @param[in] inputDims: input dimensions of tensor
     * @param[in] inputDims: output dimensions of tensor
     * @return std::vector containing coords in original tensor reference frame
     */
    static std::vector<float> untransformCoordinates(
        const std::vector<DimSize_t> &transformedCoords,
        const std::vector<DimSize_t> &inputDims,
        const std::vector<DimSize_t> &outputDims,
        const Interpolation::CoordinateTransformation coordTransfoMode);

    /**
     * @brief retrieves neighbouring value of a given index
     * @param[in] tensorValues raw pointer of the tensor values
     * retrieved with
     * @code
     * tensor->getImpl()->rawPtr()
     * @endcode
     * @param[in] tensorDimensions dimensions of given tensor
     * retrieved with
     * @code
     * tensor->dims()
     * @endcode
     * @param[in] coords coordinates in the tensor of the values we want to
     * find the neighbours of.
     * @return static std::vector<std::pair<std::vector<DimSize_t>, T>>
     * containing both indexes of neighbours & their values
     */
    template <typename T>
    static std::set<Point<T>>
    retrieveNeighbours(const T *tensorValues,
                       const std::vector<DimSize_t> &tensorDims,
                       const std::vector<float> &coords,
                       const PadBorderType paddingMode = PadBorderType::Zero);

    /* @brief interpolation type */
    enum Mode {
        Cubic,
        Linear,
        RoundPreferFloor,
        RoundPreferCeil,
        Floor,
        Ceil
    };

    /*
     * @brief Interpolates values given via input in given mode.
     *
     * @warning This function is empty and is meant to be overridden in derived
     * class in backend libraries.
     *
     * Values are contiguously arranged in a "square" shape around the point to
     * interpolate. Depending on interpolation mode.
     * The point that will be interpolated is located right in the
     * middle of all points.
     * Immediate neighbours :
     * 1D interp :     2D interp :
     *                 . . . . . .
     * . . 1 2 . .     . . . . . .
     *                 . . 1 2 . .
     *                 . . 3 4 . .
     *                 . . . . . .
     *                 . . . . . .
     *
     * 2 neighbours :
     * 1D interp :         2D interp :
     *                   .  .  .  .  .  .  . .
     *                   .  .  .  .  .  .  . .
     * . . 1 2 3 4 . .   .  .  1  2  3  4  . .
     *                   .  .  5  6  7  8  . .
     *                   .  .  9 10 11 12  . .
     *                   .  . 13 14 15 16  . .
     *                   .  .  .  .  .  .  . .
     *                   .  .  .  .  .  .  . .
     *
     * @param[in] originalIndex: index of the point to in the original picture
     * Since the coord are being transformed from the interpolatedTensor frame
     * to originalTensor frame, the result might be in float.
     * @param[in] points : points to interpolate, arranged in a vector of a
     * pairs ((point_coord), value) :
     * [[[X1, X2, ..., XN], Xval], ...., [[A1, A2, ..., AN],Aval]].
     * With :
     * - N: the number of dimensions.
     * - A: the number of points of the grid to interpolate.
     * - All coordinates expressed in originalTensor frame.
     * @param[in] interpMode: interpolation mode
     * @return interpolated value
     */
    template <typename T>
    [[noreturn]] static T interpolate(const std::vector<float> &originalIndex,
                                      const std::vector<Point<T>> &points,
                                      const Mode interpMode);
};
} // namespace Aidge

#endif
