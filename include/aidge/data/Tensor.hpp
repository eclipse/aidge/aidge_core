/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_DATA_TENSOR_H_
#define AIDGE_CORE_DATA_TENSOR_H_

#include <algorithm>
#include <cstddef>      // std::size_t
#include <cstring>
#include <functional>   // std::multiplies
#include <set>
#include <memory>
#include <numeric>      // std::accumulate
#include <string>
#include <type_traits>  // std::is_arithmetic
#include <vector>

#include <fmt/core.h>

#include "aidge/backend/TensorImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/DataType.hpp"
#include "aidge/data/DataFormat.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
/**
 * @brief Description for the tensor data structure.
 * @details Sets the properties of the tensor without actually containing any data.
 * Contains a pointer to an actual contiguous implementation of data.
 */
class Tensor : public Data,
               public Registrable<Tensor, std::tuple<std::string, DataType>, std::function<std::shared_ptr<TensorImpl>(DeviceIdx_t device, std::vector<DimSize_t> dims)>> {
   private:
    DataType mDataType = DataType::Float32; /** enum to specify data type. */
    DataFormat mDataFormat = DataFormat::Default; /** enum to specify data format. */
    std::vector<DimSize_t> mDims; /** Dimensions of the tensor. */
    std::vector<DimSize_t> mStrides; /** Stride dimensions of the tensor. */
    std::shared_ptr<TensorImpl> mImpl = nullptr; /** Pointer to the actual data implementation. */
    std::size_t mImplOffset = 0;
    std::shared_ptr<Tensor> mGrad = nullptr; /** Pointer to the associated gradient Tensor instance. */

    // Cached data
    /// @brief Number of elements in the Tensor.
    std::size_t mSize;
    /// @brief Whether or not data are contiguous in memory.
    bool mContiguous = true;

   public:
    static constexpr const char *Type = "Tensor";

    /**
     * @brief Construct a new empty Tensor object.
     * It is considered undefined, i.e. dims can't be forwarded from such a Tensor.
     * @ref undefined() method for details
     */
    Tensor(DataType dtype = DataType::Float32, DataFormat dformat = DataFormat::Default)
        : Data(Type),
          mDataType(dtype),
          mDataFormat(dformat),
          mDims(std::vector<DimSize_t>({})),
          mStrides({1}),
          mSize(0)
    {
        // ctor
    }

    /**
     * @brief Construct a new Tensor object from an arithmetic parameter.
     *
     * @tparam T Type of the input parameter.
     * @tparam VT Decayed type of the input parameter.
     * @param val Input value.
     */
    template<typename T,
             typename VT = std::enable_if_t<std::is_arithmetic<T>::value, std::decay_t<T>>>
    Tensor(T val)
        : Data(Type),
          mDataType(NativeType_v<VT>),
          mDataFormat(DataFormat::Default),
          mDims({}),
          mStrides({1}),
          mImpl(Registrar<Tensor>::create({"cpu", NativeType_v<VT>})(0, std::vector<std::size_t>())),
          mSize(1)
    {
        *static_cast<VT*>(mImpl->rawPtr()) = static_cast<VT>(val);
    }

    /**
     * @brief Construct a new Tensor object from dimensions.
     *
     * @param dims dimensions of the tensor
     */
    Tensor(const std::vector<DimSize_t>& dims)
        : Data(Type)
    {
        // set mDims, mStrides, mContiguous, mSize
        resize(dims);
    }

    /**
     * @brief Construct a new Tensor object from the 1-dimension Vector helper.
     * @tparam T datatype
     */
    template <typename T>
    Tensor(Vector<T> &&arr)
        : Data(Type),
          mDataType(NativeType_v<T>),
          mDims({arr.data.size()}),
          mStrides({1}),
          mImpl(Registrar<Tensor>::create({"cpu", NativeType_v<T>})(0, {arr.data.size()})),
          mSize(arr.data.size())
    {
        mImpl->copyFromHost(&arr.data[0], arr.data.size());
    }

    /**
     * @brief Construct a new Tensor object from the 1-dimension Array helper.
     * @tparam T datatype
     * @tparam SIZE_0 first array dimension.
     */
    template <typename T, std::size_t SIZE_0>
    constexpr Tensor(Array1D<T, SIZE_0> &&arr)
        : Data(Type),
          mDataType(NativeType_v<T>),
          mDataFormat(DataFormat::Default),
          mDims({SIZE_0}),
          mStrides({1}),
          mImpl(Registrar<Tensor>::create({"cpu", NativeType_v<T>})(0, {SIZE_0})),
          mSize(SIZE_0)
    {
        mImpl->copyFromHost(&arr.data[0], SIZE_0);
    }

    /**
     * @brief Construct a new Tensor object from the 2-dimensions Array helper.
     * @tparam T datatype
     * @tparam SIZE_0 first array dimension.
     * @tparam SIZE_1 second array dimension.
     */
    template <typename T, std::size_t SIZE_0, std::size_t SIZE_1>
    constexpr Tensor(Array2D<T, SIZE_0, SIZE_1> &&arr)
        : Data(Type),
          mDataType(NativeType_v<T>),
          mDataFormat(DataFormat::Default),
          mDims({SIZE_0, SIZE_1}),
          mStrides({SIZE_1, 1}),
          mImpl(Registrar<Tensor>::create({"cpu", NativeType_v<T>})(0, {SIZE_0, SIZE_1})),
          mSize(SIZE_0 * SIZE_1) {
        mImpl->copyFromHost(&arr.data[0][0], SIZE_0 * SIZE_1);
    }

    /**
     * @brief Construct a new Tensor object from the 3-dimensions Array helper.
     * @tparam T datatype
     * @tparam SIZE_0 first array dimension.
     * @tparam SIZE_1 second array dimension.
     * @tparam SIZE_2 third array dimension.
     */
    template <typename T, std::size_t SIZE_0, std::size_t SIZE_1, std::size_t SIZE_2>
    constexpr Tensor(Array3D<T, SIZE_0, SIZE_1, SIZE_2> &&arr)
        : Data(Type),
          mDataType(NativeType_v<T>),
          mDataFormat(DataFormat::Default),
          mDims({SIZE_0, SIZE_1, SIZE_2}),
          mStrides({SIZE_1 * SIZE_2, SIZE_2, 1}),
          mImpl(Registrar<Tensor>::create({"cpu", NativeType_v<T>})(0, {SIZE_0, SIZE_1, SIZE_2})),
          mSize(SIZE_0 * SIZE_1 * SIZE_2) {
        mImpl->copyFromHost(&arr.data[0][0][0], SIZE_0 * SIZE_1 * SIZE_2);
    }

    /**
     * @brief Construct a new Tensor object from the 4-dimensions Array helper.
     * @tparam T datatype
     * @tparam SIZE_0 first array dimension.
     * @tparam SIZE_1 second array dimension.
     * @tparam SIZE_2 third array dimension.
     * @tparam SIZE_3 fourth array dimension.
     */
    template <typename T, std::size_t SIZE_0, std::size_t SIZE_1, std::size_t SIZE_2, std::size_t SIZE_3>
    constexpr Tensor(Array4D<T, SIZE_0, SIZE_1, SIZE_2, SIZE_3> &&arr)
        : Data(Type),
          mDataType(NativeType_v<T>),
          mDataFormat(DataFormat::Default),
          mDims({SIZE_0, SIZE_1, SIZE_2, SIZE_3}),
          mStrides({SIZE_1 * SIZE_2 * SIZE_3, SIZE_2 * SIZE_3, SIZE_3, 1}),
          mImpl(Registrar<Tensor>::create({"cpu", NativeType_v<T>})(0, {SIZE_0, SIZE_1, SIZE_2, SIZE_3})),
          mSize(SIZE_0 * SIZE_1 * SIZE_2 * SIZE_3) {
        mImpl->copyFromHost(&arr.data[0][0][0][0], SIZE_0 * SIZE_1 * SIZE_2 * SIZE_3);
    }

    /**
     * @brief Copy constructor. Construct a new Tensor object from another one
     * (shallow copy). Data memory is not copied, but shared between the new
     * Tensor and the initial one.
     * @param other
     */
    Tensor(const Tensor& other);

    /**
     * @brief Move constructor.
     * @param other
     */
    Tensor(Tensor&& other);

    /**
     * @brief Copy dimensions, datatype and data from another Tensor.
     * Tensor backend/device are also copied and only a shallow copy
     * is performed for data. Implementation will be shared with original Tensor.
     * @param other other Tensor object.
     * @return Tensor&
     */
    Tensor &operator=(const Tensor& other);
    Tensor &operator=(Tensor&& other);

    template <typename T>
    constexpr Tensor &operator=(Vector<T> &&arr) {
        *this = Tensor(std::move(arr));
        return *this;
    }

    template <typename T, std::size_t SIZE_0>
    constexpr Tensor &operator=(Array1D<T, SIZE_0> &&arr) {
        *this = Tensor(std::move(arr));
        return *this;
    }

    template <typename T, std::size_t SIZE_0, std::size_t SIZE_1>
    constexpr Tensor &operator=(Array2D<T, SIZE_0, SIZE_1> &&arr) {
        *this = Tensor(std::move(arr));
        return *this;
    }

    template <typename T, std::size_t SIZE_0, std::size_t SIZE_1, std::size_t SIZE_2>
    constexpr Tensor &operator=(Array3D<T, SIZE_0, SIZE_1, SIZE_2> &&arr) {
        *this = Tensor(std::move(arr));
        return *this;
    }

    template <typename T, std::size_t SIZE_0, std::size_t SIZE_1, std::size_t SIZE_2, std::size_t SIZE_3>
    constexpr Tensor &operator=(Array4D<T, SIZE_0, SIZE_1, SIZE_2, SIZE_3> &&arr) {
        *this = Tensor(std::move(arr));
        return *this;
    }

    /**
     * @brief Assess data type, dimensions, backend and data are the same.
     * @param otherTensor
     */
    bool operator==(const Tensor &otherTensor) const {
        if ((!mImpl && !otherTensor.mImpl) || (dataType() != otherTensor.dataType()) ||
            (dims() != otherTensor.dims()) || (mImpl->backend() != otherTensor.mImpl->backend())) {
            return false;
        }
        return *mImpl == *(otherTensor.mImpl);
    }

    /**
     * @brief Element-wise addition operation for two ``Tensor``s.
     * @note ``Tensor``s should be stored on the same backend.
     * @todo If input ``Tensor``s have a different dataType, the output should
     * have the dataType of the ``Tensor`` with the highest precision.
     *
     * @param other
     * @return Tensor
     */
    Tensor operator+(const Tensor& other) const;
    template<typename T,
             typename VT = std::enable_if_t<std::is_arithmetic<T>::value, std::decay_t<T>>>
    Tensor operator+(T val) const { return *this + Tensor(val); }
    template<typename T,
             typename VT = std::enable_if_t<std::is_arithmetic<T>::value, std::decay_t<T>>>
    friend Tensor operator+(T val, const Tensor& other) { return other + val; }

    Tensor& operator+=(const Tensor& other);
    template<typename T,
             typename VT = std::enable_if_t<std::is_arithmetic<T>::value, std::decay_t<T>>>
    Tensor& operator+=(T val) {return *this += Tensor(val); }

    /**
     * @brief Element-wise subtraction operation for two ``Tensor``s.
     * @note ``Tensor``s should be stored on the same backend.
     * @todo If input ``Tensor``s have a different dataType, the output should
     * have the dataType of the ``Tensor`` with the highest precision.
     *
     * @param other
     * @return Tensor
     */
    Tensor operator-(const Tensor& other) const;
    template<typename T,
             typename VT = std::enable_if_t<std::is_arithmetic<T>::value, std::decay_t<T>>>
    inline Tensor operator-(T val) const { return *this - Tensor(val); }
    template<typename T,
             typename VT = std::enable_if_t<std::is_arithmetic<T>::value, std::decay_t<T>>>
    friend inline Tensor operator-(T val, const Tensor& other) { return other - val; }

    Tensor& operator-=(const Tensor& other);
    template<typename T,
             typename VT = std::enable_if_t<std::is_arithmetic<T>::value, std::decay_t<T>>>
    inline Tensor& operator-=(T val) {return *this -= Tensor(val); }

    /**
     * @brief Element-wise multiplication operation for two ``Tensor``s.
     * @note ``Tensor``s should be stored on the same backend.
     * @todo If input ``Tensor``s have a different dataType, the output should
     * have the dataType of the ``Tensor`` with the highest precision.
     *
     * @param other
     * @return Tensor
     */
    Tensor operator*(const Tensor& other) const;
    template<typename T,
             typename VT = std::enable_if_t<std::is_arithmetic<T>::value, std::decay_t<T>>>
    inline Tensor operator*(T val) const { return *this * Tensor(val); }
    template<typename T,
             typename VT = std::enable_if_t<std::is_arithmetic<T>::value, std::decay_t<T>>>
    friend inline Tensor operator*(T val, const Tensor& other) { return other * val; }

    Tensor& operator*=(const Tensor& other);
    template<typename T,
             typename VT = std::enable_if_t<std::is_arithmetic<T>::value, std::decay_t<T>>>
    inline Tensor& operator*=(T val) {return *this *= Tensor(val); }

    /**
     * @brief Element-wise division operation for two ``Tensor``s.
     * @note ``Tensor``s should be stored on the same backend.
     * @todo If input ``Tensor``s have a different dataType, the output should
     * have the dataType of the ``Tensor`` with the highest precision.
     *
     * @param other
     * @return Tensor
     */
    Tensor operator/(const Tensor& other) const;
    template<typename T,
             typename VT = std::enable_if_t<std::is_arithmetic<T>::value, std::decay_t<T>>>
    inline Tensor operator/(T val) const { return *this / Tensor(val); }

    Tensor& operator/=(const Tensor& other);
    template<typename T,
             typename VT = std::enable_if_t<std::is_arithmetic<T>::value, std::decay_t<T>>>
    inline Tensor& operator/=(T val) {return *this /= Tensor(val); }

    /**
     * @brief Element-wise sqrt operation for Tensor.
     * @return Tensor
     */
    Tensor sqrt() const;

    /**
     * @brief Element-wise abs operation for Tensor.
     * @return Tensor
     */
    Tensor abs() const;

    /**
     * @brief Mean operation for Tensor.
     * @return Tensor
     */
    Tensor mean() const;

    ~Tensor() noexcept;

public:
    /**
     * @brief Perform a deep copy of the tensor.
    */
    Tensor clone() const {
        Tensor newTensor(*this); // shallow copy
        // handle deepcopy of implementation if any
        if (newTensor.hasImpl()) {
            if (!newTensor.isContiguous()) {
                newTensor.makeContiguous();
            }
            else {
                std::shared_ptr<TensorImpl> newImpl = Registrar<Tensor>::create({mImpl->backend(), mDataType})(mImpl->device().second, mDims);
                newImpl->copy(mImpl->rawPtr(mImplOffset), mSize);
                newTensor.setImpl(newImpl);
            }
        }
        return newTensor;
    }

    const std::string backend() const {
        return hasImpl() ? getImpl()->backend() : "";
    }


    /**
     * @brief Get the device index.
     * @return DeviceIdx_t
     */
    DeviceIdx_t device() const noexcept { return mImpl ? mImpl->device().second : static_cast<DeviceIdx_t>(0); }

    /**
     * @brief Set the backend of the Tensor associated implementation. If there
     * was no previous implementation set, data will be allocated, but it will
     * not be initialized to any particular value.
     * If data was already initialized in a previous backend, it will be moved
     * to the new one except if copyFrom is false.
     * @param name Backend name
     * @param device Backend device
     * @param copyFrom If true (default), move data from previous backend/device
     * to the new one. Previous data is lost otherwise.
     */
    void setBackend(const std::string &name, DeviceIdx_t device = 0, bool copyFrom = true);

    /**
     * @brief Get a list of available backends.
     * @return std::set<std::string>
     */
    static std::set<std::string> getAvailableBackends();

    /**
     * @brief Get the data type enum.
     * @return constexpr DataType
     */
    constexpr DataType dataType() const noexcept { return mDataType; }

    /**
     * @brief Get the data format enum.
     * @return constexpr DataFormat
     */
    constexpr DataFormat dataFormat() const noexcept { return mDataFormat; }

    /**
     * @brief Set the DataType of the Tensor and converts data
     * if the Tensor has already been initialized and copyCast is true.
     * @param dt DataType
     * @param copyCast If true (default), previous data is copy-casted. Otherwise
     * previous data is lost.
     */
    void setDataType(const DataType dt, bool copyCast = true) {
        if (mImpl && (dataType() != dt)) {
            std::shared_ptr<TensorImpl> newImpl = Registrar<Tensor>::create({mImpl->backend(), dt})(mImpl->device().second, mDims);
            if (copyCast) {
                newImpl->copyCast(mImpl->rawPtr(mImplOffset), mDataType, mImpl->size());
            }
            setImpl(newImpl);
        }
        mDataType = dt;
    }

    /**
     * @brief Set the DataFormat of the Tensor and transpose data, only
     * if the Tensor has already been initialized and copyTrans is true.
     * In this case, a transposition occurs only if both previous format and
     * new format are different from DataFormat::Default.
     * @param df New DataFormat
     * @param copyTrans If true (default), when both previous format and new
     *                  format are different from DataFormat::Default, previous
     *                  data is copy-transposed.
     */
    void setDataFormat(const DataFormat df, bool copyTrans = true) {
        if (mImpl && copyTrans && (dataFormat() != df) && df != DataFormat::Default && dataFormat() != DataFormat::Default) {
            copyTranspose(*this, getDataFormatTranspose(dataFormat(), df));
        }
        mDataFormat = df;
    }

    /**
     * @brief Get the Impl object
     * @return constexpr const std::shared_ptr<TensorImpl>&
     */
    constexpr const std::shared_ptr<TensorImpl>& getImpl() const noexcept { return mImpl; }
    constexpr std::size_t getImplOffset() const noexcept { return mImplOffset; }

    /**
     * @brief Set the Impl object
     *
     * @param impl New impl shared pointer
     * @param implOffset Storage offset in this new impl for this Tensor
     */
    void setImpl(std::shared_ptr<TensorImpl> impl, std::size_t implOffset = 0) {
        mImpl = impl;
        mImplOffset = implOffset;
    }

    /**
     * @brief Return if an implementation has been associated.
     * @return true
     * @return false
     */
    bool hasImpl() const noexcept { return mImpl ? true : false; }

    /**
     * @brief Get number of dimensions of the Tensor.
     * @return std::size_t
     */
    inline std::size_t nbDims() const { return mDims.size(); }

    /**
     * @brief Get dimensions of the Tensor object.
     * @tparam DIM number of dimensions.
     * @return constexpr std::array<DimSize_t, DIM>
     */
    template <DimIdx_t DIM>
    constexpr std::array<DimSize_t, DIM> dims() const {
        assert(DIM == mDims.size() && "wrong number of dimensions");
        return to_array<DIM>(mDims.cbegin());
    }

    /**
     * @brief Get dimensions of the Tensor object.
     * @return constexpr const std::vector<DimSize_t>&
     */
    constexpr inline const std::vector<DimSize_t>& dims() const noexcept { return mDims; }

    inline DimSize_t dim(DimIdx_t idx) const { return mDims[idx]; }

    /**
     * @brief Get strides of the Tensor object.
     * @return constexpr const std::vector<DimSize_t>&
     */
    constexpr inline const std::vector<DimSize_t>& strides() const noexcept { return mStrides; }

    inline DimSize_t stride(DimIdx_t idx) const { return mStrides[idx]; }

    /**
     * @brief Return true if Tensor is contiguous in memory.
     * @return bool
     */
    constexpr bool isContiguous() const noexcept { return mContiguous; }

    /**
     * @brief Get the number of elements in the Tensor object.
     * @return constexpr std::size_t
     */
    constexpr std::size_t size() const noexcept { return mSize; }

    /**
     * @brief Return the current capacity of the tensor, i.e. the actual memory
     * currently being allocated. It can be different from the size:
     * - Capacity conservatively returns 0 is no implementation is provided.
     * - Capacity can be 0 if the tensor memory was not yet initialized (because
     *   of lazy initialization, memory is allocated only when it needs to be
     *   accessed the first time).
     * - Capacity can be > size if the tensor was downsized but memory was not
     *   reallocated.
    */
    inline std::size_t capacity() const noexcept { return mImpl ? mImpl->capacity(): 0; }


    /**
     * @brief Change the dimensions of the Tensor object according to the given argument.
     * If the overall size is not changed (meaning we actually only performed a
     * reshape), data is guaranteed to remain valid.
     * Otherwise, no guarantee is provided regarding the validy of previous data
     * (unlike std::vector). If the new overall size is larger than the previous
     * one, all previous data is invalided. Otherwise, previous data may or may
     * not remain valid, depending on the backend implementation.
     * @tparam DIM Number of dimensions.
     * @param dims New dimensions
     */
    template <std::array<DimSize_t, 1>::size_type DIM> // deducing std::array size_type and declaring DIM accordingly
    inline void resize(const std::array<DimSize_t, DIM> &dims) {
        resize(std::vector<DimSize_t>(dims.begin(), dims.end()));
    }

    /**
     * @brief Change the dimensions of the Tensor object according to the given argument.
     * If the overall size is not changed (meaning we actually only performed a
     * reshape), data is guaranteed to remain valid.
     * Otherwise, no guarantee is provided regarding the validy of previous data
     * (unlike std::vector). If the new overall size is larger than the previous
     * one, all previous data is invalided. Otherwise, previous data may or may
     * not remain valid, depending on the backend implementation.
     * @param dims New dimensions
     * @param strides Stride of the tensor (if not specified, "nested" stride is used)
     */
    void resize(const std::vector<DimSize_t> &dims, std::vector<DimSize_t> strides = std::vector<DimSize_t>());

    /**
     * @brief Return whether the Tensor object as a rank of 0, i.e. dimensions == {}.
     * For defined Tensors, this implies that the Tensor is scalar.
     * For backward compatibility reasons, it is valid to call this predicate
     * even on undefined Tensors, in which case it returns true.
     * Hence before test the rank with this method, always check that the
     * Tensor is not undefined().
     * In particular for operations such as forwardDims(), one should always
     * use undefined() to test whether the Tensor dimensions have been defined.
     * In this case empty() can be used to distinguish scalars from N-D Tensors.
     * @return true if rank is 0 or the tensor is undefined
     */
    bool empty() const { return mDims.empty(); }

     /**
     * @brief Returns whether the Tensor object is undefined.
     * An undefined Tensor is equivalent to a tensor for which dimensions have not
     * been defined yet. Hence, dimensions forwarding can't be done from undefined tensors.
     * The only cases where a tensor is undefined is after the default constructor
     * and before any call to resize().
     * Also, as soon as the resize() method has been called, the Tensor is irreversibly defined.
     * @ref empty() method for distinguishing an undefined from a scalar
     * @return true if undefined
     */
    bool undefined() const { return mSize == 0; }

    /**
     * @brief Set each element of the tensor to zero.
     */
    void zeros() const {
        if (mImpl) {
            mImpl->zeros();
        }
    }

    template <typename expectedType>
    const expectedType& get(std::size_t idx) const {
        AIDGE_ASSERT(NativeType_v<expectedType> == mDataType, "Tensor::get<>({}): wrong data type, expected {}, got {}", idx, mDataType, NativeType_v<expectedType>);
        AIDGE_ASSERT(mImpl->hostPtr() != nullptr, "Tensor::get<>({}): can only be used for backends providing a valid host pointer.", idx);
        AIDGE_ASSERT(idx < mSize, "Tensor::get<>({}): idx {} out of range, tensor size {}", idx, mSize);
        return *reinterpret_cast<expectedType *>(mImpl->hostPtr(mImplOffset + idx));
    }

    template <typename expectedType>
    const expectedType& get(std::vector<std::size_t> coordIdx) const {
        return get<expectedType>(getStorageIdx(coordIdx));
    }

    template <typename expectedType>
    void set(std::size_t idx, expectedType value){
        AIDGE_ASSERT(NativeType_v<expectedType> == mDataType, "wrong data type");
        AIDGE_ASSERT(mImpl->hostPtr() != nullptr, "get() can only be used for backends providing a valid host pointer");
        AIDGE_ASSERT(idx < mSize, "idx out of range");
        expectedType* dataPtr = static_cast<expectedType*>(mImpl->hostPtr(mImplOffset + idx));
        *dataPtr = value;
    }

    template <typename expectedType>
    void set(std::vector<std::size_t> coordIdx, expectedType value){
        set<expectedType>(getStorageIdx(coordIdx), value);
    }

    std::string toString(int precision = -1, std::size_t offset = 0) const override;

    inline void print() const { fmt::print("{}\n", toString()); }

    /**
     * @brief Get the gradient Tensor. If not initialized, set a Tensor instance
     * and set its implementation if none was previously set.
     * @note Dimensions for the Tensor instance are copied from the original current Tensor.
     * @note If a Tensor instance was already associated, only the implementation is created
     * with values set to 0.
     * @note If Tensor instance and implementation already existed for the gradient
     * nothing is done.
     */
    std::shared_ptr<Tensor> grad() {
        if (!mGrad) {
            mGrad = std::make_shared<Tensor>(mDims);
        }
        if (!mGrad->hasImpl()) {
            mGrad->setDataType(dataType());
            mGrad->setDataFormat(dataFormat());
            mGrad->setBackend(hasImpl() ? mImpl->backend() : "cpu");
            mGrad->zeros();
        }
        return mGrad;
    }

    void setGrad(std::shared_ptr<Tensor> newGrad) {
        mGrad = newGrad;
    }

    /**
     * @brief From the the 1D contiguous index, return the coordinate of an element in the tensor.
     * Beware: do not use this function with the storage index!
     *
     * @param index 1D contiguous index of the value considering a flatten, contiguous, tensor.
     * @return std::vector<DimSize_t>
     */
    static std::vector<std::size_t>
    toCoord(const std::vector<Aidge::DimSize_t> &dimensions, std::size_t index);


    /**
     * @brief From the the 1D contiguous index, return the coordinate of an element in the tensor.
     * Beware: do not use this function with the storage index!
     *
     * @param flatIdx 1D contiguous index of the value considering a flatten, contiguous, tensor.
     * @return std::vector<DimSize_t>
     */
    std::vector<std::size_t> getCoord(std::size_t index) const {
        if (isInBounds(mDims, index)) {
            return toCoord(mDims, index);
        } else {
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Out of bound coordinates.");
        }
    }

    /**
     * @brief From the coordinate returns the 1D contiguous index of an element in the tensor.
     * If the number of coordinates is inferior to the number of dimensions,
     * the remaining coordinates are assumed to be 0.
     * Beware: the contiguous index will only correspond to the storage index
     * if the tensor is contiguous!
     * Note that the coordIdx may be an empty vector.
     *
     * @param coords Coordinate to an element in the tensor
     * @return DimSize_t Contiguous index
     */
    static std::size_t toIndex(const std::vector<DimSize_t>& dimensions, const std::vector<std::size_t>& coords);

    /**
     * @brief From the coordinate returns the 1D contiguous index of an element in the tensor.
     * If the number of coordinates is inferior to the number of dimensions,
     * the remaining coordinates are assumed to be 0.
     * Beware: the contiguous index will only correspond to the storage index
     * if the tensor is contiguous!
     * Note that the coordIdx may be an empty vector.
     *
     * @param coordIdx Coordinate to an element in the tensor
     * @return DimSize_t Contiguous index
     */
    std::size_t getIdx(const std::vector<std::size_t>& coords) const {
        if (isInBounds<std::size_t>(mDims, coords)) {
            return toIndex(mDims, coords);
        } else {
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Out of bound coordinates.");
        }
    }

    /**
     * @brief check if index is in bound of given tensor dimensions
     * @warning this function is templated in order to welcome cases like interpolation where indexes are not integers.
     * However, the only types accepted are floating, integer & size_t
     * @param tensorDims : tensor dimensions
     * @param coords : coords of the tensor you want to flattened index of
     * @return true if all coords are in bound. False otherwise
     */
    template<typename T>
    static bool isInBounds(const std::vector<DimSize_t>& dimensions, const std::vector<T>& coords);

    static bool isInBounds(const std::vector<DimSize_t>& dimensions, const std::size_t index);

    /**
     * @brief From the coordinate returns the 1D storage index of an element in the tensor.
     * If the number of coordinates is inferior to the number of dimensions,
     * the remaining coordinates are assumed to be 0.
     *
     * @param coordIdx Coordinate to an element in the tensor
     * @return DimSize_t Storage index
     */
    std::size_t getStorageIdx(const std::vector<std::size_t>& coordIdx) const {
        AIDGE_ASSERT(coordIdx.size() <= mDims.size(), "Coordinates does not match number of dimensions");
        for(std::size_t i = 0; i < coordIdx.size(); ++i) {
            AIDGE_ASSERT(coordIdx[i] < mDims[i], "Coordinates dimensions does not fit the dimensions of the tensor");
        }
        return std::inner_product(coordIdx.cbegin(), coordIdx.cend(), mStrides.cbegin(), DimSize_t(0));
    }

    /**
     * @brief Returns a sub-tensor with equal or lower number of dimensions.
     *
     * @note For instance, ``t.extract({1})`` on a CHW tensor will return the HW tensor
     * of channel #1.
     * Likewise, ``t.extract({0, 1})`` on a NCHW tensor will return the HW tensor
     * of batch #0 and channel #1.
     * @note No memory copy is performed, the returned tensor does not own the memory.
     * @note If the number of coordinates matches the number of dimensions, a scalar
     * tensor is returned.
     * @note If current tensor was contiguous, the returned tensor is guaranteed to be
     * contiguous as well.
     *
     * @param coordIdx Coordinates of the sub-tensor to extract
     * @return Tensor Sub-tensor.
    */
    Tensor extract(const std::vector<std::size_t>& coordIdx) const;

    /**
     * @brief Returns a sub-tensor at some coordinate and with some dimension.
     *
     * @note Data contiguity of the returned Tensor is not guaranteed.
     *
     * @param coordIdx First coordinates of the sub-tensor to extract
     * @param dims Dimensions of the sub-tensor to extract
     * @return Tensor Sub-tensor.
    */
    Tensor extract(const std::vector<std::size_t>& coordIdx, const std::vector<std::size_t>& dims) const;

    /**
     * @brief Make the tensor's storage contiguous, if it is not already the case.
     * If not contiguous, a new memory space is allocated.
    */
    void makeContiguous();

    /**
     * Copy-cast data from a Tensor on the same device.
     * If current tensor backend/device is set and is different from src, an
     * assertion is raised.
     * @param src Source tensor to copy-cast from.
    */
    void copyCast(const Tensor& src);

    /**
     * Copy data from a Tensor from another backend/device.
     * If current tensor data type is set and is different from src, an
     * assertion is raised.
     * @param src Source tensor to copy from.
    */
    void copyFrom(const Tensor& src);

    /**
     * Transpose data from another Tensor (which can be itself).
     * @param src Source tensor to copy from.
    */
    void copyTranspose(const Tensor& src, const std::vector<DimSize_t>& transpose);
    void copyTranspose(const Tensor& src, const DataFormatTranspose& transpose);

    /**
     * Copy-cast data from a Tensor.
     * @param src Source tensor to copy-cast from.
     * @param movedSrc shared_ptr to an indermediate Tensor that will
     * contain the moved data if a device change should occur AND a type
     * conversion is necessary (otherwise it remains unused).
     * Any data already present will be overwritten. No new memory allocation
     * will occur if movedSrc has already been allocated with the right
     * type/size/device.
     * If required, memory is always allocated on current (destination)
     * Tensor's device.
    */
    void copyCastFrom(const Tensor& src, std::shared_ptr<Tensor>& movedSrc);

    /**
     * Copy-cast data from a Tensor.
     * In case of both a device change AND a data type conversion, an
     * intermediate buffer on will be allocated and deallocated each time.
     * If required, buffer's memory is always allocated on current (destination)
     * Tensor's device.
     * @param src Source tensor to copy-cast from.
    */
    void copyCastFrom(const Tensor& src) {
        // Internal buffer will be allocated and deallocated at each call
        // (only if needed)
        std::shared_ptr<Tensor> movedSrc;
        copyCastFrom(src, movedSrc);
    }

    /**
     * Return a reference to a Tensor that is guaranteed to be contiguous:
     * - itself, if already contiguous;
     * - the provided Tensor, overwritten with the copied data.
     * The data type, backend and device stay the same.
     * @param fallback A shared_ptr to Tensor ready to be overwritten if necessary.
     * The shared_ptr does not need to be initialized. No new memory allocation
     * will occur if fallback has already been allocated with the right
     * type/size/device.
     * @return Reference to either itself or to fallback.
    */
    Tensor& refContiguous(std::shared_ptr<Tensor>& fallback);
    const Tensor& refContiguous(std::shared_ptr<Tensor>& fallback) const;

    /**
     * Return a reference to a Tensor casted to the desired data type:
     * - itself, if already at the right data type;
     * - the provided Tensor, overwritten with the copy-casted data.
     * The backend stays the same.
     * @param fallback A shared_ptr to Tensor ready to be overwritten if necessary.
     * The shared_ptr does not need to be initialized. No new memory allocation
     * will occur if fallback has already been allocated with the right
     * type/size/device.
     * @param dt The desired data type.
     * @return Reference to either itself or to fallback.
    */
    Tensor& refCast(std::shared_ptr<Tensor>& fallback, const Aidge::DataType& dt);
    const Tensor& refCast(std::shared_ptr<Tensor>& fallback, const Aidge::DataType& dt) const;

    /**
     * Return a reference to a Tensor on the desired backend/device:
     * - itself, if already on the right device;
     * - the provided Tensor, overwritten with the copied data.
     * The data type stays the same.
     * @param fallback A shared_ptr to Tensor ready to be overwritten if necessary.
     * The shared_ptr does not need to be initialized. No new memory allocation
     * will occur if fallback has already been allocated with the right
     * type/size/device.
     * @param backend The desired backend.
     * @param device The desired device.
     * @return Reference to either itself or to fallback.
    */
    Tensor& refFrom(std::shared_ptr<Tensor>& fallback, const std::string &backend, DeviceIdx_t device = 0);
    const Tensor& refFrom(std::shared_ptr<Tensor>& fallback, const std::string &backend, DeviceIdx_t device = 0) const;

    /**
     * Return a reference to a Tensor on desired data type and backend/device:
     * - itself, if already with the right characteristics;
     * - the provided Tensor, overwritten with the copy-casted data.
     * If required, fallback is always allocated on desired (destination)
     * device.
     * @param fallback A shared_ptr to Tensor ready to be overwritten if necessary.
     * The shared_ptr does not need to be initialized. No new memory allocation
     * will occur if fallback has already been allocated with the right
     * type/size/device.
     * @param dt The desired data type.
     * @param backend The desired backend.
     * @param device The desired device.
     * @return Reference to either itself or to fallback.
    */
    Tensor& refCastFrom(std::shared_ptr<Tensor>& fallback, const Aidge::DataType& dt, const std::string &backend, DeviceIdx_t device = 0) {
        // First refFrom, to ensure that fallback, if required, is also on desired device
        return refFrom(fallback, backend, device).refCast(fallback, dt);
    }

    /**
     * Return a reference to a Tensor with same characteristics
     * (data type, backend/device) as targetReqs Tensor:
     * - itself, if already with the right characteristics;
     * - the provided Tensor, overwritten with the copy-casted data.
     * If required, fallback is always allocated on current (destination)
     * Tensor's device.
     * @param fallback A shared_ptr to Tensor ready to be overwritten if necessary.
     * The shared_ptr does not need to be initialized. No new memory allocation
     * will occur if fallback has already been allocated with the right
     * type/size/device.
     * @param targetReqs Tensor with the desired target characteristics.
     * @return Reference to either itself or to fallback.
    */
    Tensor& refCastFrom(std::shared_ptr<Tensor>& fallback, const Tensor& targetReqs) {
        const auto& device = targetReqs.getImpl()->device();
        return refCastFrom(fallback, targetReqs.dataType(), device.first, device.second);
    }

    /**
     * @brief Return a reference to a Tensor on desired data type and backend/device:
     * - itself, if already with the right characteristics;
     * - the provided Tensor, overwritten with the right characteristics.
     * @note no data is copy-casted. If it was so in a previous refCastFrom() on
     * the same fallback, it remains valid, otherwise, data is invalid.
     * @param fallback A shared_ptr to Tensor ready to be overwritten if necessary.
     * The shared_ptr does not need to be initialized. No new memory allocation
     * will occur if fallback has already been allocated with the right
     * type/size/device.
     * @param dt The desired data type.
     * @param backend The desired backend.
     * @param device The desired device.
     * @return Reference to either itself or to fallback.
    */
    Tensor& ref(std::shared_ptr<Tensor>& fallback, const Aidge::DataType& dt, const std::string &backend, DeviceIdx_t device = 0);
    const Tensor& ref(std::shared_ptr<Tensor>& fallback, const Aidge::DataType& dt, const std::string &backend, DeviceIdx_t device = 0) const;

    /**
     * @brief Return a reference to a Tensor with same characteristics
     * (data type, backend/device) as targetReqs Tensor:
     * - itself, if already with the right characteristics;
     * - the provided Tensor, overwritten with the right characteristics.
     * @note no data is copy-casted. If it was so in a previous refCastFrom() on
     * the same fallback, it remains valid, otherwise, data is invalid.
     * @param fallback A shared_ptr to Tensor ready to be overwritten if necessary.
     * The shared_ptr does not need to be initialized. No new memory allocation
     * will occur if fallback has already been allocated with the right
     * type/size/device.
     * @param targetReqs Tensor with the desired target characteristics.
     * @return Reference to either itself or to fallback.
    */
    Tensor& ref(std::shared_ptr<Tensor>& fallback, const Tensor& targetReqs) {
        const auto& device = targetReqs.getImpl()->device();
        return ref(fallback, targetReqs.dataType(), device.first, device.second);
    }

private:
    /**
     * @brief Compute the number of elements in the Tensor.
     * @note If dimensions are not empty, they are multiplied to get the total number
     * of elements. Else, the Tensor represents a scalar and contains a single element.
     */
    void computeSize() {
        mSize = std::accumulate(mDims.begin(), mDims.end(), DimSize_t(1), std::multiplies<DimSize_t>());
    }
};
}  // namespace Aidge

template<>
struct fmt::formatter<Aidge::Tensor> {
    // Only stores override precision from format string
    int precision_override = -1;

    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        auto it = ctx.begin();
        if (it != ctx.end() && *it == '.') {
            ++it;
            if (it != ctx.end() && *it >= '0' && *it <= '9') {
                precision_override = 0;
                do {
                    precision_override = precision_override * 10 + (*it - '0');
                    ++it;
                } while (it != ctx.end() && *it >= '0' && *it <= '9');
            }
        }

        if (it != ctx.end() && *it == 'f') {
            ++it;
        }

        return it;
    }

    template<typename FormatContext>
    auto format(Aidge::Tensor const& t, FormatContext& ctx) const {
        // Use precision_override if specified, otherwise toString will use default
        return fmt::format_to(ctx.out(), "Tensor({})", t.toString(precision_override, 7));
    }
};

#endif /* AIDGE_CORE_DATA_TENSOR_H_ */
