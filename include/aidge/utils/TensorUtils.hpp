/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_UTILS_TENSOR_UTILS_H_
#define AIDGE_CORE_UTILS_TENSOR_UTILS_H_

#include <cmath>  // std::abs

#include "aidge/data/DataType.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Log.hpp"

namespace Aidge {

/**
 * @brief Compare two Aidge::Tensor value wise. The comparison function is:
 *
 * |t1-t2| <= absolute + relative * |t2|
 *
 * If a tensor value is different from the other tensor return False
 * If the tensor does not have the same size, return False
 * If the datatype is not the same between each tensor return False
 * If the templated type does not correspond to the datatype of each tensor, raise an assertion error
 *
 * @tparam T1 should correspond to the type of the first tensor, defines part of the type for absolute and relative error
 * @tparam T2 should correspond to the type of the second tensor, defaults to T1
 * @param t1 first Aidge::Tensor to test
 * @param t2 second Aidge::Tensor to test
 * @param relative relative difference allowed (should be between 0 and 1)
 * @param absolute absolute error allowed (should be positive)
 * @return true if both tensors are approximately equal and have the same datatype and shape. Else return false
 */
template <typename T1, typename T2 = T1>
bool approxEq(const Tensor& t1, const Tensor& t2, float relative = 1e-5f, float absolute = 1e-8f) {
    // Check template type matches tensor datatype
    if (t1.dataType() != NativeType_v<T1>) {
        Log::error("First tensor datatype ({}) does not match template type", t1.dataType());
        return false;
    }

    if (t2.dataType() != NativeType_v<T2>) {
        Log::error("Second tensor datatype ({}) does not match template type", t2.dataType());
        return false;
    }

    // Validate parameters
    if (relative < 0.0f) {
        Log::error("Relative error must be non-negative (got {})", relative);
        return false;
    }

    if (absolute < 0.0f || absolute > 1.0f) {
        Log::error("Absolute error must be between 0 and 1 (got {})", absolute);
        return false;
    }

    // Check tensor sizes match
    if (t1.size() != t2.size()) {
        Log::error("Tensor sizes do not match: {} vs {}", t1.size(), t2.size());
        return false;
    }

    // Compare values/
    for (size_t i = 0; i < t1.size(); ++i) {
        const auto val1 = t1.get<T1>(i);
        const auto val2 = t2.get<T2>(i);
        const float diff = static_cast<float>(std::abs(val1 - val2));
        const float threshold = absolute + (relative * static_cast<float>(std::abs(val2)));

        if (diff > threshold) {
            Log::notice("Tensor values differ at index {}: {} vs {} (diff: {}, threshold: {})\n"
                "Tensor 1:\n{}\nTensor 2:\n{}",
                i, val1, val2, diff, threshold, t1, t2);
            return false;
        }
    }

    return true;
}

} // namespace Aidge

#endif // AIDGE_CORE_UTILS_TENSOR_UTILS_H_
