/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_UTILS_DYNAMICATTRIBUTES_H_
#define AIDGE_CORE_UTILS_DYNAMICATTRIBUTES_H_

#include <map>
#include <vector>
#include <type_traits>
#include <typeinfo>
#include <cassert>
#include <string>
#include <typeindex>

#include "aidge/utils/future_std/any.hpp"
#include "aidge/utils/Attributes.hpp"
#include "aidge/utils/ErrorHandling.hpp"

#ifdef PYBIND
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/embed.h>

namespace py = pybind11;
#endif


namespace Aidge {
#if __cplusplus >= 201703L || (defined(_MSVC_LANG) && _MSVC_LANG >= 201703L)
#define AIDGE_DYNATTR_HAVE_CPP17
#endif

#if defined(AIDGE_DYNATTR_HAVE_CPP17) || defined(__cpp_lib_void_t)
using std::void_t;
#else
template <typename...>
using void_t = void;
#endif

// Detection idiom to check if a type T has a less-than operator
template <typename T, typename = void>
struct has_less_than_operator : std::false_type {};

template <typename T>
struct has_less_than_operator<T, void_t<decltype(std::declval<T>() < std::declval<T>())>> : std::true_type {};

///\todo store also a fix-sized code that indicates the type
///\todo managing complex types or excluding non-trivial, non-aggregate types
class DynamicAttributes : public Attributes {
public:
    DynamicAttributes() = default;
    DynamicAttributes(const std::map<std::string, future_std::any>& attrs): mAttrs(attrs) {}
    DynamicAttributes& operator=(const std::map<std::string, future_std::any>& attrs) {
        mAttrs = attrs;
        return *this;
    }

    /**
     * \brief Returning an Attribute identified by its name
     * \tparam T expected Attribute type
     * \param name Attribute name
     * \details assert if T is not the actual Attribute type or if the Attribute does not
     *  exist
     * \note at() throws if the Attribute does not exist, using find to test for Attribute existence
     */
    template<class T> T getAttr(const std::string& name) const
    {
        const auto dot = name.find('.');
        if (dot == name.npos) {
            mAnyUtils.emplace(typeid(T), std::unique_ptr<AnyUtils<T>>(new AnyUtils<T>()));

            const auto& attr = mAttrs.at(name);
#ifdef PYBIND
            if (attr.type() == typeid(py::object)) {
                // Note: because of cast<T>(), this function cannot return a const reference!
                return future_std::any_cast<const py::object&>(attr).cast<T>();
            }
            else
#endif
            {
                return future_std::any_cast<const T&>(attr);
            }
        }
        else {
            const auto ns = name.substr(0, dot);
            const auto nsName = name.substr(dot + 1);
            return future_std::any_cast<const DynamicAttributes&>(mAttrs.at(ns)).getAttr<T>(nsName);
        }
    }

    template<class T> T& getAttr(const std::string& name) {
        const auto dot = name.find('.');
        if (dot == name.npos) {
            mAnyUtils.emplace(typeid(T), std::unique_ptr<AnyUtils<T>>(new AnyUtils<T>()));

            auto& attr = mAttrs.at(name);
#ifdef PYBIND
            AIDGE_ASSERT(attr.type() != typeid(py::object), "getAttr(): cannot return a reference to a Python-defined attribute.");
#endif
            return future_std::any_cast<T&>(attr);
        }
        else {
            const auto ns = name.substr(0, dot);
            const auto nsName = name.substr(dot + 1);
            return future_std::any_cast<DynamicAttributes&>(mAttrs.at(ns)).getAttr<T>(nsName);
        }
    }

    ///\brief Add a new Attribute, identified by its name. If it already exists, asserts.
    ///\tparam T expected Attribute type
    ///\param name Attribute name
    ///\param value Attribute value
    template<class T> void addAttr(const std::string& name, const T& value)
    {
        const auto dot = name.find('.');
        if (dot == name.npos) {
            mAnyUtils.emplace(typeid(T), std::unique_ptr<AnyUtils<T>>(new AnyUtils<T>()));

            const auto& res = mAttrs.emplace(std::make_pair(name, future_std::any(value)));
            AIDGE_ASSERT(res.second, "addAttr(): attribute \"{}\" already exists. Use setAttr() if this is expected.", name);
        }
        else {
            const auto ns = name.substr(0, dot);
            const auto nsName = name.substr(dot + 1);
            const auto& res = mAttrs.emplace(std::make_pair(ns, future_std::any(DynamicAttributes())));
            future_std::any_cast<DynamicAttributes&>(res.first->second).addAttr(nsName, value);
        }
    }

    ///\brief Set an Attribute value, identified by its name. If it already exists, its value (and type, if different) is changed.
    ///\tparam T expected Attribute type
    ///\param name Attribute name
    ///\param value Attribute value
    template<class T> void setAttr(const std::string& name, const T& value)
    {
        const auto dot = name.find('.');
        if (dot == name.npos) {
            mAnyUtils.emplace(typeid(T), std::unique_ptr<AnyUtils<T>>(new AnyUtils<T>()));

            auto res = mAttrs.emplace(std::make_pair(name, future_std::any(value)));
            if (!res.second)
                res.first->second = future_std::any(value);
        }
        else {
            const auto ns = name.substr(0, dot);
            const auto nsName = name.substr(dot + 1);
            auto res = mAttrs.emplace(std::make_pair(ns, future_std::any(DynamicAttributes())));
            future_std::any_cast<DynamicAttributes&>(res.first->second).setAttr<T>(nsName, value);
        }
    }

    void delAttr(const std::string& name) {
        const auto dot = name.find('.');
        if (dot == name.npos) {
            mAttrs.erase(name);
        }
        else {
            const auto ns = name.substr(0, dot);
            const auto nsName = name.substr(dot + 1);
            future_std::any_cast<DynamicAttributes&>(mAttrs.at(ns)).delAttr(nsName);
        }
    }

#ifdef PYBIND
    void addAttrPy(const std::string& name, py::object&& value)
    {
        addAttr(name, std::move(value));
    }

    void setAttrPy(const std::string& name, py::object&& value) override final
    {
        setAttr(name, std::move(value));
    }

    py::dict dict() const override {
        py::dict attributes;
        for (const auto& elt : mAttrs) {
            if (elt.second.type() == typeid(DynamicAttributes)) {
                attributes[elt.first.c_str()] = future_std::any_cast<const DynamicAttributes&>(elt.second).dict();
            }
            else {
                // At this point, not every attribute may be known to mAnyUtils
                const auto anyUtilsIt = mAnyUtils.find(elt.second.type());
                if (anyUtilsIt != mAnyUtils.end()) {
                    attributes[elt.first.c_str()] = anyUtilsIt->second->cast(elt.second);
                }
                else {
                    attributes[elt.first.c_str()] = "???";
                }
            }
        }
        return attributes;
    }

    std::string str() const override {
        return repr();
    }

    std::string repr() const override {
        // Call the __repr__ method of the base class py::dict
        return fmt::format("AttrDict({})",  static_cast<std::string>(py::str(dict())));
        // return fmt::format("AttrDict({})",  dict().attr("__repr__")().cast<std::string>());
    }

#endif

    //////////////////////////////////////
    ///     Generic Attributes API
    //////////////////////////////////////
    bool hasAttr(const std::string& name) const override final {
        const auto dot = name.find('.');
        if (dot == name.npos) {
            return (mAttrs.find(name) != mAttrs.cend());
        }
        else {
            const auto ns = name.substr(0, dot);
            const auto it = mAttrs.find(ns);
            if (it != mAttrs.cend()) {
                const auto nsName = name.substr(dot + 1);
                return future_std::any_cast<const DynamicAttributes&>(it->second).hasAttr(nsName);
            }
            else {
                return false;
            }
        }
    }

    std::string getAttrType(const std::string& name) const override final {
        // In order to remain consistent between C++ and Python, with or without PyBind, the name of the type is:
        // - C-style for C++ created attributes
        // - Python-style for Python created attributes
        const auto dot = name.find('.');
        if (dot == name.npos) {
            const auto& attr = mAttrs.at(name);
#ifdef PYBIND
            if (attr.type() == typeid(py::object)) {
                return std::string(Py_TYPE(future_std::any_cast<const py::object&>(attr).ptr())->tp_name);
            }
            else
#endif
            {
                return attr.type().name();
            }
        }
        else {
            const auto ns = name.substr(0, dot);
            const auto nsName = name.substr(dot + 1);
            return future_std::any_cast<const DynamicAttributes&>(mAttrs.at(ns)).getAttrType(nsName);
        }
    }

    std::set<std::string> getAttrsName() const override final {
        std::set<std::string> attrsName;
        for(auto const& it: mAttrs)
            attrsName.insert(it.first);
        return attrsName;
    }

#ifdef PYBIND
    /**
     * @detail See https://github.com/pybind/pybind11/issues/1590 as to why a
     * generic type caster for std::any is not feasible.
     * The strategy here is to store a cast() function for each attribute type ever used.
    */
    inline py::object getAttrPy(const std::string& name) const override final {
        const auto dot = name.find('.');
        if (dot == name.npos) {
            const auto& attr = mAttrs.at(name);
            return mAnyUtils.at(attr.type())->cast(attr);
        }
        else {
            const auto ns = name.substr(0, dot);
            const auto nsName = name.substr(dot + 1);
            return future_std::any_cast<const DynamicAttributes&>(mAttrs.at(ns)).getAttrPy(nsName);
        }
    };
#endif

    future_std::any getAny(const std::string& name) const
    {
        const auto dot = name.find('.');
        if (dot == name.npos) {
            return mAttrs.at(name);
        }
        else {
            const auto ns = name.substr(0, dot);
            const auto nsName = name.substr(dot + 1);
            return future_std::any_cast<const DynamicAttributes&>(mAttrs.at(ns)).getAny(nsName);
        }
    }

    std::map<std::string, future_std::any> getAttrs() const override {
        return mAttrs;
    }

    virtual ~DynamicAttributes() {
#ifdef PYBIND
        if (!Py_IsInitialized()) {
            // Resets the internal pointer of py::object to nullptr without decreasing the object's reference count.
            // At this point, the Python interpreter may have exited (it is the case if the current DynamicAttribute being destroyed is static),
            // in which case py:object has already being destroyed despite the reference counting being > 0.
            // See https://github.com/pybind/pybind11/issues/1598
            for (auto& attr : mAttrs) {
                if (attr.second.type() == typeid(py::object)) {
                    future_std::any_cast<py::object&>(attr.second).release();
                }
            }
        }
#endif
    }

    friend bool operator<(const DynamicAttributes& lhs, const DynamicAttributes& rhs);
    friend struct std::hash<DynamicAttributes>;

private:
    std::map<std::string, future_std::any> mAttrs;

public:
    struct AnyUtils_ {
#ifdef PYBIND
        virtual py::object cast(const future_std::any& attr) const = 0;
#endif
        virtual bool compare(const future_std::any&, const future_std::any&) const = 0;
        virtual size_t hash(const future_std::any&) const = 0;
        virtual ~AnyUtils_() = default;
    };

    template <class T>
    struct AnyUtils : public AnyUtils_ {
#ifdef PYBIND
        py::object cast(const future_std::any& attr) const override final {
            return py::cast(future_std::any_cast<const T&>(attr));
        }
#endif

        bool compare(const future_std::any& lhs, const future_std::any& rhs) const override final {
#ifdef PYBIND
            if (lhs.type() == typeid(py::object) && rhs.type() != typeid(py::object)) {
                return (future_std::any_cast<py::object>(lhs).cast<T>() < future_std::any_cast<T>(rhs));
            }
            else if (lhs.type() != typeid(py::object) && rhs.type() == typeid(py::object)) {
                return (future_std::any_cast<T>(lhs) < future_std::any_cast<py::object>(rhs).cast<T>());
            }
            else
#endif
            {
                return (future_std::any_cast<T>(lhs) < future_std::any_cast<T>(rhs));
            }
        }

        size_t hash(const future_std::any& attr) const override final {
            return std::hash<T>()(future_std::any_cast<T>(attr));
        }
    };

    template<typename T>
    static inline typename std::enable_if<!has_less_than_operator<T>::value, void>::type makeTypeConditionallyAvailable() {}

    template<typename T>
    static typename std::enable_if<has_less_than_operator<T>::value, void>::type makeTypeConditionallyAvailable() {
        mAnyUtils.emplace(typeid(T), std::unique_ptr<AnyUtils<T>>(new AnyUtils<T>()));
    }

    // Stores typed utils functions for each attribute type ever used
    static std::map<std::type_index, std::unique_ptr<AnyUtils_>> mAnyUtils;
};

template<> void DynamicAttributes::setAttr<future_std::any>(const std::string& name, const future_std::any& value);

#ifdef PYBIND
template <>
struct DynamicAttributes::AnyUtils<py::object> : public DynamicAttributes::AnyUtils_ {
    py::object cast(const future_std::any& attr) const override {
        return future_std::any_cast<const py::object&>(attr);
    }

    bool compare(const future_std::any& lhs, const future_std::any& rhs) const override {
        return (future_std::any_cast<py::object>(lhs) < future_std::any_cast<py::object>(rhs));
    }

    size_t hash(const future_std::any& attr) const override final {
        // Here we are mixing Python and C++ hashes... if both are
        // well implemented, this should not increase the collision
        // probability for the same number of stored hashes.
        return py::hash(future_std::any_cast<py::object>(attr));
    }
};
#endif

inline bool operator<(const DynamicAttributes& lhs, const DynamicAttributes& rhs) {
    return (lhs.mAttrs < rhs.mAttrs);
}

// Combine the hashes (boost-like hash combining, see boost::hash_combine())
inline void hash_combine(std::size_t& seed, const std::size_t& value) {
    seed ^= value + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}
}

namespace std {
    // Make DynamicAttributes hashable so that is can be stored in hash-based containers.
    // This is particularly useful in Python since set() and dict() are hash-based.
    template <>
    struct hash<Aidge::DynamicAttributes> {
        size_t operator()(const Aidge::DynamicAttributes& attrs) const {
            std::size_t seed = 0;
            for (const auto& pair : attrs.mAttrs) {
                Aidge::hash_combine(seed, std::hash<std::string>()(pair.first));
                Aidge::hash_combine(seed, Aidge::DynamicAttributes::mAnyUtils.at(pair.second.type())->hash(pair.second));
            }
            return seed;
        }
    };

    // General specialization of std::hash for any container that has iterators (e.g., std::vector, std::list, std::set)
    template <template <typename...> class Container, typename T, typename... Args>
    struct hash<Container<T, Args...>> {
        std::size_t operator()(const Container<T, Args...>& iterable) const {
            std::size_t seed = 0;
            for (const auto& v : iterable) {
                // Recursively hash the value pointed by the iterator
                Aidge::hash_combine(seed, std::hash<T>()(v));
            }
            return seed;
        }
    };

    // Special case for std::array
    template <typename T, std::size_t N>
    struct hash<std::array<T, N>> {
        std::size_t operator()(const std::array<T, N>& iterable) const {
            std::size_t seed = 0;
            for (const auto& v : iterable) {
                // Recursively hash the value pointed by the iterator
                Aidge::hash_combine(seed, std::hash<T>()(v));
            }
            return seed;
        }
    };

    // Specialization of std::hash for std::pair<T1, T2>
    template <typename T1, typename T2>
    struct hash<std::pair<T1, T2>> {
        std::size_t operator()(const std::pair<T1, T2>& p) const {
            std::size_t seed = 0;
            Aidge::hash_combine(seed, std::hash<T1>()(p.first));
            Aidge::hash_combine(seed, std::hash<T2>()(p.second));
            return seed;
        }
    };
}

namespace future_std {
bool operator<(const future_std::any& lhs, const future_std::any& rhs);
}

#endif /* AIDGE_CORE_UTILS_DYNAMICATTRIBUTES_H_ */
