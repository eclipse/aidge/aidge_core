/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_UTILS_LOG_H_
#define AIDGE_CORE_UTILS_LOG_H_

#include <memory>
#include <string>
#include <vector>

#include <fmt/format.h>
#include <fmt/color.h>
#ifdef PYBIND
#include <pybind11/pybind11.h>
#endif

#include "aidge/utils/logger/EnumString.hpp"


namespace Aidge {
#ifdef PYBIND
namespace py = pybind11;
#endif


/**
 * Helper to define a context anywhere, hiding the scoped variable name
 * which has no relevance.
 */
#define AIDGE_LOG_CONTEXT(...)                                                \
    const Log::Context logContext_##__LINE__(__VA_ARGS__)

/**
 * @brief Logging utility class for development and debugging
 *
 * The Log class provides a static interface for logging messages at different
 * severity levels. It supports both console output (with optional colors) and
 * file output. The logging behavior can be configured through environment
 * variables or programmatically.
 *
 * Environment variables:
 * - AIDGE_LOGLEVEL_CONSOLE: Set console output level (Debug,Info,Notice,Warn,Error,Fatal)
 * - AIDGE_LOGLEVEL_FILE: Set file output level
 * - AIDGE_LOG_COLOR: Enable/disable colored output ("off"/"OFF"/"0" to disable)
 * - AIDGE_LOG_FILE: Set log file path
 *
 * @note Console colors are enabled by default
 */
class Log {
public:
    /**
     * @brief Log severity levels in ascending order
     */
    enum Level {
        Debug = 0,    ///< Detailed information for debugging
        Info,         ///< General information about program execution
        Notice,       ///< Normal but significant conditions
        Warn,         ///< Warning messages for potentially problematic situations
        Error,        ///< Error messages for serious problems
        Fatal         ///< Critical errors that lead to program termination
    };

    /**
     * @brief RAII class for managing logging contexts
     *
     * This class allows adding temporary context information to logs. When an instance
     * is created, it adds a context message that will be displayed with all logs until
     * the instance is destroyed.
     *
     * Example:
     * @code
     * {
     *     Log::Context ctx("Processing file: {}", filename);
     *     Log::info("Starting process"); // Will show context
     *     // Context automatically removed when ctx is destroyed
     * }
     * @endcode
     */
    class Context {
    public:
        /**
         * @brief Create a new logging context
         * @param args Format string and arguments for the context message
         */
        template <typename... Args>
        Context(Args&&... args) {
            Log::mContext.push_back(fmt::format(std::forward<Args>(args)...));
        }

        /**
         * @brief Destroy the context, removing it from the active contexts
         */
        ~Context() {
            Log::mContext.pop_back();
        }
    };

    /**
     * @brief Log a debug message
     * @param args Format string and arguments for the message
     * @note Debug messages are only compiled in debug builds
     */
#ifndef NDEBUG
    template <typename... Args>
    static void debug(Args&&... args) {
        log(Debug, fmt::format(std::forward<Args>(args)...));
    }
#else
    template <typename... Args>
    static void debug(Args&&... /*args*/) {
        // Debug logging disabled in release builds
    }
#endif

    /**
     * @brief Log an info message
     * @param args Format string and arguments for the message
     */
    template <typename... Args>
    static void info(Args&&... args) {
        log(Info, fmt::format(std::forward<Args>(args)...));
    }

    /**
     * @brief Log a notice message
     * @param args Format string and arguments for the message
     */
    template <typename... Args>
    static void notice(Args&&... args) {
        log(Notice, fmt::format(std::forward<Args>(args)...));
    }

    /**
     * @brief Log a warning message
     * @param args Format string and arguments for the message
     */
    template <typename... Args>
    static void warn(Args&&... args) {
        log(Warn, fmt::format(std::forward<Args>(args)...));
    }

    /**
     * @brief Log an error message
     * @param args Format string and arguments for the message
     */
    template <typename... Args>
    static void error(Args&&... args) {
        log(Error, fmt::format(std::forward<Args>(args)...));
    }

    /**
     * @brief Log a fatal error message
     * @param args Format string and arguments for the message
     */
    template <typename... Args>
    static void fatal(Args&&... args) {
        log(Fatal, fmt::format(std::forward<Args>(args)...));
    }

    /**
     * @brief Set the minimum level for console output
     * @param level Minimum level to display
     */
    static void setConsoleLevel(Level level) {
        mConsoleLevel = level;
        // Note: In python each module has a compiled version of Log.
        // This means each static instance is separated and does not communicate.
        // To allow the communication between the different modules, we use PyCapsule.
        // https://docs.python.org/3/extending/extending.html#providing-a-c-api-for-an-extension-module
#ifdef PYBIND
#define _CRT_SECURE_NO_WARNINGS
        if (Py_IsInitialized()){
            // Note: Setting mConsoleLevel and not level is important
            // to avoid garbage collection of the pointer.
            py::set_shared_data("consoleLevel", &mConsoleLevel);
        }
#endif // PYBIND
    }

    static Level getConsoleLevel() {
#ifdef PYBIND
#define _CRT_SECURE_NO_WARNINGS
        if (Py_IsInitialized()){
            auto shared_data = reinterpret_cast<Level *>(py::get_shared_data("consoleLevel"));
            if (shared_data)
                mConsoleLevel = *shared_data;
        }
#endif // PYBIND
        return mConsoleLevel;
    }

    /**
     * @brief Enable or disable colored console output
     * @param enabled True to enable colors, false to disable
     */
    static void setConsoleColor(bool enabled) noexcept { mConsoleColor = enabled; }

    /**
     * @brief Set the minimum level for file output
     * @param level Minimum level to write to file
     */
    static void setFileLevel(Level level) noexcept { mFileLevel = level; }

    /**
     * @brief Set the log file path
     * @param fileName Path to log file (empty to disable file logging)
     * @throw std::runtime_error if file cannot be opened
     */
    static void setFileName(const std::string& fileName);

    /**
     * @brief Set the precision format for floating point numbers.
     * @param precision number of digits displayed on the right-hand of the
     * decimal point.
     */
    static void setPrecision(int precision) noexcept {
        if (precision < 0) {
            Log::notice("Impossible to set precision to {}. Must be a positive number.", precision);
            return;
        }
        mFloatingPointPrecision = precision;
#ifdef PYBIND
#define _CRT_SECURE_NO_WARNINGS
        if (Py_IsInitialized()){
            // Note: Setting mFloatingPointPrecision is important
            // to avoid garbage collection of the pointer.
            py::set_shared_data("floatingPointPrecision", &mFloatingPointPrecision);
        }
#endif // PYBIND
    }
    static int getPrecision() noexcept {
        return mFloatingPointPrecision;
    }

private:
    static void log(Level level, const std::string& msg);
    static void initFile(const std::string& fileName);

    struct fcloseDeleter {
        void operator()(FILE* f) const noexcept { std::fclose(f); }
    };

    static Level mConsoleLevel;        ///< Minimum level for console output
    static bool mConsoleColor;         ///< Whether to use colored output
    static Level mFileLevel;           ///< Minimum level for file output
    static std::string mFileName;      ///< Path to log file
    static std::unique_ptr<FILE, fcloseDeleter> mFile;  ///< File handle
    static std::vector<std::string> mContext;  ///< Stack of active contexts

    static int mFloatingPointPrecision;
};

} // namespace Aidge

// Formatter specialization for Log::Level
template <>
struct fmt::formatter<Aidge::Log::Level> : formatter<const char*> {
    template <typename FormatContext>
    auto format(const Aidge::Log::Level& level, FormatContext& ctx) const {
        const char* name = EnumStrings<Aidge::Log::Level>::data[static_cast<int>(level)];
        return formatter<const char*>::format(name, ctx);
    }
};

namespace {
template <>
const char* const EnumStrings<Aidge::Log::Level>::data[] = {
    "DEBUG", "INFO", "NOTICE", "WARNING", "ERROR", "FATAL"
};
}

#endif // AIDGE_CORE_UTILS_LOG_H_
