/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
#ifndef AIDGE_DIRECTORIES_H_
#define AIDGE_DIRECTORIES_H_

#include <algorithm>
#include <errno.h>
#include <string>
// #include <string_view> available in c++-17
#include <vector>

#include <fmt/core.h>
#include <fmt/format.h>
#include <sys/stat.h>

#ifndef _S_ISTYPE
#define _S_ISTYPE(mode, mask)  (((mode) & _S_IFMT) == (mask))
#endif

#ifndef S_ISREG
#define S_ISREG(mode) _S_ISTYPE((mode), _S_IFREG)
#endif

#ifndef S_ISDIR
#define S_ISDIR(mode) _S_ISTYPE((mode), _S_IFDIR)
#endif

#ifdef WIN32
#include <direct.h>
#else
#include <sys/types.h>
#include <unistd.h>
#endif

namespace Aidge {
    bool isNotValidFilePath(int c) {
        return (iscntrl(c)
            || c == '<'
            || c == '>'
            || c == ':'
            || c == '"'
            || c == '|'
            || c == '?'
            || c == '*');
    }

    std::string filePath(const std::string& str) {
        std::string filePath(str);
        std::replace_if(filePath.begin(), filePath.end(),
                        isNotValidFilePath, '_');
        return filePath;
    }

	// std::string_view could be used if C++ version was 17 or higher
    // bool createDirectories(std::string_view dirName) {
	bool createDirectories(const std::string& dirName) {
        std::string currentPath;
        std::string remaining = dirName;
        size_t pos = 0;
        int status = 0;

        while ((pos = remaining.find('/')) != std::string::npos && status == 0) {
            currentPath += fmt::format("{}/", remaining.substr(0, pos));
            remaining = remaining.substr(pos + 1);

            struct stat fileStat;
            if (stat(currentPath.c_str(), &fileStat) != 0) {
                // Directory does not exist
#ifdef WIN32
                status = mkdir(currentPath.c_str());
#else
#if defined(S_IRWXU)
                status = mkdir(currentPath.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
#else
                status = mkdir(currentPath.c_str());
#endif
#endif
            } else if (!S_ISDIR(fileStat.st_mode)) {
                status = -1;
            }
        }

        // Handle the last component if any
        if (!remaining.empty() && status == 0) {
            currentPath += fmt::format("{}/", remaining);
            struct stat fileStat;
            if (stat(currentPath.c_str(), &fileStat) != 0) {
#ifdef WIN32
                status = mkdir(currentPath.c_str());
#else
#if defined(S_IRWXU)
                status = mkdir(currentPath.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
#else
                status = mkdir(currentPath.c_str());
#endif
#endif
            } else if (!S_ISDIR(fileStat.st_mode)) {
                status = -1;
            }
        }

        return (status == 0 || errno == EEXIST);
    }
}

#endif //AIDGE_DIRECTORIES_H_
