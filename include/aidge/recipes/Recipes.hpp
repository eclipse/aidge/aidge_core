/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_UTILS_RECIPES_H_
#define AIDGE_CORE_UTILS_RECIPES_H_

#include <memory>
#include <set>

#include "aidge/graph/Node.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Matching.hpp"


namespace Aidge {

void constantFolding(std::shared_ptr<GraphView> graph);

// FUSE MATMUL + ADD -> FC

/**
 * @brief Merge ``MatMul`` and :cpp:function:`Aidge::Add` Node into a :cpp:function:`Aidge::FC` Node.
 *
 * @param nodes Strict set of Node to merge.
 */
void matMulToFC(std::shared_ptr<Node> matmul, std::shared_ptr<Node> add = nullptr);

/**
 * @brief Merge ``MatMul`` and :cpp:function:`Aidge::Add` Node into a :cpp:function:`Aidge::FC` Node.
 *
 * @param graphView Graph view to use graph matching on, in order to apply transformations.
 */
void matMulToFC(std::shared_ptr<GraphView> graphView);

/**
 * @brief Remove a node type.
 *
 * @param graphView Graph view to use graph matching on, in order to apply transfomrations.
 * @param type Type of the nodes to remove
 * @param incProducers If true, also remove the producers attached to the removed nodes
 * @return size_t Number of identity nodes removed
 */
size_t removeNode(std::shared_ptr<GraphView> graphView, const std::string& type, bool incProducers = false);

/**
 * @brief Fuses constant => Generic | constantOfShape and transforms it into a Producer
 * @param graph Graph to manipulate
 * @return size_t Number of replacement
 */
size_t removeConstantOfShape(std::shared_ptr<GraphView> graph_view);

/**
 * @brief Remove ``Dropout`` Node.
 *
 * @param graphView Graph view to use graph matching on, in order to apply transfomrations.
 * @return size_t Number of identity nodes removed
 */
size_t removeDropout(std::shared_ptr<GraphView> graphView);

/**
 * Remove all identity nodes
 * @param graph Graph to manipulate
 * @return size_t Number of identity nodes removed
*/
size_t removeIdentity(std::shared_ptr<GraphView> graph);

// REMOVE FLATTEN + FC -> FC

/**
 * @brief Remove ``Flatten`` before :cpp:function:`Aidge::FC` Node.
 *
 * @param nodes Strict set of Node to merge.
 */
void removeFlatten(std::shared_ptr<Node> flatten);

/**
 * @brief Remove ``Flatten`` before :cpp:function:`Aidge::FC` Node.
 *
 * @param graphView Graph view to use graph matching on, in order to apply transformations.
 */
void removeFlatten(std::shared_ptr<GraphView> graphView);

// FUSE BN + FC || CONV -> FC || CONV

/**
 * @brief Fuse :cpp:function:`Aidge::BatchNorm` with :cpp:function:`Aidge::Conv` or :cpp:function:`Aidge::FC` Nodes.
 * Ref: https://nenadmarkus.com/p/fusing-batchnorm-and-conv/
 *
 * @param nodes Strict set of Node to merge.
 */
void fuseBatchNorm(std::shared_ptr<Node> conv,std::shared_ptr<Node> batchnorm);

/**
 * @brief Fuse :cpp:function:`Aidge::BatchNorm` with :cpp:function:`Aidge::Conv` or :cpp:function:`Aidge::FC` Nodes.
 * Ref: https://nenadmarkus.com/p/fusing-batchnorm-and-conv/
 *
 * @param graphView Graph view to use graph matching on, in order to apply transformations.
 */
void fuseBatchNorm(std::shared_ptr<GraphView> graphView);

std::set<std::shared_ptr<Node>> getConvHorizontalTiling(const std::shared_ptr<Node>& node, const DimIdx_t axis, const std::size_t nbSlices);
// void horizontalTiling(std::shared_ptr<Node> node, DimIdx_t dim, std::size_t nbSlices);
// std::set<std::shared_ptr<Node>> getHorizontalTiling(std::set<std::shared_ptr<Node>> setOfNodes, DimIdx_t dim, std::size_t nbSlices);
// void horizontalTiling(std::set<std::shared_ptr<Node>> setOfNodes, DimIdx_t dim, std::size_t nbSlices);


/**
 * Add Cast and Move operators where needed to ensure no conversion needs to be done
 * at the Operator level.
*/
void explicitCastMove(std::shared_ptr<GraphView> graphView);

/**
 * Add Transpose operators where needed to ensure no transposition needs to be done
 * at the Operator level.
*/
void explicitTranspose(std::shared_ptr<GraphView> graphView);

/**
 * Replace a single meta operator by its micro graph.
 * @return true if node is indeed a meta operator and could be expanded.
*/
bool expandMetaOp(std::shared_ptr<Node> node);

/**
 * Flatten the graph by replacing the meta operators by their micro graph.
 * @param recursive If true, recursively replace meta operators until there is
 * no more meta operator in the graph.
*/
void expandMetaOps(std::shared_ptr<GraphView> graph, bool recursive = false);

/**
 * @brief Tile any :cpp:function:`Aidge::MatMul` operator to several fixed size matrix multiplications.
 * For instance, for a MatMul of size 80x80 and a tiling of 16x16, this will tile 
 * the MatMul operator to 25 (5 by 5) MatMul operators of size 16x16, with Slice
 * operators inserted at the inputs and Concat operators inserted at the outputs.
 * 
 * This is especially useful when matrix multiplication must be mapped to fixed 
 * maximum size hardware TPU (Tensor Processing Unit) or MMA (Matrix Multiplication 
 * Accelerator). This recipe can be combined with the :cpp:function:`Aidge::convToMatMul` recipe in 
 * order to convert convolutions to matrix multiplication beforehand, and 
 * :cpp:function:`Aidge::constantFolding` recipe to fold sliced constant tensors.
 * 
 * @param matMul MatMul operator to be tiled.
 * @param maxDims Maximum output dimensions of the tiled MatMul operators.
 */
void matMulTiling(NodePtr matMul, const std::vector<DimSize_t>& maxDims);

/**
 * Fuse each sub-graph matching a query in a Meta Operator.
 * @param gm SinglePassGraphMatching containing the graph to manipulate
 * @param query Sub-graph matching query
 * @param type Type name of the resulting meta operators
 * @return size_t Number of replacement
*/
size_t fuseToMetaOps(SinglePassGraphMatching& gm, const std::string& query, const std::string& type = "");

/**
 * Fuse each sub-graph matching a query in a Meta Operator.
 * @param graph Graph to manipulate
 * @param query Sub-graph matching query
 * @param type Type name of the resulting meta operators
 * @return size_t Number of replacement
*/
size_t fuseToMetaOps(std::shared_ptr<GraphView> graph, const std::string& query, const std::string& type = "");

/**
 * Transform Conv layers with MatMul.
 * @param graph Graph to manipulate
 * @return size_t Number of replacement
*/
size_t convToMatMul(std::shared_ptr<GraphView> graph);

/**
 * @brief Adapt a graph to the available kernels of a backend.
 * 
 * @param graph Graph to manipulate
 */
void adaptToBackend(std::shared_ptr<GraphView> graph);


/**
 * @brief Create a GenericOp from an Operator and replace it
 * 
 * @param node Node which Operator will be changed into a generic Operator
 */
void toGenericOp(std::shared_ptr<Node> node);

/**
 * @brief The node passed contains an operator which input of index 1 is supposed be be weights of type Int4, Int3, Int2, binary.
 *        This recipie only operates memory transformations on the weight tensor. 
 *        First, permutes the dimensions to match the dataformat NHWC
 *        Second, compact the last dimension of the weights (Channel dimension) into 8bits 
 * 
 * @param node Node 
 */
void applyWeightInterleaving(std::shared_ptr<Node> node);

} // namespace Aidge

#endif /* AIDGE_CORE_UTILS_RECIPES_H_ */
