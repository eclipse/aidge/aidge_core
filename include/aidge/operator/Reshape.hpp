/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_RESHAPE_H_
#define AIDGE_CORE_OPERATOR_RESHAPE_H_

#include <memory>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
/**
 * @brief Implementation of the Reshape operator.
 * @note This operator implementation is agnostic to the backend and is located here instead of in aidge_backend.
 */
class Reshape_OpImpl : public OperatorImpl {
public:
    /**
     * @brief Constructor for Reshape_OpImpl.
     * @param[in] op The Operator instance.
     * @param[in] backend The backend name (optional).
     */
    Reshape_OpImpl(const Operator& op, const std::string& backend = "")
        : OperatorImpl(op, backend) {}

    /**
     * @brief Perform the forward operation for the reshape.
     */
    void forward() override;
    void backward() override;
};

/**
 * @enum ReshapeAttr
 * @brief Enumeration of attributes specific to the Reshape operator.
 */
enum class ReshapeAttr {
    /**
     * @brief The target shape for the output tensor.
     */
    Shape,
    
    /**
     * @brief Whether zeros in the shape attribute are allowed.
     * 
     * When true, zeros in the target shape retain the corresponding dimension size from the input tensor.
     */
    AllowZero
};

/**
 * @brief Description of Reshape operator that adjusts the shape of the input tensor.
 *
 * This operator reshapes the input tensor according to the specified target shape. 
 * If the target shape is not compatible with the input tensor's total number of elements, 
 * the operation will fail. If the `AllowZero` attribute is true, zeros in the target shape 
 * retain the corresponding dimensions from the input tensor.
 *
 * @example Input: Tensor of dimensions `[2, 3]` with `Shape = {3, 2}` results in a tensor with dimensions `[3, 2]`.
 * @example Input: Tensor of dimensions `[4, 3]` with `Shape = {0, -1}` and `AllowZero = true` results in a tensor with dimensions `[4, 3]`.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Reshape_Op : public OperatorTensor,
                   public Registrable<Reshape_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const Reshape_Op&)>> {

public:
    /**
     * @brief Static type string for the Reshape operator.
     */
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<ReshapeAttr, std::vector<std::int64_t>, bool>;
    template <ReshapeAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    /**
     * @brief Deleted default constructor.
     */
    Reshape_Op() = delete;

    /**
     * @brief Constructor for the Reshape operator.
     * @param[in] shape Target shape for the output tensor.
     * @param[in] allowzero Whether zeros in the shape retain input tensor dimensions.
     */
    Reshape_Op(const std::vector<std::int64_t>& shape = {}, bool allowzero = false);

    /**
     * @brief Copy-constructor.
     * @param[in] op Reshape_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    Reshape_Op(const Reshape_Op& op);

    /**
     * @brief Clone the operator using its copy constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Check whether the dimensions have been forwarded successfully.
     * @return True if dimensions were successfully forwarded.
     */
    bool dimsForwarded() const override final;

    /**
     * @brief Compute the output dimensions during the forward pass.
     * @param[in] allowDataDependency Whether to allow data-dependent dimensions.
     * @return Boolean indicating whether dimension computation was successful.
     */
    bool forwardDims(bool allowDataDependency = false) override final;

    /**
     * @brief Set the backend for the Reshape operator.
     * @param[in] name Name of the backend.
     * @param[in] device Device index (optional).
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;

    /**
     * @brief Get the available backends for the Reshape operator.
     * @return A set of backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     * @return A shared pointer to the attributes.
     */
    std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get or modify the target shape for the output tensor.
     * @return Reference to the shape attribute.
     */
    inline std::vector<std::int64_t>& shape() const { return mAttributes->template getAttr<ReshapeAttr::Shape>(); }

    /**
     * @brief Get or modify the AllowZero attribute.
     * @return Reference to the AllowZero attribute.
     */
    inline bool& allowZero() const { return mAttributes->template getAttr<ReshapeAttr::AllowZero>(); }

    /**
     * @brief Get the input tensor names for the Reshape operator.
     * @return A vector of input tensor names.
     */
    static const std::vector<std::string> getInputsName() {
        return {"data_input"};
    }

    /**
     * @brief Get the output tensor names for the Reshape operator.
     * @return A vector of output tensor names.
     */
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Create a Reshape operation node.
 *
 * @param[in] shape Target shape for the output tensor (optional).
 * @param[in] allowzero Whether zeros in the shape retain input tensor dimensions.
 * @param[in] name Name of the operator (optional).
 * @return A shared pointer to the Node containing the Reshape operator.
 */
std::shared_ptr<Node> Reshape(const std::vector<std::int64_t>& shape = {},
                              bool allowzero = false,
                              const std::string& name = "");

}  // namespace Aidge

namespace {
/**
 * @brief EnumStrings specialization for ReshapeAttr.
 */
template <>
const char *const EnumStrings<Aidge::ReshapeAttr>::data[] = {"shape", "allow_zero"};
}

#endif /* AIDGE_CORE_OPERATOR_RESHAPE_H_ */
