/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_SQUEEZE_H_
#define AIDGE_CORE_OPERATOR_SQUEEZE_H_

#include <cstdint>
#include <cstdlib>
#include <functional>
#include <limits>
#include <memory>
#include <string>
#include <vector>

#include "aidge/graph/Node.hpp"
#include "aidge/operator/Operator.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
/**
 * @brief implementation of the operator squeeze.
 * @note Since this operator implementation is agnostic to the backend it is
 * located here instead of in aidge_backend_cpu/cuda.
 */
class Squeeze_OpImpl : public OperatorImpl {
public:
  Squeeze_OpImpl(const Operator &op, const std::string &backend = "")
      : OperatorImpl(op, backend) {}
  void forward() override;
};

enum class SqueezeAttr {
  /**
   * @brief axes to squeeze, if left empty all 1 sized
   * dimensions will be removed.
   */
  Axes
};

/**
 * @brief This operator has as purpose to remove dummy dimensions around given
 * axes.
 * input#0 : Tensor to squeeze
 * input#1 Optional : 1D tensor that lists the axes to squeeze
 * @note the axes to squeeze can either be given via attribute or via input #1,
 * for the sake of simplicity of the example unders, the axes to squeeze are
 * given via attribute
 * @example Calling squeeze(1) on a tensor of dimensions (2,1,3,4) will result
 * in a tensor of dim (2,3,4).
 * @example Calling squeeze(1) on a tensor of dimensions (1,2,3,4) will result
 * in a tensor of dim (1,2,3,4).
 * @example Calling squeeze() with no argument will result in the removal of
 * every 1-sized dimension in the tensor.
 */
class Squeeze_Op
    : public OperatorTensor,
      public Registrable<Squeeze_Op, std::string,
                         std::function<std::shared_ptr<OperatorImpl>(const Squeeze_Op &)>> {

public:
  static const std::string
      Type; // name of the type of the operation (Here "Squeeze")

private:
  using Attributes_ = StaticAttributes<SqueezeAttr, std::vector<int8_t>>;
  template <SqueezeAttr e> using attr = typename Attributes_::template attr<e>;
  const std::shared_ptr<Attributes_> mAttributes;

public:
  /**
   * @brief constructor for Squeeze op
   * @param[in] axes around which perform the operation
   */
  Squeeze_Op(const std::vector<int8_t> &axes = {})
      : OperatorTensor(Type, {InputCategory::Data, InputCategory::OptionalData},
                       1),
        mAttributes(
            std::make_shared<Attributes_>(attr<SqueezeAttr::Axes>(axes))) {
    mImpl = std::make_shared<Squeeze_OpImpl>(*this);
  }

  /**
   * @brief Copy-constructor. Copy the operator attributes and its output
   * tensor(s), but not its input tensors (the new operator has no input
   * associated).
   * @param op Operator to copy.
   */
  Squeeze_Op(const Squeeze_Op &op)
      : OperatorTensor(op), mAttributes(op.mAttributes) {
    if (!op.backend().empty()) {
      SET_IMPL_MACRO(Squeeze_Op, *this, op.backend());
    } else {
      mImpl = std::make_shared<Squeeze_OpImpl>(*this);
    }
  }

  /**
   * @brief Clone the operator using its copy-constructor.
   * @see Operator::MatMul_Op
   */
  std::shared_ptr<Operator> clone() const override final {
    return std::make_shared<Squeeze_Op>(*this);
  }

  /**
   * @brief Compute dimensions for the output Tensor
   */
  bool forwardDims(bool allowDataDependency = false) override final;
  bool dimsForwarded() const override final;

  void setBackend(const std::string &name,
                  DeviceIdx_t device = 0) override final;
    std::set<std::string> getAvailableBackends() const override;

  inline std::shared_ptr<Attributes> attributes() const override {
    return mAttributes;
  }

  /**
   * @brief axes to squeeze, if left empty all 1 sized
   * dimensions will be removed.
   */
  inline std::vector<int8_t> &axes() const noexcept {
    return mAttributes->template getAttr<SqueezeAttr::Axes>();
  }

  static const std::vector<std::string> getInputsName() {
    return {"data_input", "axes_to_squeeze"};
  }
  static const std::vector<std::string> getOutputsName() {
    return {"squeezed"};
  }
};

// helper with C-style array instead of std::array for kernel_dims to allow
// automatic template DIM deduction
inline std::shared_ptr<Node> Squeeze(const std::vector<int8_t> axes = {},
                                     const std::string &name = "") {
  return std::make_shared<Node>(std::make_shared<Squeeze_Op>(axes), name);
}
} // namespace Aidge

namespace {
template <>
const char *const EnumStrings<Aidge::SqueezeAttr>::data[] = {"Axes"};
}

#endif // AIDGE_CORE_OPERATOR_SQUEEZE_H_
