/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_MATMUL_H_
#define AIDGE_CORE_OPERATOR_MATMUL_H_

#include <memory>
#include <string>
#include <vector>

#include "aidge/utils/Types.h"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"

namespace Aidge {

/**
 * @brief Description of a Matrix Multiplication (MatMul) operation on input Tensors.
 *
 * Performs matrix multiplication on two input Tensors A and B:
 * - If both inputs are 2D matrices, the function is defined as:
 *   `C = A @ B`, where `@` represents matrix multiplication.
 * - For higher-dimensional Tensors, it performs batched matrix multiplication:
 *   The last two dimensions of A and B are used for matrix multiplication, 
 *   while the remaining dimensions are broadcast according to NumPy broadcasting rules.
 *
 * Requirements:
 * - The number of columns in the last dimension of A must match the number of rows
 *   in the last dimension of B.
 *
 * The output Tensor shape is determined as:
 * - For 2D matrices A (m x n) and B (n x p), the output shape is (m x p).
 * - For higher-dimensional Tensors, broadcasting rules are applied to the batch dimensions,
 *   and the resulting shape is `[broadcasted_batch_dims, m, p]`.
 *
 * @example:
 * - Input A shape: (2, 3, 4), Input B shape: (3, 4, 5)
 * - Batch dimensions (first dimension) are broadcasted: A is repeated along the first axis.
 * - Last two dimensions are multiplied: (4, 5) for A and B.
 * - Output shape: (3, 4, 5).
 *
 * @see OperatorTensor
 * @see Registrable
 */
class MatMul_Op : public OperatorTensor,
              public Registrable<MatMul_Op,
                                 std::string,
                                 std::function<std::shared_ptr<OperatorImpl>(const MatMul_Op &)>> {
public:
    static const std::string Type;

    MatMul_Op() : OperatorTensor(Type, {InputCategory::Data, InputCategory::Data}, 1) {}

    /**
     * @brief Copy-constructor.
     * @param op MatMul_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    MatMul_Op(const MatMul_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::MatMul_Op
     */
    std::shared_ptr<Operator> clone() const override final;

    /**
     * @brief Compute dimensions for the output Tensor following the same rules as
     * numpy.matmul.
     * @note - Both inputs are 2-D Tensors: classic matrix multiplication
     * @note - Either input is N-D with N > 2: it is treated as a stack of matrices residing
     * in the last two indexes and broadcast accordingly.
     * @note - First input is 1-D: it is promoted to a matrix by prepending a 1 to its
     * dimensions (D) -> (1,D). The prepended 1 is removed after computation.
     * @note - Second input is 1-D: it is promoted to a matrix by appending a 1 to its
     * dimensions (D) -> (D,1). The appended 1 is removed after computation.
     */
    bool forwardDims(bool allowDataDependency = false) override final;


    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;
    std::set<std::string> getAvailableBackends() const override;

    static const std::vector<std::string> getInputsName() {
        return {"data_input1", "data_input2"};
    }
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

std::shared_ptr<Node> MatMul(const std::string& name = "");
} // namespace Aidge

#endif /* AIDGE_CORE_OPERATOR_MATMUL_H_ */
