/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_GRIDSAMPLE_H_
#define AIDGE_CORE_OPERATOR_GRIDSAMPLE_H_

#include <cstddef>  // std::size_t
#include <memory>
#include <string>
#include <vector>

#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/logger/EnumString.hpp"

namespace Aidge {
enum class GridSampleAttr {
	Mode,			// Specifies the interpolation mode (e.g., Linear, Nearest, Cubic).
	PaddingMode,	// Specifies how to handle out-of-boundary grid values.
	AlignCorners	// Determines whether grid values are normalized to align with the image corners.
};

/**
 * @class GridSample_Op
 * @brief Implements the GridSample operation for sampling input data using a grid.
 *
 * GridSample is used to perform spatial transformation on an input tensor by
 * sampling its values based on a grid tensor. It supports different
 * interpolation and padding modes for flexibility.
 *
 * ### Attributes:
 * - **Mode**: Determines the interpolation technique to use.
 *   - `Linear`: Bilinear interpolation.
 *   - `Nearest`: Nearest neighbor interpolation.
 *   - `Cubic`: Bicubic interpolation.
 * - **PaddingMode**: Specifies how to handle grid values outside the input boundaries.
 *   - `Zeros`: Pads with zeros.
 *   - `Border`: Extends the edge values.
 *   - `Reflection`: Mirrors the input tensor.
 * - **AlignCorners**: Aligns grid points with the corners of the input image.
 *   - `true`: Aligns grid values to the image corners.
 *   - `false`: Normalizes grid values in the range [-1, 1].
 *
 * ### Example Usage:
 * - Input shape: (N, C, H, W) // Batch size N, Channels C, Height H, Width W.
 * - Grid shape: (N, H_out, W_out, 2) // Grid specifying output coordinates.
 * - Output shape: (N, C, H_out, W_out).
 *
 * @see OperatorTensor
 * @see Registrable
 */
class GridSample_Op : public OperatorTensor,
	public Registrable<GridSample_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const GridSample_Op&)>> {

public:
	static const std::string Type;

	/**
	 * @enum Mode
	 * @brief Interpolation modes available for GridSample.
	 */
	enum class Mode { Linear, Nearest, Cubic };

	/**
	 * @enum PaddingMode
	 * @brief Padding modes available for GridSample.
	 */
	enum class PaddingMode { Zeros, Border, Reflection };

private:
	using Attributes_ = StaticAttributes<GridSampleAttr, Mode, PaddingMode, bool>;
	template <GridSampleAttr e>
	using attr = typename Attributes_::template attr<e>;
	const std::shared_ptr<Attributes_> mAttributes;

public:
	/**
	 * @brief Constructs a GridSample operator with specified attributes.
	 * @param[in] mode Interpolation mode (e.g., Linear, Nearest, Cubic).
	 * @param[in] paddingMode Padding mode (e.g., Zeros, Border, Reflection).
	 * @param[in] alignCorners Whether to align grid values with input corners.
	 */
	GridSample_Op(Mode mode = Mode::Linear,
				PaddingMode paddingMode = PaddingMode::Zeros,
				bool alignCorners = false);

	/**
	 * @brief Copy-constructor. Copies the operator attributes and its output tensor(s).
	 * @param[in] other Operator to copy.
	 */
	GridSample_Op(const GridSample_Op& other);

	/**
	 * @brief Destructor.
	 */
	~GridSample_Op() noexcept;

	/**
	 * @brief Clone the operator using its copy-constructor.
	 * @return A shared pointer to the cloned operator.
	 */
	std::shared_ptr<Operator> clone() const override;

	/**
	 * @brief Determines whether dimensions can be forwarded.
	 * @param allowDataDependencies Allow data-dependent dimensions.
	 * @return True if dimensions are forwarded successfully.
	 */
	bool forwardDims(bool /*allowDataDependencies*/ = false) override final;

	/**
	 * @brief Sets the backend for execution.
	 * @param name Backend name.
	 * @param device Device index.
	 */
	void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;

	/**
	 * @brief Retrieves the available backends.
	 * @return A set of available backend names.
	 */
    std::set<std::string> getAvailableBackends() const override;

	/**
	 * @brief Retrieves the operator's attributes.
	 * @return Shared pointer to the attributes.
	 */
	inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

	/**
	 * @brief Gets the interpolation mode.
	 * @return Current interpolation mode.
	 */
	inline Mode mode() const { return mAttributes->template getAttr<GridSampleAttr::Mode>(); }

	/**
	 * @brief Gets the padding mode.
	 * @return Current padding mode.
	 */
	inline PaddingMode paddingMode() const { return mAttributes->template getAttr<GridSampleAttr::PaddingMode>(); }

	/**
	 * @brief Checks if grid values align with input corners.
	 * @return True if corners are aligned.
	 */
	inline bool alignCorners() const { return mAttributes->template getAttr<GridSampleAttr::AlignCorners>(); }

	/**
	 * @brief Retrieves the input names for GridSample.
	 * @return Vector of input tensor names.
	 */
	static const std::vector<std::string> getInputsName() {
		return {"data_input", "grid_field"};
	}

	/**
	 * @brief Retrieves the output names for GridSample.
	 * @return Vector of output tensor names.
	 */
	static const std::vector<std::string> getOutputsName() {
		return {"data_output"};
	}
};

/**
 * @brief Creates a GridSample operator node.
 *
 * @param[in] mode Interpolation mode.
 * @param[in] paddingMode Padding mode.
 * @param[in] alignCorners Whether to align grid points with corners.
 * @param[in] name Name of the operator.
 * @return Shared pointer to the GridSample node.
 */
std::shared_ptr<Node> GridSample(
                        typename GridSample_Op::Mode mode = GridSample_Op::Mode::Linear,
                        typename GridSample_Op::PaddingMode paddingMode = GridSample_Op::PaddingMode::Zeros,
                        bool alignCorners = false,
                        const std::string& name = "");

} // namespace Aidge

namespace {
template <>
const char* const EnumStrings<Aidge::GridSampleAttr>::data[] = {
    "mode",
    "padding_mode",
    "align_corners"
};
}

#endif /* AIDGE_CORE_OPERATOR_GRIDSAMPLE_H_ */
