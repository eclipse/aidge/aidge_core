/***************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ***************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_DEPTHTOSPACE_H_
#define AIDGE_CORE_OPERATOR_DEPTHTOSPACE_H_

#include <array>
#include <memory>
#include <vector>

#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @class DepthToSpace_OpImpl
 * @brief Implementation of the DepthToSpace operation for rearranging data from depth into spatial dimensions.
 */
class DepthToSpace_OpImpl : public OperatorImpl {
public:
    /**
     * @brief Constructor for the DepthToSpace_OpImpl.
     * @param op Operator containing attributes for DepthToSpace.
     * @param backend The backend used for computation.
     */
    DepthToSpace_OpImpl(const Operator& op, const std::string& backend = "") : OperatorImpl(op, backend) {}

    /**
     * @brief Perform the forward computation for DepthToSpace.
     */
    void forward() override;
};

/**
 * @enum DepthToSpaceAttr
 * @brief Attributes for the DepthToSpace operation.
 */
enum class DepthToSpaceAttr {
    BlockSize, /**< The block size for rearranging depth to spatial dimensions. */
    Mode       /**< The mode for depth-to-space transformation. */
};

/**
 * @class DepthToSpace_Op
 * @brief Represents the DepthToSpace operation to rearrange data from depth to spatial dimensions.
 *
 * This operator splits the depth dimension of a tensor into blocks and distributes them across spatial dimensions.
 *
 * ### Output Shape Calculation
 * Given an input tensor with shape (N, C, H, W):
 * - Block size (B): The depth dimension (C) must be divisible by B^2.
 * - Output shape: (N, C / (B^2), H * B, W * B).
 *
 * ### Modes
 * - `DCR`: Depth-Channel-Row ordering.
 * - `CRD`: Channel-Row-Depth ordering.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class DepthToSpace_Op : public OperatorTensor,
                public Registrable<DepthToSpace_Op,
                    std::string,
                    std::function<std::shared_ptr<OperatorImpl>(const DepthToSpace_Op &)>> {
public:
    /**
     * @brief The type identifier for the DepthToSpace operator.
     */
    static const std::string Type;

    /**
     * @enum Mode
     * @brief Defines the modes for depth-to-space transformation.
     */
    enum class Mode { DCR, CRD };

private:
    using Attributes_ = StaticAttributes<DepthToSpaceAttr, std::uint32_t, Mode>;
    template <DepthToSpaceAttr e>
    using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    DepthToSpace_Op() = delete;

    /**
     * @brief Constructor for DepthToSpace_Op.
     * @param blockSize Size of the blocks to split depth into spatial dimensions.
     * @param mode Depth-to-space transformation mode (DCR or CRD).
     */
    DepthToSpace_Op(const std::uint32_t blockSize, const Mode mode = Mode::CRD);

    /**
     * @brief Copy-constructor.
     * @param op Operator to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    DepthToSpace_Op(const DepthToSpace_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::DepthToSpace_Op
     */
    std::shared_ptr<Operator> clone() const override;

    bool forwardDims(bool /*allowDataDependency*/ = false) override final;

    /**
     * @brief Set the backend for this operator.
     * @param name Backend name.
     * @param device Device index for the backend.
     */
    void setBackend(const std::string &name, DeviceIdx_t device = 0) override final;

    /**
     * @brief Get the available backends for this operator.
     * @return A set of strings representing available backends.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     * @return Shared pointer to the attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get the block size for the DepthToSpace operation.
     * @return Block size.
     */
    inline std::uint32_t& blockSize() const { return mAttributes->template getAttr<DepthToSpaceAttr::BlockSize>(); }

    /**
     * @brief Get the mode of the DepthToSpace operation.
     * @return Depth-to-space mode.
     */
    inline Mode& mode() const { return mAttributes->template getAttr<DepthToSpaceAttr::Mode>(); }

    /**
     * @brief Get the input tensor names.
     * @return A vector of input tensor names.
     */
    static const std::vector<std::string> getInputsName() {
        return {"data_input"};
    }

    /**
     * @brief Get the output tensor names.
     * @return A vector of output tensor names.
     */
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Create a DepthToSpace node.
 * @param blockSize Size of the blocks to split depth into spatial dimensions.
 * @param mode Depth-to-space transformation mode (DCR or CRD).
 * @param name Name of the operator.
 * @return Shared pointer to the created DepthToSpace node.
 */
std::shared_ptr<Node> DepthToSpace(const std::uint32_t blockSize,
                                    const DepthToSpace_Op::Mode mode = DepthToSpace_Op::Mode::CRD,
                                    const std::string& name = "");

}  // namespace Aidge

namespace {
template <>
const char *const EnumStrings<Aidge::DepthToSpaceAttr>::data[] = { "block_size", "mode" };
}

#endif //AIDGE_CORE_OPERATOR_DEPTHTOSPACE_H_
