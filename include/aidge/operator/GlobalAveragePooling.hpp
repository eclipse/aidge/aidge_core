/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_GLOBAL_AVERAGE_POOLING_H_
#define AIDGE_CORE_OPERATOR_GLOBAL_AVERAGE_POOLING_H_

#include <memory>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @brief Description of a Global Average Pooling operation on an input Tensor.
 *
 * Global Average Pooling computes the average value of each feature map (channel) 
 * in the input Tensor across all spatial dimensions (height and width).
 *
 * For each channel in the input Tensor, the function is defined as:
 * `f(x) = (sum(x)) / N`, where `x` represents the spatial elements in that channel,
 * and `N` is the total number of spatial elements (height * width) in the input.
 *
 * The output Tensor retains the batch and channel dimensions, but the spatial dimensions 
 * are reduced to 1. Specifically:
 * - If the input has shape `(N, C, H, W)`, where:
 *   - `N` is the batch size
 *   - `C` is the number of channels
 *   - `H` and `W` are the spatial dimensions
 * - Then the output shape will be `(N, C, 1, 1)`.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class GlobalAveragePooling_Op
    : public OperatorTensor,
      public Registrable<GlobalAveragePooling_Op, std::string,
                         std::function<std::shared_ptr<OperatorImpl>(
                             const GlobalAveragePooling_Op &)>> {
public:
	static const std::string Type;

	GlobalAveragePooling_Op() : OperatorTensor(Type, {InputCategory::Data}, 1) {}

	/**
	 * @brief Copy-constructor.
	 * @param op GlobalAveragePooling_Op to copy.
	 * @details Copies the operator attributes and its output tensor(s), but not
	 * its input tensors. The new operator has no associated input.
	 */
	GlobalAveragePooling_Op(const GlobalAveragePooling_Op &op);

	/**
	 * @brief Clone the operator using its copy-constructor.
	 * @see Operator::GlobalAveragePooling_Op
	 */
	std::shared_ptr<Operator> clone() const override;

	bool forwardDims(bool allowDataDependency = false) override final;

	void setBackend(const std::string &name, DeviceIdx_t device = 0) override final;
	std::set<std::string> getAvailableBackends() const override;

	static const std::vector<std::string> getInputsName() {
	return {"data_input"};
	}
	static const std::vector<std::string> getOutputsName() {
	return {"data_output"};
	}
};

std::shared_ptr<Node> GlobalAveragePooling(const std::string &name = "");

} // namespace Aidge

#endif /* AIDGE_CORE_OPERATOR_GLOBAL_AVERAGE_POOLING_H_ */
