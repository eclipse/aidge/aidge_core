/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_PRODUCER_H_
#define AIDGE_CORE_OPERATOR_PRODUCER_H_

#include <array>
#include <cstddef>
#include <functional>
#include <memory>
#include <set>
#include <string>
#include <stdexcept>
#include <vector>

#include "aidge/utils/Types.h"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Registrar.hpp"

namespace Aidge {

/**
 * @enum ProdAttr
 * @brief Attributes specific to the `Producer_Op` class.
 */
enum class ProdAttr { Constant };

/**
 * @class Producer_Op
 * @brief Represents an operator that stores a tensor in memory and provides it as an output.
 * 
 * The `Producer_Op` class is a specialized operator designed to store a tensor in memory 
 * and return it as an output tensor. It is typically used to store parameters or input 
 * values for a computational graph. A `Producer_Op` does not have any input data, parameters, 
 * or attributes, making it a fundamental building block for constant or initialized values 
 * within the graph.
 * 
 * Key characteristics of a `Producer_Op`:
 * - No inputs: The operator does not accept any input tensors.
 * - No parameters or attributes: It is solely responsible for producing an output tensor.
 * - Stores and returns a tensor: The stored tensor is accessible as the operator's output.
 * 
 * This operator is useful for scenarios where fixed or pre-initialized tensors need to 
 * be introduced into a graph, such as weights, biases, or constant values.
 * 
 * @see OperatorTensor
 * @see Registrable
 */

class Producer_Op
    : public OperatorTensor,
      public Registrable<Producer_Op,
                         std::string,
                         std::function<std::shared_ptr<OperatorImpl>( const Producer_Op& )>>
{
public:
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<ProdAttr, bool>;
    template <ProdAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    Producer_Op() = delete;

    /**
     * @brief Constructs a `Producer_Op` object with specific dimensions.
     * 
     * @tparam DIM The number of dimensions for the tensor.
     * @param[in] dims Array defining the dimensions of the tensor.
     * @param[in] constant Indicates whether the tensor is constant.
     */
    template <std::size_t DIM>
    Producer_Op(const std::array<DimSize_t, DIM>& dims, bool constant = false);

    /**
     * @brief Constructs a `Producer_Op` object from an existing tensor.
     * 
     * @param[in] tensor A shared pointer to the tensor to be produced.
     * @param[in] constant Indicates whether the tensor should be constant.
     */
    Producer_Op(const std::shared_ptr<Tensor> tensor, bool constant = false);

    /**
     * @brief Copy constructor.
     * 
     * Copies the attributes and output tensors of the operator. 
     * Input tensors are not copied, and the new operator will have no associated inputs.
     * 
     * @param[in] op The `Producer_Op` object to copy.
     */
    Producer_Op(const Producer_Op& op);

public:
    /**
     * @brief Conversion operator to retrieve the output tensor.
     * 
     * @return A shared pointer to the output tensor.
     */
    operator std::shared_ptr<Tensor>() const { return mOutputs[0]; }

    /**
     * @brief Clones the operator using the copy constructor.
     * 
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Retrieves the dimensions of the output tensor.
     * 
     * @return A vector containing the dimensions of the output tensor.
     */
    inline const std::vector<DimSize_t> dims() const noexcept { return mOutputs[0]->dims(); }

    /**
     * @brief Sets the backend for the operator's execution.
     * 
     * @param[in] name The name of the backend.
     * @param[in] device The device index (default is 0).
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override;

    /**
     * @brief Retrieves the list of available backends for this operator.
     * 
     * @return A set containing the names of available backends.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Retrieves the operator's attributes.
     * 
     * @return A shared pointer to the operator's attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Retrieves the constant attribute.
     * 
     * @return A reference to the constant attribute.
     */
    inline bool& constant() const { return mAttributes->template getAttr<ProdAttr::Constant>(); }

    /**
     * @brief Performs the forward operation for the operator.
     * 
     * Generates the output tensor based on the defined attributes and configuration.
     */
    void forward() override final;

    /**
     * @brief Placeholder for the backward operation.
     * 
     * This function logs a debug message, as `Producer_Op` typically does not support backpropagation.
     */
    void backward() override final {
        Log::debug("Basic Producer backward() function.");
    }

    /**
     * @brief Associates an input tensor with the operator.
     * 
     * This operation is not supported by `Producer_Op` as it does not take inputs.
     * 
     * @param[in] inputIdx The index of the input.
     * @param[in] data A shared pointer to the data to associate.
     * 
     * @throws std::runtime_error Always throws, as inputs are not supported.
     */
    void associateInput(const IOIndex_t /*inputIdx*/, const std::shared_ptr<Data>& /*data*/) override final {
        AIDGE_THROW_OR_ABORT(std::runtime_error, "Producer operator takes no input.");
    }

    /**
     * @brief Checks whether dimensions are forwarded.
     * 
     * @return Always true for `Producer_Op`.
     */
    inline bool forwardDims(bool /*allowDataDependency*/ = false) override final { return true; }

    /**
     * @brief Confirms that dimensions have been forwarded.
     * 
     * @return Always true for `Producer_Op`.
     */
    inline bool dimsForwarded() const noexcept override final { return true; }

    /**
     * @brief Retrieves the names of the inputs for the operator.
     * 
     * @return An empty vector, as `Producer_Op` takes no inputs.
     */
    static const std::vector<std::string> getInputsName() { return {}; }

    /**
     * @brief Retrieves the names of the outputs for the operator.
     * 
     * @return A vector containing the output name "data_output".
     */
    static const std::vector<std::string> getOutputsName() { return {"data_output"}; }

    /**
     * @brief Sets the output tensor for the operator.
     * 
     * @param[in] outputIdx Index of the output to set.
     * @param[in] data A shared pointer to the data.
     */
    void setOutput(const IOIndex_t outputIdx, const std::shared_ptr<Data>& data) const override;
};

/**
 * @brief Helper function to create a producer node with specified dimensions.
 * 
 * @tparam DIM The number of dimensions.
 * @param[in] dims Array defining the dimensions of the tensor.
 * @param[in] name Optional name for the node.
 * @param[in] constant Indicates whether the tensor should be constant.
 * 
 * @return A shared pointer to the created node.
 */
template <std::size_t DIM>
std::shared_ptr<Node> Producer(const std::array<DimSize_t, DIM>& dims, const std::string& name = "", bool constant = false);

/**
 * @brief Helper function with a C-style array for dimension deduction.
 * 
 * @param[in] dims C-style array defining the tensor dimensions.
 * @param[in] name Optional name for the node.
 * @param[in] constant Indicates whether the tensor should be constant.
 * 
 * @return A shared pointer to the created node.
 */
template <std::size_t DIM>
inline std::shared_ptr<Node> Producer(DimSize_t const (&dims)[DIM], const std::string& name = "", bool constant = false) {
    return Producer(to_array(dims), name, constant);
}
std::shared_ptr<Node> Producer(const std::shared_ptr<Tensor> tensor, const std::string& name = "", bool constant = false);

template <std::array<DimSize_t, 1>::size_type DIM>
std::shared_ptr<Node> addProducer(std::shared_ptr<Node>& otherNode,
            const IOIndex_t inputIdx,
            const std::array<DimSize_t, DIM>& dims,
            const std::string& extension);

/**
 * @brief Adds a producer node to another node with a C-style array.
 * 
 * @param[in] otherNode The node to associate with the producer.
 * @param[in] inputIdx The input index.
 * @param[in] dims C-style array defining the tensor dimensions.
 * @param[in] extension An extension string for the producer.
 * 
 * @return A shared pointer to the updated node.
 */
template <std::size_t DIM>
std::shared_ptr<Node> addProducer(std::shared_ptr<Node>& otherNode, const IOIndex_t inputIdx, DimSize_t const (&dims)[DIM], const std::string& extension) {
    return addProducer(otherNode, inputIdx, to_array(dims), extension);
}

} // namespace Aidge

namespace {
/**
 * @brief Enum string representation for `ProdAttr`.
 */
template <>
const char* const EnumStrings<Aidge::ProdAttr>::data[] = {"constant"};
}

#endif /* AIDGE_CORE_OPERATOR_PRODUCER_H_ */
