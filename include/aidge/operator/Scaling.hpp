/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_SCALING_H_
#define AIDGE_CORE_OPERATOR_SCALING_H_

#include <cstddef>  // std::size_t
#include <vector>
#include <memory>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

// Caution: This operator is now deprecated and should no longer be used. 
// It has been replaced by the MetaOperator "Quantizer" (located directly in aidge_quantization).

namespace Aidge {
enum class ScalingAttr {
    /**
     * @brief Scaling factor applied to the input tensor.
     *
     * This floating-point value is used to scale the input tensor.
     */
    ScalingFactor,

    /**
     * @brief Number of quantization bits.
     *
     * Specifies the bit-width used for quantization. 
     * For example, a value of `8` represents 8-bit quantization.
     */
    QuantizedNbBits,

    /**
     * @brief Indicates whether the output is unsigned.
     *
     * - `true`: The quantized output values are unsigned integers.
     * - `false`: The quantized output values are signed integers.
     */
    IsOutputUnsigned
};

/**
 * @brief Description of a scaling operation to scale and quantize input tensors.
 *
 * The `Scaling_Op` class applies a scaling factor to the input tensor, quantizes 
 * the scaled values to a specified bit-width, and outputs either signed or unsigned integers 
 * based on the configuration.
 *
 * The input and output Tensors have the same dimensions.
 *
 * ### Deprecation Notice
 * This operator is deprecated and has been replaced by the `Quantizer` MetaOperator.
 * It is retained for backward compatibility and should not be used in new implementations.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Scaling_Op
    : public OperatorTensor,
      public Registrable<Scaling_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const Scaling_Op&)>> {

public:
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<ScalingAttr, float, std::size_t, bool>;
    template <ScalingAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    Scaling_Op() = delete;

    /**
     * @brief Constructor for the Scaling operator.
     * @param[in] scalingFactor Scaling factor to be applied to the input tensor.
     * @param[in] nbBits Number of bits for quantization.
     * @param[in] isOutputUnsigned Flag indicating whether the output should be unsigned.
     */
    Scaling_Op(float scalingFactor, std::size_t nbBits, bool isOutputUnsigned);

    /**
     * @brief Copy-constructor.
     * @param[in] op Scaling_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not its input tensors. 
     * The new operator has no associated input.
     */
    Scaling_Op(const Scaling_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     */
    std::shared_ptr<Operator> clone() const override;

    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get the scaling factor.
     */
    inline float& scalingFactor() const noexcept { return mAttributes -> getAttr<ScalingAttr::ScalingFactor>(); }

    /**
     * @brief Get the number of quantization bits.
     */
    inline std::size_t& quantizedNbBits() const noexcept { return mAttributes -> getAttr<ScalingAttr::QuantizedNbBits>(); }

    /**
     * @brief Check if the output is unsigned.
     */
    inline bool& isOutputUnsigned() const noexcept { return mAttributes -> getAttr<ScalingAttr::IsOutputUnsigned>(); }

    static const std::vector<std::string> getInputsName() {
        return {"data_input"};
    }

    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Apply a scaling and quantization operation on a tensor.
 *
 * @param[in] scalingFactor Scaling factor to apply to the input tensor.
 * @param[in] quantizedNbBits Number of bits for quantization.
 * @param[in] isOutputUnsigned Whether the quantized output should be unsigned.
 * @param[in] name Name of the Operator.
 * @return std::shared_ptr<Node> Node containing the Operator.
 */
std::shared_ptr<Node> Scaling(float scalingFactor = 1.0f,
                                     std::size_t quantizedNbBits = 8,
                                     bool isOutputUnsigned = true,
                                     const std::string& name = "");
} // namespace Aidge

namespace {
template <>
const char* const EnumStrings<Aidge::ScalingAttr>::data[]
    = {"scaling_factor", "quantized_nb_bits", "is_output_unsigned"};
}

#endif /* AIDGE_CORE_OPERATOR_SCALING_H_ */
