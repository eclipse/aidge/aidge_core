/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_METAOPERATORDEFS_H_
#define AIDGE_CORE_OPERATOR_METAOPERATORDEFS_H_

#include <array>
#include <memory>
#include <string>

#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/graph/OpArgs.hpp" // Sequential
#include "aidge/operator/AvgPooling.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/ConvDepthWise.hpp"
#include "aidge/operator/MaxPooling.hpp"
#include "aidge/operator/MetaOperator.hpp"
#include "aidge/operator/Pad.hpp"
#include "aidge/operator/Sigmoid.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
/**
 * @brief Creates a padded convolution operation (Conv2D/Conv3D).
 *
 * This function creates a padded convolution operation that applies padding before the convolution operation.
 * It uses various parameters like the number of input/output channels, kernel dimensions, stride, padding, etc.
 *
 * @param[in] in_channels The number of input channels.
 * @param[in] out_channels The number of output channels.
 * @param[in] kernel_dims The dimensions of the convolution kernel.
 * @param[in] name Optional name for the operation.
 * @param[in] stride_dims The stride dimensions for the convolution operation (default is 1).
 * @param[in] padding_dims The padding dimensions to apply before convolution (default is 0).
 * @param[in] dilation_dims Dilation factor for convolution (default is 1).
 * @param[in] no_bias Whether to disable the bias (default is false).
 * @return A shared pointer to the Node representing the padded convolution operation.
 */
template <std::array<DimSize_t, 1>::size_type DIM>
extern std::shared_ptr<Node>
PaddedConv(DimSize_t in_channels,
           DimSize_t out_channels,
           const std::array<DimSize_t, DIM> &kernel_dims,
           const std::string &name = "",
           const std::array<DimSize_t, DIM> &stride_dims =
               create_array<DimSize_t, DIM>(1),
           const std::array<DimSize_t, 2 * DIM> &padding_dims =
               create_array<DimSize_t, 2 * DIM>(0),
           const std::array<DimSize_t, DIM> &dilation_dims =
               create_array<DimSize_t, DIM>(1),
           bool no_bias = false);

/**
 * @brief Creates a padded convolution operation as a MetaOperator.
 *
 * This function creates a graph-based MetaOperator representing a padded convolution operation (Conv2D/Conv3D).
 *
 * @param[in] kernel_dims The dimensions of the convolution kernel.
 * @param[in] stride_dims The stride dimensions for the convolution operation (default is 1).
 * @param[in] padding_dims The padding dimensions to apply before convolution (default is 0).
 * @param[in] dilation_dims Dilation factor for convolution (default is 1).
 * @return A shared pointer to the MetaOperator_Op representing the padded convolution operation.
 */
template <std::array<DimSize_t, 1>::size_type DIM>
extern std::shared_ptr<MetaOperator_Op> PaddedConv_Op(
                                  const std::array<DimSize_t, DIM> &kernel_dims,
                                  const std::array<DimSize_t, DIM> &stride_dims = create_array<DimSize_t,DIM>(1),
                                  const std::array<DimSize_t, 2*DIM> &padding_dims = create_array<DimSize_t,2*DIM>(0),
                                  const std::array<DimSize_t, DIM> &dilation_dims = create_array<DimSize_t,DIM>(1));

// Helper function with C-style array for kernel_dims, enabling automatic template DIM deduction.s
template <DimSize_t DIM>
extern std::shared_ptr<Node>
PaddedConv(DimSize_t in_channels,
           DimSize_t out_channels,
           DimSize_t const (&kernel_dims)[DIM],
           const std::string &name = "",
           const std::array<DimSize_t, DIM> &stride_dims =
               create_array<DimSize_t, DIM>(1),
           const std::array<DimSize_t, 2 * DIM> &padding_dims =
               create_array<DimSize_t, 2 * DIM>(0),
           const std::array<DimSize_t, DIM> &dilation_dims =
               create_array<DimSize_t, DIM>(1),
           bool no_bias = false);

////////////////////////////////////////////////////////////////////////////////

/**
 * @brief Creates a padded depthwise convolution operation (Conv2DDepthwise/Conv3DDepthwise).
 *
 * This function creates a depthwise convolution operation with padding, where each input channel has its own filter.
 *
 * @param[in] nb_channels The number of input/output channels (same for depthwise convolution).
 * @param[in] kernel_dims The dimensions of the convolution kernel.
 * @param[in] name Optional name for the operation.
 * @param[in] stride_dims The stride dimensions for the convolution operation (default is 1).
 * @param[in] padding_dims The padding dimensions to apply before convolution (default is 0).
 * @param[in] dilation_dims Dilation factor for convolution (default is 1).
 * @param[in] no_bias Whether to disable the bias (default is false).
 * @return A shared pointer to the Node representing the padded depthwise convolution operation.
 */
template <std::array<DimSize_t, 1>::size_type DIM>
std::shared_ptr<Node>
PaddedConvDepthWise(const DimSize_t nb_channels,
                    const std::array<DimSize_t, DIM> &kernel_dims,
                    const std::string &name = "",
                    const std::array<DimSize_t, DIM> &stride_dims =
                        create_array<DimSize_t, DIM>(1),
                    const std::array<DimSize_t, 2 * DIM> &padding_dims =
                        create_array<DimSize_t, 2 * DIM>(0),
                    const std::array<DimSize_t, DIM> &dilation_dims =
                        create_array<DimSize_t, DIM>(1),
                    bool no_bias = false);

/**
 * @brief Creates a padded depthwise convolution operation as a MetaOperator.
 *
 * This function creates a graph-based MetaOperator representing a padded depthwise convolution operation.
 *
 * @param[in] kernel_dims The dimensions of the convolution kernel.
 * @param[in] stride_dims The stride dimensions for the convolution operation (default is 1).
 * @param[in] padding_dims The padding dimensions to apply before convolution (default is 0).
 * @param[in] dilation_dims Dilation factor for convolution (default is 1).
 * @return A shared pointer to the MetaOperator_Op representing the padded depthwise convolution operation.
 */
template <std::array<DimSize_t, 1>::size_type DIM>
std::shared_ptr<MetaOperator_Op> PaddedConvDepthWise_Op(
                                  const std::array<DimSize_t, DIM> &kernel_dims,
                                  const std::array<DimSize_t, DIM> &stride_dims = create_array<DimSize_t,DIM>(1),
                                  const std::array<DimSize_t, 2*DIM> &padding_dims = create_array<DimSize_t,2*DIM>(0),
                                  const std::array<DimSize_t, DIM> &dilation_dims = create_array<DimSize_t,DIM>(1));

// Helper function for depthwise convolution, with C-style array to automatically deduce DIM.
template <DimSize_t DIM>
inline std::shared_ptr<Node>
PaddedConvDepthWise(const DimSize_t nb_channels,
                    DimSize_t const (&kernel_dims)[DIM],
                    const std::string &name = "",
                    const std::array<DimSize_t, DIM> &stride_dims =
                        create_array<DimSize_t, DIM>(1),
                    const std::array<DimSize_t, 2 * DIM> &padding_dims =
                        create_array<DimSize_t, 2 * DIM>(0),
                    const std::array<DimSize_t, DIM> &dilation_dims =
                        create_array<DimSize_t, DIM>(1),
                    bool no_bias = false);

////////////////////////////////////////////////////////////////////////////////

/**
 * @brief Creates a padded average pooling operation.
 *
 * This function creates an average pooling operation with padding before pooling.
 *
 * @param[in] kernel_dims The dimensions of the pooling window.
 * @param[in] name Optional name for the operation.
 * @param[in] stride_dims The stride dimensions for pooling (default is 1).
 * @param[in] padding_dims Padding dimensions before pooling (default is 0).
 * @return A shared pointer to the Node representing the padded average pooling operation.
 */
template <std::array<DimSize_t, 1>::size_type DIM>
extern std::shared_ptr<Node> PaddedAvgPooling(const std::array<DimSize_t, DIM> &kernel_dims,
                                  const std::string& name = "",
                                  const std::array<DimSize_t, DIM> &stride_dims = create_array<DimSize_t,DIM>(1),
                                  const std::array<DimSize_t, 2*DIM> &padding_dims = create_array<DimSize_t,2*DIM>(0));

/**
 * @brief Creates a padded average pooling operation as a MetaOperator.
 *
 * This function creates a graph-based MetaOperator representing a padded average pooling operation.
 *
 * @param[in] kernel_dims The dimensions of the pooling window.
 * @param[in] stride_dims The stride dimensions for pooling (default is 1).
 * @param[in] padding_dims Padding dimensions before pooling (default is 0).
 * @return A shared pointer to the MetaOperator_Op representing the padded average pooling operation.
 */
template <std::array<DimSize_t, 1>::size_type DIM>
extern std::shared_ptr<MetaOperator_Op> PaddedAvgPooling_Op(const std::array<DimSize_t, DIM> &kernel_dims,
                                  const std::array<DimSize_t, DIM> &stride_dims = create_array<DimSize_t,DIM>(1),
                                  const std::array<DimSize_t, 2*DIM> &padding_dims = create_array<DimSize_t,2*DIM>(0));

// Helper function for average pooling with C-style array for kernel_dims, enabling automatic DIM deduction.
template <DimSize_t DIM>
extern std::shared_ptr<Node>
PaddedAvgPooling(DimSize_t const (&kernel_dims)[DIM],
                 const std::string &name = "",
                 const std::array<DimSize_t, DIM> &stride_dims =
                     create_array<DimSize_t, DIM>(1),
                 const std::array<DimSize_t, 2 * DIM> &padding_dims =
                     create_array<DimSize_t, 2 * DIM>(0));

////////////////////////////////////////////////////////////////////////////////

/**
 * @brief Creates a padded max pooling operation.
 *
 * This function creates a max pooling operation with padding before pooling.
 *
 * @param[in] kernel_dims The dimensions of the pooling window.
 * @param[in] name Optional name for the operation.
 * @param[in] stride_dims The stride dimensions for pooling (default is 1).
 * @param[in] padding_dims Padding dimensions before pooling (default is 0).
 * @param[in] ceil_mode Whether to use ceiling mode for pooling (default is false).
 * @return A shared pointer to the Node representing the padded max pooling operation.
 */
template <std::array<DimSize_t, 1>::size_type DIM>
inline std::shared_ptr<Node> PaddedMaxPooling(const std::array<DimSize_t, DIM> &kernel_dims,
                                  const std::string& name = "",
                                  const std::array<DimSize_t, DIM> &stride_dims = create_array<DimSize_t,DIM>(1),
                                  const std::array<DimSize_t, 2*DIM> &padding_dims = create_array<DimSize_t,2*DIM>(0),
                                  bool ceil_mode = false) {
    auto graph = Sequential({
        Pad<DIM>(padding_dims, (!name.empty()) ? name + "_pad" : ""),
        MaxPooling(kernel_dims, (!name.empty()) ? name + "_maxpooling" : "", stride_dims, ceil_mode)
    });

    return MetaOperator(("PaddedMaxPooling" + std::to_string(DIM) + "D").c_str(), graph, {}, name);
}

/**
 * @brief Creates a padded max pooling operation as a MetaOperator.
 *
 * This function creates a graph-based MetaOperator representing a padded max pooling operation.
 *
 * @param[in] kernel_dims The dimensions of the pooling window.
 * @param[in] stride_dims The stride dimensions for pooling (default is 1).
 * @param[in] padding_dims Padding dimensions before pooling (default is 0).
 * @param[in] ceil_mode Whether to use ceiling mode for pooling (default is false).
 * @return A shared pointer to the MetaOperator_Op representing the padded max pooling operation.
 */
template <std::array<DimSize_t, 1>::size_type DIM>
inline std::shared_ptr<MetaOperator_Op> PaddedMaxPooling_Op(const std::array<DimSize_t, DIM> &kernel_dims,
                                  const std::array<DimSize_t, DIM> &stride_dims = create_array<DimSize_t,DIM>(1),
                                  const std::array<DimSize_t, 2*DIM> &padding_dims = create_array<DimSize_t,2*DIM>(0),
                                  bool ceil_mode = false) {
    auto graph = Sequential({
        Pad<DIM>(padding_dims, ""),
        MaxPooling(kernel_dims, "", stride_dims, ceil_mode)
    });
    return std::make_shared<MetaOperator_Op>(("PaddedMaxPooling" + std::to_string(DIM) + "D").c_str(), graph);
}

// Helper function for max pooling with C-style array to automatically deduce DIM.
template <DimSize_t DIM>
inline std::shared_ptr<Node> PaddedMaxPooling(
    DimSize_t const (&kernel_dims)[DIM],
    const std::string& name = "",
    const std::array<DimSize_t, DIM> &stride_dims = create_array<DimSize_t,DIM>(1),
    const std::array<DimSize_t, 2*DIM> &padding_dims = create_array<DimSize_t,2*DIM>(0),
    bool ceil_mode= false) {
    return PaddedMaxPooling(to_array(kernel_dims), name, stride_dims, padding_dims, ceil_mode);
}

/**
 * @brief Creates an LSTM (Long Short-Term Memory) operation as a MetaOperator.
 *
 * This function creates an LSTM operation as a MetaOperator for use in graph-based computation.
 *
 * @param[in] seq_length The length of the input sequence.
 * @return A shared pointer to the MetaOperator_Op representing the LSTM operation.
 */
std::shared_ptr<MetaOperator_Op> LSTM_Op(DimSize_t seq_length,
                                         const std::string &name = "");

/**
 * @brief Creates an LSTM (Long Short-Term Memory) operator.
 *
 * This function creates an LSTM operation which is a popular recurrent neural network (RNN) layer for sequence processing.
 *
 * @param[in] in_channels The number of input channels.
 * @param[in] hidden_channels The number of hidden channels in the LSTM.
 * @param[in] seq_length The length of the input sequence.
 * @param[in] noBias Whether to disable the bias (default is false).
 * @param[in] name Optional name for the operation.
 * @return A shared pointer to the Node representing the LSTM operation.
 */
std::shared_ptr<Node> LSTM(DimSize_t in_channels,
                           DimSize_t hidden_channels,
                           DimSize_t seq_length,
                           bool noBias = false,
                           const std::string &name = "");

std::shared_ptr<MetaOperator_Op> LeakyOp();
std::shared_ptr<Node> Leaky(const int nbTimeSteps,
                            const float beta,
                            const float threshold = 1.0,
                            const std::string &name = "");

} // namespace Aidge

#endif /* AIDGE_CORE_OPERATOR_METAOPERATORDEFS_H_ */
