/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_GENERICOPERATOR_H_
#define AIDGE_CORE_OPERATOR_GENERICOPERATOR_H_

#include <memory>
#include <vector>
#include <string>

#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/DynamicAttributes.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @class GenericOperator_Op
 * @brief Represents a generic operator used as a fallback when the requested operator is not natively supported by Aidge.
 *
 * This class enables the import and simulation of unknown or custom operators. 
 * It provides a flexible structure for defining operators dynamically with attributes, inputs, and outputs.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class GenericOperator_Op
    : public OperatorTensor,
      public Registrable<GenericOperator_Op, std::array<std::string, 2>, std::function<std::shared_ptr<OperatorImpl>(const GenericOperator_Op &)>> {
private:
    using ComputeDimsFunc = std::function<std::vector<std::vector<size_t>>(const std::vector<std::vector<size_t>>&)>;

    /// Function to compute output dimensions based on input dimensions.
    ComputeDimsFunc mForwardDims;

    /// Dynamic attributes associated with the operator.
    const std::shared_ptr<DynamicAttributes> mAttributes;

public:

    /**
     * @brief Constructs a generic operator with input categories and output count.
     * @param[in] type Type of the operator (e.g., the operator name or key).
     * @param[in] inputsCategory Vector specifying the categories of inputs.
     * @param[in] nbOut Number of output tensors produced by the operator.
     */
    GenericOperator_Op(const std::string& type, const std::vector<InputCategory>& inputsCategory, IOIndex_t nbOut);

    /**
     * @brief Constructs a generic operator with specified data, parameters, and output counts.
     * @param[in] type Type of the operator.
     * @param[in] nbData Number of data inputs.
     * @param[in] nbParam Number of parameter inputs.
     * @param[in] nbOut Number of output tensors.
     */
    GenericOperator_Op(const std::string& type, IOIndex_t nbData, IOIndex_t nbParam, IOIndex_t nbOut);

    /**
     * @brief Copy-constructor.
     * @param[in] op Operator to copy.
     * @details Copies the operator attributes and its output tensor(s), but does not associate input tensors with the new operator.
     */
    GenericOperator_Op(const GenericOperator_Op& op);

    /**
     * @brief Destructor.
     */
    ~GenericOperator_Op() noexcept;

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::GenericOperator_Op
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override;

public:

    /**
     * @brief Computes the output dimensions of the operator.
     * @param[in] allowDataDependency If true, allows computations based on data dependencies.
     * @return True if dimensions are successfully computed, otherwise false.
     */
    bool forwardDims(bool allowDataDependency = false) override final;

    /**
     * @brief Sets the backend for the operator.
     * @param[in] name Backend name.
     * @param[in] device Device index for the backend.
     */
    void setBackend(const std::string &name, DeviceIdx_t device = 0) override;

    /**
     * @brief Retrieves the available backends for the operator.
     * @return A set of available backend names.
     */
    std::set<std::string> getAvailableBackends() const override { return std::set<std::string>(); }

    /**
     * @brief Retrieves the attributes of the operator.
     * @return A shared pointer to the attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Retrieves a specific attribute by name.
     * @tparam T Expected type of the attribute.
     * @param[in] name Name of the attribute.
     * @return Reference to the attribute value.
     */
    template <class T>
    inline T& getAttr(const std::string& name) { return mAttributes->template getAttr<T>(name); }

    /**
     * @brief Retrieves a specific attribute by name (const version).
     * @tparam T Expected type of the attribute.
     * @param[in] name Name of the attribute.
     * @return Value of the attribute.
     */
    template <class T>
    inline T getAttr(const std::string& name) const { return mAttributes->template getAttr<T>(name); }

    /**
     * @brief Adds a new attribute or asserts if it already exists.
     * @tparam T Expected type of the attribute.
     * @param[in] name Name of the attribute.
     * @param[in] value Value to set for the attribute.
     */
    template <class T>
    inline void addAttr(const std::string& name, const T& value) const { mAttributes->template addAttr<T>(name, value); }

    /**
     * @brief Sets multiple attributes at once.
     * @param[in] attrs A map of attribute names to values.
     */
    inline void setAttrs(const std::map<std::string, future_std::any>& attrs) {
        *mAttributes = attrs;
    }

    // Helper functions that can be used with setForwardDims():
    /**
     * @brief Identity function for forward dimension computation.
     */
    static const ComputeDimsFunc Identity;

    /**
     * @brief Function to copy input dimensions to output dimensions.
     * @param[in] inputIdx Index of the input whose dimensions are to be copied.
     * @param[in] nbOutputs Number of outputs to generate.
     * @return A ComputeDimsFunc instance.
     */
    static const ComputeDimsFunc InputIdentity(IOIndex_t inputIdx, IOIndex_t nbOutputs);

    /**
     * @brief Sets the forward dimension computation function.
     * @param[in] func The function to compute output dimensions.
     */
    inline void setForwardDims(ComputeDimsFunc func) { mForwardDims = func; }
};

/**
 * @brief Creates a generic operator with input categories and output count.
 * @param[in] type Type of the operator.
 * @param[in] inputCategory List of input categories.
 * @param[in] nbOut Number of output tensors.
 * @param[in] name Optional name for the operator.
 * @return A shared pointer to the created operator node.
 */
std::shared_ptr<Node> GenericOperator(const std::string& type, const std::vector<InputCategory>& inputCategory, IOIndex_t nbOut,
                                             const std::string& name = "");

/**
 * @brief Creates a generic operator with specified input and output counts.
 * @param[in] type Type of the operator.
 * @param[in] nbData Number of input data tensors.
 * @param[in] nbParam Number of parameter tensors.
 * @param[in] nbOut Number of output tensors.
 * @param[in] name Optional name for the operator.
 * @return A shared pointer to the created operator node.
 */
std::shared_ptr<Node> GenericOperator(const std::string& type, IOIndex_t nbData, IOIndex_t nbParam, IOIndex_t nbOut,
                                             const std::string& name = "");

/**
 * @brief Creates a generic operator based on another operator.
 * @param[in] type Type of the generic operator.
 * @param[in] op Existing operator to derive from.
 * @param[in] name Optional name for the operator.
 * @return A shared pointer to the created operator node.
 */
std::shared_ptr<Aidge::Node> GenericOperator(const std::string& type,
                                            std::shared_ptr<OperatorTensor> op,
                                            const std::string& name = "");
}  // namespace Aidge

#endif /* AIDGE_CORE_OPERATOR_GENERICOPERATOR_H_ */
