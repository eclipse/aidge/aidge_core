/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_POP_H_
#define AIDGE_CORE_OPERATOR_POP_H_

#include <memory>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @class Pop_ProdConso
 * @brief Implements the producer-consumer principle for the `Pop` operator.
 *
 * The `Pop_ProdConso` class defines the logic for managing data dependencies during
 * the forward process of the `Pop` operator.
 *
 * This class ensures that:
 * - All data consumed by the `Pop` operator is correctly handled.
 * - The operator respects memory and data requirements during the forward computation.
 */
class Pop_ProdConso : public ProdConso {
public:
    /**
     * @brief Constructor for the `Pop_ProdConso` class.
     * @param[in] op The operator instance for which producer-consumer relationships are managed.
     *
     * @details:
     * - The provided `Operator` instance is used to initialize the base `ProdConso` class.
     * - This operator determines specific requirements for data consumption during the forward process.
     */
    Pop_ProdConso(const Operator& op): ProdConso(op) {}

    /**
     * @brief Get the number of data elements required from an input tensor for forward computation.
     * @param[in] inputIdx The index of the input tensor.
     * @return The number of required elements (`Elts_t`).
     *
     * @details:
     * - For each input tensor identified by `inputIdx`, this method calculates the
     *   minimum amount of data needed by the `Pop` operator to perform its forward step.
     */
    Elts_t getNbRequiredData(const IOIndex_t inputIdx) const override;
};

/**
 * @class Pop_OpImpl
 * @brief Implementation of the `Pop` operation.
 *
 * The `Pop_OpImpl` class defines the backend-agnostic logic for executing
 * the forward pass of the `Pop` operator.
 */
class Pop_OpImpl : public OperatorImpl {
public:
    /**
     * @brief Constructs a `Pop_OpImpl` object.
     * @param[in] op The operator to be implemented.
     * @param[in] backend The backend used for execution (optional).
     */
    Pop_OpImpl(const Operator& op, const std::string& backend = ""): OperatorImpl(op, backend) {}

    /**
     * @brief Get the Producer Consumer object of the operator.
     * @return A shared pointer to the `Pop_ProdConso` object.
     */
    std::shared_ptr<ProdConso> getProdConso() const override { return std::make_shared<Pop_ProdConso>(mOp); }

    /**
     * @brief Executes the forward pass for the `Pop` operation.
     */
    void forward() override;
};

/**
 * @enum PopAttr
 * @brief Attributes specific to the `Pop` operator.
 */
enum class PopAttr {
    ForwardStep     // Tracks the current step in the forward pass
};

/**
 * @class Pop_Op
 * @brief The `Pop` operator is responsible for removing and outputting elements from a data structure.
 *
 * `Pop` operators are used in models that manipulate sequential data or stacks.
 * - **Inputs**:
 *   - `data_input`: The main data input from which elements are removed.
 * - **Outputs**:
 *   - `data_output`: The output after performing the `Pop` operation.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Pop_Op : public OperatorTensor,
    public Registrable<Pop_Op, std::string, std::function<std::unique_ptr<OperatorImpl>(const Pop_Op&)>> {
public:
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<PopAttr, std::uint32_t>;
    template <PopAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    /**
     * @brief Default constructor for the `Pop` operator.
     */
    Pop_Op();

    /**
     * @brief Copy-constructor. Copies the operator's attributes and its output tensors.
     * The new operator has no input tensors associated initially.
     * @param op Operator to copy.
     */
    Pop_Op(const Pop_Op& op);

    /**
     * @brief Clone the operator by creating a copy of it.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Assign a specific backend and device for computation.
     * @param name Name of the backend.
     * @param device The device index (default is 0).
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;

    /**
     * @brief Get the list of available backends compatible with this operator.
     * @return A set of strings representing backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Perform dimension inference for the operator, optionally allowing
     * data dependency during the process.
     * @param allowDataDependency Flag indicating whether data dependency is allowed.
     * @return True if dimensions were successfully inferred.
     */
    bool forwardDims(bool allowDataDependency = false) override final;

    /**
     * @brief Update the producer-consumer relationships for the `Pop` operator.
     * @details:
     * - Ensures all required outputs are properly handled during the forward pass.
     */
    void updateConsummerProducer() override;

    /**
     * @brief Executes the forward pass for the `Pop` operation.
     */
    void forward() override;

    /**
     * @brief Access the operator's attributes.
     * @return A shared pointer to the operator's attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get or set the forward step counter for the operator.
     * @return A reference to the forward step counter.
     */
    inline std::uint32_t& forwardStep() const { return mAttributes->template getAttr<PopAttr::ForwardStep>(); }

    /**
     * @brief Retrieve the names of the operator's input tensors.
     * @return A vector of strings representing input tensor names.
     */
    static const std::vector<std::string> getInputsName() {
        return {"data_input"};
    }

    /**
     * @brief Retrieve the names of the operator's output tensors.
     * @return A vector of strings representing output tensor names.
     */
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Create a `Pop` operator node.
 * @param name The optional name for the node.
 * @return A shared pointer to the newly created `Pop` operator node.
 */
std::shared_ptr<Node> Pop(const std::string& name = "");
}  // namespace Aidge

namespace {
/**
 * @brief String representations of the `Pop` operator's attributes.
 */
template <>
const char *const EnumStrings<Aidge::PopAttr>::data[] = {
    "forward_step"
};
}

#endif /* AIDGE_CORE_OPERATOR_POP_H_ */
