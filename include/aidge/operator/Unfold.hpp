/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_UNFOLD_H_
#define AIDGE_CORE_OPERATOR_UNFOLD_H_

#include <array>
#include <cmath>
#include <cstddef>
#include <string>
#include <utility>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
/**
 * @brief Implementation of the Unfold operator.
 * @tparam DIM Number of dimensions in the operation.
 */
template <DimIdx_t DIM>
class Unfold_OpImpl : public OperatorImpl {
public:
    /**
     * @brief Constructor for Unfold_OpImpl.
     * @param[in] op The Operator instance.
     * @param[in] backend The backend name (optional).
     */
    Unfold_OpImpl(const Operator& op, const std::string& backend = "")
        : OperatorImpl(op, backend) {}

    /**
     * @brief Perform the forward operation for the unfold.
     */
    void forward() override;
};

/**
 * @enum UnfoldAttr
 * @brief Enumeration of attributes specific to the Unfold operator.
 */
enum class UnfoldAttr {
    /**
     * @brief Stride dimensions for the unfolding operation.
     */
    StrideDims,

    /**
     * @brief Dilation dimensions for the unfolding operation.
     */
    DilationDims,

    /**
     * @brief Kernel dimensions for the unfolding operation.
     */
    KernelDims
};

/**
 * @brief Describes the operation of unfolding a tensor into sliding blocks.
 * 
 * The Unfold operator extracts sliding blocks from the input tensor along
 * specified dimensions, controlled by stride, dilation, and kernel size.
 * 
 * @tparam DIM Number of dimensions involved in the operation.
 *
 * @example Input: Tensor of dimensions `[1, 3, 32, 32]`, with `KernelDims = {3, 3}`,
 *   `StrideDims = {1, 1}`, and `DilationDims = {1, 1}` results in a tensor of blocks
 *   extracted from the input with overlapping areas.
 *
 * @see OperatorTensor
 * @see Registrable
 */
template <DimIdx_t DIM>
class Unfold_Op : public OperatorTensor,
                  public Registrable<Unfold_Op<DIM>, std::string, std::function<std::shared_ptr<OperatorImpl>(const Unfold_Op<DIM>&)>> {
public:
    /**
     * @brief Static type string for the Unfold operator.
     */
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<UnfoldAttr,
                                         std::array<DimSize_t, DIM>,
                                         std::array<DimSize_t, DIM>,
                                         std::array<DimSize_t, DIM>>;
    template <UnfoldAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    /**
     * @brief Deleted default constructor.
     */
    Unfold_Op() = delete;

    /**
     * @brief Constructor for the Unfold operator.
     * @param[in] kernelDims Size of the sliding window for each dimension.
     * @param[in] strideDims Step size for moving the window (optional).
     * @param[in] dilationDims Spacing between elements in the kernel (optional).
     */
    Unfold_Op(const std::array<DimSize_t, DIM>& kernelDims,
              const std::array<DimSize_t, DIM>& strideDims = create_array<DimSize_t, DIM>(1),
              const std::array<DimSize_t, DIM>& dilationDims = create_array<DimSize_t, DIM>(1));

    /**
     * @brief Copy-constructor.
     * @param op Unfold_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    Unfold_Op(const Unfold_Op<DIM>& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Compute the output dimensions during the forward pass.
     * @param[in] allowDataDependency Whether to allow data-dependent dimensions.
     * @return Boolean indicating whether dimension computation was successful.
     */
    bool forwardDims(bool /*allowDataDependency*/ = false) override final;

    /**
     * @brief Set the backend for the Unfold operator.
     * @param[in] name Name of the backend.
     * @param[in] device Device index (optional).
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override;

    /**
     * @brief Get the available backends for the Unfold operator.
     * @return A set of backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     * @return A shared pointer to the attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get or modify the stride dimensions.
     * @return Reference to the stride dimensions attribute.
     */
    inline std::array<DimSize_t, DIM>& strideDims() const {
        return mAttributes->template getAttr<UnfoldAttr::StrideDims>();
    }

    /**
     * @brief Get or modify the dilation dimensions.
     * @return Reference to the dilation dimensions attribute.
     */
    inline std::array<DimSize_t, DIM>& dilationDims() const {
        return mAttributes->template getAttr<UnfoldAttr::DilationDims>();
    }

    /**
     * @brief Get or modify the kernel dimensions.
     * @return Reference to the kernel dimensions attribute.
     */
    inline std::array<DimSize_t, DIM>& kernelDims() const {
        return mAttributes->template getAttr<UnfoldAttr::KernelDims>();
    }

    /**
     * @brief Get the input tensor names for the Unfold operator.
     * @return A vector of input tensor names.
     */
    static const std::vector<std::string> getInputsName() {
        return {"data_input"};
    }

    /**
     * @brief Get the output tensor names for the Unfold operator.
     * @return A vector of output tensor names.
     */
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Create an Unfold operation node.
 *
 * @param[in] kernelDims Size of the sliding window.
 * @param[in] name Name of the operator (optional).
 * @param[in] strideDims Step size for moving the window (optional).
 * @param[in] dilationDims Spacing between elements in the kernel (optional).
 * @return A shared pointer to the Node containing the Unfold operator.
 */
template <std::array<DimSize_t, 1>::size_type DIM>
std::shared_ptr<Node> Unfold(const std::array<DimSize_t, DIM> &kernelDims,
                            const std::string& name = "",
                            const std::array<DimSize_t, DIM> &strideDims = create_array<DimSize_t,DIM>(1),
                            const std::array<DimSize_t, DIM> &dilationDims = create_array<DimSize_t,DIM>(1));

template <DimSize_t DIM>
inline std::shared_ptr<Node> Unfold( DimSize_t const (&kernelDims)[DIM],
                                    const std::string& name = "",
                                    const std::array<DimSize_t, DIM> &strideDims = create_array<DimSize_t,DIM>(1),
                                    const std::array<DimSize_t, DIM> &dilationDims = create_array<DimSize_t,DIM>(1)) {
                                    static_assert(DIM<=MaxDim,"Too many kernel dimensions required by Unfold, not supported");
    return Unfold(to_array(kernelDims), name, strideDims, dilationDims);
}

}  // namespace Aidge

extern template class Aidge::Unfold_Op<2>;

namespace {
/**
 * @brief EnumStrings specialization for UnfoldAttr.
 */
template <>
const char* const EnumStrings<Aidge::UnfoldAttr>::data[] = {
    "stride_dims",
    "dilation_dims",
    "kernel_dims"
};
}

#endif /* AIDGE_CORE_OPERATOR_UNFOLD_H_ */
