/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_ROUND_H_
#define AIDGE_CORE_OPERATOR_ROUND_H_

#include <memory>
#include <vector>
#include <string>

#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @brief Description of an element-wise Round operation on an input Tensor.
 *
 * For each element x in the input, the function is defined as:
 * `f(x) = round(x)`, where round(x) rounds x to the nearest integer value. 
 * Halfway cases are rounded away from zero.
 *
 * The input and output Tensors have the same dimensions.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Round_Op : public OperatorTensor,
                public Registrable<Round_Op,  // <Op, backend, implementation creation function>
                                std::string,
                                std::function<std::shared_ptr<OperatorImpl>(const Round_Op&)>>
{
public:
    static const std::string Type;

    Round_Op() : OperatorTensor(Type, {InputCategory::Data}, 1) {}

    /**
     * @brief Copy-constructor.
     * @param op Round_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    Round_Op(const Round_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::Round_Op
     */
    std::shared_ptr<Operator> clone() const override;

    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;
    std::set<std::string> getAvailableBackends() const override;
    static const std::vector<std::string> getInputsName(){
        return {"data_input"};
    }
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }
};

std::shared_ptr<Node> Round(const std::string& name = "");
}


#endif /* AIDGE_CORE_OPERATOR_ROUND_H_ */
