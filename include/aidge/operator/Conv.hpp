/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_CONV_H_
#define AIDGE_CORE_OPERATOR_CONV_H_

#include <array>
#include <cmath>    // std::floor
#include <cstddef>  // std::size_t
#include <string>
#include <utility>  // std::pair
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp" // SET_IMPL_MACRO
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @enum ConvAttr
 * @brief Attributes used for the Convolution operation.
 */
enum class ConvAttr {
    StrideDims,     // The stride dimensions
    DilationDims,   // The dilation dimensions
    KernelDims      // The kernel dimensions
};

/**
 * @class Conv_Op
 * @brief Convolution operator for performing a multi-dimensional convolution.
 * 
 * The Conv_Op class implements a convolution operator for tensors with customizable 
 * kernel dimensions, stride, and dilation values. The operator performs a convolution 
 * operation on the input tensor and produces an output tensor.
 * 
 * ### Attributes:
 * - `strideDims`: Stride for each dimension of the input.
 * - `dilationDims`: Dilation for each dimension of the input.
 * - `kernelDims`: Kernel size for each dimension.
 *
 * The output shape will depend on the kernel size, stride, and dilation values:
 *    Output Dimension=((Input Dimension−Kernel Dimension+2×Padding​)/Stride)+1
 *
 * @example: Basic 2D Convolution
 *      - Input shape: (1, 3, 32, 32)
 *      - Kernel dimensions: {3, 3} (2D kernel)
 *      - Stride dimensions: {1, 1} (stride of 1 in both height and width)
 *      - Dilation dimensions: {1, 1} (no dilation)
 *      - Padding: None
 *      - Output shape: 
 *         (1, 64, (32−3+2×0)/1+1, (32−3+2×0)/1+1) = (1, 64, 30, 30)
 *
 * @see OperatorTensor
 * @see Registrable
 */
template <DimIdx_t DIM>
class Conv_Op : public OperatorTensor,
                public Registrable<Conv_Op<DIM>, std::string, std::function<std::shared_ptr<OperatorImpl>(const Conv_Op<DIM> &)>> {

public:
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<ConvAttr,
                                        std::array<DimSize_t, DIM>,
                                        std::array<DimSize_t, DIM>,
                                        std::array<DimSize_t, DIM>>;

    template <ConvAttr e>
    using attr = typename Attributes_::template attr<e>;

    const std::shared_ptr<Attributes_> mAttributes;

public:
    Conv_Op() = delete;

    /**
     * @brief Constructor for the Conv_Op operator.
     * @param[in] kernelDims The dimensions of the kernel.
     * @param[in] strideDims The stride dimensions (default is an array of ones).
     * @param[in] dilationDims The dilation dimensions (default is an array of ones).
     */
    constexpr Conv_Op(const std::array<DimSize_t, DIM> &kernelDims,
                      const std::array<DimSize_t, DIM> &strideDims = create_array<DimSize_t,DIM>(1),
                      const std::array<DimSize_t, DIM> &dilationDims = create_array<DimSize_t,DIM>(1))
        : OperatorTensor(Type, {InputCategory::Data, InputCategory::Param, InputCategory::OptionalParam}, 1),
          mAttributes(std::make_shared<Attributes_>(
            attr<ConvAttr::StrideDims>(strideDims),
            attr<ConvAttr::DilationDims>(dilationDims),
            attr<ConvAttr::KernelDims>(kernelDims)))
    {}

    /**
     * @brief Copy-constructor.
     * @param op The Conv_Op to copy.
     * @details This constructor copies the operator attributes and its output tensors, but not the input tensors.
     */
    Conv_Op(const Conv_Op<DIM>& op);

    /**
     * @brief Clone the operator using its copy constructor.
     * @return A shared pointer to the cloned Conv_Op object.
     */
    std::shared_ptr<Operator> clone() const override {
        return std::make_shared<Conv_Op<DIM>>(*this);
    }


    /**
     * @brief Compute forward dimensions for the operator.
     * @param allowDataDependency Flag to allow data dependency in dimension calculation.
     * @return true if the dimensions are computed successfully.
     */
    bool forwardDims(bool /*allowDataDependency*/ = false) override final;

    /**
     * @brief Calculate the receptive field of the convolution operation.
     * @param firstEltDims The dimensions of the first element.
     * @param outputDims The dimensions of the output tensor.
     * @param outputIdx Index of the output tensor.
     * @return A pair containing the receptive field dimensions.
     */
    std::vector<std::pair<std::vector<DimSize_t>, std::vector<DimSize_t>>>
    computeReceptiveField(const std::vector<DimSize_t>& firstEltDims,
                          const std::vector<DimSize_t>& outputDims,
                          const IOIndex_t outputIdx = 0) const override;

    /**
     * @brief Set the backend for the operator.
     * @param name The name of the backend.
     * @param device The device index (default is 0).
     */
    void setBackend(const std::string &name, DeviceIdx_t device = 0) override;

    /**
     * @brief Get the list of available backends for the operator.
     * @return A set of available backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the number of input channels.
     * @return The number of input channels.
     * @throws std::runtime_error If the operator has no associated weight tensor.
     */
    DimSize_t inChannels() const {
        if (!getInput(1)) {
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Convolution operator has no weight Tensor associated so no specific number of input channel imposed.");
        }
        return getInput(1)->template dims<DIM+2>()[1];
    }

    /**
     * @brief Get the number of output channels.
     * @return The number of output channels.
     * @throws std::runtime_error If the operator has no associated weight tensor.
     */
    DimSize_t outChannels() const {
        if (!getInput(1)) {
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Convolution operator has no weight Tensor associated so no specific number of output channel imposed.");
        }
        return getInput(1)->template dims<DIM+2>()[0];
    }

    /**
     * @brief Get the attributes of the operator.
     * @return A shared pointer to the operator's attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get the stride dimensions.
     * @return The stride dimensions as a reference.
     */
    inline std::array<DimSize_t, DIM>& strideDims() const { return mAttributes->template getAttr<ConvAttr::StrideDims>(); }

    /**
     * @brief Get the dilation dimensions.
     * @return The dilation dimensions as a reference.
     */
    inline std::array<DimSize_t, DIM>& dilationDims() const { return mAttributes->template getAttr<ConvAttr::DilationDims>(); }

    /**
     * @brief Get the kernel dimensions.
     * @return The kernel dimensions as a reference.
     */
    inline std::array<DimSize_t, DIM>& kernelDims() const { return mAttributes->template getAttr<ConvAttr::KernelDims>(); }

    static const std::vector<std::string> getInputsName(){
        return {"data_input", "weight", "bias"};
    }

    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }
};

/**
 * @brief Create a Conv_Op operator for performing convolution.
 *
 * This function constructs a Convolution operation by specifying the input and output channels,
 * kernel dimensions, stride, and dilation dimensions.
 *
 * @tparam DIM The number of dimensions of the input tensor.
 * @param[in] inChannels The number of input channels.
 * @param[in] outChannels The number of output channels.
 * @param[in] kernelDims The kernel dimensions.
 * @param[in] name The name of the operator (optional).
 * @param[in] strideDims The stride dimensions (optional).
 * @param[in] dilationDims The dilation dimensions (optional).
 * @param[in] noBias A flag indicating if no bias is used (default is false).
 * @return A shared pointer to the created Node containing the Conv_Op.
 */
template <std::array<DimSize_t, 1>::size_type DIM>
std::shared_ptr<Node> Conv(DimSize_t inChannels,
                           DimSize_t outChannels,
                           const std::array<DimSize_t, DIM> &kernelDims,
                           const std::string& name = "",
                           const std::array<DimSize_t, DIM> &strideDims = create_array<DimSize_t,DIM>(1),
                           const std::array<DimSize_t, DIM> &dilationDims = create_array<DimSize_t,DIM>(1),
                           bool noBias = false);

/**
 * @brief Helper function for Conv with C-style arrays.
 *
 * This helper function allows automatic template deduction of the number of dimensions (DIM)
 * based on the kernel dimensions provided.
 */
template <DimSize_t DIM>
inline std::shared_ptr<Node> Conv(
    DimSize_t inChannels,
    DimSize_t outChannels,
    DimSize_t const (&kernelDims)[DIM],
    const std::string& name = "",
    const std::array<DimSize_t, DIM> &strideDims = create_array<DimSize_t,DIM>(1),
    const std::array<DimSize_t, DIM> &dilationDims = create_array<DimSize_t,DIM>(1),
    bool noBias = false) {
    static_assert(DIM<=MaxDim,"Too many kernel dimensions required by Conv, not supported");
    return Conv(inChannels, outChannels, to_array(kernelDims), name, strideDims, dilationDims, noBias);
}

}  // namespace Aidge

extern template class Aidge::Conv_Op<1>;
extern template class Aidge::Conv_Op<2>;

namespace {
template <>
const char *const EnumStrings<Aidge::ConvAttr>::data[] = {
    "stride_dims",
    "dilation_dims",
    "kernel_dims"
};
}

#endif /* AIDGE_CORE_OPERATOR_CONV_H_ */
