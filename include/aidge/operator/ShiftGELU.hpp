/********************************************************************************
 * Copyright (c) 2024 Thales
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * Author: Lucas RAKOTOARIVONY, Thales Research & Technology France
 * Date: 10.09.2024
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_SHIFTGELU_H_
#define AIDGE_CORE_OPERATOR_SHIFTGELU_H_

#include <cassert>
#include <memory>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @brief Description of an element-wise Shifted Gaussian Error Linear Unit (ShiftGELU) operation
 * on an input Tensor.
 *
 * The ShiftGELU operation applies a quantization and scaling-based approximation of the GELU activation.
 * For each element `x` in the input tensor, the operation can be summarized as:
 *
 * `f(x) = scale_factor * quantize(x / SF) * shift(x)`
 *
 * Where:
 * - `quantize` is a rounding operation to represent the input in a lower precision format.
 * - `SF` is the scaling factor for quantization.
 * - `shift(x)` is a transformation using exponent shifting and clamping to approximate smooth non-linear behavior.
 * - `scale_factor` adjusts the final output to match the desired numerical range for quantized tensors.
 *
 * This approach replaces computationally expensive operations like `tanh` with efficient approximations, enabling
 * smooth activation behavior while optimizing for low-precision neural network inference.
 *
 * The input and output Tensors have the same dimensions.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class ShiftGELU_Op : public OperatorTensor,
    public Registrable<ShiftGELU_Op,  // <Op, backend, implementation creation function>
        std::string,
        std::function<std::shared_ptr<OperatorImpl>(const ShiftGELU_Op&)>>
{
public:
    static const std::string Type;

    ShiftGELU_Op();

    /**
     * @brief Copy-constructor.
     * @param op ShiftGELU_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    ShiftGELU_Op(const ShiftGELU_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::ShiftGELU_Op
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Set the backend for the Reshape operator.
     * @param[in] name Name of the backend.
     * @param[in] device Device index (optional).
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;
    std::set<std::string> getAvailableBackends() const override;

    static const std::vector<std::string> getInputsName(){
        return {"data_input"};
    }
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }
};

std::shared_ptr<Node> ShiftGELU(const std::string& name = "");
}

#endif /* AIDGE_CORE_OPERATOR_SHIFTGELU_H_ */
