/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_SIGMOID_H_
#define AIDGE_CORE_OPERATOR_SIGMOID_H_

#include <cassert>
#include <memory>
#include <vector>

#include "aidge/utils/Registrar.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @brief Description of an element-wise Sigmoid operation on an input Tensor.
 *
 * For each element x in the input, the function is defined as:
 * `f(x) = 1 / (1 + exp(-x))`
 *
 * The input and output Tensors have the same dimensions.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Sigmoid_Op : public OperatorTensor,
    public Registrable<Sigmoid_Op,  // <Op, backend, implementation creation function>
        std::string,
        std::function<std::shared_ptr<OperatorImpl>(const Sigmoid_Op&)>>
{
public:
    static const std::string Type;

    Sigmoid_Op();

    /**
     * @brief Copy-constructor.
     * @param op Sigmoid_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    Sigmoid_Op(const Sigmoid_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::Sigmoid_Op
     */
    std::shared_ptr<Operator> clone() const override;

    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;
    std::set<std::string> getAvailableBackends() const override;

    static const std::vector<std::string> getInputsName(){
        return {"data_input"};
    }
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }
};

std::shared_ptr<Node> Sigmoid(const std::string& name = "");
}

#endif /* AIDGE_CORE_OPERATOR_SIGMOID_H_ */
