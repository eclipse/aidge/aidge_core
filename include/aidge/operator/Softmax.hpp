/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_SOFTMAX_H_
#define AIDGE_CORE_OPERATOR_SOFTMAX_H_

#include <memory>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
enum class SoftmaxAttr {
    /**
     * @brief Axis along which the softmax operation is applied.
     *
     * Determines the dimension in the input tensor over which the softmax
     * operation will compute normalized exponential values.
     */
    Axis
};

/**
 * @brief Description of a Softmax operation on input Tensor along a specified axis.
 *
 * The Softmax operation normalizes the input tensor values along a specified axis
 * into a probability distribution where the sum of values is equal to 1.
 *
 * The Softmax function for a given input `x_i` is defined as:
 * `softmax(x_i) = exp(x_i) / sum(exp(x_j))`, where the sum is taken over all elements in the specified dimension.
 *
 * The input and output Tensors have the same dimensions.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Softmax_Op : public OperatorTensor,
                   public Registrable<Softmax_Op,
                                      std::string,
                                      std::function<std::shared_ptr<OperatorImpl>(const Softmax_Op&)>> {

public:
    /**
     * @brief Static type string for the Softmax operator.
     */
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<SoftmaxAttr, std::int32_t>;
    template <SoftmaxAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    /**
     * @brief Deleted default constructor.
     */
    Softmax_Op() = delete;

    /**
     * @brief Constructor for the Softmax operator.
     * @param[in] axis Axis along which the softmax operation is applied.
     */
    Softmax_Op(std::int32_t axis);

    /**
     * @brief Copy-constructor.
     * @param op Softmax_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    Softmax_Op(const Softmax_Op& op);

    /**
     * @brief Clone the operator using its copy constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Set the backend for the Softmax operator.
     * @param[in] name Name of the backend.
     * @param[in] device Device index (optional).
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;

    /**
     * @brief Get the available backends for the Softmax operator.
     * @return A set of backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     * @return A shared pointer to the attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get the axis along which the softmax operation is applied.
     * @return Reference to the axis attribute.
     */
    inline std::int32_t& axis() const noexcept { return mAttributes->getAttr<SoftmaxAttr::Axis>(); }

    /**
     * @brief Get the input names for the Softmax operator.
     * @return A vector of input tensor names.
     */
    static const std::vector<std::string> getInputsName() {
        return {"data_input"};
    }

    /**
     * @brief Get the output names for the Softmax operator.
     * @return A vector of output tensor names.
     */
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Create a Softmax operation node.
 *
 * @param[in] axis Axis along which the softmax operation is applied.
 * @param[in] name Name of the operator (optional).
 * @return A shared pointer to the Node containing the Softmax operator.
 */
std::shared_ptr<Node> Softmax(std::int32_t axis, const std::string& name = "");

} // namespace Aidge

namespace {
/**
 * @brief EnumStrings specialization for SoftmaxAttr.
 */
template <>
const char* const EnumStrings<Aidge::SoftmaxAttr>::data[] = {"axis"};
}

#endif /* AIDGE_CORE_OPERATOR_SOFTMAX_H_ */
