/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_ARGMAX_H_
#define AIDGE_CORE_OPERATOR_ARGMAX_H_

#include <cstdint>    // std::int32_t
#include <memory>
#include <string>
#include <vector>

#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

enum class ArgMaxAttr {
    /**
     * @brief Specifies the dimension along which the ArgMax operation is performed.
     */
    Axis,
    /**
     * Indicates whether reduced dimensions should be kept or removed.
     */
    KeepDims,
    /**
     * Determines whether to select the first or last index in case of ties.
     */
    SelectLastIndex
};

/**
 * @brief Description of the ArgMax operation on a Tensor.
 *
 * The ArgMax operation identifies the index of the maximum value along a specified axis of a Tensor.
 *
 * The output of the ArgMax operation can retain the dimensionality of the input Tensor or reduce 
 * it by removing the specified axis. Additionally, in cases where multiple maximum values exist, 
 * the user can specify whether to select the first or the last occurrence of the maximum value.
 *
 * Attributes:
 * - `Axis`: The axis along which the ArgMax operation is performed. For example, if the axis is `0`,
 *   the operation is applied along rows; if it is `1`, it is applied along columns.
 * - `KeepDims`: A boolean indicating whether to retain the reduced axis as a dimension of size `1` 
 *   (`true`) or to completely remove it (`false`).
 * - `SelectLastIndex`: A boolean indicating how to handle ties (multiple maximum values along the axis):
 *   - If `true`, the last index of the maximum value is selected.
 *   - If `false`, the first index of the maximum value is selected.
 *
 * @example:
 * - Input Tensor: Shape (2, 3)
 * - ArgMax(axis=1, keep_dims=false, select_last_index=false):
 *   Output Tensor: Shape (2)
 *
 * @see OperatorTensor
 * @see Registrable
 */
class ArgMax_Op : public OperatorTensor,
                public Registrable<ArgMax_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const ArgMax_Op &)>> {

public:
    /// The type of the operator as a string.
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<ArgMaxAttr,
                                        std::int32_t,
                                        bool,
                                        bool>;
    template <ArgMaxAttr e>
    using attr = typename Attributes_::template attr<e>;
    /// Pointer to the attribute storage.
    const std::shared_ptr<Attributes_> mAttributes;

public:
    ArgMax_Op() = delete;

    /**
     * @brief Constructs an ArgMax operator.
     * @param[in] axis The axis along which the ArgMax operation is performed.
     * @param[in] keep_dims Whether to retain reduced dimensions with size 1 (`true`) or remove them (`false`).
     * @param[in] select_last_index Whether to select the last occurrence of the maximum value (`true`) or the first (`false`).
     */
    ArgMax_Op(std::int32_t axis = 0, bool keep_dims = true, bool select_last_index = false)
        : OperatorTensor(Type, {InputCategory::Data}, 1),
          mAttributes(std::make_shared<Attributes_>(
            attr<ArgMaxAttr::Axis>(axis),
            attr<ArgMaxAttr::KeepDims>(keep_dims),
            attr<ArgMaxAttr::SelectLastIndex>(select_last_index)))
    {}

    /**
     * @brief Copy constructor for the ArgMax operator.
     *
     * Creates a new ArgMax_Op instance by copying attributes and output tensors from another
     * instance. The input tensors are not copied, so the new instance has no inputs associated.
     *
     * @param op The ArgMax_Op instance to copy.
     */
    ArgMax_Op(const ArgMax_Op& op);

    /**
     * @brief Creates a copy of the current ArgMax operator.
     * @return A shared pointer to the new ArgMax operator instance.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Performs dimension inference for the ArgMax operation.
     * @param[in] allowDataDependency Whether data dependency is allowed during dimension inference.
     * @return `true` if dimensions were successfully inferred, `false` otherwise.
     */
    bool forwardDims(bool allowDataDependency = false) override final;

    /**
     * @brief Sets the backend for the operator.
     * @param name The name of the backend.
     * @param device The device index on which the backend operates (default is 0).
     */
    void setBackend(const std::string &name, DeviceIdx_t device = 0) override final;

    /**
     * @brief Retrieves a list of available backends for the ArgMax operator.
     * @return A set of strings representing the available backends.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Gets the attribute storage for the ArgMax operator.
     * @return A shared pointer to the attribute storage.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Gets a reference to the axis attribute.
     * @return A reference to the axis attribute.
     */
    inline std::int32_t& axis() const noexcept { return mAttributes -> getAttr<ArgMaxAttr::Axis>(); }

    /**
     * @brief Gets a reference to the keepDims attribute.
     * @return A reference to the keepDims attribute.
     */
    inline bool& keepDims() const noexcept { return mAttributes -> getAttr<ArgMaxAttr::KeepDims>(); }

    /**
     * @brief Gets a reference to the selectLastIndex attribute.
     * @return A reference to the selectLastIndex attribute.
     */
    inline bool& selectLastIndex() const noexcept { return mAttributes -> getAttr<ArgMaxAttr::SelectLastIndex>(); }

    /**
     * @brief Returns the names of the input tensors for the ArgMax operator.
     * @return A vector of strings containing the input names.
     */
    static const std::vector<std::string> getInputsName() {
        return {"data_input"};
    }

    /**
     * @brief Returns the names of the output tensors for the ArgMax operator.
     * @return A vector of strings containing the output names.
     */
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Creates an ArgMax operation node.
 *
 * This function constructs a new Node containing an ArgMax_Op operator with the specified
 * attributes.
 *
 * @param[in] axis The axis along which the ArgMax operation is performed.
 * @param[in] keep_dims Whether to retain reduced dimensions with size 1 (`true`) or remove them (`false`).
 * @param[in] select_last_index Whether to select the last occurrence of the maximum value (`true`) or the first (`false`).
 * @param[in] name The name of the Node (optional).
 * @return A shared pointer to the newly created Node.
 */
std::shared_ptr<Node> ArgMax(std::int32_t axis = 0,
                             bool keep_dims = true,
                             bool select_last_index = false,
                             const std::string& name = "");

}  // namespace Aidge

/**
 * @brief Provides string representations for the ArgMaxAttr enumeration.
 */
namespace {
template <>
const char *const EnumStrings<Aidge::ArgMaxAttr>::data[] = {"axis", "keep_dims", "select_last_index"};
}

#endif /* AIDGE_CORE_OPERATOR_ARGMAX_H_ */
