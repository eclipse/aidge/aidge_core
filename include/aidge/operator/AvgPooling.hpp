/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_AVGPOOLING_H_
#define AIDGE_CORE_OPERATOR_AVGPOOLING_H_

#include <array>
#include <string>
#include <vector>

#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
/**
 * @brief Attributes specific to the AvgPooling operation.
 */
enum class AvgPoolingAttr {
    /**
     * @brief Stride dimensions for sliding the pooling window.
     * Specifies the step size of the sliding window along each spatial dimension.
     */
    StrideDims,

    /**
     * @brief Kernel dimensions for the pooling operation.
     * Specifies the size of the pooling window along each spatial dimension.
     */
    KernelDims
};

/**
 * @brief Class representing an Average Pooling operation.
 *
 * The AvgPooling operation computes the average value within sliding windows of specified size
 * (kernel dimensions) over the input tensor. The stride dimensions determine how the window
 * moves across the input. This operation is commonly used in neural networks to reduce the spatial
 * dimensions while preserving features.
 *
 * @tparam DIM Number of dimensions for the pooling operation.
 */
template <DimIdx_t DIM>
class AvgPooling_Op : public OperatorTensor,
                public Registrable<AvgPooling_Op<DIM>, std::string, std::function<std::shared_ptr<OperatorImpl>(const AvgPooling_Op<DIM> &)>> {

public:
    /**
     * @brief Type identifier for the AvgPooling operation.
     */
    static const std::string Type;

private:
    /**
     * @brief Static attributes representing kernel and stride dimensions.
     */
    using Attributes_ = StaticAttributes<AvgPoolingAttr,
                                             std::array<DimSize_t, DIM>,
                                             std::array<DimSize_t, DIM>>;
    template <AvgPoolingAttr e>
    using attr = typename Attributes_::template attr<e>;

    /**
     * @brief Shared pointer to the attributes of the operation.
     */
    const std::shared_ptr<Attributes_> mAttributes;

public:
    /**
     * @brief Default constructor is deleted.
     */
    AvgPooling_Op() = delete;

    /**
     * @brief Constructs an AvgPooling operation with specified kernel and stride dimensions.
     * @param kernel_dims Size of the pooling window for each spatial dimension.
     * @param stride_dims Step size (stride) for sliding the pooling window across the input dimensions.
     * Defaults to 1 for each dimension.
     */
    constexpr AvgPooling_Op(const std::array<DimSize_t, DIM> &kernel_dims,
                            const std::array<DimSize_t, DIM> &stride_dims = create_array<DimSize_t, DIM>(1))
        : OperatorTensor(Type, {InputCategory::Data}, 1),
          mAttributes(std::make_shared<Attributes_>(
                        attr<AvgPoolingAttr::StrideDims>(stride_dims),
                        attr<AvgPoolingAttr::KernelDims>(kernel_dims)))
    {}

    /**
     * @brief Copy-constructor.
     * @param op AvgPooling_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    AvgPooling_Op(const AvgPooling_Op<DIM>& op);

    /**
     * @brief Clones the operator using its copy-constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override final;

    /**
     * @brief Calculates the output dimensions based on the input dimensions and operator attributes.
     * @param allowDataDependency If true, considers data-dependent operations. Defaults to false.
     * @return True if the dimensions are successfully calculated.
     */
    bool forwardDims(bool /*allowDataDependency*/ = false) override final;

    /**
     * @brief Computes the receptive field of the operator.
     * @param firstEltDims Dimensions of the first element.
     * @param outputDims Dimensions of the output tensor.
     * @param outputIdx Index of the output tensor. Defaults to 0.
     * @return A vector of pairs representing the receptive fields.
     */
    std::vector<std::pair<std::vector<DimSize_t>, std::vector<DimSize_t>>>
    computeReceptiveField(const std::vector<DimSize_t>& firstEltDims,
                          const std::vector<DimSize_t>& outputDims,
                          const IOIndex_t outputIdx = 0) const override final;

    /**
     * @brief Sets the backend for the operation.
     * @param name Name of the backend.
     * @param device Device index. Defaults to 0.
     */
    void setBackend(const std::string &name, DeviceIdx_t device = 0) override final;

    /**
     * @brief Retrieves the available backends for the operation.
     * @return A set of strings representing the available backends.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Accessor for the operation attributes.
     * @return Shared pointer to the attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Accessor for the stride dimensions.
     * @return An array representing the stride dimensions.
     */
    inline std::array<DimSize_t, DIM>& strideDims() const { return mAttributes->template getAttr<AvgPoolingAttr::StrideDims>(); }

    /**
     * @brief Accessor for the kernel dimensions.
     * @return An array representing the kernel dimensions.
     */
    inline std::array<DimSize_t, DIM>& kernelDims() const { return mAttributes->template getAttr<AvgPoolingAttr::KernelDims>(); }

    /**
     * @brief Retrieves the names of the input tensors.
     * @return A vector of strings representing the input tensors names.
     */
    static const std::vector<std::string> getInputsName() {
        return {"data_input"};
    }

    /**
     * @brief Retrieves the names of the output tensors.
     * @return A vector of strings representing the output tensors names.
     */
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Creates an AvgPooling operator node.
 * @tparam DIM Number of dimensions for the pooling operation.
 * @param kernel_dims Size of the pooling window for each spatial dimension.
 * @param name Name of the operator node. Defaults to an empty string.
 * @param stride_dims Step size (stride) for sliding the pooling window across the input dimensions. Defaults to 1 for each dimension.
 * @return A shared pointer to the created operator node.
 */
template <std::array<DimSize_t, 1>::size_type DIM>
std::shared_ptr<Node> AvgPooling(const std::array<DimSize_t, DIM> &kernel_dims,
                                 const std::string& name = "",
                                 const std::array<DimSize_t, DIM> &stride_dims = create_array<DimSize_t,DIM>(1));

/**
 * @brief Overload of AvgPooling for C-style arrays.
 * @tparam DIM Number of dimensions for the pooling operation.
 * @param kernel_dims C-style array specifying the kernel dimensions.
 * @param name Name of the operator node. Defaults to an empty string.
 * @param stride_dims Step size (stride) for sliding the pooling window across the input dimensions. Defaults to 1 for each dimension.
 * @return A shared pointer to the created operator node.
 */
template <DimSize_t DIM>
inline std::shared_ptr<Node> AvgPooling(
    DimSize_t const (&kernel_dims)[DIM],
    const std::string& name = "",
    const std::array<DimSize_t, DIM> &stride_dims = create_array<DimSize_t,DIM>(1)) {
    static_assert(DIM<=MaxDim,"Too many kernel dimensions required by AvgPooling, not supported");
    return AvgPooling(to_array(kernel_dims), name, stride_dims);
}
}  // namespace Aidge

/**
 * @brief Explicit template instantiations for AvgPooling_Op with 1 to 4 dimensions.
 */
extern template class Aidge::AvgPooling_Op<1>;
extern template class Aidge::AvgPooling_Op<2>;
extern template class Aidge::AvgPooling_Op<3>;
extern template class Aidge::AvgPooling_Op<4>;

namespace {
/**
 * @brief String representation of the AvgPooling attributes.
 */
template <>
const char *const EnumStrings<Aidge::AvgPoolingAttr>::data[] = {
    "stride_dims",
    "kernel_dims"
};
}

#endif /* AIDGE_CORE_OPERATOR_AVGPOOLING_H_ */
