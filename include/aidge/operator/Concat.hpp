/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_CONCAT_H_
#define AIDGE_CORE_OPERATOR_CONCAT_H_

#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

#include "aidge/utils/Registrar.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @class Concat_OpImpl
 * @brief Implementation of the Concat operator.
 *
 * Since Concat operation is backend-agnostic, its implementation is located in aidge_core.
 */
class Concat_OpImpl : public OperatorImpl {
public:
    /**
     * @brief Constructor for Concat_OpImpl.
     * @param[in] op Operator instance.
     * @param[in] backend Name of the backend.
     */
    Concat_OpImpl(const Operator& op, const std::string& backend = "")
        : OperatorImpl(op, backend)
    {}

    /**
     * @brief Perform the forward pass of the Concat operator.
     */
    void forward() override;
};

enum class ConcatAttr {
    /**
     * @brief Axis along which to concat the input tensor.
     *
     * The specified axis determines the direction of concatenating.
     */
    Axis 
};

/**
 * @class Concat_Op
 * @brief Implements the Concat operation to concatenate multiple tensors along a specified axis.
 *
 * The Concat operation combines multiple input tensors into a single tensor by concatenating them
 * along a specified axis. The tensors must have the same shape except for the concatenating axis.
 *
 * ### Output Shape Calculation
 * - Input shapes: [(N1, D1, D2, ..., DN), (N2, D1, D2, ..., DN), ...]
 * - Axis: 1
 * - Output shape: (N1 + N2, D1, D2, ..., DN)
 *
 * @example:
 * - Input shapes: [(2, 4, 8), (3, 4, 8), (1, 4, 8)]
 * - Axis: 0
 * - Output shape: (6, 4, 8)
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Concat_Op : public OperatorTensor,
    public Registrable<Concat_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const Concat_Op&)>> {

public:
    /**
     * @brief Type identifier for the Concat operator.
     */
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<ConcatAttr, std::int32_t>;

    template <ConcatAttr e>
    using attr = typename Attributes_::template attr<e>;

    const std::shared_ptr<Attributes_> mAttributes;

public:
    /**
     * @brief Default constructor is deleted to enforce explicit initialization.
     */
    Concat_Op() = delete;

    /**
     * @brief Constructor to initialize the Concat operator.
     * @param[in] nbIn Number of input tensors.
     * @param[in] axis Axis along which concatenation is performed.
     */
    Concat_Op(const IOIndex_t nbIn, const std::int32_t axis);

    /**
     * @brief Copy-constructor. Copies the operator attributes and its output tensors,
     * but not its input tensors (the new operator has no input associated).
     * @param[in] op Operator to copy.
     */
    Concat_Op(const Concat_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Forward the dimensions of the operator's inputs and outputs.
     * @param[in] allowDataDependency Allow data dependency during dimension propagation.
     * @return True if dimensions were forwarded successfully.
     */
    bool forwardDims(bool allowDataDependency = false) override final;

    /**
     * @brief Set the backend for the operator.
     * @param[in] name Backend name.
     * @param[in] device Device index (default: 0).
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override;

    /**
     * @brief Get the set of available backends for the operator.
     * @return A set of backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the Concat operator.
     * @return A shared pointer to the attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get or modify the axis along which tensors are concatenated.
     * @return A reference to the axis attribute.
     */
    inline std::int32_t& axis() const { return mAttributes->template getAttr<ConcatAttr::Axis>(); }

    /**
     * @brief Get the names of the input tensors.
     * @return A vector of input tensor names.
     */
    static const std::vector<std::string> getInputsName() {
        return { "data_input_0", "data_input_n" };
    }

    /**
     * @brief Get the names of the output tensors.
     * @return A vector of output tensor names.
     */
    static const std::vector<std::string> getOutputsName() {
        return { "data_output" };
    }
};

/**
 * @brief Factory function to create a Concat node.
 * @param[in] nbIn Number of input tensors.
 * @param[in] axis Axis along which concatenation is performed (default: 0).
 * @param[in] name (Optional) Name of the node.
 * @return A shared pointer to the created node.
 */
std::shared_ptr<Node> Concat(const IOIndex_t nbIn, const std::int32_t axis = 0, const std::string& name = "");

} // namespace Aidge

namespace {
/**
 * @brief Specialization of EnumStrings for ConcatAttr.
 */
template <>
const char* const EnumStrings<Aidge::ConcatAttr>::data[] = {
    "axis"
};
}

#endif /* AIDGE_CORE_OPERATOR_CONCAT_H_ */
