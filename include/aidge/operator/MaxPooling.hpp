/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_MAXPOOLING_H_
#define AIDGE_CORE_OPERATOR_MAXPOOLING_H_

#include <array>
#include <cmath>       // std::ceil, std::floor
#include <cstddef>     // std::size_t
#include <functional>
#include <memory>
#include <stdexcept>   // std::runtime_error
#include <vector>

#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @enum MaxPoolingAttr
 * @brief Attributes defining the configuration of a MaxPooling operation.
 */
enum class MaxPoolingAttr {
  /**
   * @brief Stride dimensions for sliding the pooling window across the input dimensions.
   * The stride specifies how much the window moves after each operation.
   * Must be positive integers.
   */
  StrideDims,

  /**
   * @brief Kernel dimensions specifying the size of the pooling window for each spatial dimension.
   * For example, common kernel dimensions include 2x2 or 3x3.
   * Must be positive integers.
   */
  KernelDims,

  /**
   * @brief Flag indicating whether to use ceil or floor when calculating output size.
   * - `true`: Use `ceil` for output size calculation.
   * - `false`: Use `floor` for output size calculation.
   */
  CeilMode,
};

/**
 * @class MaxPooling_Op
 * @tparam DIM Dimensionality of the input tensor (e.g., 1D, 2D, 3D).
 * @brief Implements the MaxPooling operation over a specified input tensor.
 *
 * MaxPooling reduces spatial dimensions by applying a max filter over a sliding window.
 * The resulting output tensor contains the maximum value within each window.
 *
 * ### Output Shape Calculation
 * - If `CeilMode` is false:
 *   `output_size = floor((input_size - kernel_size) / stride + 1)`
 * - If `CeilMode` is true:
 *   `output_size = ceil((input_size - kernel_size) / stride + 1)`
 *
 * @example Example usage:
 * - Input shape: (1, 3, 32, 32) // Batch size 1, 3 channels, 32x32 spatial dimensions
 * - KernelDims: (2, 2)
 * - StrideDims: (2, 2)
 * - CeilMode: false
 * - Output shape: (1, 3, 16, 16)
 *
 * @see OperatorTensor
 * @see Registrable
 */
template <DimIdx_t DIM>
class MaxPooling_Op : public OperatorTensor,
                public Registrable<MaxPooling_Op<DIM>,
                                   std::string,
                                   std::function<std::shared_ptr<OperatorImpl>(const MaxPooling_Op<DIM> &)>>
{
public:
    static const std::string Type; ///< Static identifier for this operator type.

    using Attributes_ = StaticAttributes<MaxPoolingAttr,
                                         std::array<DimSize_t, DIM>,
                                         std::array<DimSize_t, DIM>,
                                         bool>;

private:
    template <MaxPoolingAttr e>
    using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes; ///< Shared pointer to operator attributes.

public:
    MaxPooling_Op() = delete; ///< Deleted default constructor.

    /**
     * @brief Constructor.
     * @param[in] kernel_dims Size of the pooling window for each spatial dimension.
     * @param[in] stride_dims Step size (stride) for sliding the pooling window across input dimensions.
     * @param[in] ceil_mode Indicates whether to use ceil or floor for output size calculation.
     */
    MaxPooling_Op(const std::array<DimSize_t, DIM> &kernel_dims,
                  const std::array<DimSize_t, DIM> &stride_dims = create_array<DimSize_t,DIM>(1),
                  bool ceil_mode = false);

    /**
     * @brief Copy-constructor.
     * @param op MaxPooling_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    MaxPooling_Op(const MaxPooling_Op<DIM>& op);

    /**
     * @brief Clones the operator using the copy constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Computes output tensor dimensions based on input dimensions and operator attributes.
     * @param[in] allowDataDependency If true, dimensions may depend on input data; otherwise, strictly attribute-based.
     * @return Boolean indicating success of the operation.
     */
    bool forwardDims(bool /*allowDataDependency*/ = false) override final;

    /**
     * @brief Sets the backend implementation for this operator.
     * @param[in] name Name of the backend.
     * @param[in] device Device index where the backend will run (default: 0).
     */
    void setBackend(const std::string &name, DeviceIdx_t device = 0) override;

    /**
     * @brief Retrieves the list of available backend implementations for this operator.
     * @return A set of available backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Accessor for operator attributes.
     * @return A shared pointer to the attributes object.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Accessor for stride dimensions.
     * @return An array representing stride dimensions.
     */
    inline std::array<DimSize_t, DIM>& strideDims() const { return mAttributes->template getAttr<MaxPoolingAttr::StrideDims>(); }

    /**
     * @brief Accessor for kernel dimensions.
     * @return An array representing kernel dimensions.
     */
    inline std::array<DimSize_t, DIM>& kernelDims() const { return mAttributes->template getAttr<MaxPoolingAttr::KernelDims>(); }

    /**
     * @brief Accessor for ceil mode flag.
     * @return Boolean value indicating whether ceil mode is enabled.
     */
    inline bool& ceilMode() const { return mAttributes->template getAttr<MaxPoolingAttr::CeilMode>(); }

    /**
     * @brief Retrieves the names of the input tensors.
     * @return A vector of input tensors names.
     */
    static const std::vector<std::string> getInputsName(){ return {"data_input"}; }

    /**
     * @brief Retrieves the names of the output tensors.
     * @return A vector of output tensors names.
     */
    static const std::vector<std::string> getOutputsName(){ return {"data_output"}; }
};

/**
 * @brief Explicit template instantiations for 1D, 2D, and 3D MaxPooling operations.
 */
extern template class Aidge::MaxPooling_Op<1>;
extern template class Aidge::MaxPooling_Op<2>;
extern template class Aidge::MaxPooling_Op<3>;

/**
 * @brief Factory function for creating MaxPooling operations.
 * @tparam DIM Dimensionality of the pooling operation (e.g., 1D, 2D, 3D).
 * @param[in] kernel_dims Kernel dimensions specifying the size of the pooling window.
 * @param[in] name Optional name for the operation.
 * @param[in] stride_dims Stride dimensions specifying the step size for the pooling window.
 * @param[in] ceil_mode Indicates whether to use ceil mode for output size calculation.
 * @return A shared pointer to a Node representing the MaxPooling operation.
 */
template <std::array<DimSize_t, 1>::size_type DIM>
std::shared_ptr<Node> MaxPooling(const std::array<DimSize_t, DIM> &kernel_dims,
                                 const std::string& name = "",
                                 const std::array<DimSize_t, DIM> &stride_dims = create_array<DimSize_t,DIM>(1),
                                 bool ceil_mode=false);

/**
 * @brief Helper function for creating MaxPooling operations with C-style array input.
 * @tparam DIM Dimensionality of the pooling operation.
 * @param[in] kernel_dims C-style array of kernel dimensions.
 * @param[in] name Optional name for the operation.
 * @param[in] stride_dims Stride dimensions specifying the step size for the pooling window.
 * @param[in] ceil_mode Indicates whether to use ceil mode for output size calculation.
 * @return A shared pointer to a Node representing the MaxPooling operation.
 */
template <DimSize_t DIM>
inline std::shared_ptr<Node> MaxPooling(
    DimSize_t const (&kernel_dims)[DIM],
    const std::string& name = "",
    const std::array<DimSize_t, DIM> &stride_dims = create_array<DimSize_t,DIM>(1),
    bool ceil_mode = false) {
    static_assert(DIM<=MaxDim,"Too many kernel dimensions required by MaxPooling, not supported");
    return MaxPooling(to_array(kernel_dims), name, stride_dims, ceil_mode);
}

}  // namespace Aidge

namespace {
/**
 * @brief String representations of MaxPooling attributes for debugging and logging.
 */
template <>
const char *const EnumStrings<Aidge::MaxPoolingAttr>::data[] = {"stride_dims", "kernel_dims", "ceil_mode"};
}

#endif /* AIDGE_CORE_OPERATOR_MAXPOOLING_H_ */
