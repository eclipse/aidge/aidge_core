/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_METAOPERATOR_H_
#define AIDGE_CORE_OPERATOR_METAOPERATOR_H_

#include <array>
#include <memory>
#include <string>

#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/OpArgs.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/scheduler/ProdConso.hpp"
#include "aidge/scheduler/SequentialScheduler.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @class MetaOperator_Op
 * @brief Represents a meta-operator, which is a composition of multiple operators.
 * 
 * A meta-operator encapsulates a micro-graph of operations, facilitating modularity
 * and reusability. It extends the functionality of `OperatorTensor` and provides
 * features such as cloning, dynamic input association, and custom backend support.
 */
class MetaOperator_Op : public OperatorTensor,
                public Registrable<MetaOperator_Op, std::array<std::string, 2>, std::function<std::shared_ptr<OperatorImpl>(const MetaOperator_Op &)>> {
public:
    // The micro-graph defining the internal operations of this meta-operator
    std::shared_ptr<GraphView> mGraph;

    // Scheduler for sequential execution of the micro-graph
    std::shared_ptr<SequentialScheduler> mScheduler;

    // Weak reference to the parent node for scheduling purposes
    std::weak_ptr<Node> mUpperNode;

private:
    // Dynamic attributes specific to this meta-operator
    const std::shared_ptr<DynamicAttributes> mAttributes = std::make_shared<DynamicAttributes>();

public:
    /**
     * @brief Constructor for MetaOperator_Op.
     * 
     * @param type The type of the meta-operator.
     * @param graph The micro-graph defining the meta-operator.
     * @param forcedInputsCategory Optional input categories to override default behavior.
     */
    MetaOperator_Op(const std::string& type, const std::shared_ptr<GraphView>& graph, const std::vector<InputCategory>& forcedInputsCategory = {});

    /**
     * @brief Copy constructor.
     * 
     * Copies the operator's attributes and output tensors, but not its input tensors.
     * 
     * @param op The operator to copy.
     */
    MetaOperator_Op(const MetaOperator_Op& op);

    /**
     * @brief Set the node for scheduling.
     * 
     * @param node The node to be used as the upper node in the scheduling hierarchy.
     */
    inline void setUpperNode(std::shared_ptr<Node> node) {
        mUpperNode = node;
    }

    /**
     * @brief Clone this meta-operator.
     * 
     * Uses the copy constructor to create a new instance with identical attributes.
     * 
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Retrieve the micro-graph defining the meta-operator.
     * 
     * @return A shared pointer to the micro-graph.
     */
    inline const std::shared_ptr<GraphView>& getMicroGraph() const noexcept {
        return mGraph;
    }

    /**
     * @brief Retrieve the scheduler for the micro-graph.
     * 
     * @return A shared pointer to the scheduler.
     */
    inline const std::shared_ptr<SequentialScheduler>& getMicroGraphScheduler() const noexcept {
        return mScheduler;
    }

    /**
     * @brief Associate an input tensor to the operator.
     * 
     * @param inputIdx Index of the input tensor.
     * @param data Shared pointer to the data tensor.
     */
    void associateInput(const IOIndex_t inputIdx, const std::shared_ptr<Data>& data) override final;

    /**
     * @brief Set an input tensor for the operator.
     * 
     * @param inputIdx Index of the input tensor.
     * @param data Shared pointer to the data tensor.
     */
    void setInput(const IOIndex_t inputIdx, const std::shared_ptr<Data>& data) override final;

    /**
     * @brief Forward the dimensions through the micro-graph.
     * 
     * @param allowDataDependency If true, allows data-dependent operations during forwarding.
     * @return True if the operation succeeded, false otherwise.
     */
    bool forwardDims(bool allowDataDependency = false) override final {
        if (inputsAssociated()) {
            // Forward dims of micro-graph
            return mGraph->forwardDims({}, allowDataDependency);
        }
        return false;
    }

    /**
     * @brief Retrieve the backend for the operator.
     * 
     * @return The name of the backend.
     */
    std::string backend() const noexcept override;

    /**
     * @brief Set the backend for the operator.
     * 
     * @param name The name of the backend.
     * @param device The device index.
     */
    void setBackend(const std::string &name, DeviceIdx_t device = 0) override;

    /**
     * @brief Get the available backends for the operator.
     * 
     * @return A set of available backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Set the data type for the operator.
     * 
     * This propagates the data type change to the micro-graph.
     * 
     * @param datatype The new data type.
     */
    void setDataType(const DataType &datatype) const override {
        // The micro-graph should always be set to the right data type, since it
        // shares input/output tensors.
        // Input/output tensors data type are updated here.
        mGraph->setDataType(datatype);
    }

    /**
     * @brief Retrieve the dynamic attributes of the operator.
     * 
     * @return A shared pointer to the attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    Elts_t getNbRequiredData(const IOIndex_t inputIdx) const override;
    Elts_t getNbRequiredProtected(const IOIndex_t inputIdx) const override;
    Elts_t getRequiredMemory(const IOIndex_t outputIdx, const std::vector<DimSize_t> &inputsSize) const override;
    Elts_t getNbConsumedData(IOIndex_t inputIdx) const override;
    Elts_t getNbProducedData(IOIndex_t outputIdx) const override;

    /**
     * @brief Update the consumer-producer relationships for the operator.
     */
    void updateConsummerProducer() override;

    /**
     * @brief Reset the consumer-producer relationships.
     */
    void resetConsummerProducer() override;

    /**
     * @brief Perform the forward pass for the operator.
     */
    void forward() override;

    /**
     * @brief Perform the backward pass for the operator.
     * 
     * @note Currently not implemented.
     */
    void backward() override {
        AIDGE_THROW_OR_ABORT(std::runtime_error, "backward() not implemented yet for a MetaOperator");
    }

    /**
     * @brief Check if the operator is atomic.
     * 
     * @return False, as meta-operators are inherently non-atomic.
     */
    inline bool isAtomic() const noexcept override final { return false; }

};

/**
 * @brief Helper function to create a MetaOperator node.
 * 
 * @param type The type of the meta-operator.
 * @param graph The micro-graph defining the meta-operator.
 * @param forcedInputsCategory Optional input categories to override default behavior.
 * @param name Optional name for the operator.
 * @return A shared pointer to the created Node.
 */
std::shared_ptr<Node> MetaOperator(const char *type,
                                  const std::shared_ptr<GraphView>& graph,
                                  const std::vector<InputCategory>& forcedInputsCategory = {},
                                  const std::string& name = "");

}  // namespace Aidge

#endif /* AIDGE_CORE_OPERATOR_METAOPERATOR_H_ */
