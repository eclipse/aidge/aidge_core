/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_STACK_H_
#define AIDGE_CORE_OPERATOR_STACK_H_

#include <memory>
#include <string>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
/**
 * @class StackProdConso
 * @brief Implements the producer-consumer principle for the `Stack` operator.
 *
 * The `StackProdConso` class defines the logic for managing data dependencies
 * during the forward process of the `Stack` operator. It ensures proper allocation
 * and consumption of resources required for stacking operations.
 */
class StackProdConso : public ProdConso {
public:
    /**
     * @brief Constructor for the `StackProdConso` class.
     * @param[in] op The operator instance for which producer-consumer relationships are managed.
     */
    StackProdConso(const Operator& op) : ProdConso(op) {}

    /**
     * @brief Compute the memory requirements for an output tensor.
     * @param[in] outputIdx The index of the output tensor.
     * @param[in] inputsSize A vector containing the dimensions of the input tensors.
     * @return The memory required (`Elts_t`) for the specified output tensor.
     *
     * @details:
     * - This method calculates how much memory is needed to store the stacked tensor.
     * - Memory requirements depend on the number and size of the input tensors.
     */
    Elts_t getRequiredMemory(const IOIndex_t outputIdx, const std::vector<DimSize_t>& inputsSize) const override final;

    /**
     * @brief Reset producer-consumer relationships for the `Stack` operator.
     *
     * @details:
     * - This method clears and reinitializes the producer-consumer relationships,
     *   ensuring proper data flow and allocation for the stacking operation.
     */
    void resetConsummerProducer() override;
};

/**
 * @class StackOpImpl
 * @brief Backend-specific implementation of the `Stack` operator.
 *
 * The `StackOpImpl` class handles the execution of the `Stack` operation, including
 * forward computation and backend-specific optimizations.
 */
class StackOpImpl : public OperatorImpl {
public:
    /**
     * @brief Constructs a StackOpImpl object.
     * @param[in] op The operator to be implemented.
     * @param[in] backend The backend used for execution.
     */
    StackOpImpl(const Operator& op, const std::string& backend = "") : OperatorImpl(op, backend) {}

    /**
     * @brief Get the Producer Consumer object of the operator.
     * @return A shared pointer to the ProdConso object.
     */
    std::shared_ptr<ProdConso> getProdConso() const override {
        return std::make_shared<StackProdConso>(mOp);
    }

    /**
     * @brief Executes the forward pass for the Stack operation.
     */
    void forward() override;
};

enum class StackAttr {
    ForwardStep,   // Tracks the current step in the forward pass.
    MaxElements    // Maximum number of elements that can be stacked.
};

/**
 * @class StackOp
 * @brief The `Stack` operator performs a stacking operation over a sequence of input tensors.
 *
 * The `Stack` operator is used to aggregate multiple tensors into a single stacked tensor along
 * a new dimension. It supports models requiring tensor aggregation or hierarchical data representation.
 *
 * - **Inputs**:
 *   - `data_input`: The main data input for the operator.
 *   - `max_elements`: An optional maximum limit on the number of elements to be stacked.
 * - **Outputs**:
 *   - `data_output`: The resulting stacked tensor.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class StackOp : public OperatorTensor,
    public Registrable<StackOp, std::string, std::function<std::unique_ptr<OperatorImpl>(const StackOp&)>> {
private:
    using Attributes_ = StaticAttributes<StackAttr, std::uint32_t, std::uint32_t>;
    template <StackAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    static const std::string s_type;

    /**
     * @brief Constructs a new Stack Operator.
     * @param maxElements The maximum number of elements to stack.
     */
    StackOp(std::uint32_t maxElements = 0);

    /**
     * @brief Copy-constructor. Copies the operator's attributes and its output tensors.
     * The new operator has no input tensors associated initially.
     * @param op Operator to copy.
     */
    StackOp(const StackOp& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Assign a specific backend and device for computation.
     * @param name Name of the backend.
     * @param device The device index (default is 0).
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;

    /**
     * @brief Get the list of available backends compatible with this operator.
     * @return A set of strings representing backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Check if dimensions have been forwarded successfully.
     * @return True if dimensions are forwarded.
     */
    bool dimsForwarded() const override final;

    /**
     * @brief Perform dimension inference for the operator, optionally allowing
     * data dependency during the process.
     * @param allowDataDependency Flag indicating whether data dependency is allowed.
     * @return True if dimensions were successfully inferred.
     */
    bool forwardDims(bool allowDataDependency = false) override final;

    /**
     * @brief Executes the forward pass for the `Stack` operation.
     */
    void forward() override;

    /**
     * @brief Access the operator's attributes.
     * @return A shared pointer to the operator's attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override {
        return mAttributes;
    }

    /**
     * @brief Get or set the maximum number of elements that can be stacked.
     * @return A reference to the maximum elements attribute.
     */
    inline std::uint32_t& maxElements() const {
        return mAttributes->template getAttr<StackAttr::MaxElements>();
    }

    /**
     * @brief Get or set the forward step counter for the operator.
     * @return A reference to the forward step counter.
     */
    inline std::uint32_t& forwardStep() const {
        return mAttributes->template getAttr<StackAttr::ForwardStep>();
    }

    /**
     * @brief Retrieve the names of the operator's input tensors.
     * @return A vector of strings representing input tensor names.
     */
    static const std::vector<std::string> getInputsName() {
        return {"data_input", "max_elements"};
    }

    /**
     * @brief Retrieve the names of the operator's output tensors.
     * @return A vector of strings representing output tensor names.
     */
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Create a Stack operator node.
 * @param maxElements The maximum number of elements to stack.
 * @param name The optional name for the node.
 * @return A shared pointer to the newly created Stack operator node.
 */
std::shared_ptr<Node> Stack(std::uint32_t maxElements = 0, const std::string& name = "");
}  // namespace Aidge

namespace {
/**
 * @brief String representations of the Stack operator's attributes.
 */
template <>
const char *const EnumStrings<Aidge::StackAttr>::data[] = {"forward_step", "max_elements"};
}

#endif /* AIDGE_CORE_OPERATOR_STACK_H_ */
