/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_SLICE_H_
#define AIDGE_CORE_OPERATOR_SLICE_H_

#include <memory>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @brief Implementation of the Slice operation.
 *
 * Since Slice operation is just backend-agnostic, its implementation is located in aidge_core.
 */
class Slice_OpImpl : public OperatorImpl {
public:
    /**
     * @brief Constructs a Slice_OpImpl object.
     * @param[in] op The operator to be implemented.
     * @param[in] backend The backend used for execution.
     */
    Slice_OpImpl(const Operator& op, const std::string& backend = ""): OperatorImpl(op, backend) {}

    /**
     * @brief Executes the forward pass for the Slice operation.
     */
    void forward() override;
};

/**
 * @enum SliceAttr
 * @brief Attributes for the Slice operation.
 */
enum class SliceAttr {
    /**
     * @brief Starting indices for the slice along each axis.
     *
     * Specifies the start position for slicing for each axis.
     * @details if index is < 0 then the input tansor's rank is added.
     * After, if index is < 0 then it is forced to 0,
     * if index > dim then index is forced to dim.
     */
    Starts,

    /**
     * @brief Ending indices for the slice along each axis.
     *
     * Specifies the end position (exclusive) for slicing for each axis.
     * @details if index is < 0 then the input tansor's rank is added.
     * After, if index is < 0 then it is forced to 0,
     * if index > dim then index is forced to dim.
     */
    Ends,

    /**
     * @brief Axes along which the slice operation is performed.
     *
     * Specifies which dimensions of the input tensor are affected by the slice.
     */
    Axes,

    /**
     * @brief Steps to move between each slice along each axis.
     *
     * Specifies the step size for slicing along each axis.
     */
    Steps
};

/**
 * @class Slice_Op
 * @brief Implements the Slice operation for extracting sub-tensors.
 *
 * The Slice operation extracts a sub-tensor from the input tensor using specified start and end indices,
 * axes, and steps. The resulting tensor dimensions depend on the provided attributes.
 *
 * - The output shape is determined by the `Starts`, `Ends`, `Axes`, and `Steps` attributes.
 * - Each dimension in the output tensor corresponds to the range defined for the respective axis.
 *
 * @example:
 * - Input shape: (4, 5, 6)
 * - Starts: {1, 0, 2}
 * - Ends: {3, 5, 6}
 * - Axes: {0, 1, 2}
 * - Steps: {1, 1, 1}
 * - Output shape: (2, 5, 4)
 *
 * @details If start or end are out of bound then it takes the max value for the given axe.
 * @example:
 * - Input: [0, 1, 2, 3]
 * - Starts: {1}
 * - Ends: {1000}
 * - Axes: {0}
 * - Output: [1, 2, 3]
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Slice_Op : public OperatorTensor,
                public Registrable<Slice_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const Slice_Op &)>> {

public:
    static const std::string Type;

    /**
     * @brief Defines static attributes for the Slice operator.
     */
    using Attributes_ = StaticAttributes<SliceAttr,
                                            std::vector<std::int64_t>, // Starts
                                            std::vector<std::int64_t>, // Ends
                                            std::vector<std::int8_t>,  // Axes
                                            std::vector<std::int64_t>>; // Steps

private:
    template <SliceAttr e>
    using attr = typename Attributes_::template attr<e>;

    const std::shared_ptr<Attributes_> mAttributes;

public:
    Slice_Op() = delete;

    /**
     * @brief Constructor for Slice operator.
     * @param[in] starts Starting indices for the slice along each axis.
     * @param[in] ends Ending indices (exclusive) for the slice along each axis.
     * @param[in] axes Axes along which the slice operation is performed.
     * @param[in] steps Step sizes for slicing along each axis.
     */
    Slice_Op(const std::vector<std::int64_t>& starts,
             const std::vector<std::int64_t>& ends,
             const std::vector<std::int8_t>& axes,
             const std::vector<std::int64_t>& steps);

    /**
     * @brief Copy-constructor.
     * @param[in] op Operator to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    Slice_Op(const Slice_Op &op);

    /**
     * @brief Clone the operator using its copy-constructor.
     */
    std::shared_ptr<Operator> clone() const override;

    bool dimsForwarded() const override final;

    bool forwardDims(bool allowDataDependency = true) override final;

    void setBackend(const std::string &name, DeviceIdx_t device = 0) override;

    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get the starting indices for the slice.
     */
    inline std::vector<std::int64_t>& starts() const noexcept { return mAttributes -> getAttr<SliceAttr::Starts>(); }

    /**
     * @brief Get the ending indices for the slice.
     */
    inline std::vector<std::int64_t>& ends() const noexcept { return mAttributes -> getAttr<SliceAttr::Ends>(); }

    /**
     * @brief Get the axes along which the slice is performed.
     */
    inline std::vector<std::int8_t>& axes() const noexcept { return mAttributes -> getAttr<SliceAttr::Axes>(); }

    /**
     * @brief Get the steps for the slice operation.
     */
    inline std::vector<std::int64_t>& steps() const noexcept { return mAttributes -> getAttr<SliceAttr::Steps>(); }

    static const std::vector<std::string> getInputsName() {
        return {"data_input", "starts", "ends", "axes", "steps"};
    }

    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Extract a sub-Tensor from a bigger original Tensor.
 *
 * @param[in] starts Starting indices for the slice.
 * @param[in] ends Ending indices (exclusive) for the slice.
 * @param[in] axes Axes along which the slice operation is performed.
 * @param[in] steps Step sizes for slicing along each axis.
 * @param[in] name Name of the Operator.
 * @return std::shared_ptr<Node> A Node containing the Operator.
 */
std::shared_ptr<Node> Slice(const std::vector<std::int64_t>& starts = {},
                            const std::vector<std::int64_t>& ends = {},
                            const std::vector<std::int8_t>& axes = {},
                            const std::vector<std::int64_t>& steps = {},
                            const std::string &name = "");

}  // namespace Aidge

namespace {
template <>
const char *const EnumStrings<Aidge::SliceAttr>::data[] = { "starts", "ends", "axes", "steps" };
}

#endif /* AIDGE_CORE_OPERATOR_SLICE_H_ */
