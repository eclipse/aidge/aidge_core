/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_CLIP_H_
#define AIDGE_CORE_OPERATOR_CLIP_H_

#include <memory>
#include <vector>
#include <limits>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @enum ClipAttr
 * @brief Enum class defining the attributes for the Clip operator.
 */
enum class ClipAttr {
    Min,  /**< Minimum value for clipping. */
    Max   /**< Maximum value for clipping. */
};

/**
 * @brief Description of the Clip operation to limit tensor values within a specified range.
 *
 * The Clip operator ensures tensor elements are within the range `[min, max]`.
 * - Values less than `min` are set to `min`.
 * - Values greater than `max` are set to `max`.
 * 
 * The input and output Tensors have the same dimensions.
 *
 * ### Attributes:
 * - `Min`: Minimum value for clipping.
 * - `Max`: Maximum value for clipping.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Clip_Op : public OperatorTensor,
    public Registrable<Clip_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const Clip_Op&)>> {

public:
    /**
     * @brief Type string identifying this operator.
     */
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<ClipAttr, float, float>;

    template <ClipAttr e>
    using attr = typename Attributes_::template attr<e>;

    const std::shared_ptr<Attributes_> mAttributes;

public:
    /**
     * @brief Deleted default constructor.
     */
    Clip_Op() = delete;

    /**
     * @brief Constructor for Clip operator.
     * @param[in] min Minimum value for clipping.
     * @param[in] max Maximum value for clipping.
     */
    Clip_Op(float min, float max)
        : OperatorTensor(Type, {InputCategory::Data, InputCategory::OptionalData, InputCategory::OptionalData}, 1),
          mAttributes(std::make_shared<Attributes_>(attr<ClipAttr::Min>(min), attr<ClipAttr::Max>(max))) {}

    /**
     * @brief Copy-constructor. Copies operator attributes and output tensors, but not input tensors.
     * @param op Clip_Op instance to copy.
     */
    Clip_Op(const Clip_Op& op)
        : OperatorTensor(op),
          mAttributes(op.mAttributes)
    {
        if (op.mImpl) {
            SET_IMPL_MACRO(Clip_Op, *this, op.backend());
        } else {
            mImpl = nullptr;
        }
    }

    /**
     * @brief Clone the operator using its copy constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override {
        return std::make_shared<Clip_Op>(*this);
    }

    bool dimsForwarded() const override final;
    bool forwardDims(bool allowDataDependency = false) override final;

    /**
     * @brief Setter to specify the backend to use.
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;

    /**
     * @brief Access the attributes of the operator.
     * @return A shared pointer to the attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Getter for the minimum clipping value.
     * @return Reference to the minimum value.
     */
    inline float& min() const noexcept { return mAttributes->getAttr<ClipAttr::Min>(); }

    /**
     * @brief Getter for the maximum clipping value.
     * @return Reference to the maximum value.
     */
    inline float& max() const noexcept { return mAttributes->getAttr<ClipAttr::Max>(); }

    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the input tensor names for the Clip operator.
     * @return A vector containing the input tensor names.
     */
    static const std::vector<std::string> getInputsName() {
        return { "data_input", "min_empty_tensor", "max_empty_tensor" };
    }

    /**
     * @brief Get the output tensor names for the Clip operator.
     * @return A vector containing the output tensor names.
     */
    static const std::vector<std::string> getOutputsName() {
        return { "data_output" };
    }
};

/**
 * @brief Factory function to create a Clip node.
 * @param[in] name Name of the node.
 * @param[in] min Minimum clipping value (default: lowest float).
 * @param[in] max Maximum clipping value (default: maximum float).
 * @return A shared pointer to the created Clip node.
 */
std::shared_ptr<Aidge::Node> Clip(
    const std::string& name = "",
    float min = std::numeric_limits<float>::lowest(),
    float max = std::numeric_limits<float>::max()
);

} // namespace Aidge

namespace {
/**
 * @brief Specialization of EnumStrings for ClipAttr.
 */
template <>
const char* const EnumStrings<Aidge::ClipAttr>::data[] = { "min", "max" };
}

#endif /* AIDGE_CORE_OPERATOR_CLIP_H_ */
