/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_GATHER_H_
#define AIDGE_CORE_OPERATOR_GATHER_H_

#include <cstdint>  // std::int8_t, std::int64_t
#include <memory>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @class Gather_OpImpl
 * @brief Backend implementation for the Gather operation.
 *
 * The Gather operation selects elements from the input tensor based on specified indices
 * and an axis, producing a tensor with a gathered shape.
 */
class Gather_OpImpl : public OperatorImpl {
public:
    Gather_OpImpl(const Operator& op, const std::string& backend = "")
        : OperatorImpl(op, backend) {}

    /**
     * @brief Execute the Gather operation.
     */
    void forward() override;
};

enum class GatherAttr {
    /**
     * @brief Axis along which to gather elements.
     */
    Axis,

    /**
     * @brief Indices specifying which elements to gather.
     */
    Indices,

    /**
     * @brief Shape of the resulting gathered tensor.
     */
    GatheredShape
};

/**
 * @brief Description for the Gather operation on an input tensor.
 *
 * The Gather operation extracts elements from an input tensor along a specified axis
 * using a set of indices, resulting in a gathered tensor with a specified shape.
 *
 * The shape of the output tensor depends on the gathered shape attribute, the axis,
 * and the indices.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Gather_Op : public OperatorTensor,
                  public Registrable<Gather_Op,
                                     std::string,
                                     std::function<std::shared_ptr<OperatorImpl>(const Gather_Op&)>> {
public:
    static const std::string Type;

    using Attributes_ = StaticAttributes<GatherAttr,
                                          std::int8_t,
                                          std::vector<int64_t>,
                                          std::vector<DimSize_t>>;

private:
    template <GatherAttr e>
    using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    /**
     * @brief Default constructor is deleted.
     */
    Gather_Op() = delete;

    /**
     * @brief Constructor for the Gather operator.
     * @param[in] axis The axis along which to gather elements.
     * @param[in] indices A vector specifying which elements to gather.
     * @param[in] gatheredShape The shape of the resulting gathered tensor.
     */
    Gather_Op(std::int8_t axis,
              const std::vector<int64_t>& indices,
              const std::vector<DimSize_t>& gatheredShape);

    /**
     * @brief Copy constructor.
     * @param[in] op The Gather operator to copy.
     * @details Copies the operator attributes and its output tensor(s), but not its input tensors.
     *          The new operator has no associated input tensors.
     */
    Gather_Op(const Gather_Op& op);

    /**
     * @brief Clone the operator using its copy constructor.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Check if dimensions have been forwarded.
     * @return True if dimensions have been forwarded, false otherwise.
     */
    bool dimsForwarded() const override final;

    /**
     * @brief Forward the dimensions.
     * @param[in] allowDataDependency Whether to allow data dependency during dimension forwarding.
     * @return True if successful, false otherwise.
     */
    bool forwardDims(bool allowDataDependency = false) override final;

    /**
     * @brief Set the backend for the operator.
     * @param name The name of the backend.
     * @param device Optional. The device index.
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override;

    /**
     * @brief Get the available backends for the operator.
     * @return A set of available backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     * @return A shared pointer to the attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get the axis along which elements are gathered.
     * @return The axis attribute.
     */
    inline std::int8_t& axis() const { return mAttributes->getAttr<GatherAttr::Axis>(); }

    /**
     * @brief Get the indices specifying which elements to gather.
     * @return The indices attribute.
     */
    inline std::vector<int64_t>& indices() const { return mAttributes->getAttr<GatherAttr::Indices>(); }

    /**
     * @brief Get the shape of the gathered tensor.
     * @return The gathered shape attribute.
     */
    inline std::vector<DimSize_t>& gatheredShape() const { return mAttributes->getAttr<GatherAttr::GatheredShape>(); }

    /**
     * @brief Get the input tensor names.
     * @return A vector of input tensor names.
     */
    static const std::vector<std::string> getInputsName() {
        return {"data_input", "indices"};
    }

    /**
     * @brief Get the output tensor names.
     * @return A vector of output tensor names.
     */
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Create a Gather node.
 *
 * Initializes a Gather node that extracts elements from an input tensor
 * along a specified axis using a set of indices.
 *
 * @param[in] axis The axis along which to gather elements. Default is 0.
 * @param[in] indices A vector specifying which elements to gather. Default is an empty vector.
 * @param[in] gatheredShape The shape of the resulting gathered tensor. Default is an empty vector.
 * @param[in] name Optional. The name of the node.
 * @return A shared pointer to a Node representing the Gather operation.
 */
std::shared_ptr<Node> Gather(std::int8_t axis = 0,
                             const std::vector<int64_t>& indices = {},
                             const std::vector<DimSize_t>& gatheredShape = {},
                             const std::string& name = "");

} // namespace Aidge

namespace {
template <>
const char *const EnumStrings<Aidge::GatherAttr>::data[] = {"axis", "indices", "gathered_shape"};
}

#endif /* AIDGE_CORE_OPERATOR_GATHER_H_ */
