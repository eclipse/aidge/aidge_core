/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_FC_H_
#define AIDGE_CORE_OPERATOR_FC_H_

#include <array>
#include <memory>
#include <vector>

#include "aidge/utils/Types.h"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Registrar.hpp"

namespace Aidge {

/**
 * @brief Description of a Fully Connected (FC) operation on an input Tensor.
 *
 * The Fully Connected (FC) operation applies a linear transformation to the input Tensor
 * by multiplying it with a weight matrix and optionally adding a bias vector: 
 * - If `bias` is included:
 *   f(x) = x × weights^T + bias
 * - If `bias` is omitted:
 *   f(x) = x × weights^T
 *
 * Attributes:
 * - `inChannels`: The number of input features (or channels). Determined from the dimensions
 *   of the weight Tensor. This represents the size of the input vector.
 * - `outChannels`: The number of output features (or channels). Determined from the dimensions
 *   of the weight Tensor. This represents the size of the output vector.
 * - `noBias`: A boolean value indicating whether the bias vector is omitted in the operation.
 *
 * @example:
 * - Input Tensor: Shape (64, 128)  // Batch size of 64, 128 input features
 * - Weights: Shape (256, 128)      // 128 input channels, 256 output channels
 * - Bias: Shape (256)              // Bias vector (optional)
 * - Output Tensor: Shape (64, 256) // Batch size of 64, 256 output features
 * @see OperatorTensor
 * @see Registrable
 */
class FC_Op : public OperatorTensor,
              public Registrable<FC_Op,
                                 std::string,
                                 std::function<std::shared_ptr<OperatorImpl>(const FC_Op &)>> {
public:
    /**
     * @brief Static type identifier for the FC operator.
     */
    static const std::string Type;

    /**
     * @brief Default constructor for the FC operator.
     *
     * Initializes the operator with a type identifier and input categories.
     */
    FC_Op()
    : OperatorTensor(Type, {InputCategory::Data, InputCategory::Param, InputCategory::OptionalParam}, 1)
    {}

    /**
     * @brief Copy constructor.
     *
     * Copies the attributes and output tensor(s) of the operator, but does not
     * copy input tensors. The new operator instance has no associated inputs.
     * 
     * @param op The `FC_Op` instance to copy.
     */
    FC_Op(const FC_Op& op)
        : OperatorTensor(op)
    {
        if (op.mImpl) {
            SET_IMPL_MACRO(FC_Op, *this, op.backend());
        } else {
            mImpl = nullptr;
        }
    }

    /**
     * @brief Clones the operator using its copy constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override final;

    /**
     * @brief Associates an input tensor with the operator.
     *
     * This method links the specified input tensor to the operator's input slot.
     *
     * @param[in] inputIdx Index of the input to associate.
     * @param[in] data Shared pointer to the input tensor data.
     */
    void associateInput(const IOIndex_t inputIdx, const std::shared_ptr<Data>& data) override final;

    /**
     * @brief Computes the output dimensions during the forward pass.
     *
     * Calculates the dimensions of the output tensor based on the input dimensions,
     * weights, and optional parameters.
     *
     * @param[in] allowDataDependency Flag to allow data-dependent dimension computation.
     * @return Boolean indicating the success of the dimension computation.
     */
    bool forwardDims(bool allowDataDependency = false) override final;

    /**
     * @brief Sets the backend for the operator.
     *
     * Configures the backend used for computation.
     *
     * @param[in] name Name of the backend.
     * @param[in] device Index of the target device (default is 0).
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override;

    /**
     * @brief Retrieves the available backends for the operator.
     * @return A set of backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Gets the number of input channels for the FC operator.
     *
     * Retrieves the number of input channels from the weight tensor.
     *
     * @return Number of input channels.
     * @throws std::runtime_error if no weight tensor is associated with the operator.
     */
    DimSize_t inChannels() const {
        if (!getInput(1)) {
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Fully Connected (FC) operator has no weight Tensor associated so no specific number of input channel imposed.");
        }
        return getInput(1)->template dims<2>()[1];
    }

    /**
     * @brief Gets the number of output channels for the FC operator.
     *
     * Retrieves the number of output channels from the weight tensor.
     *
     * @return Number of output channels.
     * @throws std::runtime_error if no weight tensor is associated with the operator.
     */
    DimSize_t outChannels() const {
        if (!getInput(1)) {
            AIDGE_THROW_OR_ABORT(std::runtime_error, "Fully Connected (FC) operator has no weight Tensor associated so no specific number of output channel imposed.");
        }
        return getInput(1)->template dims<2>()[0];
    }

    /**
     * @brief Retrieves the input tensor names for the FC operator.
     * @return A vector of input tensor names: `{"data_input", "weight", "bias"}`.
     */
    static const std::vector<std::string> getInputsName() {
        return {"data_input", "weight", "bias"};
    }

    /**
     * @brief Retrieves the output tensor names for the FC operator.
     * @return A vector of output tensor names: `{"data_output"}`.
     */
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Creates a Fully Connected operation node.
 *
 * Constructs an FC operator node with the specified input and output channels.
 *
 * @param[in] inChannels Number of input channels.
 * @param[in] outChannels Number of output channels.
 * @param[in] noBias Flag indicating whether to use a bias term (default is `false`).
 * @param[in] name Name of the operator (optional).
 * @return A shared pointer to the Node containing the FC operator.
 */
std::shared_ptr<Node> FC(const DimSize_t inChannels, const DimSize_t outChannels, bool noBias = false, const std::string& name = "");

} // namespace Aidge

#endif /* AIDGE_CORE_OPERATOR_FC_H_ */
