/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_POW_H_
#define AIDGE_CORE_OPERATOR_POW_H_

#include <memory>
#include <string>
#include <vector>

#include "aidge/utils/Registrar.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @brief Description of an element-wise And operation on input Tensors,
 * supporting NumPy broadcasting.
 *
 * For each pair of elements x and y from the input Tensors, the function 
 * is defined as:
 * `f(x, y) = x^y`
 *
 * Broadcasting adjusts shapes of the input Tensors to make them compatible:
 * - Tensors are aligned from the rightmost dimensions.
 * - Dimensions are compatible if they are equal, one of them is 1, or missing.
 *
 * The output Tensor shape is determined by taking the maximum size along 
 * each dimension of the input Tensors after broadcasting.
 * 
 * Examples:
 * 1. Input A: (3, 4, 2), Input B: (2), Output: (3, 4, 2)
 * 2. Input A: (1, 5, 3), Input B: (2, 1, 3), Output: (2, 5, 3)
 * 
 * @see OperatorTensor
 * @see Registrable
 */
class Pow_Op : public OperatorTensor,
    public Registrable<Pow_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const Pow_Op&)>> {
public:
    static const std::string Type;

    Pow_Op() : OperatorTensor(Type, {InputCategory::Data, InputCategory::Data}, 1) {}

    /**
     * @brief Copy-constructor.
     * @param op Pow_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    Pow_Op(const Pow_Op& op)
        : OperatorTensor(op)
    {
        if (op.mImpl){
            SET_IMPL_MACRO(Pow_Op, *this, op.backend());
        }else{
            mImpl = nullptr;
        }
    }

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::Pow_Op
     */
    std::shared_ptr<Operator> clone() const override {
        return std::make_shared<Pow_Op>(*this);
    }

    bool forwardDims(bool allowDataDependency = false) override final;


    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;
    std::set<std::string> getAvailableBackends() const override;

    static const std::vector<std::string> getInputsName() {
        return {"data_input_1", "data_input_2"};
    }
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

std::shared_ptr<Node> Pow(const std::string& name = "");
} // namespace Aidge

#endif /* AIDGE_CORE_OPERATOR_POW_H_ */
