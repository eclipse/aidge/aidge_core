/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_WEIGHTINTERLEAVING_H_
#define AIDGE_CORE_OPERATOR_WEIGHTINTERLEAVING_H_

#include <cassert>
#include <memory>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"


namespace Aidge {

/**
 * @brief WeightInterleaving operator Compresses the last dimension of a tensor by packing low-bitwidth values
 * (e.g., 2, 3, or 4 bits) into fewer bytes.
 *
 * The operator reduces the size of the last dimension based on the bitwidth (`nb_bits`), 
 * packing multiple values into each byte. For example, 4-bit values result in a halved last dimension, 
 * while 2-bit values reduce it by a factor of 4.
 * 
 * The output tensor has the same shape as the input, except for the compressed last dimension.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class WeightInterleaving_Op :
    public OperatorTensor,
    public Registrable<WeightInterleaving_Op,  // <Op, backend, implementation creation function>
        std::string,
        std::function<std::shared_ptr<OperatorImpl>(const WeightInterleaving_Op&)>>
{
public:
    static const std::string Type;

    WeightInterleaving_Op() : OperatorTensor(Type, {InputCategory::Data}, 1) {}

    /**
     * @brief Copy-constructor.
     * @param op WeightInterleaving_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    WeightInterleaving_Op(const WeightInterleaving_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::WeightInterleaving_Op
     */
    std::shared_ptr<Operator> clone() const override;

    bool forwardDims(bool allowDataDependency = false) override final;

    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;
    std::set<std::string> getAvailableBackends() const override;

    static const std::vector<std::string> getInputsName(){
        return {"data_input"};
    }
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }

    /**
     * @brief Calculates the required size for the 8-bits`compactData` vector.
     * 
     * This function determines the minimum number of bytes needed in `compactData`
     * to store `dataSize` elements compacted to `nb_bits` bits each.
     * 
     * @param dataSize The total number of elements in the input data array.
     * @param nb_bits The number of bits to use for each compacted element (from 1 to 7).
     * @return std::size_t The required size in bytes for `compactData`.
     */
    std::size_t compactDataSize(std::size_t dataSize, std::uint8_t nb_bits);

};

std::shared_ptr<Node> WeightInterleaving(const std::string& name = "");
}

#endif /* AIDGE_CORE_OPERATOR_RELU_H_ */
