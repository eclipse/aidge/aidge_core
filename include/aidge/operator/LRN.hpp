/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_LRN_H_
#define AIDGE_CORE_OPERATOR_LRN_H_

#include <memory>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
enum class LRNAttr {
    Alpha,  ///< Scale factor for normalization.
    Beta,   ///< Exponent applied to the normalization term.
    Bias,   ///< Constant bias added to the normalization term.
    Size    ///< Number of channels to normalize over.
};

/**
 * @brief Description of a Local Response Normalization (LRN) operation on an input Tensor.
 *
 * LRN is a normalization technique that applies across channels in a local region 
 * to enhance generalization and promote competition between neurons. It is commonly 
 * used in Convolutional Neural Networks (CNNs).
 *
 * For each element x in the input Tensor, the function is defined as:
 * `f(x) = x / (bias + alpha * sum(x_i^2))^beta`, where:
 * - `x` is the current element being normalized.
 * - The summation `sum(x_i^2)` is taken over a local region of `size` channels 
 *   surrounding `x` (both before and after the current channel, if available).
 * - `bias`, `alpha`, and `beta` are scalar hyperparameters controlling the 
 *   normalization behavior.
 *
 * Parameters:
 * - `size`: The number of channels in the local region over which normalization is performed.
 * - `bias`: A scalar added to the normalization term to ensure numerical stability.
 * - `alpha`: A scaling factor for the squared sum of elements in the local region.
 * - `beta`: The exponent applied to the normalization term.
 *
 * The input and output Tensors have the same shape. If the input Tensor has shape `(N, C, H, W)`, 
 * the output Tensor will also have shape `(N, C, H, W)`.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class LRN_Op : public OperatorTensor,
                public Registrable<LRN_Op,
                                   std::string,
                                   std::function<std::shared_ptr<OperatorImpl>(const LRN_Op&)>> {

public:
    /**
     * @brief Static type string for the LRN operator.
     */
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<LRNAttr, float, float, float, std::int32_t>;
    template <LRNAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    /**
     * @brief Deleted default constructor.
     */
    LRN_Op() = delete;

    /**
     * @brief Constructor for the LRN operator.
     * @param[in] size Normalization size (span across adjacent channels).
     */
    LRN_Op(std::int32_t size);

    /**
     * @brief Copy-constructor.
     * @param[in] op LRN_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    LRN_Op(const LRN_Op& op);

    /**
     * @brief Clone the operator using its copy constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Set the backend for the LRN operator.
     * @param[in] name Name of the backend.
     * @param[in] device Device index (optional).
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;

    /**
     * @brief Get the available backends for the LRN operator.
     * @return A set of backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     * @return A shared pointer to the attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get or modify the `alpha` attribute.
     * @return Reference to the `alpha` attribute.
     */
    inline float& alpha() const noexcept { return mAttributes->getAttr<LRNAttr::Alpha>(); }

    /**
     * @brief Get or modify the `beta` attribute.
     * @return Reference to the `beta` attribute.
     */
    inline float& beta() const noexcept { return mAttributes->getAttr<LRNAttr::Beta>(); }

    /**
     * @brief Get or modify the `bias` attribute.
     * @return Reference to the `bias` attribute.
     */
    inline float& bias() const noexcept { return mAttributes->getAttr<LRNAttr::Bias>(); }

    /**
     * @brief Get or modify the `size` attribute.
     * @return Reference to the `size` attribute.
     */
    inline std::int32_t& size() const noexcept { return mAttributes->getAttr<LRNAttr::Size>(); }

    /**
     * @brief Get the input tensor names for the LRN operator.
     * @return A vector of input tensor names.
     */
    static const std::vector<std::string> getInputsName() {
        return {"data_input"};
    }

    /**
     * @brief Get the output tensor names for the LRN operator.
     * @return A vector of output tensor names.
     */
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Create an LRN operation node.
 *
 * @param[in] size Normalization size (must be an odd positive integer).
 * @param[in] name Name of the operator (optional).
 * @return A shared pointer to the Node containing the LRN operator.
 */
std::shared_ptr<Node> LRN(std::int32_t size, const std::string& name = "");

} // namespace Aidge

namespace {
/**
 * @brief EnumStrings specialization for LRNAttr.
 */
template <>
const char *const EnumStrings<Aidge::LRNAttr>::data[] = {"alpha", "beta", "bias", "size"};
}

#endif /* AIDGE_CORE_OPERATOR_LRN_H_ */
