/********************************************************************************
 * Copyright (c) 2024 Thales
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * Author: Lucas RAKOTOARIVONY, Thales Research & Technology France
 * Date: 10.09.2024
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_SHIFTMAX_H_
#define AIDGE_CORE_OPERATOR_SHIFTMAX_H_

#include <cassert>
#include <memory>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @brief Description of an element-wise Shifted Maximum (ShiftMax) operation on an input Tensor.
 *
 * The ShiftMax operation computes a shifted and quantized version of the input tensor's values,
 * designed to approximate the maximum function while optimizing for quantized tensor computations.
 * For each element `x` in the input tensor, the operation can be summarized as:
 *
 * `f(x) = scale_factor * quantize(shift(x - max_val))`
 *
 * Where:
 * - `max_val` is the maximum value of the quantized tensor within a given dimension.
 * - `quantize` represents a rounding operation to convert values into a lower precision format.
 * - `shift` applies exponent shifting and clamping to the adjusted values to mimic smooth scaling.
 * - `scale_factor` adjusts the final output to match the desired numerical range for quantized tensors.
 *
 * The algorithm ensures efficient computation by using integer arithmetic and avoids precision loss by
 * scaling the outputs with a dynamic factor based on the tensor's sum.
 *
 * The input and output Tensors have the same dimensions, and this operation is particularly suited for
 * neural networks requiring efficient inference with quantized inputs.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class ShiftMax_Op : public OperatorTensor,
    public Registrable<ShiftMax_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const ShiftMax_Op&)>> {
public:
    static const std::string Type;

    ShiftMax_Op();

    /**
     * @brief Copy-constructor.
     * @param op ShiftMax_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    ShiftMax_Op(const ShiftMax_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::ShiftMax_Op
     */
    std::shared_ptr<Operator> clone() const override;


    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;
    std::set<std::string> getAvailableBackends() const override;

    static const std::vector<std::string> getInputsName(){
        return {"data_input"};
    }
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }
};

std::shared_ptr<Node> ShiftMax(const std::string& name = "");
}

#endif /* AIDGE_CORE_OPERATOR_SHIFTMAX_H_ */
