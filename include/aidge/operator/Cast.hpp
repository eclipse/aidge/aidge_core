/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_CAST_H_
#define AIDGE_CORE_OPERATOR_CAST_H_

#include <cassert>
#include <memory>
#include <vector>

#include "aidge/utils/Registrar.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
class Cast_OpImpl : public OperatorImpl {
public:
    Cast_OpImpl(const Operator& op, const std::string& backend = ""): OperatorImpl(op, backend) {}
    void forward() override;
};

/**
 * @enum CastAttr
 * @brief Enum class defining the attributes for the Cast operator.
 */
enum class CastAttr {
    /**
     * @brief Target data type for the cast operation.
     */
    TargetType
};

/**
 * @brief Description of the Cast operation to convert a tensor's data type.
 *
 * The Cast operator changes the data type of input tensor elements to a specified target type.
 *
 * ### Attributes:
 * - `TargetType`: Specifies the data type to which the tensor elements are cast.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Cast_Op : public OperatorTensor,
    public Registrable<Cast_Op, std::string, std::function<std::unique_ptr<OperatorImpl>(const Cast_Op&)>> {

public:
    /**
     * @brief Type string identifying this operator.
     */
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<CastAttr, DataType>;

    template <CastAttr e>
    using attr = typename Attributes_::template attr<e>;

    const std::shared_ptr<Attributes_> mAttributes;

public:
    /**
     * @brief Deleted default constructor.
     */
    Cast_Op() = delete;

    /**
     * @brief Constructor for Cast operator.
     * @param[in] targetType The desired data type to which the tensor will be cast.
     */
    Cast_Op(const DataType targetType);

    /**
     * @brief Copy-constructor. Copy the operator attributes and its output tensor(s), but not its input tensors (the new operator has no input associated).
     * @param op Operator to copy.
     */
    Cast_Op(const Cast_Op& op)
        : OperatorTensor(op),
          mAttributes(op.mAttributes)
    {
        if (!op.backend().empty()) {
            SET_IMPL_MACRO(Cast_Op, *this, op.backend());
        }
        else {
            mImpl = std::make_shared<Cast_OpImpl>(*this);
        }
    }

    /**
     * @brief Clone the operator using its copy constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override {
        return std::make_shared<Cast_Op>(*this);
    }


    void setBackend(const std::string& name, DeviceIdx_t device = 0) override;

    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Access the attributes of the operator.
     * @return A shared pointer to the attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get the target data type for the cast operation.
     * @return Reference to the target data type.
     */
    inline DataType& targetType() const { return mAttributes->template getAttr<CastAttr::TargetType>(); }

    /**
     * @brief Get the input tensor names for the Cast operator.
     * @return A vector containing the input tensor names.
     */
    static const std::vector<std::string> getInputsName() {
        return {"data_input"};
    }

    /**
     * @brief Get the output tensor names for the Cast operator.
     * @return A vector containing the output tensor names.
     */
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Factory function to create a Cast node.
 * @param[in] targetType The desired data type to cast to.
 * @param[in] name Name of the operator node.
 * @return A shared pointer to the created Cast node.
 */
std::shared_ptr<Node> Cast(const DataType targetType, const std::string& name = "");

}  // namespace Aidge

namespace {
template <>
const char* const EnumStrings<Aidge::CastAttr>::data[] = { "target_type" };
}

#endif /* AIDGE_CORE_OPERATOR_CAST_H_ */
