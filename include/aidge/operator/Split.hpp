/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_SPLIT_H_
#define AIDGE_CORE_OPERATOR_SPLIT_H_

#include <memory>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @class  Implementation of the Split operation.
 *
 * Since Split operation is just backend-agnostic, its implementation is located in aidge_core.
 */
class Split_OpImpl : public OperatorImpl {
public:
    /**
     * @brief Constructor for the Split operator implementation.
     * @param[in] op Operator to be implemented.
     * @param[in] backend Name of the backend.
     */
    Split_OpImpl(const Operator& op, const std::string& backend = "") : OperatorImpl(op, backend) {}

    /**
     * @brief Executes the forward pass for the Split operation.
     */
    void forward() override;
};

/**
 * @enum SplitAttr
 * @brief Enumeration of Split operator attributes.
 */
enum class SplitAttr {
    /**
     * @brief Axis along which to split the input tensor.
     *
     * The specified axis determines the direction of splitting.
     */
    Axis,

    /**
     * @brief Sizes of each output tensor after splitting.
     *
     * If specified, the sum of the split sizes must match the size of the input
     * tensor along the specified axis.
     */
    Split
};

/**
 * @class Split_Op
 * @brief Implements the Split operation to divide a tensor into multiple sub-tensors along a specified axis.
 *
 * The Split operation divides the input tensor into several output tensors along
 * a specified axis. The size of each output tensor can be defined by the Split
 * attribute. If no split sizes are provided, the input is divided evenly.
 *
 * ### Output Shape Calculation
 * - Input shape: (N, D1, D2, ..., DN)
 * - Axis: 1
 * - Split: {2, 3}
 * - Output shapes: [(N, 2, D2, ..., DN), (N, 3, D2, ..., DN)]
 *
 * @example:
 * - Input shape: (6, 4, 8)
 * - Axis: 0
 * - Split: {2, 2, 2}
 * - Output shapes: [(2, 4, 8), (2, 4, 8), (2, 4, 8)]
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Split_Op : public OperatorTensor,
                 public Registrable<Split_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const Split_Op &)>> {

public:
    /**
     * @brief Static type string for the Split operator.
     */
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<SplitAttr, std::int8_t, std::vector<DimSize_t>>;
    template <SplitAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    Split_Op() = delete;

    /**
     * @brief Constructor for the Split operator.
     * @param[in] axis Axis along which to split the input tensor.
     * @param[in] nbOutputs Number of output tensors to create.
     * @param[in] split Sizes of each split (optional).
     */
    Split_Op(std::int8_t axis, DimSize_t nbOutputs, const std::vector<DimSize_t>& split);

    /**
     * @brief Copy-constructor.
     * @param op Split_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    Split_Op(const Split_Op& op);

    /**
     * @brief Clone the operator using its copy constructor.
     */
    std::shared_ptr<Operator> clone() const override;

    bool dimsForwarded() const override final;
    bool forwardDims(bool allowDataDependency = false) override final;

    /**
     * @brief Set the backend for the Split operator.
     * @param[in] name Backend name.
     * @param[in] device Device index (optional).
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override;

    /**
     * @brief Get the available backends for the Split operator.
     * @return A set of backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get the axis along which the input tensor is split.
     */
    inline std::int8_t& axis() const { return mAttributes->template getAttr<SplitAttr::Axis>(); }

    /**
     * @brief Get the sizes of each split.
     */
    inline std::vector<DimSize_t>& split() const { return mAttributes->template getAttr<SplitAttr::Split>(); }

    /**
     * @brief Get the input names for the Split operator.
     * @return A vector of input tensor names.
     */
    static const std::vector<std::string> getInputsName() {
        return {"data_input", "split"};
    }

    /**
     * @brief Get the output names for the Split operator.
     * @return A vector of output tensor names.
     */
    static const std::vector<std::string> getOutputsName() {
        return {"data_output_0", "data_output_n"};
    }
};

/**
 * @brief Split a tensor into multiple sub-tensors along a specified axis.
 *
 * @param[in] nbOutput Number of output tensors to create.
 * @param[in] axis Axis along which to split the input tensor.
 * @param[in] split Sizes of each split (optional).
 * @param[in] name Name of the Operator.
 * @return std::shared_ptr<Node> Node containing the Operator.
 */
std::shared_ptr<Node> Split(DimSize_t nbOutput,
                            std::int8_t axis = 0,
                            const std::vector<DimSize_t>& split = {},
                            const std::string& name = "");

}  // namespace Aidge

namespace {
/**
 * @brief EnumStrings specialization for SplitAttr.
 */
template <>
const char* const EnumStrings<Aidge::SplitAttr>::data[] = {"axis", "split"};
}

#endif /* AIDGE_CORE_OPERATOR_SPLIT_H_ */
