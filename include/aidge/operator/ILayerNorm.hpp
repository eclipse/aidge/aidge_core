/********************************************************************************
 * Copyright (c) 2024 Thales
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * Author: Lucas RAKOTOARIVONY, Thales Research & Technology France
 * Date: 10.09.2024
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_ILAYERNORM_H_
#define AIDGE_CORE_OPERATOR_ILAYERNORM_H_

#include <cassert>
#include <memory>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @brief Description of the Layer Normalization operation on an input tensor.
 *
 * Layer Normalization normalizes the inputs across the features of a given tensor:
 *  f(x) =
 *
 *
 * The input and output Tensors have the same dimensions.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class ILayerNorm_Op : public OperatorTensor,
    public Registrable<ILayerNorm_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const ILayerNorm_Op&)>> {

public:
    static const std::string Type;

    /**
     * @brief Default constructor.
     */
    ILayerNorm_Op()
    : OperatorTensor(Type, {InputCategory::Data, InputCategory::Param, InputCategory::Param}, 1)
    {}

    /**
     * @brief Copy-constructor.
     * @param[in] op ILayerNorm_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not its input tensors. 
     * The new operator has no associated input.
     */
    ILayerNorm_Op(const ILayerNorm_Op& op)
        : OperatorTensor(op)
    {
        if (op.mImpl){
            SET_IMPL_MACRO(ILayerNorm_Op, *this, op.backend());
        } else {
            mImpl = nullptr;
        }
    }

    /**
     * @brief Clone the operator using its copy-constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override {
        return std::make_shared<ILayerNorm_Op>(*this);
    }

    /**
     * @brief Associates an input tensor with the operator.
     * @param inputIdx The index of the input.
     * @param data The input data to associate.
     */
    void associateInput(const IOIndex_t inputIdx, const std::shared_ptr<Data>& data) override final;

    /**
     * @brief Propagates the dimensions from the input tensors to the output tensors.
     * @param allowDataDependency Whether to allow dimension propagation based on data.
     * @return True if propagation is successful, false otherwise.
     */
    bool forwardDims(bool allowDataDependency = false) override final;

    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Gets the names of the input tensors.
     * @return A vector containing the names of input tensors.
     */
    static const std::vector<std::string> getInputsName(){
        return {"data_input", "weight", "bias"};
    }

    /**
     * @brief Gets the names of the output tensors.
     * @return A vector containing the names of output tensors.
     */
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }
};

/**
 * @brief Constructs a Layer Normalization operation node.
 * @param[in] name Optional name for the operator.
 * @return A shared pointer to a Node containing the ILayerNorm_Op operator.
 */
inline std::shared_ptr<Node> ILayerNorm(const std::string& name = "") {
    return std::make_shared<Node>(std::make_shared<ILayerNorm_Op>(), name);
}

} // namespace Aidge

#endif /* AIDGE_CORE_OPERATOR_ILAYERNORM_H_ */
