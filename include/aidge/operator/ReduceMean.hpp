/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_REDUCEMEAN_H_
#define AIDGE_CORE_OPERATOR_REDUCEMEAN_H_

#include <cstdint>    // std::int32_t
#include <memory>
#include <string>
#include <vector>

#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
enum class ReduceMeanAttr {
  /**
   * @brief Axes over which the mean operation is performed.
   *
   * Axes are specified as a vector of integers, each representing a dimension
   * of the input tensor to be reduced.
   */
  Axes,

  /**
   * @brief Flag indicating whether to keep reduced dimensions.
   *
   * - `true`: Retain reduced dimensions with size 1.
   * - `false`: Completely remove reduced dimensions.
   */
  KeepDims,

  /**
   * @brief Flag indicating behavior when axes are empty.
   *
   * - `true`: No operation is performed if axes are empty.
   * - `false`: Reduction is performed over all axes if none are specified.
   */
  NoopWithEmptyAxes
};

/**
 * @class ReduceMean_Op
 * @brief Implements the ReduceMean operation to compute the mean of a tensor along specified axes.
 *
 * ReduceMean reduces specified axes of a tensor by computing the mean value.
 * The resulting output tensor dimensions depend on the provided attributes.
 *
 * Output Shape Calculation
 * - If `KeepDims` is true:
 *   Reduced dimensions are retained with size 1.
 * - If `KeepDims` is false:
 *   Reduced dimensions are completely removed.
 *
 * @example:
 * - Input shape: (2, 3, 4, 5) // Batch size 2, 3 channels, 4x5 spatial dimensions
 * - Axes: {2, 3}
 * - KeepDims: true
 * - Output shape: (2, 3, 1, 1)
 * @example:
 * - Input shape: (2, 3, 4, 5)
 * - Axes: {1, 2}
 * - KeepDims: false
 * - Output shape: (2, 5)
 *
 * @see OperatorTensor
 * @see Registrable
 */
class ReduceMean_Op : public OperatorTensor,
                public Registrable<ReduceMean_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const ReduceMean_Op &)>> {

public:
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<ReduceMeanAttr,
                                            std::vector<std::int32_t>,
                                            bool,
                                            bool>;

    template <ReduceMeanAttr e>
    using attr = typename Attributes_::template attr<e>;

    const std::shared_ptr<Attributes_> mAttributes;

public:
    ReduceMean_Op() = delete;

    /**
     * @brief constructor for ReduceMean operator
     * @param[in] axes around which perform the operation
     * @param[in] keep_dims if true we set a dimension of 1 in the place of the reduced axes and
     * if false we remove the dimension completely
     * @param[in] noop_with_empty_axes used when no axes are provided, if set to true, the operator does nothing
     * and if false, we reduce on all axes
     */
    ReduceMean_Op(const std::vector<std::int32_t>& axes, bool keep_dims = true, bool noop_with_empty_axes = false);

    /**
     * @brief Copy-constructor.
     * @param[in] op ReduceMean_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    ReduceMean_Op(const ReduceMean_Op& op);

    /**
     * @brief Clone the operator using its copy constructor.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Compute the output dimensions during the forward pass.
     * @param[in] allowDataDependency Whether to allow data-dependent dimensions.
     * @return Boolean indicating whether dimension computation was successful.
     */
    bool forwardDims(bool allowDataDependency = false) override final;

    /**
     * @brief Set the backend for the Reshape operator.
     * @param[in] name Name of the backend.
     * @param[in] device Device index (optional).
     */
    void setBackend(const std::string &name, DeviceIdx_t device = 0) override final;
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get the axes over which the mean is computed.
     */
    inline std::vector<std::int32_t>& axes() const noexcept { return mAttributes -> getAttr<ReduceMeanAttr::Axes>(); }

    /**
     * @brief Get whether reduced dimensions are retained.
     */
    inline bool& keepDims() const noexcept { return mAttributes -> getAttr<ReduceMeanAttr::KeepDims>(); }

    /**
     * @brief Get the behavior when axes are empty.
     */
    inline bool& noopWithEmptyAxes() const noexcept { return mAttributes -> getAttr<ReduceMeanAttr::NoopWithEmptyAxes>(); }

    static const std::vector<std::string> getInputsName() {
        return {"data_input"};
    }

    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }

    virtual ~ReduceMean_Op() noexcept;
};

/**
 * @brief Compute the mean value of a Tensor over the specified axes.
 *
 * Dimensions may be reduced by erasing the specified axes or retaining them with size 1.
 *
 * @param[in] axes Dimensions over which data mean should be computed.
 * @param[in] keep_dims Whether or not reduced dimensions are to be retained.
 * @param[in] noop_with_empty_axes Behavior when no axes are specified.
 * @param[in] name Name of the Operator.
 * @return std::shared_ptr<Node> Node containing the Operator.
 */
std::shared_ptr<Node> ReduceMean(const std::vector<std::int32_t> &axes,
                                        bool keep_dims = true,
                                        bool noop_with_empty_axes = false,
                                        const std::string& name = "");

}  // namespace Aidge

namespace {
template <>
const char *const EnumStrings<Aidge::ReduceMeanAttr>::data[] = {
    "axes",
    "keep_dims",
    "noop_with_empty_axes"
};
}

#endif /* AIDGE_CORE_OPERATOR_REDUCEMEAN_H_ */
