/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_TRANSPOSE_H_
#define AIDGE_CORE_OPERATOR_TRANSPOSE_H_

#include <array>
#include <cmath>
#include <numeric>
#include <vector>

#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
/**
 * @brief implementation of the operator Transpose.
 * @note Since this operator implementation is agnostic to the backend it is
 * located here instead of in aidge_backend.
 */
class TransposeImpl : public OperatorImpl {
public:
    /**
     * @brief Constructor for TransposeImpl.
     * @param[in] op The Operator instance.
     * @param[in] backend The backend name (optional).
     */
    TransposeImpl(const Operator& op, const std::string& backend = "")
        : OperatorImpl(op, backend)
    {}

    /**
     * @brief Perform the forward operation for the transpose.
     */
    void forward() override;
};

/**
 * @enum TransposeAttr
 * @brief Enumeration of attributes specific to the Transpose operator.
 */
enum class TransposeAttr {
    /**
     * @brief Order of the output dimensions relative to the input dimensions.
     * 
     * If this attribute is empty, the dimensions of the input tensor will
     * be reversed.
     */
    OutputDimsOrder
};

/**
 * @brief Describes the operation of transposing the axes of a given tensor.
 *
 * The Transpose operator rearranges the axes of an input tensor according to a specified
 * permutation order. If no order is provided, it reverses the axes.
 *
 * @example Input: Tensor of dimensions `[1, 2, 3]` with `OutputDimsOrder = {1, 0, 2}` results in
 *   a tensor with dimensions `[2, 1, 3]`.
 * @example Input: Tensor of dimensions `[1, 2, 3, 4]` with an empty `OutputDimsOrder` results in
 *   a tensor with dimensions `[4, 3, 2, 1]`.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Transpose_Op : public OperatorTensor,
                public Registrable<Transpose_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const Transpose_Op&)>> {

public:
    /**
     * @brief Static type string for the Transpose operator.
     */
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<TransposeAttr, std::vector<DimSize_t>>;
    template <TransposeAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    /**
     * @brief Deleted default constructor.
     */
    Transpose_Op() = delete;

    /**
     * @brief Constructor for the Transpose operator.
     * @param[in] outputDimsOrder Axes permutation order. If empty, axes are reversed.
     */
    Transpose_Op(const std::vector<DimSize_t> &outputDimsOrder);

    /**
     * @brief Copy-constructor.
     * @param op Transpose_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    Transpose_Op(const Transpose_Op& op);

    /**
     * @brief Clone the operator using its copy constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Compute the output dimensions during the forward pass.
     * @param allowDataDependency Whether to allow data-dependent dimensions.
     * @return Boolean indicating whether dimension computation was successful.
     */
    bool forwardDims(bool /*allowDataDependency*/ = false) override final;

    /**
     * @brief Set the backend for the Transpose operator.
     * @param[in] name Name of the backend.
     * @param[in] device Device index (optional).
     */
    void setBackend(const std::string &name, DeviceIdx_t device = 0) override;

    /**
     * @brief Get the available backends for the Transpose operator.
     * @return A set of backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     * @return A shared pointer to the attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get or modify the axes permutation order.
     * @return Reference to the permutation order attribute.
     *
     * If left empty, axes will be reversed.
     */
    inline std::vector<DimSize_t>& outputDimsOrder() const noexcept {
        return mAttributes->getAttr<TransposeAttr::OutputDimsOrder>();
    }

    /**
     * @brief Get the input tensor names for the Transpose operator.
     * @return A vector of input tensor names.
     */
    static const std::vector<std::string> getInputsName() {
        return {"data_input"};
    }

    /**
     * @brief Get the output tensor names for the Transpose operator.
     * @return A vector of output tensor names.
     */
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Create a Transpose operation node.
 *
 * @param[in] outputDimsOrder Axes permutation order (optional).
 * @param[in] name Name of the operator (optional).
 * @return A shared pointer to the Node containing the Transpose operator.
 */
std::shared_ptr<Node> Transpose(const std::vector<DimSize_t> &outputDimsOrder = {},
                                const std::string& name = "");

}  // namespace Aidge

namespace {
/**
 * @brief EnumStrings specialization for TransposeAttr.
 */
template <>
const char *const EnumStrings<Aidge::TransposeAttr>::data[] = {"output_dims_order"};
}

#endif /* AIDGE_CORE_OPERATOR_TRANSPOSE_H_ */
