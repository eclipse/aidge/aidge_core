/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_LEAKYRELU_H_
#define AIDGE_CORE_OPERATOR_LEAKYRELU_H_

#include <memory>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
enum class LeakyReLUAttr {
    /**
     * @brief Slope for the negative input values.
     */
    NegativeSlope
};

/**
 * @class LeakyReLU_Op
 * @brief Implements the LeakyReLU activation function.
 *
 * The LeakyReLU operation introduces a small, non-zero gradient for negative input values,
 * defined as:
 *      - x < 0: f(x) = negativeSlope * x
 *      - x >= 0 : f(x) = x
 *
 * The output tensor has the same shape as the input tensor.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class LeakyReLU_Op : public OperatorTensor,
    public Registrable<LeakyReLU_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const LeakyReLU_Op&)>> {

public:
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<LeakyReLUAttr, float>;
    template <LeakyReLUAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:

    /**
     * @brief Default constructor is deleted.
     */
    LeakyReLU_Op() = delete;

    /**
     * @brief Constructor for LeakyReLU operator.
     * @param[in] negativeSlope The slope for negative input values.
     */
    LeakyReLU_Op(float negativeSlope)
        : OperatorTensor(Type, {InputCategory::Data}, 1),
          mAttributes(
            std::make_shared<Attributes_>(
                attr<LeakyReLUAttr::NegativeSlope>(negativeSlope)))
    {}

    /**
     * @brief Copy-constructor.
     * @param[in] op LeakyReLU_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not its input tensors. 
     * The new operator has no associated input.
     */
    LeakyReLU_Op(const LeakyReLU_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     */
    std::shared_ptr<Operator> clone() const override;

    void setBackend(const std::string& name, DeviceIdx_t device = 0) override;
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get the negative slope value.
     */
    inline float& negativeSlope() const noexcept { return mAttributes -> getAttr<LeakyReLUAttr::NegativeSlope>(); }

    /**
     * @brief Get the names of the input tensors.
     * @return A vector containing the names of input tensors.
     */
    static const std::vector<std::string> getInputsName(){
        return {"data_input"};
    }

    /**
     * @brief Get the names of the output tensors.
     * @return A vector containing the names of output tensors.
     */
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }
};

/**
 * @brief Apply the LeakyReLU activation function to a tensor.
 *
 * @param[in] negativeSlope Slope for the negative input values.
 * @param[in] name Name of the Operator.
 * @return std::shared_ptr<Node> Node containing the Operator.
 */
std::shared_ptr<Node> LeakyReLU(float negativeSlope = 0.0f, const std::string& name = "");
}

namespace {
template <>
const char* const EnumStrings<Aidge::LeakyReLUAttr>::data[]
    = {"negative_slope"};
}

#endif /* AIDGE_CORE_OPERATOR_LEAKYRELU_H_ */
