/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_REDUCESUM_H_
#define AIDGE_CORE_OPERATOR_REDUCESUM_H_

#include <cstdint>    // std::int32_t
#include <memory>
#include <string>
#include <vector>

#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
enum class ReduceSumAttr {
/**
   * @brief Axes over which the mean operation is performed.
   *
   * Axes are specified as a vector of integers, each representing a dimension
   * of the input tensor to be reduced.
   */
  Axes,

  /**
   * @brief Flag indicating whether to keep reduced dimensions.
   *
   * - `true`: Retain reduced dimensions with size 1.
   * - `false`: Completely remove reduced dimensions.
   */
  KeepDims,

  /**
   * @brief Flag indicating behavior when axes are empty.
   *
   * - `true`: No operation is performed if axes are empty.
   * - `false`: Reduction is performed over all axes if none are specified.
   */
  NoopWithEmptyAxes
};

/**
 * @class ReduceSum_Op
 * @brief Implements the ReduceSum operation to compute the sum of a tensor along specified axes.
 *
 * ReduceSum reduces specified axes of a tensor by computing the sum value.
 * The resulting output tensor dimensions depend on the provided attributes.
 *
 * ### Output Shape Calculation
 * - If `KeepDims` is true:
 *   Reduced dimensions are retained with size 1.
 * - If `KeepDims` is false:
 *   Reduced dimensions are completely removed.
 *
 * @example:
 * - Input shape: (2, 3, 4, 5) // Batch size 2, 3 channels, 4x5 spatial dimensions
 * - Axes: {2, 3}
 * - KeepDims: true
 * - Output shape: (2, 3, 1, 1)
 * @example:
 * - Input shape: (2, 3, 4, 5)
 * - Axes: {1, 2}
 * - KeepDims: false
 * - Output shape: (2, 5)
 *
 * @see OperatorTensor
 * @see Registrable
 */
class ReduceSum_Op : public OperatorTensor,
                public Registrable<ReduceSum_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const ReduceSum_Op &)>> {

public:
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<ReduceSumAttr,
                                            std::vector<std::int32_t>,
                                            bool,
                                            bool>;
    template <ReduceSumAttr e>
    using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    ReduceSum_Op() = delete;

    /**
     * @brief constructor for ReduceSum op
     * @param[in] axes around which perform the operation
     * @param[in] keep_dims if true we set a dimension of 1 in the place of the reduced axes and 
     * if false we remove the dimension completely
     * @param[in] noop_with_empty_axes used when no axes are provided, if set to true, the operator does nothing
     * and if false, we reduce on all axes
     */
    ReduceSum_Op(const std::vector<std::int32_t>& axes, bool keep_dims, bool noop_with_empty_axes)
        : OperatorTensor(Type, {InputCategory::Data}, 1),
          mAttributes(std::make_shared<Attributes_>(
            attr<ReduceSumAttr::Axes>(axes),
            attr<ReduceSumAttr::KeepDims>(keep_dims),
            attr<ReduceSumAttr::NoopWithEmptyAxes>(noop_with_empty_axes)))
    {}

    /**
     * @brief Copy-constructor.
     * @param op ReduceSum_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    ReduceSum_Op(const ReduceSum_Op& op)
        : OperatorTensor(op),
          mAttributes(op.mAttributes)
    {
        if (op.mImpl){
            SET_IMPL_MACRO(ReduceSum_Op, *this, op.backend());
        } else {
            mImpl = nullptr;
        }
    }

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::ReduceSum_Op
     */
    std::shared_ptr<Operator> clone() const override {
        return std::make_shared<ReduceSum_Op>(*this);
    }

    bool forwardDims(bool allowDataDependency = false) override final;

    void setBackend(const std::string &name, DeviceIdx_t device = 0) override final;
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get the axes over which the mean is computed.
     */
    inline std::vector<std::int32_t>& axes() const noexcept { return mAttributes -> getAttr<ReduceSumAttr::Axes>(); }

    /**
     * @brief Get whether reduced dimensions are retained.
     */
    inline bool& keepDims() const noexcept { return mAttributes -> getAttr<ReduceSumAttr::KeepDims>(); }

    /**
     * @brief Get the behavior when axes are empty.
     */
    inline bool& noopWithEmptyAxes() const noexcept { return mAttributes -> getAttr<ReduceSumAttr::NoopWithEmptyAxes>(); }


    static const std::vector<std::string> getInputsName() {
        return {"data_input"};
    }
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Compute the sum value of a Tensor over the specified axes.
 *
 * Dimensions may be reduced by erasing the specified axes or retaining them with size 1.
 *
 * @param axes Dimensions over which data sum should be computed.
 * @param keep_dims Whether or not reduced dimensions are to be retained.
 * @param noop_with_empty_axes Behavior when no axes are specified.
 * @param name Name of the Operator.
 * @return std::shared_ptr<Node> Node containing the Operator.
 */
inline std::shared_ptr<Node> ReduceSum(const std::vector<std::int32_t> &axes={},
                                        bool keep_dims=true,
                                        bool noop_with_empty_axes=false,
                                        const std::string& name = "") {
    // FIXME: properly handle default w&b initialization in every cases
    AIDGE_ASSERT(axes.size()<=MaxDim, "Too many kernel dimensions required by ReduceSum, not supported");
    return std::make_shared<Node>(std::make_shared<ReduceSum_Op>(axes, keep_dims, noop_with_empty_axes), name);

}
}  // namespace Aidge

namespace {
template <>
const char *const EnumStrings<Aidge::ReduceSumAttr>::data[] = {"axes", "keep_dims", "noop_with_empty_axes"};
}

#endif /* AIDGE_CORE_OPERATOR_REDUCESUM_H_ */
