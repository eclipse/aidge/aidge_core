/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_ERF_H_
#define AIDGE_CORE_OPERATOR_ERF_H_

#include <memory>
#include <string>
#include <vector>

#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @brief Description of an element-wise Error Function (Erf) operation
 * on an input Tensor.
 *
 * For each element x in the input, the function is defined as:
 * `f(x) = erf(x)`, where erf(x) is the Gauss error function, given by:
 * `erf(x) = (2 / sqrt(pi)) * integral from 0 to x of exp(-t^2) dt`
 *
 * The input and output Tensors have the same dimensions.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Erf_Op : public OperatorTensor,
    public Registrable<Erf_Op,  // <Op, backend, implementation creation function>
        std::string,
        std::function<std::shared_ptr<OperatorImpl>(const Erf_Op&)>>
{
public:
    static const std::string Type;

    Erf_Op() : OperatorTensor(Type, {InputCategory::Data}, 1) {}

    /**
     * @brief Copy-constructor.
     * @param op Erf_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    Erf_Op(const Erf_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::Erf_Op
     */
    std::shared_ptr<Operator> clone() const override;

    void setBackend(const std::string& name, DeviceIdx_t device = 0) override;
    std::set<std::string> getAvailableBackends() const override;

    static const std::vector<std::string> getInputsName(){
        return {"data_input"};
    }
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }
};

std::shared_ptr<Node> Erf(const std::string& name = "");
}

#endif /* AIDGE_CORE_OPERATOR_ERF_H_ */
