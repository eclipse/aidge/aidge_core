/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_SHAPE_H_
#define AIDGE_CORE_OPERATOR_SHAPE_H_

#include <cstdint>  // For fixed-width integer types (std::int64_t)
#include <memory>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @class Shape_OpImpl
 * @brief Backend-agnostic implementation of the Shape operator.
 *
 * This implementation is responsible for extracting and returning the shape
 * of the input tensor. Specific backend functionality can extend this.
 */
class Shape_OpImpl : public OperatorImpl {
public:
    /**
     * @brief Constructor for the Shape_OpImpl class.
     * @param[in] op The Operator instance.
     * @param[in] backend The backend name (optional).
     */
    Shape_OpImpl(const Operator& op, const std::string& backend = ""): OperatorImpl(op, backend) {}

    /**
     * @brief Perform the forward operation to compute the shape of the tensor.
     */
    void forward() override;
};

/**
 * @enum ShapeAttr
 * @brief Enumeration of attributes specific to the Shape operator.
 */
enum class ShapeAttr {
    /**
     * @brief Start index of the slice of dimensions to return.
     */
    Start,
    /**
     * End index of the slice of dimensions to return (exclusive).
     */
    End
};

/**
 * @brief Description of the operation of extracting the shape of a tensor.
 *
 * The Shape operator extracts the shape (dimensions) of a tensor as an output tensor.
 * It allows optional slicing of the dimensions by specifying a start and end index.
 *
 * @example Input: Tensor with shape `[4, 5, 6, 7]`, `start=1`, `end=3` -> Output: `[5, 6]`
 * @example Input: Tensor with shape `[4, 5, 6]`, `start=0`, `end=-1` (default) -> Output: `[4, 5, 6]`
 */
class Shape_Op : public OperatorTensor,
                public Registrable<Shape_Op,
                                   std::string,
                                   std::function<std::shared_ptr<OperatorImpl>(const Shape_Op&)>> {

public:
    /**
     * @brief Static type string for the Shape operator.
     */
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<ShapeAttr, std::int64_t, std::int64_t>;
    template <ShapeAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    /**
     * @brief Constructor for the Shape operator.
     * @param[in] start Start index for slicing dimensions.
     * @param[in] end End index (exclusive) for slicing dimensions.
     */
    Shape_Op(const std::int64_t start = 0, const std::int64_t end = -1);

    /**
     * @brief Copy-constructor.
     * @param op Shape_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    Shape_Op(const Shape_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Compute the output dimensions during the forward pass.
     * @param allowDataDependency Whether to allow data-dependent dimensions.
     * @return Boolean indicating whether dimension computation was successful.
     */
    bool forwardDims(bool /*allowDataDependency*/ = false) override final;

    /**
     * @brief Set the backend for the Shape operator.
     * @param[in] name Name of the backend.
     * @param[in] device Device index (optional).
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override;

    /**
     * @brief Get the available backends for the Shape operator.
     * @return A set of backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     * @return A shared pointer to the attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get or modify the start index for slicing dimensions.
     * @return Reference to the start index attribute.
     */
    inline std::int64_t& start() const noexcept { return mAttributes->getAttr<ShapeAttr::Start>(); }

    /**
     * @brief Get or modify the end index for slicing dimensions.
     * @return Reference to the end index attribute.
     */
    inline std::int64_t& end() const noexcept { return mAttributes->getAttr<ShapeAttr::End>(); }

    /**
     * @brief Get the input tensor names for the Shape operator.
     * @return A vector of input tensor names.
     */
    static const std::vector<std::string> getInputsName() {
        return {"data_input"};
    }

    /**
     * @brief Get the output tensor names for the Shape operator.
     * @return A vector of output tensor names.
     */
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Create a Shape operation node.
 *
 * @param[in] start Start index for slicing dimensions (default is 0).
 * @param[in] end End index (exclusive) for slicing dimensions (default is -1, meaning all dimensions).
 * @param[in] name Name of the operator (optional).
 * @return A shared pointer to the Node containing the Shape operator.
 */
std::shared_ptr<Node> Shape(const std::int64_t start = 0, const std::int64_t end = -1, const std::string& name = "");

} // namespace Aidge

namespace {
/**
 * @brief EnumStrings specialization for ShapeAttr.
 */
template <>
const char *const EnumStrings<Aidge::ShapeAttr>::data[] = {"start", "end"};
}

#endif /* AIDGE_CORE_OPERATOR_SHAPE_H_ */
