/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_FLATTEN_H_
#define AIDGE_CORE_OPERATOR_FLATTEN_H_

#include <memory>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @brief Implementation of the Flatten operation.
 *
 * Since Flatten operation is just backend-agnostic, its implementation is located in aidge_core.
 */
class Flatten_OpImpl : public OperatorImpl {
public:
    /**
     * @brief Constructor for Flatten operator implementation.
     * @param op Operator instance.
     * @param backend Optional. Name of the backend.
     */
    Flatten_OpImpl(const Operator& op, const std::string& backend = ""): OperatorImpl(op, backend) {}

    /**
     * @brief Compute the forward pass of the Flatten operation.
     */
    void forward() override;
};

/**
 * @enum FlattenAttr
 * @brief Defines attributes for the Flatten operator.
 */
enum class FlattenAttr {
    /**
     * @brief The axis at which to flatten the input tensor.
     */
    Axis
};

/**
 * @brief Description the Flatten operation to reshape a tensor into a 2D matrix.
 *
 * The Flatten operation rearranges a tensor such that the specified axis is
 * the start of the second dimension, flattening all dimensions before it
 * into the first dimension and all dimensions after it into the second.
 *
 * @example:
 * - Input shape: (2, 3, 4, 5)
 * - Axis: 2
 * - Output shape: (6, 20)
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Flatten_Op : public OperatorTensor,
                   public Registrable<Flatten_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const Flatten_Op&)>> {

public:
    /**
     * @brief The type identifier for the Flatten operator.
     */
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<FlattenAttr, std::int64_t>;
    template <FlattenAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    /**
     * @brief Deleted default constructor.
     */
    Flatten_Op() = delete;

    /**
     * @brief Constructor for the Flatten operator.
     * @param axis The axis at which to flatten the tensor.
     */
    Flatten_Op(std::int64_t axis = 1);

    /**
     * @brief Copy-constructor. Copies the operator attributes and its output tensor(s), but not its input tensors.
     * @param[in] op Operator to copy.
     */
    Flatten_Op(const Flatten_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Compute the forward dimensions.
     * @param[in] allowDataDependency Whether to allow data dependency in computation.
     * @return True if successful, false otherwise.
     */
    bool forwardDims(bool allowDataDependency = false) override final;

    /**
     * @brief Set the backend for the operator.
     * @param[in] name The name of the backend.
     * @param[in] device Optional. The device index.
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;

    /**
     * @brief Get the set of available backends for the operator.
     * @return A set of backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     * @return A shared pointer to the attributes.
     */
    std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get the axis at which the tensor is flattened.
     * @return A reference to the axis attribute.
     */
    inline std::int64_t& axis() const { return mAttributes->template getAttr<FlattenAttr::Axis>(); }

    /**
     * @brief Get the names of the input tensors.
     * @return A vector of input tensor names.
     */
    static const std::vector<std::string> getInputsName(){
        return {"data_input"};
    }

    /**
     * @brief Get the names of the output tensors.
     * @return A vector of output tensor names.
     */
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }
};

/**
 * @brief Create a Flatten node.
 *
 * Initializes a Flatten node that reshapes a tensor into a 2D matrix based
 * on the specified axis.
 *
 * @param[in] axis The axis at which to flatten the tensor.
 * @param[in] name Optional. The name of the node.
 * @return A shared pointer to a Node representing the Flatten operation.
 */
std::shared_ptr<Node> Flatten(std::int64_t axis = 1,
                            const std::string &name = "");
}  // namespace Aidge

namespace {
template <>
const char *const EnumStrings<Aidge::FlattenAttr>::data[] = { "axis" };
}

#endif /* AIDGE_CORE_OPERATOR_FLATTEN_H_ */
