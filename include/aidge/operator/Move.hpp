/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_MOVE_H_
#define AIDGE_CORE_OPERATOR_MOVE_H_

#include <cassert>
#include <memory>
#include <vector>

#include "aidge/utils/Registrar.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
class Move_OpImpl : public OperatorImpl {
public:
    Move_OpImpl(const Operator& op, const std::string& backend = ""): OperatorImpl(op, backend) {}
    void forward() override;
};

/**
 * @brief Description of a Move operation that copies the input Tensor to the output Tensor.
 *
 * For each element x in the input, the function is defined as:
 * `f(x) = x`
 *
 * This operation copies the input Tensor to the output Tensor without modification.
 * The input and output Tensors have the same dimensions.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Move_Op : public OperatorTensor,
    public Registrable<Move_Op, std::tuple<std::string, std::string>, std::function<std::unique_ptr<OperatorImpl>(const Move_Op&)>> {
public:
    static const std::string Type;

    Move_Op();

    /**
     * @brief Copy-constructor.
     * @param op Move_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    Move_Op(const Move_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::Move_Op
     */
    std::shared_ptr<Operator> clone() const override;

    void setBackend(const std::string& name, DeviceIdx_t device = 0) override;
    std::set<std::string> getAvailableBackends() const override;

    static const std::vector<std::string> getInputsName(){
        return {"data_input"};
    }
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }
};

std::shared_ptr<Node> Move(const std::string& name = "");

} // namespace Aidge

#endif /* AIDGE_CORE_OPERATOR_MOVE_H_ */
