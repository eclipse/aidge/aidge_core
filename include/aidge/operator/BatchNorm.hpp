/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_BATCHNORM_H_
#define AIDGE_CORE_OPERATOR_BATCHNORM_H_

#include <array>
#include <memory>
#include <vector>

#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

enum class BatchNormAttr {
  /**
   * @brief Epsilon value to avoid division by zero during normalization.
   *
   * A small value added to the denominator during normalization to ensure numerical stability.
   * Commonly used in batch normalization to avoid very small variance values.
   */
  Epsilon,

  /**
   * @brief Momentum factor for the moving average of batch statistics.
   *
   * Controls the weighting of past running averages in the batch normalization statistics.
   * - `0.0`: Full reliance on current batch statistics.
   * - `1.0`: Complete reliance on the previous moving average.
   */
  Momentum,

  /**
   * @brief Flag indicating whether the operator is in training mode.
   *
   * - `true`: Uses the current batch statistics for normalization.
   * - `false`: Uses moving average statistics accumulated during training.
   */
  TrainingMode
};

/**
 * @class BatchNorm_Op
 * @brief Implements the Batch Normalization (BN) operation, a technique used to normalize the inputs of a layer.
 *
 * BatchNorm standardizes the inputs to a layer by adjusting and scaling activations based on the batch's mean and variance.
 * This operator computes the following:
 *   - Normalization of input features
 *   - Scaling and shifting of normalized values based on learned parameters
 *
 * @example:
 * - Input shape: (batch_size, num_features) // Batch size, number of features
 * - Epsilon: 1e-5
 * - Momentum: 0.1
 * - TrainingMode: true
 * - Output shape: (batch_size, num_features)
 *
 * @see OperatorTensor
 * @see Registrable
 */
template <DimIdx_t DIM>
class BatchNorm_Op : public OperatorTensor,
                public Registrable<BatchNorm_Op<DIM>, std::string, std::function<std::shared_ptr<OperatorImpl>(const BatchNorm_Op<DIM> &)>> {

public:
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<BatchNormAttr, float, float, bool>;

    template <BatchNormAttr e>
    using attr = typename Attributes_::template attr<e>;

    const std::shared_ptr<Attributes_> mAttributes;

public:
    BatchNorm_Op() = delete;

    /**
     * @brief Constructor for BatchNorm operator.
     * @param[in] epsilon Small value to add to the denominator for numerical stability.
     * @param[in] momentum Momentum for the moving average of statistics.
     * @param[in] trainingMode Flag indicating whether to use current or moving average statistics.
     */
    constexpr BatchNorm_Op(float epsilon, float momentum, bool trainingMode)
        : OperatorTensor(Type,
                            {InputCategory::Data,
                             InputCategory::Param,
                             InputCategory::Param,
                             InputCategory::Param,
                             InputCategory::Param},
                            1),
          mAttributes(std::make_shared<Attributes_>(
            attr<BatchNormAttr::Epsilon>(epsilon),
            attr<BatchNormAttr::Momentum>(momentum),
            attr<BatchNormAttr::TrainingMode>(trainingMode)
            )) {}

    /**
     * @brief Copy-constructor. Copy the operator attributes and its output tensor(s), but not its input tensors (the new operator has no input associated).
     * @param op Operator to copy.
     */
    BatchNorm_Op(const BatchNorm_Op<DIM>& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::BatchNorm_Op
     */
    std::shared_ptr<Operator> clone() const override;

    bool forwardDims(bool /*allowDataDependency*/ = false) override final;

    void setBackend(const std::string &name, DeviceIdx_t device = 0) override final;
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get the epsilon value.
     */
    inline float& epsilon() const { return mAttributes->template getAttr<BatchNormAttr::Epsilon>(); }

    /**
     * @brief Get the momentum value.
     */
    inline float& momentum() const { return mAttributes->template getAttr<BatchNormAttr::Momentum>(); }

    /**
     * @brief Get whether the operator is in training mode.
     */
    inline bool& trainingMode() const { return mAttributes->template getAttr<BatchNormAttr::TrainingMode>(); }

    static const std::vector<std::string> getInputsName() {
        return {"data_input", "scale", "shift", "mean", "variance"};
    }

    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

extern template class Aidge::BatchNorm_Op<2>;
extern template class Aidge::BatchNorm_Op<3>;
extern template class Aidge::BatchNorm_Op<4>;

template <DimSize_t DIM>
std::shared_ptr<Node> BatchNorm(const DimSize_t nbFeatures,
                                       const float epsilon = 1.0e-5F,
                                       const float momentum = 0.1F,
                                       const bool trainingMode = false,
                                       const std::string& name = "");
}  // namespace Aidge

extern template std::shared_ptr<Aidge::Node> Aidge::BatchNorm<2>(const DimSize_t, const float, const float, const bool, const std::string&);
extern template std::shared_ptr<Aidge::Node> Aidge::BatchNorm<3>(const DimSize_t, const float, const float, const bool, const std::string&);
extern template std::shared_ptr<Aidge::Node> Aidge::BatchNorm<4>(const DimSize_t, const float, const float, const bool, const std::string&);

namespace {
template <>
const char *const EnumStrings<Aidge::BatchNormAttr>::data[] = { "epsilon", "momentum", "training_mode" };
}

#endif /* AIDGE_CORE_OPERATOR_BATCHNORM_H_ */
