/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_FOLD_H_
#define AIDGE_CORE_OPERATOR_FOLD_H_

#include <array>
#include <cmath>    // std::floor
#include <cstddef>  // std::size_t
#include <string>
#include <utility>  // std::pair
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp" // SET_IMPL_MACRO
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @enum FoldAttr
 * @brief Enumeration for the attributes of the Fold operation.
 */
enum class FoldAttr {
    /**
     * @brief Output dimensions of the fold operation.
     *
     * Specifies the shape of the output tensor after applying the fold operation.
     */
    OutputDims,

    /**
     * @brief Stride dimensions used during the fold operation.
     *
     * Strides are the step sizes in each dimension during the fold operation.
     */
    StrideDims,

    /**
     * @brief Dilation dimensions for the fold operation.
     *
     * Dilation is the spacing between elements in the kernel during the fold.
     */
    DilationDims,

    /**
     * @brief Kernel dimensions used for the fold operation.
     *
     * Specifies the size of the kernel or filter applied during the fold.
     */
    KernelDims
};

/**
 * @class Fold_Op
 * @brief Implements the Fold operation to combine or transform tensor dimensions.
 *
 * The Fold operation applies a transformation to the tensor by folding it according to
 * the specified dimensions and kernel properties. This is commonly used in operations
 * like convolution or pooling, where the kernel slides over the input tensor and
 * applies an operation (e.g., sum or max) over a specified region.
 *
 * The output shape depends on the `OutputDims`, `StrideDims`, `DilationDims`, and `KernelDims` attributes:
 *     Let the input tensor be of shape `(batch_size, channels, height, width)` (assuming 2D spatial dims).
 *     - The number of output channels is the input channels divided by the product of kernel dimensions:
 *      output channels = input channels / (product of kernelDims)
 *     - The output height and width are calculated as:
 *       output height (out_h) = floor((input height - kernel height) / stride height) + 1
 *       output width (out_w) = floor((input width - kernel width) / stride width) + 1
 *      - The exact output shape will depend on these calculations for each spatial dimension (height, width) and the number of output channels.
 *         
 * @example:
 *  - Input shape: (1, 16, 32, 32)  // Batch size: 1, Channels: 16, Height: 32, Width: 32
 *  - Kernel dimensions: (3, 3)  // 3x3 kernel
 *  - Stride: (1, 1)  // Stride of 1
 *  - Dilation: (1, 1)  // No dilation
 *  - Output shape: (1, 16, 30, 30)  // Output height and width after applying the kernel and stride
 *
 * @see OperatorTensor
 * @see Registrable
 */

template <DimIdx_t DIM>
class Fold_Op : public OperatorTensor,
                public Registrable<Fold_Op<DIM>, std::string, std::function<std::shared_ptr<OperatorImpl>(const Fold_Op<DIM> &)>> {

public:
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<FoldAttr,
                                        std::array<DimSize_t, DIM>,
                                        std::array<DimSize_t, DIM>,
                                        std::array<DimSize_t, DIM>,
                                        std::array<DimSize_t, DIM>>;

    template <FoldAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    Fold_Op() = delete;

    /**
     * @brief Constructor for Fold_Op operator.
     * @param[in] outputDims Output dimensions of the folded tensor.
     * @param[in] kernelDims Kernel size for the fold operation.
     * @param[in] strideDims Stride dimensions for the fold operation.
     * @param[in] dilationDims Dilation dimensions for the fold operation.
     */
    constexpr Fold_Op(const std::array<DimSize_t, DIM>& outputDims,
                      const std::array<DimSize_t, DIM>& kernelDims,
                      const std::array<DimSize_t, DIM>& strideDims = create_array<DimSize_t,DIM>(1),
                      const std::array<DimSize_t, DIM>& dilationDims = create_array<DimSize_t,DIM>(1))
        : OperatorTensor(Type, {InputCategory::Data}, 1),
          mAttributes(std::make_shared<Attributes_>(
            attr<FoldAttr::OutputDims>(outputDims),
            attr<FoldAttr::StrideDims>(strideDims),
            attr<FoldAttr::DilationDims>(dilationDims),
            attr<FoldAttr::KernelDims>(kernelDims))) {}

    /**
     * @brief Copy-constructor.
     * @param op Fold_Op operator to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    Fold_Op(const Fold_Op<DIM>& op);

    /**
     * @brief Clone the operator using its copy constructor.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Compute forward dimensions for the operator.
     * @param allowDataDependency Flag to allow data dependency in dimension calculation.
     * @return true if the dimensions are computed successfully.
     */
    bool forwardDims(bool /*allowDataDependency*/ = false) override final;

    /**
     * @brief Set the backend for the operator.
     * @param name Name of the backend.
     * @param device Index of the device.
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override;

    /**
     * @brief Get the available backends for this operator.
     * @return A set of available backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     * @return Shared pointer to the attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get the output dimensions for the fold operation.
     * @return Output dimensions of the folded tensor.
     */
    inline std::array<DimSize_t, DIM>& outputDims() const { return mAttributes->template getAttr<FoldAttr::OutputDims>(); }

    /**
     * @brief Get the stride dimensions for the fold operation.
     * @return Stride dimensions.
     */
    inline std::array<DimSize_t, DIM>& strideDims() const { return mAttributes->template getAttr<FoldAttr::StrideDims>(); }

    /**
     * @brief Get the dilation dimensions for the fold operation.
     * @return Dilation dimensions.
     */
    inline std::array<DimSize_t, DIM>& dilationDims() const { return mAttributes->template getAttr<FoldAttr::DilationDims>(); }

    /**
     * @brief Get the kernel dimensions for the fold operation.
     * @return Kernel dimensions.
     */
    inline std::array<DimSize_t, DIM>& kernelDims() const { return mAttributes->template getAttr<FoldAttr::KernelDims>(); }

    /**
     * @brief Get the input names for the Fold operation.
     * @return List of input names.
     */
    static const std::vector<std::string> getInputsName(){
        return {"data_input"};
    }

    /**
     * @brief Get the output names for the Fold operation.
     * @return List of output names.
     */
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }
};

/**
 * @brief Create a Fold operation node.
 * 
 * This function creates a Fold operation node that applies a fold transformation
 * to a tensor based on the specified attributes.
 *
 * @param[in] outputDims Output dimensions for the fold operation.
 * @param[in] kernelDims Kernel dimensions.
 * @param[in] name Name of the operator.
 * @param[in] strideDims Stride dimensions for the fold operation.
 * @param[in] dilationDims Dilation dimensions for the fold operation.
 * @return A shared pointer to the created Node.
 */
template <std::array<DimSize_t, 1>::size_type DIM>
std::shared_ptr<Node> Fold(const std::array<DimSize_t, DIM>& outputDims,
                           const std::array<DimSize_t, DIM>& kernelDims,
                           const std::string& name = "",
                           const std::array<DimSize_t, DIM>& strideDims = create_array<DimSize_t,DIM>(1),
                           const std::array<DimSize_t, DIM>& dilationDims = create_array<DimSize_t,DIM>(1));

template <DimSize_t DIM>
inline std::shared_ptr<Node> Fold(
    DimSize_t const (&outputDims)[DIM],
    DimSize_t const (&kernelDims)[DIM],
    const std::string& name = "",
    const std::array<DimSize_t, DIM>& strideDims = create_array<DimSize_t,DIM>(1),
    const std::array<DimSize_t, DIM>& dilationDims = create_array<DimSize_t,DIM>(1)) {
    static_assert(DIM <= MaxDim, "Too many kernel dimensions required by Fold, not supported");
    return Fold(to_array(outputDims), to_array(kernelDims), name, strideDims, dilationDims);
}

extern template class Aidge::Fold_Op<2>;

}  // namespace Aidge

namespace {
template <>
const char* const EnumStrings<Aidge::FoldAttr>::data[] = {
    "output_dims",
    "stride_dims",
    "dilation_dims",
    "kernel_dims"
};
}

#endif /* AIDGE_CORE_OPERATOR_FOLD_H_ */
