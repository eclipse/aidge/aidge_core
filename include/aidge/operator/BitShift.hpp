/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_BITSHIFT_H_
#define AIDGE_CORE_OPERATOR_BITSHIFT_H_

#include <memory>
#include <string>
#include <vector>

#include "aidge/utils/Registrar.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/utils/Types.h"
#include "aidge/utils/StaticAttributes.hpp"

namespace Aidge {


enum class BitShiftAttr {
    /**
     * 
     */
    BitShiftdirection
};

/**
 * @class BitShift_Op
 * @brief A tensor operator to perform element-wise bitwise shift operations on tensors.
 *
 * The operator takes two inputs:
 * - **InputTensor**: The tensor whose elements will be shifted.
 * - **ShiftAmount**: The tensor specifying the shift amount for each element.
 *
 * The shift is applied in the direction specified by the attribute `BitShiftdirection`, 
 * which can either be `left` or `right`.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class BitShift_Op : public OperatorTensor,
    public Registrable<BitShift_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const BitShift_Op&)>> {

public:
    /**
     * @enum BitShiftDirection
     * @brief Specifies the direction of the bitwise shift.
     */
    enum BitShiftDirection { left, right };

    /**
     * @brief Type identifier for the operator.
     */
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<BitShiftAttr, BitShiftDirection>;

    template <BitShiftAttr e>
    using attr = typename Attributes_::template attr<e>;

    const std::shared_ptr<Attributes_> mAttributes;

public:
    /**
     * @brief Constructor to initialize the `BitShift_Op` with a shift direction.
     * @param[in] direction The direction of the bitwise shift (left or right).
     */
    BitShift_Op(BitShiftDirection direction)
        : OperatorTensor(Type, {InputCategory::Data, InputCategory::Data}, 1),
          mAttributes(std::make_shared<Attributes_>(
              attr<BitShiftAttr::BitShiftdirection>(direction))) {}

    /**
     * @brief Copy-constructor. Copies operator attributes and output tensors but not input tensors.
     * @param[in] op Operator instance to copy.
     */
    BitShift_Op(const BitShift_Op& op)
        : OperatorTensor(op),
          mAttributes(op.mAttributes)
    {
        if (op.mImpl) {
            SET_IMPL_MACRO(BitShift_Op, *this, op.backend());
        } else {
            mImpl = nullptr;
        }
    }

    /**
     * @brief Clone the operator using its copy-constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override {
        return std::make_shared<BitShift_Op>(*this);
    }

    bool forwardDims(bool allowDataDependency = false) override final;

    /**
     * @brief Set the backend to be used for this operator.
     * @param[in] name Backend name.
     * @param[in] device Device index (default: 0).
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override;

    /**
     * @brief Get the set of available backends for this operator.
     * @return A set of backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     * @return A shared pointer to the attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Retrieve the direction of the bitwise shift (left or right).
     * @return The direction of the bitwise shift.
     */
    inline BitShiftDirection& direction() const noexcept {
        return mAttributes->template getAttr<BitShiftAttr::BitShiftdirection>();
    }

    /**
     * @brief Get the names of the input tensors.
     * @return A vector containing the input tensor names.
     */
    static const std::vector<std::string> getInputsName() {
        return { "InputTensor", "ShiftAmount" };
    }

    /**
     * @brief Get the names of the output tensors.
     * @return A vector containing the output tensor names.
     */
    static const std::vector<std::string> getOutputsName() {
        return { "OutputTensor" };
    }
};

/**
 * @brief Factory function to create a `BitShift` node.
 * @param[in] direction The direction of the bitwise shift (`left` or `right`).
 * @param[in] name (Optional) Name of the node.
 * @return A shared pointer to the created node.
 */
inline std::shared_ptr<Node> BitShift(const BitShift_Op::BitShiftDirection direction, const std::string& name = "") {
    return std::make_shared<Node>(std::make_shared<BitShift_Op>(direction), name);
}

} // namespace Aidge

namespace {
/**
 * @brief Specialization of `EnumStrings` for `BitShiftAttr`.
 */
template <>
const char* const EnumStrings<Aidge::BitShiftAttr>::data[] = { "BitShiftdirection" };
}

#endif /* AIDGE_CORE_OPERATOR_BITSHIFT_H_ */
