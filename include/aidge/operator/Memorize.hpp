/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_MEMORIZE_H_
#define AIDGE_CORE_OPERATOR_MEMORIZE_H_

#include <memory>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
/**
 * @class Memorize_ProdConso
 * @brief Implements the producer-consumer principle for the `Memorize` operator.
 *
 * The `Memorize_ProdConso` class defines the logic for managing data dependencies during
 * the forward process of the `Memorize` operator.
 *
 * This class ensures that:
 * - All data produced by the `Memorize` operator is properly consumed.
 * - All required outputs are correctly filled during the forward pass.
 *
 * It also calculates data and memory requirements specific to the `Memorize` operator.
 */
class Memorize_ProdConso : public ProdConso {
public:
    /**
     * @brief Constructor for the `Memorize_ProdConso` class.
     * @param[in] op The operator instance for which producer-consumer relationships are managed.
     *
     * @details:
     * - The provided `Operator` instance is used to initialize the base `ProdConso` class.
     * - This operator will determine the specific requirements for data production
     *   and consumption during the forward process.
     */
    Memorize_ProdConso(const Operator& op): ProdConso(op) {}

    /**
     * @brief Get the number of data elements required from an input tensor for forward computation.
     * @param[in] inputIdx The index of the input tensor.
     * @return The number of required elements (`Elts_t`).
     *
     * @details:
     * - For each input tensor identified by `inputIdx`, this method calculates the
     *   minimum amount of data needed by the `Memorize` operator to perform its forward step.
     */
    Elts_t getNbRequiredData(const IOIndex_t inputIdx) const override final;

    /**
     * @brief Compute the memory requirements for an output tensor.
     * @param[in] outputIdx The index of the output tensor.
     * @param[in] inputsSize A vector containing the dimensions of the input tensors.
     * @return The memory required (`Elts_t`) for the specified output tensor.
     *
     * @details:
     * - This method evaluates how much memory is needed for the `outputIdx` tensor
     *   based on the input tensor dimensions and the attributes of the `Memorize` operator.
     * - Memory requirements are influenced by factors such as sequence length and
     *   the forward step configuration.
     */
    Elts_t getRequiredMemory(const IOIndex_t outputIdx, const std::vector<DimSize_t>& inputsSize) const override final;

    /**
     * @brief Update the producer-consumer relationships for the `Memorize` operator.
     * @details:
     * - This method ensures that all data produced by the `Memorize` operator is
     *   appropriately consumed by downstream operators in the computational graph.
     * - It also verifies that all required outputs are filled during the forward pass,
     *   maintaining consistency in the data flow.
     * - This step is crucial for ensuring correctness in recurrent computations and
     *   maintaining dependencies in the graph.
     */
    void updateConsummerProducer() override;
};

/**
 * @brief Implementation of the Memorize operation.
 *
 * Since Memorize operation is just backend-agnostic, its implementation is located in aidge_core.
 */
class Memorize_OpImpl : public OperatorImpl {
public:
    /**
     * @brief Constructs a Memorize_OpImpl object.
     * @param[in] op The operator to be implemented.
     * @param[in] backend The backend used for execution.
     */
    Memorize_OpImpl(const Operator& op, const std::string& backend = ""): OperatorImpl(op, backend) {}

    /**
     * @brief Get the Producer Consumer object of the operator.
     * @return A shared pointer to the ProdConso object.
     */
    std::shared_ptr<ProdConso> getProdConso() const override { return std::make_shared<Memorize_ProdConso>(mOp); };

    /**
     * @brief Executes the forward pass for the Memorize operation.
     */
    void forward() override;
};

enum class MemorizeAttr {
    ScheduleStep,   // Defines the step interval for scheduling memory updates.
    ForwardStep,    // Tracks the current step in the forward pass.
    EndStep         // The final step for which memory updates will occur.
};

/**
 * @class Memorize_Op
 * @brief The Memorize Operator is responsible for storing a tensor's state over a defined 
 * number of iterations and providing the stored value as output at each iteration.
 *
 *  Memorize operators are used in models with recurrent structures or feedback loops, such as LSTMs.
 * They maintain an internal state that is updated and accessed sequentially.
 * - **Inputs**:
 *   - `data_input`: The main data input for the operator.
 *   - `data_input_init`: The initial value used to initialize the internal memory.
 * - **Outputs**:
 *   - `data_output`: The current output of the internal memory.
 *   - `data_output_rec`: The recurrent output that feeds back into the memory for subsequent steps.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Memorize_Op : public OperatorTensor,
    public Registrable<Memorize_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const Memorize_Op&)>> {
public:
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<MemorizeAttr, std::uint32_t, std::uint32_t, std::uint32_t>;
    template <MemorizeAttr e>
    using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    Memorize_Op() = delete;

    /**
     * @brief Construct a new Memorize Operator.
     * @param endStep Defines the number of steps the operator will retain memory.
     */
    Memorize_Op(const std::uint32_t endStep);

    /**
     * @brief Copy-constructor. Copies the operator's attributes and its output tensors.
     * The new operator has no input tensors associated initially.
     * @param op Operator to copy.
     */
    Memorize_Op(const Memorize_Op& op);

    /**
     * @brief Clone the operator by creating a copy of it.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Assign a specific backend and device for computation.
     * @param name Name of the backend.
     * @param device The device index (default is 0).
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;

    /**
     * @brief Get the list of available backends compatible with this operator.
     * @return A set of strings representing backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Perform dimension inference for the operator, optionally allowing
     * data dependency during the process.
     * @param allowDataDependency Flag indicating whether data dependency is allowed.
     * @return True if dimensions were successfully inferred.
     */
    bool forwardDims(bool allowDataDependency = false) override final;

    /**
     * @brief Check if dimensions have been forwarded successfully.
     * @return True if dimensions are forwarded.
     */
    bool dimsForwarded() const override;

    void updateConsummerProducer() override;
    void forward() override;

    /**
     * @brief Access the operator's attributes.
     * @return A shared pointer to the operator's attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get or set the scheduling step for the operator.
     * @return A reference to the scheduling step.
     */
    inline std::uint32_t& scheduleStep() const { return mAttributes->template getAttr<MemorizeAttr::ScheduleStep>(); }

    /**
     * @brief Get or set the forward step counter for the operator.
     * @return A reference to the forward step counter.
     */
    inline std::uint32_t& forwardStep() const { return mAttributes->template getAttr<MemorizeAttr::ForwardStep>(); }

    /**
     * @brief Get or set the end step defining the memory duration.
     * @return A reference to the end step value.
     */
    inline std::uint32_t& endStep() const { return mAttributes->template getAttr<MemorizeAttr::EndStep>(); }

    /**
     * @brief Retrieve the names of the operator's input tensors.
     * @return A vector of strings representing input tensor names.
     */
    static const std::vector<std::string> getInputsName(){
        return {"data_input", "data_input_init"};
    }

    /**
     * @brief Retrieve the names of the operator's output tensors.
     * @return A vector of strings representing output tensor names.
     */
    static const std::vector<std::string> getOutputsName(){
        return {"data_output", "data_output_rec"};
    }
};

/**
 * @brief Create a Memorize operator node.
 * @param endStep The step duration for memory storage.
 * @param name The optional name for the node.
 * @return A shared pointer to the newly created Memorize operator node.
 */
std::shared_ptr<Node> Memorize(const std::uint32_t endStep, const std::string& name = "");
}  // namespace Aidge

namespace {
/**
 * @brief String representations of the Memorize operator's attributes.
 */
template <>
const char *const EnumStrings<Aidge::MemorizeAttr>::data[] = {
    "schedule_step",
    "forward_step",
    "end_step"
};
}

#endif /* AIDGE_CORE_OPERATOR_MEMORIZE_H_ */
