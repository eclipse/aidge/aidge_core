/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_RESIZE_H_
#define AIDGE_CORE_OPERATOR_RESIZE_H_

#include <memory>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/data/Interpolation.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Pad.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/* @brief attributes for the aidge operator */
enum class ResizeAttr {
    //   antialias,
    // axes,
    CoordinateTransformationMode,
    CubicCoeffA,
    // excludeOutside,
    //   extrapolation_value,
    //   keep_aspect_ratio_policy,
    InterpolationMode,
    PaddingMode
};

/**
 * @brief Resize operator, will up/downscale a given tensor given the input.
 * @verbatim
 * Output size can be computed in 2 ways :
 * 1. Image can be rescaled proportionally to the input size :
 *    output_dimension = floor(input_dimension * (roi_end - roi_start) * scale)
 * 2. Output dimensions are directly given via the size input(#4)
 *
 * Hence, either input Scale or Input Sizes can be defined, if both are
 * connected, the operator will throw an error.
 *
 * Resize takes (up to) 4 different inputs :
 * #1 Input to resize :
 *   N-D tensor.
 *
 * #2 ROI (optional) :
 *   1-D tensor of coordinates given as [start1, …, startN, end1, …, endN]
 *   where N is the rank of X or the length of axes, if provided. The RoIs’
 *   coordinates are normalized in the coordinate system of the input image.
 *   If not set default ROI is the entier image.
 * #3 scales (optional) - tensor(float):
 *   The scale array along each dimension.
 *    The number of elements of ‘scales’ should be the same as the rank of
 * input ‘X’ or the length of ‘axes’, if provided. Accepted values: (0,inf)
 *    - (0,1)   : downsampling
 *    - 1       : identity
 *    - (1,inf) : upsampling
 * #4. Sizes - tensor(int64):
 *   Target size of the output tensor.
 *   Its interpretation depends on the ‘keep_aspect_ratio_policy’ value.
 *   The number of elements of ‘sizes’ should be the same as either :
 *   - The rank of input ‘X’
 *   - The length of ‘axes’ attribute, if provided.
 * @endverbatim
 * @warning : Only one of ‘scales’ and ‘sizes’ can be specified.
 * @param coordinate_transformation_mode
 * @param cubic_coeff_a the a coefficient of cubic interpolation. Moost often
 * it is set to -0.75
 * @param InterpolationMode type of interpolation (currently only support cubic
 * interpolation)
 */
class Resize_Op
    : public OperatorTensor,
      public Registrable<
          Resize_Op,
          std::string,
          std::function<std::shared_ptr<OperatorImpl>(const Resize_Op &)>> {

  private:
    using Attributes_ =
        StaticAttributes<ResizeAttr,
                         Interpolation::CoordinateTransformation,
                         float,
                         Interpolation::Mode,
                         PadBorderType>;
    template <ResizeAttr e>
    using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

  public:
    static const std::string Type;
    /**
     * @brief creates a resize operator
     * This node can take 4 different inputs, more details in the class
     * doxygen.
     * 1. Input to resize :
     * 2. ROI NOT SUPPORTED (optional) :
     * 3. scales (optional) - tensor(float):
     * 4. sizes - tensor(int64):
     * @param[in] coordinate_transformation_mode
     * @param[in] cubic_coeff_a the a coefficient of cubic interpolation. Only
     * used if interpolation_mode = Interpolation::Mode::Cubic
     * @param[in] interpolationMode : Type of interpolation used for
     * up/downsampling
     * @warning Scales & ROI input cannot be set simultaneously. If bot are
     * set, forward will fail.
     * @return NodePtr
     */
    Resize_Op(
        Interpolation::CoordinateTransformation coordTransfoMode,
        Interpolation::Mode interpol_mode = Interpolation::Mode::RoundPreferFloor,
        float cubic_coef_a = -.75f,
        PadBorderType paddingMode = PadBorderType::Edge)
        : OperatorTensor(Type,
                         {InputCategory::Data,
                          InputCategory::OptionalData,
                          InputCategory::OptionalData,
                          InputCategory::OptionalData},
                         1),
          mAttributes(std::make_shared<Attributes_>(
              attr<ResizeAttr::CubicCoeffA>(cubic_coef_a),
              attr<ResizeAttr::CoordinateTransformationMode>(coordTransfoMode),
              attr<ResizeAttr::InterpolationMode>(interpol_mode),
              attr<ResizeAttr::PaddingMode>(paddingMode))) {}

    /**
     * @brief Copy-constructor. Copy the operator attributes and its output
     * tensor(s), but not its input tensors : The new operator has no input
     * associated).
     * @param op Operator to copy.
     */
    Resize_Op(const Resize_Op &op)
        : OperatorTensor(op), mAttributes(op.mAttributes) {
        if (!op.backend().empty()) {
            SET_IMPL_MACRO(Resize_Op, *this, op.backend());
        } else {
            mImpl = nullptr;
        }
    }

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::Resize_Op
     */
    std::shared_ptr<Operator> clone() const override final {
        return std::make_shared<Resize_Op>(*this);
    }

    bool dimsForwarded() const override final;
    bool forwardDims(bool allowDataDependency = false) override final;

    void setBackend(const std::string &name,
                    DeviceIdx_t device = 0) override final;
    std::set<std::string> getAvailableBackends() const override {
        return Registrar<Resize_Op>::getKeys();
    }

    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    inline Interpolation::CoordinateTransformation
    coordinateTransformationMode() const {
        return mAttributes
            ->template getAttr<ResizeAttr::CoordinateTransformationMode>();
    }
    inline float cubicCoefA() const {
        return mAttributes->template getAttr<ResizeAttr::CubicCoeffA>();
    }
    inline Interpolation::Mode interpolationMode() const {
        return mAttributes->template getAttr<ResizeAttr::InterpolationMode>();
    }
    inline PadBorderType paddingMode() const {
        return mAttributes->template getAttr<ResizeAttr::PaddingMode>();
    }

    static const std::vector<std::string> getInputsName() {
        //  roi, scales, sizes, even if considered as const parameters/input
        return {"data_input", "roi ", "scales", "sizes"};
    }
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief creates a node that contains a resize operator
 * This node can take 4 different inputs, more details in the class doxygen.
 * #0 Input to resize
 * #1 ROI NOT SUPPORTED (optional) - Tensor(double|float|float16)
 * #2 scales (optional) - tensor(float)
 * #3 sizes - tensor(int64)
 * @param[in] coordinate_transformation_mode
 * @param[in] interpolationMode type of interpolation used in case of
 * upsampling
 * @param[in] cubic_coeff_a the "a" coefficient of cubic interpolation. Only
 * used if interpolation_mode = Interpolation::Mode::Cubic
 * @warning Scales & ROI input cannot be set simultaneously. If bot are set,
 * forward will fail.
 * @warning Padding mode will tell how values out of bound are treated.
 * @return NodePtr
 */
std::shared_ptr<Node>
Resize(std::vector<float> scale = std::vector<float>(),
        std::vector<std::size_t> size = std::vector<std::size_t>(),
       Interpolation::CoordinateTransformation coordTransfoMode =
           Interpolation::CoordinateTransformation::HalfPixel,
       Interpolation::Mode interpolMode =
           Interpolation::Mode::RoundPreferFloor,
       float cubicCoefA = -.75f,
       const std::string &name = "");

} // namespace Aidge

namespace {
template <>
const char *const EnumStrings<Aidge::ResizeAttr>::data[] = {
    "coordinateTransformationMode",
    "cubicCoeffA",
    "InterpolationMode",
    "PaddingMode"
};
}
#endif /* AIDGE_CORE_OPERATOR_RESIZE_H_ */
