/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_OPERATORTENSOR_H_
#define AIDGE_CORE_OPERATOR_OPERATORTENSOR_H_

#include <memory>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Operator.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

class Tensor;

/**
 * @class OperatorTensor
 * @brief Base class for all operators that work with tensor inputs and outputs.
 *
 * The `OperatorTensor` class provides an abstraction for operations on tensors
 * with features such as input/output management, dimension handling, and
 * receptive field computation.
 *
 * @see Operator
 */
class OperatorTensor : public Operator {
    /* TODO: Add an attribute specifying the type of Data used by the Operator.
     * The same way ``Type`` attribute specifies the type of Operator. Hence this
     * attribute could be checked in the forwardDims function to assert Operators
     * being used work with Tensors and cast them to OperatorTensor instead of
     * Operator.
     */
    /* TODO: Maybe change type attribute of Data object by an enum instead of an
     * array of char. Faster comparisons.
     */
protected:
    /**
     * @brief Inputs of the operator represented as tensors.
     */
    std::vector<std::shared_ptr<Tensor>> mInputs;

    /**
     * @brief Outputs of the operator represented as tensors.
     */
    std::vector<std::shared_ptr<Tensor>> mOutputs;

public:
    OperatorTensor() = delete;

    /**
     * @brief Operator tensor constructor. This function is not meant to be called directly but by a derived class constructor
     * every operator class derive from this class.
     *
	 * @param[in] type     : type of operator (i.e. "Add", "AveragePool",...)
	 * @param[in] inputsCategory : describes the type of each input.
	 * @param[in] nbOut    : Number of tensors this operator will output
     */
    OperatorTensor(const std::string& type, const std::vector<InputCategory>& inputsCategory,
                   const IOIndex_t nbOut);

    /**
     * @brief Copy constructor.
     * @param[in] other Another `OperatorTensor` instance to copy.
     */
    OperatorTensor(const OperatorTensor& other);

    /**
     * @brief Destructor for the OperatorTensor class.
     */
    ~OperatorTensor();

public:
    ///////////////////////////////////////////////////
    /**
     * @brief Associates an input tensor to the operator.
     * @param[in] inputIdx Index of the input to associate.
     * @param[in] data     Shared pointer to the data to associate.
     */
    virtual void associateInput(const IOIndex_t inputIdx,
                                const std::shared_ptr<Data>& data) override;

    /**
     * @brief Resets the input tensor at a given index.
     * @param[in] inputIdx Index of the input to reset.
     */
    void resetInput(const IOIndex_t inputIdx) override final;
    ///////////////////////////////////////////////////

    ///////////////////////////////////////////////////
    // Tensor access

    /**
     * @brief Sets an input tensor for the operator.
     * @param[in] inputIdx Index of the input to set.
     * @param[in] data     Shared pointer to the data to set.
     */
    void setInput(const IOIndex_t inputIdx, const std::shared_ptr<Data>& data) override;

    /**
     * @brief Retrieves an input tensor.
     * @param[in] inputIdx Index of the input to retrieve.
     * @return Shared pointer to the input tensor.
     */
    const std::shared_ptr<Tensor>& getInput(const IOIndex_t inputIdx) const;

    virtual const std::vector<std::shared_ptr<Tensor>>& getInputs() const;

    /**
     * @brief Retrieves a raw input tensor.
     * @param[in] inputIdx Index of the input to retrieve.
     * @return Shared pointer to the raw input tensor.
     */
    std::shared_ptr<Data> getRawInput(const IOIndex_t inputIdx) const override final;

    /**
     * @brief Sets an output tensor for the operator.
     * @param[in] outputIdx Index of the output to set.
     * @param[in] data      Shared pointer to the data to set.
     */
    void setOutput(const IOIndex_t outputIdx, const std::shared_ptr<Data>& data) const override;

    /**
     * @brief Retrieves an output tensor.
     * @param[in] outputIdx Index of the output to retrieve.
     * @return Shared pointer to the output tensor.
     */
    virtual const std::shared_ptr<Tensor>& getOutput(const IOIndex_t outputIdx) const;

    virtual const std::vector<std::shared_ptr<Tensor>>& getOutputs() const;

    /**
     * @brief Retrieves a raw output tensor.
     * @param[in] outputIdx Index of the output to retrieve.
     * @return Shared pointer to the raw output tensor.
     */
    std::shared_ptr<Aidge::Data> getRawOutput(const Aidge::IOIndex_t outputIdx) const override final;
    ///////////////////////////////////////////////////

    ///////////////////////////////////////////////////
    // Tensor dimensions

    /**
     * @brief Computes the receptive field for a given output feature area.
     *
     * @param[in] firstIdx    First index of the output feature.
     * @param[in] outputDims  Dimensions of the output feature.
     * @param[in] outputIdx   Index of the output (default is 0).
     *
     * @return Vector of pairs containing, for each data input tensor, the first index and dimensions of the feature area.
     */
    virtual std::vector<std::pair<std::vector<Aidge::DimSize_t>, std::vector<DimSize_t>>> computeReceptiveField(const std::vector<DimSize_t>& firstEltDims, const std::vector<DimSize_t>& outputDims, const IOIndex_t outputIdx = 0) const;

    /**
     * @brief Computes the dimensions of the operator's output tensor based on input sizes.
     *
     * - If the output dimensions depend on undefined inputs, this function returns false and enters TOKEN mode.
     * - TOKEN mode ensures that all inputs and outputs of the graph the node belongs to are connected.
     *
     * @param[in] allowDataDependency Flag to indicate if output dimensions depend on optional parameter tensors.
     * @return True if dimensions are successfully computed, false otherwise.
     */
    virtual bool forwardDims(bool allowDataDependency = false);

    /**
     * @brief Checks if dimensions have been successfully forwarded.
     * @return True if dimensions are forwarded, false otherwise.
     */
    virtual bool dimsForwarded() const;
    ///////////////////////////////////////////////////

    /**
     * @brief Sets the data type of the operator's tensors.
     * @param dataType Data type to set.
     */
    virtual void setDataType(const DataType& dataType) const override;

    /**
     * @brief Sets the data format of the operator's tensors.
     * @param dataFormat Data format to set.
     */
    virtual void setDataFormat(const DataFormat& dataFormat) const override;

    /**
     * @brief Executes the forward operation for the operator.
     */
    virtual void forward() override;

protected:
    /**
     * @brief Checks if all inputs are associated.
     * @param checkNonEmpty Flag to indicate if non-empty check is required.
     * @return True if inputs are associated, false otherwise.
     */
    bool inputsAssociated(bool checkNonEmpty = true) const;
};

}  // namespace Aidge

#endif  // AIDGE_CORE_OPERATOR_OPERATORTENSOR_H_
