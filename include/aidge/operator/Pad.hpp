/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_PAD_H_
#define AIDGE_CORE_OPERATOR_PAD_H_

#include <array>
#include <memory>
#include <string>
#include <vector>

#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @enum PadAttr
 * @brief Attributes for the Pad operator.
 */
enum class PadAttr {
    BeginEndBorders, ///< Specifies the padding sizes for the beginning and end of each dimension.
    BorderType,      ///< Type of border handling during padding.
    BorderValue      ///< Value to be used for constant padding.
};

/**
 * @enum PadBorderType
 * @brief Types of border handling available for padding.
 */
enum class PadBorderType {
    Constant, ///< Out-of-bound values set to a constant value.
    Edge,     ///< Values are replicated from the nearest edge.
    Reflect,  ///< Values are mirrored around the edges.
    Wrap,     ///< Values wrap around the tensor dimensions.
    Zero      ///< All out-of-bound values are set to 0.
};

/**
 * @class Pad_Op
 * @brief Implementation of the Pad operator.
 *
 * The Pad operator adds padding to the input tensor along specified dimensions.
 * Padding can be customized by specifying the border type and border value.
 *
 * @tparam DIM The number of dimensions for the padding operation.
 */
/**
 * @brief Description of tensor padding along spatial dimensions with configurable border handling.
 *
 * The operator adds Padding for each spatial dimension (leaving the batch and channel dimensions unchanged).
 * The operator supports various border handling techniques (e.g., constant padding, reflection, wrapping).
 *
 * ### Output Tensor Shape:
 * If the input tensor has a shape `[B, C, d1, d2, ..., dN]`, where `B` is the batch size, 
 * `C` is the number of channels, and `[d1, d2, ..., dN]` are the spatial dimensions, 
 * and the padding is defined by `beginEndTuples = {b1, e1, b2, e2, ..., bN, eN}`, 
 * the output tensor shape will be:
 * 
 * `[B, C, d1 + b1 + e1, d2 + b2 + e2, ..., dN + bN + eN]`.
 * 
 * The padding values `b_i` and `e_i` specify the number of elements to add before and after 
 * the corresponding spatial dimension `d_i`. Batch size and channel count remain unchanged.
 *
 * @example Constant Padding:
 *    - beginEndTuples = [1, 1, 2, 2]
 *    - Input tensor shape: `[B, C, 3, 4]` (batch and channel are fixed)
 *    - Output tensor shape: `[B, C, 3 + 1 + 1, 4 + 2 + 2] = [B, C, 5, 8]`
 *    - Padding values: Zero (0.0).
 *
 * @example Edge Padding:
 *    - padding = [2, 2, 3, 3]
 *    - Input tensor shape: `[B, C, 5, 5]`
 *    - Output tensor shape: `[B, C, 5 + 2 + 2, 5 + 3 + 3] = [B, C, 9, 11]`
 *    - Padding values replicate the edge values of the tensor.
 *
 * @example Reflect Padding:
 *    - padding = [1, 1, 2, 2, 0, 0]
 *    - Input tensor shape: `[B, C, 4, 5, 6]`
 *    - Output tensor shape: `[B, C, 4 + 1 + 1, 5 + 2 + 2, 6 + 0 + 0] = [B, C, 6, 9, 6]`
 *    - Padding values mirror the existing tensor values.
 *
 * This operator is commonly used for image processing, extending spatial dimensions while maintaining 
 * batch and channel consistency, or aligning tensor dimensions in machine learning workflows.
 */
template <DimIdx_t DIM>
class Pad_Op : public OperatorTensor,
               public Registrable<Pad_Op<DIM>, std::string, std::function<std::shared_ptr<OperatorImpl>(const Pad_Op<DIM>&)>> {
public:
    /**
     * @brief Static string indicating the type of the operator.
     */
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<PadAttr,
                                         std::array<DimSize_t, 2 * DIM>, // Padding for start and end of each dimension.
                                         PadBorderType,                  // Border handling type.
                                         double                          // Border value for constant padding.
                                         >;
    template <PadAttr e>
    using attr = typename Attributes_::template attr<e>;

    const std::shared_ptr<Attributes_> mAttributes; ///< Holds operator attributes.

public:
    /**
     * @brief Deleted default constructor.
     */
    Pad_Op() = delete;

    /**
     * @brief Constructor for the Pad operator.
     * @param beginEndTuples Array specifying padding for the beginning and end of each dimension.
     * @param borderType Type of border handling (default is constant).
     * @param borderValue Value to use for constant padding (default is 0.0).
     */
    constexpr Pad_Op(const std::array<DimSize_t, 2 * DIM>& beginEndTuples,
                     PadBorderType borderType = PadBorderType::Constant,
                     double borderValue = 0.0)
        : OperatorTensor(Type, {InputCategory::Data}, 1),
          mAttributes(std::make_shared<Attributes_>(
              attr<PadAttr::BeginEndBorders>(beginEndTuples),
              attr<PadAttr::BorderType>(borderType),
              attr<PadAttr::BorderValue>(borderValue))) {}

    /**
     * @brief Copy-constructor.
     * @param op Operator to copy.
     * @details Copies operator attributes and its output tensors, but not its input tensors. The new operator has no associated input.
     */
    Pad_Op(const Pad_Op& op)
        : OperatorTensor(op),
          mAttributes(op.mAttributes) {}

    /**
     * @brief Clone the operator using its copy-constructor.
     * @return A shared pointer to the cloned operator.
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Compute output dimensions during the forward pass.
     * @param[in] allowDataDependency Flag indicating whether to allow data-dependent dimensions.
     * @return True if dimensions are computed successfully.
     */
    bool forwardDims(bool /*allowDataDependency*/ = false) override final;

    /**
     * @brief Set the backend for the Pad operator.
     * @param name Name of the backend.
     * @param device Device index (optional).
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override;

    /**
     * @brief Get the available backends for the Pad operator.
     * @return A set of available backend names.
     */
    std::set<std::string> getAvailableBackends() const override;

    /**
     * @brief Get the attributes of the operator.
     * @return A shared pointer to the attributes.
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }

    /**
     * @brief Get or modify the padding values for each dimension.
     * @return Reference to the padding values.
     */
    inline std::array<DimSize_t, 2 * DIM>& beginEndBorders() const noexcept {
        return mAttributes->template getAttr<PadAttr::BeginEndBorders>();
    }

    /**
     * @brief Get or modify the border type for padding.
     * @return Reference to the border type.
     */
    inline PadBorderType& borderType() const noexcept {
        return mAttributes->template getAttr<PadAttr::BorderType>();
    }

    /**
     * @brief Get or modify the border value for constant padding.
     * @return Reference to the border value.
     */
    inline double& borderValue() const noexcept {
        return mAttributes->template getAttr<PadAttr::BorderValue>();
    }

    /**
     * @brief Get the input tensor names.
     * @return Vector of input tensor names.
     */
    static const std::vector<std::string> getInputsName() {
        return {"data_input"};
    }

    /**
     * @brief Get the output tensor names.
     * @return Vector of output tensor names.
     */
    static const std::vector<std::string> getOutputsName() {
        return {"data_output"};
    }
};

/**
 * @brief Create a Pad operation node.
 *
 * @tparam DIM Number of dimensions for the operation.
 * @param[in] beginEndTuples Array specifying padding for the beginning and end of each dimension.
 * @param[in] name Name of the operator (optional).
 * @param[in] borderType Type of border handling (optional, default is constant).
 * @param[in] borderValue Value for constant padding (optional, default is 0.0).
 * @return A shared pointer to the Node containing the Pad operator.
 */
template <std::array<DimSize_t, 1>::size_type DIM>
std::shared_ptr<Node> Pad(const std::array<DimSize_t, 2 * DIM>& beginEndTuples,
                          const std::string& name = "",
                          PadBorderType borderType = PadBorderType::Constant,
                          double borderValue = 0.0);

// Helper overload with C-style array for automatic DIM deduction
template <DimSize_t DIM>
inline std::shared_ptr<Node> Pad(
    DimSize_t const (&beginEndTuples)[2 * DIM],
    const std::string& name = "",
    PadBorderType borderType = PadBorderType::Constant,
    double borderValue = 0.0) {
    return Pad<DIM>(to_array(beginEndTuples), name, borderType, borderValue);
}

}  // namespace Aidge

// Explicit instantiations for common dimensions
extern template class Aidge::Pad_Op<1>;
extern template class Aidge::Pad_Op<2>;

namespace {

/**
 * @brief EnumStrings specialization for PadAttr.
 */
template <>
const char* const EnumStrings<Aidge::PadAttr>::data[] = {
    "begin_end_borders",
    "border_type",
    "border_value"
};

/**
 * @brief EnumStrings specialization for PadBorderType.
 */
template <>
const char* const EnumStrings<Aidge::PadBorderType>::data[] = {
    "Constant",
    "Edge",
    "Reflect",
    "Wrap",
    "Zero"
};

}  // namespace

#endif /* AIDGE_CORE_OPERATOR_PAD_H_ */
