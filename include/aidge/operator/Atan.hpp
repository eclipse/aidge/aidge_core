/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_ATAN_H_
#define AIDGE_CORE_OPERATOR_ATAN_H_

#include <cassert>
#include <memory>
#include <vector>

#include "aidge/utils/Registrar.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @brief Description of an element-wise Arc Tangent (Atan) operation
 * on an input Tensor.
 *
 * For each element x in the input, the function is defined as:
 * `f(x) = atan(x)`, where atan(x) is the arc tangent of x, 
 * returning the angle in radians whose tangent is x.
 *
 * The input and output Tensors have the same dimensions.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Atan_Op : public OperatorTensor,
    public Registrable<Atan_Op,  // <Op, backend, implementation creation function>
        std::string,
        std::function<std::shared_ptr<OperatorImpl>(const Atan_Op&)>>
{
public:
    static const std::string Type;

    Atan_Op();

    /**
     * @brief Copy-constructor.
     * @param op Atan_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    Atan_Op(const Atan_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::Atan_Op
     */
    std::shared_ptr<Operator> clone() const override;

    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;
    std::set<std::string> getAvailableBackends() const override;

    static const std::vector<std::string> getInputsName(){
        return {"data_input"};
    }
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }
};

std::shared_ptr<Node> Atan(const std::string& name = "");
}

#endif /* AIDGE_CORE_OPERATOR_ATAN_H_ */
