/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_ABS_H_
#define AIDGE_CORE_OPERATOR_ABS_H_

#include <memory>
#include <string>
#include <vector>

#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
* @brief Description of an element-wise Absolute Value (Abs) operation
 * on an input Tensor.
 *
 * For each element x in the input, the function is defined as:
 * `f(x) = |x|`, where |x| denotes the absolute value of x.
 *
 * The input and output Tensors have the same dimensions.
 *
 * @see OperatorTensor
 * @see Registrable
 */
class Abs_Op : public OperatorTensor,
    public Registrable<Abs_Op,  // <Op, backend, implementation creation function>
        std::string,
        std::function<std::shared_ptr<OperatorImpl>(const Abs_Op&)>> {
public:
    static const std::string Type;

    Abs_Op() : OperatorTensor(Type, {InputCategory::Data}, 1) {}

    /**
     * @brief Copy-constructor.
     * @param op Abs_Op to copy.
     * @details Copies the operator attributes and its output tensor(s), but not
     * its input tensors. The new operator has no associated input.
     */
    Abs_Op(const Abs_Op& op)
        : OperatorTensor(op)
    {
        if (op.mImpl) {
            SET_IMPL_MACRO(Abs_Op, *this, op.backend());
        } else {
            mImpl = nullptr;
        }
    }

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::Abs_Op
     */
    std::shared_ptr<Operator> clone() const override {
        return std::make_shared<Abs_Op>(*this);
    }

    void setBackend(const std::string& name, DeviceIdx_t device = 0) override;
    std::set<std::string> getAvailableBackends() const override;

    static const std::vector<std::string> getInputsName(){
        return {"data_input"};
    }
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }
};

inline std::shared_ptr<Node> Abs(const std::string& name = "") {
    return std::make_shared<Node>(std::make_shared<Abs_Op>(), name);
}
}

#endif /* AIDGE_CORE_OPERATOR_ABS_H_ */
