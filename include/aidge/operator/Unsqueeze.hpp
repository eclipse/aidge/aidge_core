/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_UNSQUEEZE_H_
#define AIDGE_CORE_OPERATOR_UNSQUEEZE_H_

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include "aidge/graph/Node.hpp"
#include "aidge/operator/Operator.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
/**
 * @brief implementation of the operator unsqueeze.
 * @note Since this operator implementation is agnostic to the backend it is
 * located here instead of in aidge_backend_cpu/cuda.
 */
class Unsqueeze_OpImpl : public OperatorImpl {
public:
  Unsqueeze_OpImpl(const Operator &op, const std::string &backend = "")
      : OperatorImpl(op, backend) {}
  void forward() override;
};

enum class UnsqueezeAttr {
  /**
   * @brief vector of axes to unsqueeze.
   * values must be comprised within
   * [ -a ; a-1 ]
   * with a = input_tensor.nbDim() + dims_to_unsqueeze.size()
   */
  Axes
};

/**
 * @brief This operator has as purpose to add a dummy dimension around given
 * axis. Unsqueezing the 2nd dim of a tensor of dim (1,2,3,4) will result in a
 * tensor of dim (1,2,1,3,4)
 * You can also unsqueeze dimensions whose index is higher than the nb of input
 * dimensions as long as :
 * dims_to_unsqueeze[i] < tensor.nbDim() +
 * dims_to_unsqueeze.size()
 */
class Unsqueeze_Op
    : public OperatorTensor,
      public Registrable<Unsqueeze_Op, std::string,
                         std::function<std::shared_ptr<OperatorImpl>(const Unsqueeze_Op &)>> {

public:
  static const std::string
      Type; // name of the type of the operation (Here "Unsqueeze")

private:
  using Attributes_ = StaticAttributes<UnsqueezeAttr, std::vector<int8_t>>;
  template <UnsqueezeAttr e>
  using attr = typename Attributes_::template attr<e>;
  const std::shared_ptr<Attributes_> mAttributes;

public:
  Unsqueeze_Op() =
      delete; // no default constructor since this class has attributes

  /**
   * @brief constructor for Unsqueeze op
   * @param[in] axis around which perform the operation
   */
  Unsqueeze_Op(const std::vector<int8_t> &axes)
      : OperatorTensor(Type, {InputCategory::Data, InputCategory::OptionalData},
                       1),
        mAttributes(
            std::make_shared<Attributes_>(attr<UnsqueezeAttr::Axes>(axes))) {
    mImpl = std::make_shared<Unsqueeze_OpImpl>(*this);
  }

  /**
   * @brief Copy-constructor. Copy the operator attributes and its output
   * tensor(s), but not its input tensors (the new operator has no input
   * associated).
   * @param op Operator to copy.
   */
  Unsqueeze_Op(const Unsqueeze_Op &op)
      : OperatorTensor(op), mAttributes(op.mAttributes) {
    if (!op.backend().empty()) {
      SET_IMPL_MACRO(Unsqueeze_Op, *this, op.backend());
    } else {
      mImpl = std::make_shared<Unsqueeze_OpImpl>(*this);
    }
  }

  /**
   * @brief Clone the operator using its copy-constructor.
   * @see Operator::MatMul_Op
   */
  std::shared_ptr<Operator> clone() const override final {
    return std::make_shared<Unsqueeze_Op>(*this);
  }

  /**
   * @brief Compute dimensions for the output Tensor
   */
  bool forwardDims(bool allowDataDependency = false) override final;
  bool dimsForwarded() const override final;

  void setBackend(const std::string &name,
                  DeviceIdx_t device = 0) override final;
  std::set<std::string> getAvailableBackends() const override;

  inline std::shared_ptr<Attributes> attributes() const override {
    return mAttributes;
  }
  /**
   * @brief vector of axes to unsqueeze.
   * values must be comprised within
   * [ -a ; a-1 ]
   * with : a = input_tensor.nbDim() + dims_to_unsqueeze.size()
   */
  inline std::vector<int8_t> &axes() const noexcept {
    return mAttributes->template getAttr<UnsqueezeAttr::Axes>();
  }

  static const std::vector<std::string> getInputsName() {
    return {"data_input", "axes_to_unsqueeze"};
  }
  static const std::vector<std::string> getOutputsName() {
    return {"unsqueezed"};
  }
};

// helper with C-style array instead of std::array for kernel_dims to allow
// automatic template DIM deduction
inline std::shared_ptr<Node> Unsqueeze(const std::vector<int8_t> &axes = {},
                                       const std::string &name = "") {
  return std::make_shared<Node>(std::make_shared<Unsqueeze_Op>(axes), name);
}
} // namespace Aidge

namespace {
template <>
const char *const EnumStrings<Aidge::UnsqueezeAttr>::data[] = {"Axes"};
}

#endif // AIDGE_CORE_OPERATOR_UNSQUEEZE_H_
