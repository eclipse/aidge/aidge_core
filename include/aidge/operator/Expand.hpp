/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPERATOR_EXPAND_H_
#define AIDGE_CORE_OPERATOR_EXPAND_H_

#include <cstdlib>
#include <memory>
#include <string>
#include <vector>

#include "aidge/graph/Node.hpp"
#include "aidge/operator/Operator.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * @brief Operator that broadcasts an input tensor to a larger provided shape.
 * @details This operator takes an input tensor and expands (broadcasts) it to
 * a target shape while following ONNX broadcasting semantics. Broadcasting
 * allows a tensor to be repeated along dimensions to match the desired output
 * dimensions.
 *
 * input#0 : Tensor to be broadcasted
 * input#1 : Expand shape
 *
 *  @example
 *    input#0 = [[[[2, 1, 3]]]] => dims = [1, 1, 3, 1]
 *    input#1 = [2, 1, 1]       => converted to [1,2,1,1]
 *    output = [[[[2, 1, 3], [2, 1, 3]]]] => dims = [1, 2, 3, 1]
 *
 * @see https://onnx.ai/onnx/repo-docs/Broadcasting.html for detailed ONNX
 * broadcasting rules
 */
class Expand_Op
    : public OperatorTensor,
      public Registrable<
          Expand_Op,
          std::string,
          std::function<std::shared_ptr<OperatorImpl>(const Expand_Op &)>> {
  public:
    static const std::string Type;

    /**
     * @brief Operator that broadcasts an input tensor to a larger provided
     * shape.
     * @details This operator takes an input tensor and expands (broadcasts) it
     * to a target shape while following ONNX broadcasting semantics.
     * Broadcasting allows a tensor to be repeated along dimensions to match
     * the desired output dimensions.
     *
     * input#0 : Tensor to be broadcasted
     * input#1 : Expand shape
     *
     *  @example
     *    input#0 = [[[[2, 1, 3]]]] => dims = [1, 1, 3, 1]
     *    input#1 = [2, 1, 1]       => converted to [1,2,1,1]
     *    output = [[[[2, 1, 3], [2, 1, 3]]]] => dims = [1, 2, 3, 1]
     *
     * @see https://onnx.ai/onnx/repo-docs/Broadcasting.html for detailed ONNX
     * broadcasting rules
     */
    Expand_Op()
        : OperatorTensor(Type,
                         {InputCategory::Data, InputCategory::Param},
                         1) {}

    Expand_Op(const Expand_Op &op);

    std::shared_ptr<Operator> clone() const override;

    bool forwardDims(bool allowDataDependency = false) override final;

    void setBackend(const std::string &name,
                    DeviceIdx_t device = 0) override final;
    std::set<std::string> getAvailableBackends() const override;

    static const std::vector<std::string> getInputsName() {
        return {"data", "shape"};
    }
    static const std::vector<std::string> getOutputsName() {
        return {"output"};
    }
};

/**
 * @brief Operator that broadcasts an input tensor to a larger provided shape.
 * @details This operator takes an input tensor and expands (broadcasts) it to
 * a target shape while following ONNX broadcasting semantics. Broadcasting
 * allows a tensor to be repeated along dimensions to match the desired output
 * dimensions.
 *
 * input#0 : Tensor to be broadcasted
 * input#1 : Expand shape
 *
 *  @example
 *    input#0 = [[[[2, 1, 3]]]] => dims = [1, 1, 3, 1]
 *    input#1 = [2, 1, 1]       => converted to [1,2,1,1]
 *    output = [[[[2, 1, 3], [2, 1, 3]]]] => dims = [1, 2, 3, 1]
 *
 * @see https://onnx.ai/onnx/repo-docs/Broadcasting.html for detailed ONNX
 * broadcasting rules
 */
std::shared_ptr<Node> Expand(const std::string &name = "");

} // namespace Aidge

#endif // AIDGE_CORE_OPERATOR_EXPAND_H_
