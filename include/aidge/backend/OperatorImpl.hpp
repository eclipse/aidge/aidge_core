/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_OPERATORIMPL_H_
#define AIDGE_BACKEND_OPERATORIMPL_H_

#include <functional>  // std::function
#include <memory>
#include <string>
#include <vector>
#include <utility>  // std::pair

#include "aidge/utils/Types.h"
#include "aidge/utils/DynamicAttributes.hpp"
#include "aidge/data/DataFormat.hpp"
#include "aidge/data/DataType.hpp"
#include "aidge/data/Elts.hpp"
#include "aidge/scheduler/ProdConso.hpp"

namespace Aidge {
class Node;
class Operator;

/**
 * @brief ImplSpec stores the requirements or the specifications of an implementation.
 *
 */
struct ImplSpec {
    struct IOSpec {
        IOSpec(DataType type_, DataFormat format_ = DataFormat::Any, const std::vector<std::pair<int, int>>& dims_ = {}):
            type(type_),
            format(format_),
            dims(dims_)
        {}

        DataType type;
        DataFormat format;
        std::vector<std::pair<int, int>> dims;
    };

    ImplSpec(const DynamicAttributes& attrs_ = DynamicAttributes());
    ImplSpec(const IOSpec& io, const DynamicAttributes& attrs_ = DynamicAttributes());
    ImplSpec(const IOSpec& i, const IOSpec& o, const DynamicAttributes& attrs_ = DynamicAttributes());
    ImplSpec(const std::vector<IOSpec>& i, const std::vector<IOSpec>& o, const DynamicAttributes& attrs_ = DynamicAttributes());
    ImplSpec(const Aidge::ImplSpec&);
    ~ImplSpec() noexcept;

    std::vector<IOSpec> inputs;
    std::vector<IOSpec> outputs;
    DynamicAttributes attrs;
};

inline bool operator==(const ImplSpec::IOSpec& lhs, const ImplSpec::IOSpec& rhs) {
    return (lhs.type == rhs.type)
        && (lhs.format == rhs.format)
        && (lhs.dims == rhs.dims);
}

inline bool operator<(const ImplSpec::IOSpec& lhs, const ImplSpec::IOSpec& rhs) {
    return (lhs.type < rhs.type)
        || (lhs.type == rhs.type && lhs.format < rhs.format)
        || (lhs.type == rhs.type && lhs.format == rhs.format && lhs.dims < rhs.dims);
}

inline bool operator<(const ImplSpec& lhs, const ImplSpec& rhs) {
    return (lhs.inputs < rhs.inputs)
        || (lhs.inputs == rhs.inputs && lhs.outputs < rhs.outputs)
        || (lhs.inputs == rhs.inputs && lhs.outputs == rhs.outputs && lhs.attrs < rhs.attrs);
}


inline bool operator==(const ImplSpec& lhs, const ImplSpec& rhs) {
    return !(lhs < rhs) && !(rhs < lhs);
}

/**
 * @brief Impl stores the details of a specific implementation.
 * It is associated to a ImplSpec in a registry.
 *
 */
template <class FwdFunc, class BwdFunc>
struct Impl {
    Impl(std::function<std::unique_ptr<ProdConso>(const Operator&)> prodConso_,
      std::function<FwdFunc> forward_,
      std::function<BwdFunc> backward_ = nullptr):
        prodConso(prodConso_), forward(forward_), backward(backward_) {}

    std::function<std::unique_ptr<ProdConso>(const Operator&)> prodConso;
    std::function<FwdFunc> forward;
    std::function<BwdFunc> backward;
};

class OperatorImpl {
public:
    OperatorImpl(const Operator& op, const std::string& backend = "");
    virtual void forward();
    virtual void backward();
    virtual std::shared_ptr<ProdConso> prodConso();

    const std::string& backend() const noexcept {
        return mBackend;
    }

    const Operator& getOperator() const noexcept {
        return mOp;
    }

    /**
     * @brief Get the operator required implementation specification, according
     * to the current operator configuration.
     *
     */
    ImplSpec getRequiredSpec() const;

    /**
     * @brief Get the best implementation that matches \p requiredSpecs.
     * If no implementation matches \p requiredSpecs, \p requiredSpecs is
     * returned.
     *
     */
    ImplSpec getBestMatch(const ImplSpec& requiredSpecs) const;

    /**
     * @brief Get an adapted meta operator corresponding to the required
     * specifications \p requiredSpecs from the implementation specifications
     * \p spec.
     *
     * @param spec Implementation specification
     * @param requiredSpecs Required specifications
     * @return std::shared_ptr<Node> Adapted meta op or nullptr
     */
    std::shared_ptr<Node> getAdaptation(const ImplSpec& spec, const ImplSpec& requiredSpecs) const;

    /**
     * @brief Get the best adapted meta operator corresponding to the required
     * specifications \p requiredSpecs.
     * The best adaptation is the one with the lowest overhead cost.
     * Currently, it is the one requiring the least number of additional
     * operators to match the available implementations.
     *
     * @param requiredSpecs Required specifications
     * @return std::shared_ptr<Node> Adapted meta op or nullptr
     */
    std::shared_ptr<Node> getBestAdaptation(const ImplSpec& requiredSpecs) const;

    virtual ~OperatorImpl() = default;

protected:
    virtual std::shared_ptr<ProdConso> getProdConso() const;
    virtual std::vector<ImplSpec> getAvailableImplSpecs() const;
    bool checkIOSpec(const ImplSpec::IOSpec& required, const ImplSpec::IOSpec& spec) const;

    const Operator &mOp;
    const std::string mBackend;
    std::shared_ptr<ProdConso> mProdConso;
};
} // namespace Aidge

template<>
struct fmt::formatter<Aidge::ImplSpec::IOSpec> {
    template<typename ParseContext>
    inline constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template<typename FormatContext>
    inline auto format(Aidge::ImplSpec::IOSpec const& ioSpec, FormatContext& ctx) const {
        return fmt::format_to(ctx.out(), "{}, {}, {}", ioSpec.type, ioSpec.format, ioSpec.dims);
    }
};

template<>
struct fmt::formatter<Aidge::ImplSpec> {
    template<typename ParseContext>
    inline constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template<typename FormatContext>
    inline auto format(Aidge::ImplSpec const& implSpec, FormatContext& ctx) const {
        return fmt::format_to(ctx.out(), "{}, {}", implSpec.inputs, implSpec.outputs);
    }
};

#endif /* AIDGE_BACKEND_OPERATORIMPL_H_ */
