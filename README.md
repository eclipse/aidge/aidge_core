![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_core/badges/main/pipeline.svg?ignore_skipped=true) ![C++ coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_core/badges/main/coverage.svg?job=coverage:ubuntu_cpp&key_text=C%2B%2B+coverage&key_width=90) ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_core/badges/main/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)

# Aidge Core library

You can find here the C++ code of the Core library of Aidge.

[TOC]

## Pip installation

To install aidge_core using pip, run the following command in your python environment :
``` bash
pip install . -v
```
> **TIPS :** Use environment variables to change compilation options:
> - `AIDGE_INSTALL` : to set the installation folder. Defaults to `<python_prefix>/lib/libAidge`
> - `AIDGE_PYTHON_BUILD_TYPE` : to set the compilation mode to **Debug** or **Release** or "" (for default flags). Defaults to **Release**.
> - `AIDGE_BUILD_GEN` : to set the build backend (for development mode) or "" for the cmake default. Default to "".
> - `AIDGE_BUILD_TEST` : to build the C++ unit tests. Set to "ON" or "OFF". Default to "OFF".


## Pip installation for development

To setup aidge_core using pip in development (or editable mode), use the `--no-build-isolation -e` options to pip.

For instance run the following command in your python environment for a typical setup :
``` bash
export AIDGE_BUILD_TEST=ON              # enable C++ unit tests
export AIDGE_PYTHON_BUILD_TYPE=         # default flags (no debug info but fastest build time)
export AIDGE_PYTHON_BUILD_TYPE=Debug    # or if one really need to debug the C++ code
pip install -U pip setuptools setuptools_scm[toml] cmake   # Pre-install build requirements (refer to the pyproject.toml [build-system] section)
pip install -v --no-build-isolation -e .
```

In this configuration python files can be modified directly without re-installation.

The C++ build dir will be created in `build/` and recompilation and install of python bindings can be done directly with:
```bash
make -C build install -j $(nproc)
# or with cmake
cmake --build build -j $(nproc) && cmake --install build
```

One can also use an alternate cmake build backend such as ninja which can be installed easily though pip, for instance :
``` bash
pip install -U ninja
export AIDGE_BUILD_GEN=Ninja
pip install -v --no-build-isolation -e .
```

In this case ninja is used instead of make as build backend, and recompilation when needed is done with:
```bash
ninja -C build install  # note that by default ninja use available parallelism, no need for -j option
# or with cmake
cmake --build build && cmake --install build
```

Note that python development (or editable mode) is not always robust to changes in the python package setup,
or when changing the build backend with `AIDGE_BUILD_GEN`.
In order to re-install when the build breaks, re-execute the commands:
```bash
rm -rf *-egg-info build/
pip install -v --no-build-isolation -e .
```


## Standard C++ Compilation

Create two directories ``build`` and ``ìnstall``.

Then **inside** ``build`` :

```bash

cmake -DCMAKE_INSTALL_PREFIX:PATH=$(path_to_install_folder) $(CMAKE PARAMETERS) $(projet_root)

make all install

```


**Compilation options**


|   Option   | Value type | Description |
|:----------:|:----------:|:-----------:|
| *-DCMAKE_INSTALL_PREFIX:PATH* | ``str``  | Path to the install folder |
| *-DCMAKE_BUILD_TYPE*          | ``str``  | If ``Debug``, compile in debug mode, ``Release`` compile with highest optimizations or "" (empty) , default= ``Release`` |
| *-DWERROR*                    | ``bool`` | If ``ON`` show warning as error during compilation phase, default=``OFF`` |
| *-DTEST*                      | ``bool`` | If ``ON`` build C++ unit tests, default=``ON`` |
| *-DPYBIND*                    | ``bool`` | If ``ON`` activate python binding, default=``OFF`` |
| *-DPYBIND_INSTALL_PREFIX:PATH*| ``str`` | Path to the python module install folder when ``-DPYBIND=ON``, defaults to ``$CMAKE_INSTALL_PREFIX/python_packages/<module>`` |

If one compiles with ``-DPYBIND=ON``, ``-DPYBIND_INSTALL_PREFIX:PATH`` can be used to install the python module directly in the
python sources tree (for instance ``$PWD/aidge_core``). ``setup.py`` takes care of this and installs the module at the right place.

## Run tests
### CPP

Inside of the build file run:

```bash
ctest --output-on-failure
```

### Python

